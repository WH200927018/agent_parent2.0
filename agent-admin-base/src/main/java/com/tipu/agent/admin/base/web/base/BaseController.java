package com.tipu.agent.admin.base.web.base;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.core.ActionException;
import com.jfinal.kit.StrKit;

import io.jboot.web.controller.JbootController;

/**
 * 控制器基类
 * @author Rlax
 *
 */
public class BaseController extends JbootController {
	
	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	private Date toDateTime(String value, Date defaultValue) {
		try {
			if (StrKit.isBlank(value))
				return defaultValue;
			return new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value.trim());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Returns the value of a request parameter and convert to Date.
	 * @param name a String specifying the name of the parameter
	 * @return a Date representing the single value of the parameter
	 */
	public Date getParaToDateTime(String name) {
		return toDateTime(getRequest().getParameter(name), null);
	}
	
}
