package com.tipu.agent.admin.base.common;

import java.util.Random;

/**
 * 常量
 * @author Administrator
 *
 */
public class CommonConstant {
	
	public static final String LOG_FILE = "file"; // 正常默认日志
	public static final String LOG_PAY = "pay"; // 扣费日志
	public static final String LOG_SMS = "sms"; // 短信日志
	public static final String LOG_NOTIFY = "notify"; // 上行日志
	public static final String LOG_DELIVERY = "delivery"; // 状态报告回执日志
	public static final String LOG_BATCH = "batch"; // 批开日志
	public static final String LOG_MARKET = "market"; // 营销日志
	
	public static final String REDIS_SEPERATOR = "_";
	
	public static final int BT_SUBSCRIBE = 1;
	public static final int BT_UPDATE = 2;
	public static final int BT_STOP = 3;
	public static final int BT_HELP = 4;
	public static final int BT_ERROR = 5;
	
	public static final String DELIVERY_RECEIIPT = "delivery_receipt_"; //状态报告回执rediskey前缀
	
//	public static final String STAT_CONTENT_SUCCESS = "stat_content_success"; //统计内容发送成功数
//	public static final String STAT_CONTENT_FAIL = "stat_content_fail"; //统计内容发送失败数

	public static final String CUR_SEND_SMS_TOTAL = "cur_send_sms_total"; //统计当天发送的短信总条数
	
	public static final String CUR_SEND_SMS_ACC_TOTAL = "cur_send_sms_acc_total"; // 统计当天接入码发送的短信总条数
	
	public static final String CUR_SEND_SMS_SUB_TOTAL = "cur_send_sms_sub_total"; //统计当天订购发送的短信总条数
	
	public static final String CUR_SEND_SMS_SUCCESS="cur_send_sms_success";//统计当天发送多少成功短信
	
	public static final String CUR_SEND_SMS_ACC_SUCCESS="cur_send_sms_acc_success";//统计当天接入码发送多少成功短信
	
	public static final String CUR_SEND_SMS_SUB_SUCCESS="cur_send_sms_sub_success";//统计当天订购码发送多少成功短信

	public static final String CUR_SEND_ARTICLE="cur_send_article";//统计当天发送多少文章

	public static final String CUR_ARTICLE_SMS_MSG ="cur_article_sms_msg";//统计当天报纸生成多少SmsMsg

	public static final String CUR_ARTICLE_SEND_TOTAL = "cur_article_send_sms_total"; //统计当天发送的报纸总份数

	public static final String CUR_ARTICLE_SEND_SMS_SUCCESS="cur_article_send_sms_success";//统计当天手机报发送短信成功多少条

	public static final String CUR_ARTICLE_STAT_CONTENT_SUCCESS="cur_article_stat_content_success";//统计手机报内容发送成功数

	public static final String CUR_ARTICLE_STAT_CONTENT_FAIL="cur_article_stat_content_fail";//统计手机报发送失败数

	public static final String BATCH_FAIL_COUNT = "batch_fail_count"; //批开失败统计
	
	public static final String IS_SEND_ARTICLE_BATCH_UPDATE_ORDERS="is_send_article_batch_update_orders";//发送手机报的时候 批量更新数据，成功失败

	
	/**
	 * 消息类型
	 * @author think
	 *
	 */
	public enum SmsType {
		content,
		subscribe,
		stop,
		help,
		error,
		market,
		batch,
		test
	}
	
	
	public static String genSerial() {
		int randomInt = new Random().nextInt(99999999);
		String serial = "";
		if(randomInt >= 0 && randomInt < 10) {
			serial = "0000000"+String.valueOf(randomInt);
		} else if(randomInt >=10 & randomInt < 100) {
			serial = "000000" + String.valueOf(randomInt);
		} else if(randomInt >=100 & randomInt < 1000) {
			serial = "00000" + String.valueOf(randomInt);
		} else if(randomInt >=1000 & randomInt < 10000) {
			serial = "0000" + String.valueOf(randomInt);
		} else if(randomInt >=10000 & randomInt < 100000) {
			serial = "000" + String.valueOf(randomInt);
		} else if(randomInt >=100000 & randomInt < 1000000) {
			serial = "00" + String.valueOf(randomInt);
		} else if(randomInt >=1000000 & randomInt < 10000000) {
			serial = "0" + String.valueOf(randomInt);
		} else {
			serial = String.valueOf(randomInt);
		}
		return serial;
	}
	
}
