package com.tipu.agent.admin.base.gencode.model;

import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.MetaBuilder;
import com.jfinal.plugin.activerecord.generator.TableMeta;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义 MetaBuilder，处理表前缀跳过生成
 * @author Rlax
 *
 */
public class AppMetaBuilder extends MetaBuilder {
    /**
     * 需要跳过生成的表前缀
     */
    private String[] skipPre = null;

    public AppMetaBuilder(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected boolean isSkipTable(String tableName) {
        for (String skip : skipPre) {
            if (tableName.startsWith(skip)) {
                return true;
            }
        }
        return false;
    }

    public void setSkipPre(String... skipPre) {
        this.skipPre = skipPre;
    }

    @Override
    protected ResultSet getTablesResultSet() throws SQLException {
        String schemaPattern = dialect instanceof OracleDialect ? dbMeta.getUserName() : null;
        return dbMeta.getTables(conn.getCatalog(), schemaPattern, null, new String[]{"TABLE", "VIEW"});
    }

    @Override
    protected void buildPrimaryKey(TableMeta tableMeta) throws SQLException {
        if (tableMeta.name.toLowerCase().endsWith("_view")) {
            tableMeta.primaryKey = "id";
        } else {
            super.buildPrimaryKey(tableMeta);
        }
    }
    
    @Override
    public List<TableMeta> build() {
		System.out.println("Build TableMeta ...");
		try {
			conn = dataSource.getConnection();
			dbMeta = conn.getMetaData();
			
			List<TableMeta> ret = new ArrayList<TableMeta>();
			buildTableNames(ret);
			for (TableMeta tableMeta : ret) {
				buildPrimaryKey(tableMeta);
				buildColumnMetas(tableMeta);
			}
			return ret;
		}
		catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally {
			if (conn != null) {
				try {conn.close();} catch (SQLException e) {throw new RuntimeException(e);}
			}
		}
	}
    
    /**
	 * 文档参考：
	 * http://dev.mysql.com/doc/connector-j/en/connector-j-reference-type-conversions.html
	 * 
	 * JDBC 与时间有关类型转换规则，mysql 类型到 java 类型如下对应关系：
	 * DATE				java.sql.Date
	 * DATETIME			java.sql.Timestamp
	 * TIMESTAMP[(M)]	java.sql.Timestamp
	 * TIME				java.sql.Time
	 * 
	 * 对数据库的 DATE、DATETIME、TIMESTAMP、TIME 四种类型注入 new java.util.Date()对象保存到库以后可以达到“秒精度”
	 * 为了便捷性，getter、setter 方法中对上述四种字段类型采用 java.util.Date，可通过定制 TypeMapping 改变此映射规则
	 */
    @Override
	public void buildColumnMetas(TableMeta tableMeta) throws SQLException {
		String sql = dialect.forTableBuilderDoBuild(tableMeta.name);
		Statement stm = conn.createStatement();
		ResultSet rs = stm.executeQuery(sql);
		ResultSetMetaData rsmd = rs.getMetaData();
		
		ResultSet columnRs = dbMeta.getColumns(conn.getCatalog(), null, tableMeta.name, null);
		
		List<ColumnMeta> columnMetas = new ArrayList<ColumnMeta>();
		List<String> columnNames = new ArrayList<String>();
		
		while (columnRs.next()) {
			ColumnMeta columnMeta = new ColumnMeta();
			columnMeta.name = columnRs.getString("COLUMN_NAME");			// 名称
			
			columnNames.add(columnMeta.name);
			
			columnMeta.type = columnRs.getString("TYPE_NAME");			// 类型
			if (columnMeta.type == null) {
				columnMeta.type = "";
			}
			
			int columnSize = columnRs.getInt("COLUMN_SIZE");				// 长度
			if (columnSize > 0) {
				columnMeta.type = columnMeta.type + "(" + columnSize;
				int decimalDigits = columnRs.getInt("DECIMAL_DIGITS");	// 小数位数
				if (decimalDigits > 0) {
					columnMeta.type = columnMeta.type + "," + decimalDigits;
				}
				columnMeta.type = columnMeta.type + ")";
			}
			
			columnMeta.isNullable = columnRs.getString("IS_NULLABLE");	// 是否允许 NULL 值
			if (columnMeta.isNullable == null) {
				columnMeta.isNullable = "";
			}
			
			columnMeta.isPrimaryKey = "   ";
			String[] keys = tableMeta.primaryKey.split(",");
			for (String key : keys) {
				if (key.equalsIgnoreCase(columnMeta.name)) {
					columnMeta.isPrimaryKey = "PRI";
					break;
				}
			}
			
			columnMeta.defaultValue = columnRs.getString("COLUMN_DEF");	// 默认值
			if (columnMeta.defaultValue == null) {
				columnMeta.defaultValue = "";
			}
			
			columnMeta.remarks = columnRs.getString("REMARKS");			// 备注
			if (columnMeta.remarks == null) {
				columnMeta.remarks = "";
			}
			
			columnMetas.add(columnMeta);
		}
		columnRs.close();
		
		for (int i=1; i<=rsmd.getColumnCount(); i++) {
			ColumnMeta cm = new ColumnMeta();
			cm.name = rsmd.getColumnName(i);
			
			if(columnNames.contains(rsmd.getColumnName(i))) {
				ColumnMeta newCm = columnMetas.get(columnNames.indexOf(rsmd.getColumnName(i)));
				cm.remarks = newCm.remarks;
			}
			
			String typeStr = null;
			if (dialect.isKeepByteAndShort()) {
				int type = rsmd.getColumnType(i);
				if (type == Types.TINYINT) {
					typeStr = "java.lang.Byte";
				} else if (type == Types.SMALLINT) {
					typeStr = "java.lang.Short";
				}
			}
			
			if (typeStr == null) {
				String colClassName = rsmd.getColumnClassName(i);
				typeStr = typeMapping.getType(colClassName);
			}
			
			if (typeStr == null) {
				int type = rsmd.getColumnType(i);
				if (type == Types.BINARY || type == Types.VARBINARY || type == Types.LONGVARBINARY || type == Types.BLOB) {
					typeStr = "byte[]";
				} else if (type == Types.CLOB || type == Types.NCLOB) {
					typeStr = "java.lang.String";
				}
				// 支持 oracle 的 TIMESTAMP、DATE 字段类型，其中 Types.DATE 值并不会出现
				// 保留对 Types.DATE 的判断，一是为了逻辑上的正确性、完备性，二是其它类型的数据库可能用得着
				else if (type == Types.TIMESTAMP || type == Types.DATE) {
					typeStr = "java.util.Date";
				}
				// 支持 PostgreSql 的 jsonb json
				else if (type == Types.OTHER) {
					typeStr = "java.lang.Object";
				} else {
					typeStr = "java.lang.String";
				}
			}
			cm.javaType = typeStr;
			
			// 构造字段对应的属性名 attrName
			cm.attrName = buildAttrName(cm.name);
			
			tableMeta.columnMetas.add(cm);
		}
		
		rs.close();
		stm.close();
	}
    
}
