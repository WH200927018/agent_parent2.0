package com.tipu.agent.admin.base.gencode.controller;

import java.util.List;

import javax.sql.DataSource;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.tipu.agent.admin.base.gencode.model.AppMetaBuilder;

import io.jboot.Jboot;
import io.jboot.codegen.CodeGenHelpler;

/**
 * Controller代码自动生成
 * @author think
 *
 */
public class AppControllerGenerator {

	public static void doGenerate() {
		AppControllerGeneratorConfig config = Jboot.config(AppControllerGeneratorConfig.class);

        System.out.println(config.toString());

        if (StrKit.isBlank(config.getModelpackage())) {
            System.err.println("jboot.admin.controller.ge.modelpackage 不可为空");
            System.exit(0);
        }

        if (StrKit.isBlank(config.getServicepackage())) {
            System.err.println("jboot.admin.controller.ge.servicepackage 不可为空");
            System.exit(0);
        }

        if (StrKit.isBlank(config.getControllerpackage())) {
            System.err.println("jboot.admin.controller.ge.controllerpackage 不可为空");
            System.exit(0);
        }
        
        if (StrKit.isBlank(config.getControllerpath())) {
            System.err.println("jboot.admin.controller.ge.controllerpath 不可为空");
            System.exit(0);
        }

        String modelPackage = config.getModelpackage();
        String servicepackage = config.getServicepackage();
        String controllerpackage = config.getControllerpackage();
        String controllerpath = config.getControllerpath();
        String viewpath = config.getViewpath();
        String serviceName = config.getServicename();
        String serviceVersion = config.getServiceversion();

        System.out.println("start generate...");
        System.out.println("generate dir:" + controllerpackage);

        DataSource dataSource = CodeGenHelpler.getDatasource();

        AppMetaBuilder metaBuilder = new AppMetaBuilder(dataSource);

        if (StrKit.notBlank(config.getRemovedtablenameprefixes())) {
            metaBuilder.setRemovedTableNamePrefixes(config.getRemovedtablenameprefixes().split(","));
        }

        if (StrKit.notBlank(config.getExcludedtableprefixes())) {
            metaBuilder.setSkipPre(config.getExcludedtableprefixes().split(","));
        }

        List<TableMeta> tableMetaList = metaBuilder.build();
        CodeGenHelpler.excludeTables(tableMetaList, config.getExcludedtable());
        
        // 生成controller
        AppJbootControllerGenerator generator = new AppJbootControllerGenerator(servicepackage , modelPackage, controllerpackage, controllerpath, viewpath, serviceName, serviceVersion);
        generator.generate(tableMetaList);
        System.out.println("controller generate finished !!!");
        
        //生成页面
        AddViewGenerator addGenerator = new AddViewGenerator(servicepackage , modelPackage, controllerpackage, controllerpath, viewpath);
        addGenerator.generate(tableMetaList);
        System.out.println("add view generate finished !!!");
        
        UpdateViewGenerator updateGenerator = new UpdateViewGenerator(servicepackage , modelPackage, controllerpackage, controllerpath, viewpath);
        updateGenerator.generate(tableMetaList);
        System.out.println("update view generate finished !!!");
        
        MainViewGenerator mainGenerator = new MainViewGenerator(servicepackage , modelPackage, controllerpackage, controllerpath, viewpath);
        mainGenerator.generate(tableMetaList);
        System.out.println("main view generate finished !!!");
        
	}
	
}
