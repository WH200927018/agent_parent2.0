package com.tipu.agent.admin.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author:骆宏图
 * @CreateDate: 10:32 2018/5/2.
 * @Description: 国际化注解标签，用来匹配数据库 字段
 * 使用的时候注意，标注在 set方法上面，因为record 没有 真真属性
 * @Modified By:
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface InternationalMap {

    /**
     * 数据库匹配名
     * @return
     */
    String name() default "";

}