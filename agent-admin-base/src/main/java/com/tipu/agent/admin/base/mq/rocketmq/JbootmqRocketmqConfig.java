/**
 * Copyright (c) 2015-2018, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tipu.agent.admin.base.mq.rocketmq;

import io.jboot.config.annotation.PropertyConfig;


@PropertyConfig(prefix = "jboot.mq.rocketmq")
public class JbootmqRocketmqConfig {


    private String consumerGroup = "SmsConsumerGroup";
    
    private String producerGroup = "SmsProducerGroup";
    
    private String namesrvAddr = "192.168.1.100:9876";
    
    private String consumerTopic = "smsTopic"; // 消费的topic名称，如果有多个，以逗号分隔，建议每个实例只部署一个smsTopic
    
	public String getConsumerGroup() {
		return consumerGroup;
	}

	public void setConsumerGroup(String consumerGroup) {
		this.consumerGroup = consumerGroup;
	}

	public String getProducerGroup() {
		return producerGroup;
	}

	public void setProducerGroup(String producerGroup) {
		this.producerGroup = producerGroup;
	}

	public String getNamesrvAddr() {
		return namesrvAddr;
	}

	public void setNamesrvAddr(String namesrvAddr) {
		this.namesrvAddr = namesrvAddr;
	}

	public String getConsumerTopic() {
		return consumerTopic;
	}

	public void setConsumerTopic(String consumerTopic) {
		this.consumerTopic = consumerTopic;
	}

}
