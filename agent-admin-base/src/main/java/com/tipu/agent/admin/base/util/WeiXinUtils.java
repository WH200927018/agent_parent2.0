package com.tipu.agent.admin.base.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class WeiXinUtils {
	public static final String GET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token";// 获取access
	public static final String GET_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";// 获取ticket
	public static final String APP_ID = "wxf2ecfa125dd6006f";
	public static final String SECRET = "e706b8f592b7cee343255091cf7d7000";
	public static final Boolean HAVE_WECHAT = false;
	//public static final String JSURL = "http://kongjia.tiputrip.com/f/agent/back/share_invite?invite_id=20cc4d113aad49fe9b0ebf939426b1a0";

	// 获取token
	public static String getToken() {
		String turl = String.format(
				"%s?grant_type=client_credential&appid=%s&secret=%s", GET_TOKEN_URL,
				APP_ID, SECRET);
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(turl);
		JsonParser jsonparer = new JsonParser();// 初始化解析json格式的对象
		String result = null;
		try {
			HttpResponse res = client.execute(get);
			String responseContent = null; // 响应内容
			HttpEntity entity = res.getEntity();
			responseContent = EntityUtils.toString(entity, "UTF-8");
			JsonObject json = jsonparer.parse(responseContent)
					.getAsJsonObject();
			// 将json字符串转换为json对象
			if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				if (json.get("errcode") != null) {// 错误时微信会返回错误码等信息，{"errcode":40013,"errmsg":"invalid appid"}
				} else {// 正常情况下{"access_token":"ACCESS_TOKEN","expires_in":7200}
					result = json.get("access_token").getAsString();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭连接 ,释放资源
			client.getConnectionManager().shutdown();
			return result;
		}
	}

	/**
	 * 生成签名的随机串
	 * @return 
	 */
	public static String createNonceStr(){
		//char chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
		char [] chars = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
		String str = "";
		    for (int i = 0; i < 16; i++) {
		    	int idx=(int)(Math.random()*35);
		        str += chars[idx];
		    }
		return str;
	}
	
	/**
	 * 签名
	 * @param accessToken
	 * @return
	 */
	public static String getTicket(String accessToken) {
		//?access_token='+access_token+'&type=jsapi
		String turl = String.format("%s?access_token=%s&type=jsapi", GET_TICKET_URL, accessToken);
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(turl);
		JsonParser jsonparer = new JsonParser();// 初始化解析json格式的对象
		String result = null;
		try {
			HttpResponse res = client.execute(get);
			String responseContent = null; // 响应内容
			HttpEntity entity = res.getEntity();
			responseContent = EntityUtils.toString(entity, "UTF-8");
			JsonObject json = jsonparer.parse(responseContent)
					.getAsJsonObject();
			// 将json字符串转换为json对象
			System.out.println(json.get("errcode"));
			if (!"0".equals(json.get("errcode").getAsString())) {// 错误时微信会返回错误码等信息，{"errcode":40013,"errmsg":"invalid appid"}
				result = "";
			} else {//{"errcode":0,"errmsg":"ok","ticket":"bxLdikRXVbTPdHSM05e5u5sUoXNKd8-41ZO3MhKoyN5OfkWITDGgnr2fwJ0m9E8NYzWKVZvdVtaUgWvsdshFKA","expires_in":7200
				result = json.get("ticket").getAsString();
			}	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭连接 ,释放资源
			client.getConnectionManager().shutdown();
			return result;
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println("=========1获取token=========");
		String accessToken = getToken();// 获取token
		if (accessToken != null){
			System.out.println("=========1获取ticket=========");
			String ticket = getTicket(accessToken);// 获取token
			if (ticket != null){
				System.out.println("生成签名url");
				System.out.println("生成签名");
				String jsurl = "http://kongjia.tiputrip.com/f/agent/back/share_invite?invite_id=20cc4d113aad49fe9b0ebf939426b1a0";
				Map<String, String> map = sign(ticket, jsurl);
				System.out.println("timestamp="+map.get("timestamp"));
				System.out.println("nonceStr="+map.get("nonceStr"));
				System.out.println("signature="+map.get("signature"));
				System.out.println("jsapi_ticket="+map.get("jsapi_ticket"));
			}
		}
		
		
	}
	public static Map<String, String> sign(String jsapi_ticket, String jsurl) {
        Map<String, String> ret = new HashMap<String, String>();
        String nonce_str = createNonceStr();
        String timestamp = create_timestamp();
        String string1;
        String signature = "";

        //注意这里参数名必须全部小写，且必须有序
        string1 = "jsapi_ticket=" + jsapi_ticket +
                  "&noncestr=" + nonce_str +
                  "&timestamp=" + timestamp +
                  "&url=" + jsurl;
        try
        {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(string1.getBytes("UTF-8"));
            signature = byteToHex(crypt.digest());
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        ret.put("url", jsurl);
        ret.put("jsapi_ticket", jsapi_ticket);
        ret.put("nonceStr", nonce_str);
        ret.put("timestamp", timestamp);
        ret.put("signature", signature);

        return ret;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash)
        {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    private static String create_timestamp() {
        return Long.toString(System.currentTimeMillis() / 1000);
    }
}
