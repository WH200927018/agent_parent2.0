package com.tipu.agent.admin.base.gencode.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.jfinal.template.Engine;
import com.jfinal.template.source.ClassPathSourceFactory;

/**
 * Controller代码自动生成
 * @author think
 *
 */
public class AppJbootControllerGenerator extends BaseModelGenerator {

	private String modelPackage;
    private String servicePackage;
    private String controllerPackage;
    private String controllerPath;
    private String serviceName;
    private String serviceVersion;
	
	public AppJbootControllerGenerator(String servicePackage, String modelPackage, 
			String controllerPackage, String controllerPath, String viewPath, String serviceName, String serviceVersion) {
		super(controllerPackage, PathKit.getWebRootPath() + "/src/main/java/" + controllerPackage.replace(".", "/"));
		
        this.modelPackage = modelPackage;
        this.servicePackage = servicePackage;
        this.controllerPackage = controllerPackage;
        this.controllerPath = controllerPath;
        this.serviceName = serviceName;
        this.serviceVersion = serviceVersion;
        this.template = "com/tipu/agent/admin/base/gencode/controller/controller_template.jf";
		
	}
	
	@Override
    public void generate(List<TableMeta> tableMetas) {
        System.out.println("Generate base model ...");
        System.out.println("Base Model Output Dir: " + baseModelOutputDir);

        Engine engine = Engine.create("forController");
        engine.setSourceFactory(new ClassPathSourceFactory());
        engine.addSharedMethod(new StrKit());
        engine.addSharedObject("getterTypeMap", getterTypeMap);
        engine.addSharedObject("javaKeyword", javaKeyword);

        for (TableMeta tableMeta : tableMetas) {
            genBaseModelContent(tableMeta);
        }
        writeToFile(tableMetas);
    }


    @Override
    protected void genBaseModelContent(TableMeta tableMeta) {
        Kv data = Kv.by("controllerPackageName", baseModelPackageName);
        data.set("generateChainSetter", generateChainSetter);
        data.set("tableMeta", tableMeta);
        data.set("basePackage", servicePackage);
        data.set("modelPackage", modelPackage);
        data.set("controllerPackage", controllerPackage);
        data.set("servicePackage", servicePackage);
        data.set("controllerpath", controllerPath);
        data.set("SERVICE_NAME", serviceName);
        data.set("SERVICE_VERSION", serviceVersion);

        Engine engine = Engine.use("forController");
        tableMeta.baseModelContent = engine.getTemplate(template).renderToString(data);
    }

    /**
     * base model 覆盖写入
     */
    @Override
    protected void writeToFile(TableMeta tableMeta) throws IOException {
        File dir = new File(baseModelOutputDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String target = baseModelOutputDir + File.separator + tableMeta.modelName + "Controller" + ".java";

        File targetFile = new File(target);
        if (targetFile.exists()) {
            return;
        }


        FileWriter fw = new FileWriter(target);
        try {
            fw.write(tableMeta.baseModelContent);
        } finally {
            fw.close();
        }
    }
	
}
