package com.tipu.agent.admin.base.gencode.controller;

import io.jboot.config.annotation.PropertyConfig;

/**
 * controller代码生成配置
 * @author think
 *
 */
@PropertyConfig(prefix="jboot.admin.controller.ge")
public class AppControllerGeneratorConfig {

	/** entity 包名 */
    private String modelpackage;
    /** service 包名 */
    private String servicepackage;
    /** controller 包名 */
    private String controllerpackage;
    /** controller前缀 */
    private String controllerpath;
    /** view目录前缀 */
    private String viewpath;
    /** 服务名称 */
    private String servicename;
    /** 服务版本 */
    private String serviceversion;
    /** 移除的表前缀 */
    private String removedtablenameprefixes;
    /** 不包含表 */
    private String excludedtable;
    /** 不包含表前缀 */
    private String excludedtableprefixes;
    
	public String getModelpackage() {
		return modelpackage;
	}
	
	public void setModelpackage(String modelpackage) {
		this.modelpackage = modelpackage;
	}
	
	public String getServicepackage() {
		return servicepackage;
	}
	
	public void setServicepackage(String servicepackage) {
		this.servicepackage = servicepackage;
	}
	
	public String getControllerpackage() {
		return controllerpackage;
	}
	
	public void setControllerpackage(String controllerpackage) {
		this.controllerpackage = controllerpackage;
	}
	
	public String getControllerpath() {
		return controllerpath;
	}

	public void setControllerpath(String controllerpath) {
		this.controllerpath = controllerpath;
	}

	public String getViewpath() {
		return viewpath;
	}

	public void setViewpath(String viewpath) {
		this.viewpath = viewpath;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getServiceversion() {
		return serviceversion;
	}

	public void setServiceversion(String serviceversion) {
		this.serviceversion = serviceversion;
	}

	public String getRemovedtablenameprefixes() {
		return removedtablenameprefixes;
	}
	
	public void setRemovedtablenameprefixes(String removedtablenameprefixes) {
		this.removedtablenameprefixes = removedtablenameprefixes;
	}
	
	public String getExcludedtable() {
		return excludedtable;
	}
	
	public void setExcludedtable(String excludedtable) {
		this.excludedtable = excludedtable;
	}
	
	public String getExcludedtableprefixes() {
		return excludedtableprefixes;
	}
	
	public void setExcludedtableprefixes(String excludedtableprefixes) {
		this.excludedtableprefixes = excludedtableprefixes;
	}
	
}
