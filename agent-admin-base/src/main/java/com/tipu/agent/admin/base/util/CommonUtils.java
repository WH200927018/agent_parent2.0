package com.tipu.agent.admin.base.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;


/**
 * CommonUtils 授权书模板
 * 
 * @author hulin
 *
 */
public class CommonUtils {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	private static String productName = PropKit.get("jboot.productName");

	public static final String NUMBERCHAR = "0123456789";

	/**
	 * 生成商品编号
	 * 
	 * @return
	 */
	public static String getProductSn() {
		return sdf.format(new Date());
	}

	public static String getRandomInt(int length) {
		StringBuffer sb = new StringBuffer("");
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(NUMBERCHAR.charAt(random.nextInt(NUMBERCHAR.length())));
		}
		return sb.toString();
	}

	/**
	 * 生成订单编号
	 * 
	 * @return
	 */
	public static String getOrderSn() {
		return sdf.format(new Date());
	}

	/**
	 * 生成发货单号
	 * 
	 * @return
	 */
	public static String getDeliverySn() {
		return sdf.format(new Date());
	}

	public static String getFormateDate(String datetime, String formateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat(formateStr);
		return sdf.format(datetime);
	}

	/**
	 * 
	 * @param fontPath
	 *            字体路径
	 * @param templatePath
	 *            授权文件路径
	 * @param authApplyType
	 *            申请方式 1微商 2电商
	 * @param savePath
	 *            保存文件路径
	 * @param param
	 *            map参数
	 * @return
	 * @throws FontFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	public static String genAuthPic(String authTemplet, String savePath,
			Map<String, String> param,String templetConfig) throws FontFormatException, IOException, ParseException {
		String templatePath = PathKit.getWebRootPath() + File.separator + "auth" + File.separator + authTemplet; // 授权书模板路径
		String fontPath = PathKit.getWebRootPath() + File.separator + "auth" + File.separator + "simhei.ttf";
		BufferedImage img = ImageIO.read(new File(templatePath));
		Graphics2D g = img.createGraphics();
		Font f = Font.createFont(Font.TRUETYPE_FONT, new File(fontPath));
		f = f.deriveFont(Font.PLAIN, 20f);
		g.setFont(f);
		g.setColor(Color.BLACK);
		if (StrKit.notBlank(templetConfig)) {
			String[] temC = templetConfig.split(";");
			for (String tc : temC) {
				if (StrKit.notBlank(tc)) {
					String[] t = tc.split(",");
					g.drawString(param.get(t[0].trim()), Integer.valueOf(t[1].trim()), Integer.valueOf(t[2].trim()));
				}
			}
		}
		ImageIO.write(img, "JPG", new File(savePath));
		g.dispose();
		return OssUtil.upload("auth/pic/" + savePath.substring(savePath.lastIndexOf(File.separator) + 1), savePath);
	}

	/**
	 * 对特殊字符转义
	 * 
	 * @param name
	 * @return
	 */
	public static String name(String name) {
		name = name.replace("&ldquo;", "“");
		name = name.replace("&rdquo;", "”");
		name = name.replace("&nbsp;", " ");
		name = name.replace("&amp;", "&");
		name = name.replace("&#39;", "'");
		name = name.replace("&rsquo;", "’");
		name = name.replace("&mdash;", "—");
		name = name.replace("&ndash;", "–");
		return name;
	}

	public static void main(String[] args) {
		String[] fontNames = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		for (int i = 0; i < fontNames.length; i++)
			System.out.println(fontNames[i]);
	}

}
