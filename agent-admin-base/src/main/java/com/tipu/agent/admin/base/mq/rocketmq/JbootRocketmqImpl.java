/**
 * Copyright (c) 2015-2018, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tipu.agent.admin.base.mq.rocketmq;

import com.google.common.collect.Sets;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;

import io.jboot.Jboot;
import io.jboot.core.mq.Jbootmq;
import io.jboot.core.mq.JbootmqBase;
import io.jboot.core.spi.JbootSpi;
import io.jboot.exception.JbootIllegalConfigException;
import io.jboot.utils.StringUtils;

import java.util.List;
import java.util.Set;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.apache.rocketmq.remoting.exception.RemotingException;

@JbootSpi(value = "rocketmq")
public class JbootRocketmqImpl extends JbootmqBase implements Jbootmq {


    private static final Log LOG = Log.getLog(JbootRocketmqImpl.class);
    private DefaultMQProducer defaultMQProducer;

    public JbootRocketmqImpl() {

        JbootmqRocketmqConfig rocketmqConfig = Jboot.config(JbootmqRocketmqConfig.class);
        
        String producerGroup = rocketmqConfig.getProducerGroup();
        String namesrvAddr = rocketmqConfig.getNamesrvAddr();
        String consumerTopic = rocketmqConfig.getConsumerTopic();
        
        if(StrKit.isBlank(producerGroup) && StrKit.isBlank(consumerTopic)) {
        	throw new JbootIllegalConfigException("producerGroup or consumerTopic not exist, please config jboot.mq.rocketmq.producerGroup to set the producerGroup or jboot.mq.rocketmq.consumerTopic to set the consumerTopic.");
        }
        
        if(StrKit.notBlank(producerGroup)) {
        	LOG.info("SmsProducer initialize!");
            LOG.info(producerGroup);
        	LOG.info(namesrvAddr);
            
            defaultMQProducer = new DefaultMQProducer(producerGroup);
    		defaultMQProducer.setNamesrvAddr(namesrvAddr);
    		defaultMQProducer.setInstanceName(String.valueOf(System.currentTimeMillis()));
    		defaultMQProducer.setVipChannelEnabled(false);
            
    		defaultMQProducer.setRetryTimesWhenSendFailed(3);
    		
    		defaultMQProducer.setRetryAnotherBrokerWhenNotStoreOK(true);
    		
    		try {
    			defaultMQProducer.start();
    		} catch (MQClientException e) {
    			e.printStackTrace();
    		}
        }
        
        if(StrKit.notBlank(consumerTopic)) {
            Set<String> consumerTopicList = Sets.newHashSet();
            consumerTopicList.addAll(StringUtils.splitToSet(consumerTopic, ","));
            for (String topic : consumerTopicList) {
            	// 分别发布订阅不同的topic
                registerListner(topic, rocketmqConfig);
            }
        }

    }
    
    private void registerListner(String topic, JbootmqRocketmqConfig rocketmqConfig) {
        if (StrKit.isBlank(topic)) {
            return;
        }
        try {
        	String consumerGroup = rocketmqConfig.getConsumerGroup();
        	String namesrvAddr = rocketmqConfig.getNamesrvAddr();
        	// 参数信息
        	LOG.info(topic + " initialize!");
        	LOG.info(consumerGroup);
        	LOG.info(namesrvAddr);

    		// 一个应用创建一个Consumer，由应用来维护此对象，可以设置为全局对象或者单例<br>
    		// 注意：ConsumerGroupName需要由应用来保证唯一
    		DefaultMQPushConsumer defaultMQPushConsumer = new DefaultMQPushConsumer(consumerGroup);
    		defaultMQPushConsumer.setNamesrvAddr(namesrvAddr);
    		defaultMQPushConsumer.setInstanceName(String.valueOf(System.currentTimeMillis()));

    		defaultMQPushConsumer.setVipChannelEnabled(false);

    		// 订阅指定MyTopic下tags等于MyTag

    		defaultMQPushConsumer.subscribe(topic, "*");

    		// 设置Consumer第一次启动是从队列头部开始消费还是队列尾部开始消费<br>
    		// 如果非第一次启动，那么按照上次消费的位置继续消费
    		defaultMQPushConsumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);

    		// 设置为集群消费(区别于广播消费)
    		defaultMQPushConsumer.setMessageModel(MessageModel.CLUSTERING);

    		defaultMQPushConsumer.setConsumeMessageBatchMaxSize(1);// 设置只处理一条

    		defaultMQPushConsumer.setMaxReconsumeTimes(0);// 1次就占时不处理

    		defaultMQPushConsumer.registerMessageListener(new MessageListenerConcurrently() {

    			// 默认msgs里只有一条消息，可以通过设置consumeMessageBatchMaxSize参数来批量接收消息
    			@Override
    			public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
    				for (MessageExt messageExt : msgs) {
    					Object o = Jboot.me().getSerializer().deserialize(messageExt.getBody());
                        notifyListeners(topic, o);
    				}
    				// 如果没有return success ，consumer会重新消费该消息，直到return success
    				return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    			}
    		});

    		// Consumer对象在使用之前必须要调用start初始化，初始化一次即可<br>
    		defaultMQPushConsumer.start();

    		LOG.info("DefaultMQPushConsumer start success!");

        } catch (MQClientException e) {
			e.printStackTrace();
		}
    }

    @Override
    public void enqueue(Object message, String toChannel) {

        publish(message, toChannel);
    }

    @Override
    public void publish(Object message, String toChannel) {

        Message msg = new Message(toChannel, toChannel, Jboot.me().getSerializer().serialize(message));
        SendResult sendResult = null;
        //这里存在网络问题，异常要考虑下怎么处理，
        try {
            sendResult = defaultMQProducer.send(msg);
        } catch (MQClientException e) {
        	e.printStackTrace();
            LOG.error(e.getMessage() + String.valueOf(sendResult));
        } catch (RemotingException e) {
            e.printStackTrace();
        } catch (MQBrokerException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }


}
