package com.tipu.agent.admin.base.common;

/**
 * 缓存目录 KEY
 * @author Rlax
 *
 */
public class CacheKey {

	/** 基础数据 对应 data 表 keyValue缓存在cache的name */
	public static final String CACHE_KEYVALUE = "keyValue";

	/** 页面数据缓存 */
	public static final String CACHE_PAGE = "pageCache";

	/** 30分钟缓存 */
	public static final String CACHE_H1M30 = "h1m30";
	
	/** 导航目录缓存 */
	public static final String CACHE_MENU = "menuCache";
	
	/** shiro 授权缓存 */
	public static final String CACHE_SHIRO_AUTH = "shiro-authorizationCache";
	
	/** shiro session在线缓存 */
	public static final String CACHE_SHIRO_ACTIVESESSION = "shiro-active-session";

	/** shiro session踢出缓存 */
	public static final String CACHE_SHIRO_KICKOUTSESSION = "shiro-kickout-session";
	
	/** shiro 密码重试缓存 */
	public static final String CACHE_SHIRO_PASSWORDRETRY = "shiro-passwordRetryCache";
	
	/** shiro SESSION KEY 缓存 */
	public static final String CACHE_SHIRO_SESSION = "shiro-session";
	
	/** 验证码 缓存 */
	public static final String CACHE_CAPTCHAR_SESSION = "captchar-cache";

	/** jwt_token */
	public static final String CACHE_JWT_TOKEN = "jwt_token";
	
	/**广告栏位 缓存*/
	public static final String CACHE_ADFIELD = "adField-cache";
	
	/**广告 缓存*/
	public static final String CACHE_ADITEM = "adItem-cache";
	
	/**代理商保证金 缓存*/
	public static final String CACHE_AGENTBOND = "agentBond-cache";
	
	/**代理商品牌 缓存*/
	public static final String CACHE_AGENTBRAND = "agentBrand-cache";
	
	/**代理商邀请 缓存*/
	public static final String CACHE_AGENTINVITE = "agentInvite-cache";
	
	/**代理商登录 缓存*/
	public static final String CACHE_AGENTLOGINLOG = "agentLoginLog-cache";
	
	/**代理商 缓存*/
	public static final String CACHE_AGENT = "agent-cache";
	
	/**代理商类型 缓存*/
	public static final String CACHE_AGENTTYPE = "agentType-cache";
	
	/**代理商常用地址 缓存*/
	public static final String CACHE_AGENTADDRESS = "agentAddress-cache";
	
	/**文章 缓存*/
	public static final String CACHE_ARTICLE = "article-cache";
	
	/**品牌 缓存*/
	public static final String CACHE_BRAND = "brand-cache";
	
	/**发货 缓存*/
	public static final String CACHE_DELIVERYITEM = "deliveryItem-cache";
	
	/**发货单 缓存*/
	public static final String CACHE_DELIVERY = "delivery-cache";
	
	/**商品图片 缓存*/
	public static final String CACHE_PRODUCTIMAGES = "productImages-cache";
	
	/**商品 缓存*/
	public static final String CACHE_PRODUCT = "product-cache";
	
	/**商品类型 缓存*/
	public static final String CACHE_PRODUCTTYPE = "productType-cache";
	
	/**防伪规则 缓存*/
	public static final String CACHE_ANTIRULE = "antiRule-cache";
	
	/**防伪记录 缓存*/
	public static final String CACHE_ANTIRULERECORD = "antiRuleRecord-cache";
	
	/**防伪 缓存*/
	public static final String CACHE_ANTICODE = "anticode-cache";
	
	/**防伪查询记录 缓存*/
	public static final String CACHE_ANTICODESEARCHLOG = "anticodeSearchLog-cache";
	
	/**防伪配置 缓存*/
	public static final String CACHE_ANTICODECONFIG = "anticodeConfig-cache";
	
	/**通告管理 缓存*/
	public static final String CACHE_OANOTIFY = "oaNotify-cache";

	/**产品订单 缓存*/
	public static final String CACHE_ORDER = "order-cache";

	/**代理商银行信息 缓存*/
	public static final String CACHE_AGENTBANKS = "agentBanks-cache";

	/**商品规格 缓存*/
	public static final String CACHE_SPEC = "spec-cache";

	/**商品规格组 缓存*/
	public static final String CACHE_SPECGROUP = "specGroup-cache";
	
	/**商品规格元素 缓存*/
	public static final String CACHE_SPECITEM = "specItem-cache";

	/**商品属性 缓存*/
	public static final String CACHE_ATTRIBUTE = "attribute-cache";
	
	/**商品属性元素 缓存*/
	public static final String CACHE_ATTRIBUTEITEM = "attributeItem-cache";

	/**商品sku管理 缓存*/
	public static final String CACHE_SKU = "sku-cache";
	
	/**商品sku管理 缓存*/
	public static final String CACHE_SKUSPECITEM = "skuSpecItem-cache";
	
	/**商品sku管理 缓存*/
	public static final String CACHE_SKUATTRIBUTEITEM = "skuAttributeItem-cache";

	/**系统设置 缓存*/
	public static final String SETTING = "setting";	
	
	/**商品价格 缓存*/
	public static final String CACHE_AGENTPRICE = "agentPrice-cache";

	/**公司信息  缓存*/
	public static final String CACHE_COMPANY = "sys-company";
	
	/**品牌 缓存*/
	public static final String CACHE_SYS_BRAND = "sys-brand";

	/**代理商授权 缓存*/
	public static final String CACHE_AGENTAUTH = "agentAuth-cache";
}
