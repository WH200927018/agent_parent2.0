package com.tipu.agent.admin.base.common;

/**
 * 服务常量
 * @author Rlax
 *
 */
public final class ServiceConst {

    /** 系统服务 */
    public final static String SERVICE_SYSTEM = "system-service";
    
    public final static String SERVICE_BASE = "front-service";

    public final static String  SERVICE_ANTICODE="anticode-service";
    
    public final static String  SERVICE_ANTICODEGEN="anticodegen-service";


    /** 服务版本 */
    public final static String VERSION_1_0 = "1.0";
    /** 服务版本 */
    public final static String VERSION_2_0 = "2.0";
}
