#sql("findList")
  SELECT
    a.*,
    b.name AS typeName
  FROM
    attribute a
  LEFT JOIN
    product_type b
  ON
    a.type_id = b.id
  WHERE
  a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
  #if(typeId!=null)
  	AND  a.type_id = #para(typeId)
  #end
  #if(attributeId!=null)
  	AND  a.id = #para(attributeId)
  #end
	ORDER BY a.create_date desc
#end