#sql("findList")
  SELECT
    a.*,
    b.name AS attributeName,
    c.name AS typeName
  FROM
    attribute_item a
  LEFT JOIN
    attribute b
  ON
    a.attribute_id = b.id
  LEFT JOIN
    product_type c
  ON
    a.type_id = c.id
  WHERE
  a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
  #if(typeId!=null)
  	AND  a.type_id = #para(typeId)
  #end
  #if(attributeId!=null)
  	AND  a.attribute_id = #para(attributeId)
  #end
  #if(attributeItemId!=null)
  	AND  a.id = #para(attributeItemId)
  #end
	ORDER BY a.create_date desc
#end