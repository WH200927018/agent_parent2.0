#sql("findList")
  SELECT
    a.*,
    b.name AS typeName
  FROM
    spec_group a
  LEFT JOIN
    product_type b
  ON
    a.type_id = b.id
  WHERE
  a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
  #if(typeId!=null)
  	AND  a.type_id = #para(typeId)
  #end
  #if(specId!=null)
  	AND  a.id = #para(specGroupId)
  #end
    
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end