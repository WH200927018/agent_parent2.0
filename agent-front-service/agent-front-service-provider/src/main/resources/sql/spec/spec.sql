#sql("findList")
  SELECT
    a.*,
    b.name AS groupName,
    c.name AS typeName
  FROM
    spec a
  LEFT JOIN
    spec_group b
  ON
    a.group_id = b.id
  LEFT JOIN
    product_type c
  ON
    a.type_id = c.id
  WHERE
  a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
  #if(typeId!=null)
  	AND  a.type_id = #para(typeId)
  #end
  #if(groupId!=null)
  	AND  a.group_id = #para(groupId)
  #end
  #if(specId!=null)
  	AND  a.id = #para(specId)
  #end
  #if(specItemId!=null)
  	AND  a.spec_item_id = #para(specItemId)
  #end
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
  
	ORDER BY a.create_date desc
#end

#sql("findSpecItemList")
	SELECT
		a.*,
		b.id AS specItemId,
		b.name AS specItemName
	FROM
		spec a
	RIGHT JOIN
		spec_item b
	on
		a.id = b.spec_id
	WHERE
		a.del_flag = 0
		
	#if(typeId!=null)
  	AND  
  		a.type_id = #para(typeId)
  	#end
  	#if(groupId!=null)
  	AND  
  		a.group_id = #para(groupId)
  	#end
  	#if(specId!=null)
  	AND  
  		a.id = #para(specId)
  	#end
  	
  	#if(specItemId!=null)
  	AND  
  		a.spec_item_id = #para(specItemId)
  	#end
  	
  	#if(companyId!=null)
  	AND  a.company_id = #para(companyId)
    #end
    
    #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
    #end
		ORDER BY a.sort ASC 
#end