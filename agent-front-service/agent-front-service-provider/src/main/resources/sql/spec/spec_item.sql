#sql("findList")
  SELECT
    a.*,
    b.name AS specName
  FROM
    spec_item a
  LEFT JOIN
    spec b
  ON
    a.spec_id = b.id
  WHERE
  a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
  #if(specId!=null)
  	AND  a.spec_id = #para(specId)
  #end
  #if(specItemId!=null)
  	AND  a.id = #para(specItemId)
  #end
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end