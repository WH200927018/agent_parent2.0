#sql("findList")
  SELECT
    a.*,
    b.delivery_no AS deliveryNo,
    c.name AS productName
  FROM
    delivery_item  a
  LEFT JOIN
   	delivery b
  ON
  	a.delivery = b.id 
  LEFT JOIN
  	product c
  ON 
  	a.product = c.id
  WHERE
  	a.del_flag = 0
  #if(delivery!=null)
  	AND  a.delivery = #para(delivery)
  #end
  #if(product!=null)
  	AND  a.product = #para(product)
  #end
  #if(antiCode!=null)
  	AND  a.anti_code = #para(antiCode)
  #end
  #if(logisticsCode!=null)
  	AND  a.logistics_code = #para(logisticsCode)
  #end
     
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end
