#sql("findList")
  SELECT
    *
  FROM
    delivery  a
  WHERE
  a.del_flag = 0
  
  #if(agentSend!=null)
  	AND  a.agent_send like #para(agentSend)
  #end
  #if(agentReceive!=null)
  	AND  a.agent_receive like #para(agentReceive)
  #end
  #if(deliveryNo!=null)
  	AND  a.delivery_no = #para(deliveryNo)
  #end
  #if(deliveryStatus!=null)
  	AND  a.delivery_status = #para(deliveryStatus)
  #end
     
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end

#sql("getMaxNo")
	SELECT 
		max(a.delivery_no)
	FROM 
		delivery a
	WHERE
  		a.del_flag = 0
  	   
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end	
#end

#sql("findDelivery")
  SELECT
    a.delivery_no,
    a.create_date,
    a.agent_receive_id,
    c.name AS agent_receive,
    SUM(b.product_num) AS productNum
  FROM
    delivery  a
  LEFT JOIN
  	delivery_item b
  ON 
  	a.id = b.delivery
  LEFT JOIN
  	agent c
  ON 
  	a.agent_receive_id = c.id
  WHERE
  		a.del_flag = 0
  		
  #if(deliveryNo!=null)
  	AND  a.delivery_no = #para(deliveryNo)
  #end
     
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end