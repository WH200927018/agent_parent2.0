#namespace("adfield")
  #include("adfield/adfield.sql")
#end

#namespace("adItem")
  #include("adfield/adItem.sql")
#end

#namespace("product")
  #include("product/product.sql")
#end

#namespace("agentType")
  #include("agent/agentType.sql")
#end

#namespace("agentInvite")
  #include("agent/agent_invite.sql")
#end

#namespace("agentBrand")
  #include("agent/agent_brand.sql")
#end

#namespace("agentLoginLog")
  #include("agent/agent_login_log.sql")
#end

#namespace("agentBond")
  #include("agent/agent_bond.sql")
#end

#namespace("agent")
  #include("agent/agent.sql")
#end

#namespace("agentAddress")
  #include("agent/agent_address.sql")
#end

#namespace("agentBanks")
  #include("agent/agent_banks.sql")
#end

#namespace("agentPrice")
  #include("agent/agent_price.sql")
#end

#namespace("agentAuth")
  #include("agent/agent_auth.sql")
#end

#namespace("delivery")
  #include("delivery/delivery.sql")
#end

#namespace("deliveryItem")
  #include("delivery/delivery_item.sql")
#end

#namespace("oaNotify")
  #include("oa/oaNotify.sql")
#end

#namespace("order")
  #include("order/order.sql")
#end

#namespace("orderProduct")
  #include("order/order_product.sql")
#end

#namespace("spec")
  #include("spec/spec.sql")
#end

#namespace("specGroup")
  #include("spec/spec_group.sql")
#end

#namespace("specItem")
  #include("spec/spec_item.sql")
#end

#namespace("attribute")
  #include("attribute/attribute.sql")
#end

#namespace("attributeItem")
  #include("attribute/attribute_item.sql")
#end

#namespace("setting")
  #include("setting/setting.sql")
#end

#namespace("sku")
  #include("sku/sku.sql")
#end

#namespace("skuSpecItem")
  #include("sku/sku_spec_item.sql")
#end

#namespace("agentRebateConfigParams")
  #include("agent/agent_rebate_config_params.sql")
#end

