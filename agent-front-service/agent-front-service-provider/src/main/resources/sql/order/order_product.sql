#sql("findList")
  SELECT
    a.*,
    b.name AS productName
  FROM
    order_product a
  LEFT JOIN
    sku b
  ON
    a.product = b.id
  WHERE
  	a.del_flag = 0
  
  #if(orderId!=null)
  AND  
  	a.order_id LIKE #para(orderId)
  #end
  
  #if(id!=null)
  AND  
  	a.id LIKE #para(id)
  #end
	ORDER BY a.create_date desc
#end

#sql("findOrderProductByOrderId")
	SELECT 
		op.*,
		p.name AS skuName,
		p.price AS skuPrice
	FROM 
		order_product op 
	JOIN 
		sku p 
	ON 
		p.id = op.product 
	WHERE 
		op.order_id = #para(orderId)
#end