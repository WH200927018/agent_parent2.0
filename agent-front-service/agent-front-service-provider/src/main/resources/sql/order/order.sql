#sql("findList")
  SELECT
    a.*,
    b.name AS username,
    c.account_asname AS bankAccountAsname,
    c.bank_name AS bankName,
    c.account AS bankAccount,
    d.name AS createName,
    e.name AS updateName,
    f.name AS orderCreateName
  FROM
    orders a
  LEFT JOIN
    agent b
  ON
    a.agent_id = b.id
  LEFT JOIN
    agent_banks c
  ON
    a.bank = c.id 
  LEFT JOIN
    agent d
  ON
    a.create_by = d.id 
  LEFT JOIN
    agent e
  ON
    a.update_by = e.id 
  LEFT JOIN
    agent f
  ON
    a.order_creater_id = f.id 
  WHERE
  	a.del_flag = 0
  
  #if(orderId!=null)
  AND  
  	a.id LIKE #para(orderId)
  #end
  
  #if(id!=null)
  AND  
  	a.id LIKE #para(id)
  #end
  
  #if(orderType!=null)
  AND  
  	a.order_type = #para(orderType)
  #end
  
  #if(state!=null)
  AND  
  	a.state = #para(state)
  #end
  
  #if(gtState!=null)
  AND  
  	a.state > #para(gtState)
  #end
    
  #if(orderGroup!=null)
  AND  
  	a.order_group_id = #para(orderGroup)
  #end
    
  #if(orderCreater!=null)
  AND  
  	a.order_creater_id = #para(orderCreater)
  #end
    
  #if(now!=null)
  AND  
  	a.create_date like #para(now)
  #end
  
  #if(agent!=null)
  AND  
  	a.agent_id = #para(agent)
  #end
  
  #if(handleAgentId!=null)
  AND  
  	a.handle_agent_id = #para(handleAgentId)
  #end
    
   #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end


#sql("countMoney")
	select 
		SUM(buy_sum)
	from 
		orders o
	where 
		(o.agent_id = #para(agentId) 
	or 
		o.agent_id 
	in 
		(select 
			a.id 
		from 
			agent a 
		left JOIN 
			agent_type t 
		on 
			a.agent_type = t.id  
		where 
			t.sort =1 
		and 
			parent_id  = #para(agentId) ))
	and 
		o.state > 0  
	and 
		o.del_flag = 0 
	and 
		o.order_type = 1 
	and 
		o.product_type = 1
#end

#sql("findChildList")
  SELECT
    a.*
  FROM
    orders a
  WHERE
  	a.del_flag = 0
  
  #if(orderId!=null)
  AND  
  	a.id LIKE #para(orderId)
  #end
  
  #if(id!=null)
  AND  
  	a.id LIKE #para(id)
  #end
  
  #if(orderType!=null)
  AND  
  	a.order_type = #para(orderType)
  #end
  
  #if(state!=null)
  AND  
  	a.state = #para(state)
  #end
    
  #if(orderGroup!=null)
  AND  
  	a.order_group_id = #para(orderGroup)
  #end
    
  #if(orderCreater!=null)
  AND 
  exists (select c.agent_id from agent_brand c where c.parent_id= #para(orderCreater)  AND c.auth_status = '1'	AND c.agent_id=a.order_creater_id) 
  #end
  
  #if(agent!=null)
  AND  
  	a.agent_id = #para(agent)
  #end
  
  #if(handleAgentId!=null)
  AND  
  	a.handle_agent_id = #para(handleAgentId)
  #end
  
	ORDER BY a.create_date desc
#end


#sql("findOrders")
	SELECT 
		* 
	FROM 
		orders o
	WHERE 
		o.del_flag=0 
	AND 
		order_type = 1
	AND 
		o.handle_agent_id = #para(handleAgentId) 
	AND 
		(o.state = 0 OR o.state = 1) 
	ORDER BY 
		o.create_date DESC
#end

#sql("myAchievementListPage")
	SELECT 
		o.*,
		tt.name AS agentTypeName,
		a.name AS agentName 
	FROM 
		orders o 
	LEFT JOIN 
		agent a 
	ON 
		o.order_creater_id = a.id 
	LEFT JOIN 
		agent_type tt ON a.agent_type = tt.id
	WHERE 
		(
			o.agent_id = #para(agentId)  OR o.agent_id IN 
			(SELECT g.id FROM agent g LEFT JOIN agent_type t ON g.agent_type = t.id WHERE t.sort=1 AND g.parent_id = #para(agentId))
		) 
	AND 
		o.state > 0 
	AND 
		o.del_flag = 0 
	AND 
		o.order_type = 1 
	AND 
		o.product_type = 1 
	ORDER BY create_date DESC

#end