#sql("findList")
  SELECT
    a.*,
    b.name AS productTypeName,
    c.name AS productBrandName 
  FROM
    product a
  LEFT JOIN
    product_type b
  ON
    a.product_type = b.id
  LEFT JOIN
  	brand c
  ON
  	a.product_brand = c.id
  WHERE
  	a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
  
  #if(sn!=null)
  	AND  a.sn like #para(sn)
  #end
  
  #if(isMarketable!=null)
  	AND  a.is_marketable = #para(isMarketable)
  #end
  
  #if(productType!=null)
  	AND  a.product_type like #para(productType)
  #end
  
  #if(productBrand!=null)
  	AND  a.product_brand like #para(productBrand)
  #end
  
  #if(isTop!=null)
  	AND  a.is_top = #para(isTop)
  #end
  
  #if(isShow!=null)
  	AND  a.is_show = #para(isShow)
  #end
  
  #if(productBrand!=null)
  	AND  a.product_brand like #para(productBrand)
  #end
  
  #if(code!=null)
  	AND  a.code like #para(code)
  #end
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end

#sql("findLastCode")
SELECT a.code FROM product a ORDER BY a.code DESC
#end

