#sql("findList")
  SELECT
    a.*,
    b.name AS username
  FROM
    agent_address a
   LEFT JOIN
    agent b
  ON
    a.agent_id = b.id  
  WHERE
  a.del_flag = 0
   
  #if(agentId!=null)
  	AND  a.agent_id = #para(agentId)
  #end
   
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end