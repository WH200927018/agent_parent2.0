#sql("findList")
  SELECT
    a.*,
    b.name AS agentTypeName
  FROM
    agent_price a
  LEFT JOIN
    agent_type b
  ON
    a.type_id = b.id  
  WHERE
  a.del_flag = 0
  
  #if(agentPriceId!=null)
  	AND  a.id = #para(agentPriceId)
  #end
  #if(agentTypeId!=null)
  	AND  a.type_id = #para(agentTypeId)
  #end
  #if(skuId!=null)
  	AND  a.sku_id = #para(skuId)
  #end
	ORDER BY a.create_date desc
#end