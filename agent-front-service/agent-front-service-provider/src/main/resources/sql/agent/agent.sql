#sql("findList")
  SELECT
    a.*
  FROM
    agent a
  LEFT JOIN
    agent_brand d
  ON
    d.agent_id = a.id
  WHERE
  a.del_flag = 0
  AND 
  d.del_flag = 0
  #if(username!=null)
  	AND  a.username = #para(username)
  #end
  
  #if(agentType!=null)
  	AND  d.agent_type = #para(agentType)
  #end
  
  #if(auditStatus!=null)
  	AND  d.audit_status = #para(auditStatus)
  #end
  
  #if(parentId!=null)
  	AND  d.parent_id = #para(parentId)
  #end
  
  #if(name!=null)
  	AND  a.name = #para(name)
  #end
  
  #if(code!=null)
  	AND  d.code = #para(code)
  #end
  
  #if(weixin!=null)
  	AND  a.weixin = #para(weixin)
  #end
  
  #if(mobile!=null)
  	AND  a.mobile = #para(mobile)
  #end
  
  #if(idNo!=null)
  	AND  a.id_no = #para(idNo)
  #end
  
  #if(openId!=null)
  	AND  a.open_id = #para(openId)
  #end
  
   #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  d.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end


#sql("findAgent")
	SELECT 
		a.*,
		b.name AS agentTypeName,
		c.name AS brandName
	FROM 
		agent a
	LEFT JOIN
		agent_type b
	ON
		a.agent_type = b.id
	LEFT JOIN
		brand c
	ON
		b.brand = c.id
	LEFT JOIN
    agent_brand d
    ON
    d.agent = a.id	
	WHERE
		a.del_flag = 0
	AND 
		(a.code = #para(param_name) 
	or 
		a.weixin = #para(param_name)
	or 
		a.mobile = #para(param_name)
	or 
		a.name = #para(param_name))
	AND
    d.auth_status=1
   #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  d.brand = #para(brandId)
  #end	
	order by 
		a.create_date DESC
#end

#sql("findAgentList")
	SELECT 
		a.*
	FROM 
		agent a	
	WHERE
		a.del_flag = 0
	 #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
	order by 
		a.create_date DESC
#end
