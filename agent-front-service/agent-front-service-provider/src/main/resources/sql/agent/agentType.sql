#sql("findList")
  SELECT
    a.*
  FROM
    agent_type a
  WHERE
  a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
  #if(sort!=null)
  	AND  a.sort >= #para(sort)
  #end
  #if(grade!=null)
  	AND  a.grade >= #para(grade)
  #end
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.grade asc
#end