#sql("findList")
  SELECT
    a.*,
    b.username AS username
  FROM
    agent_login_log a
  LEFT JOIN
    agent b
  ON
    a.agent = b.id
  WHERE
  a.del_flag = 0
  
  #if(agent!=null)
  	AND  a.agent like #para(agent)
  #end
	ORDER BY a.create_date desc
#end