#sql("findList")
  SELECT
    a.*,
    (SELECT d.audit_status FROM agent_bond d WHERE a.agent_id=d.agent_id AND d.del_flag = 0) AS bondStatus,
    b.name AS agentName,
    b.userName AS agentUserName,
    b.mobile AS agentMobile
  FROM
    agent_auth a
  LEFT JOIN
    agent b
  ON
    a.agent_id = b.id
  WHERE
  a.del_flag = 0
  
  #if(id!=null)
  	AND  a.id = #para(id)
  #end
  
  #if(agentId!=null)
  	AND  a.agent_id = #para(agentId)
  #end
  
  #if(authStatus!=null)
  	AND  a.audit_status = #para(authStatus)
  #end
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end

#sql("findListUpToDate")
	SELECT 
		* 
	FROM 
		(SELECT * FROM agent_auth ORDER BY create_date DESC) a 
	WHERE 
		a.del_flag = 0
	#if(agentId!=null)
  		AND a.agent_id = #para(agentId)
  	#end
  	
  	#if(auditStatus!=null)
	  	AND  a.audit_status = #para(auditStatus)
	#end
    
    #if(brandId!=null)
  	    AND  a.brand_id = #para(brandId)
    #end
	GROUP BY 
		a.store_type 
		
#end

