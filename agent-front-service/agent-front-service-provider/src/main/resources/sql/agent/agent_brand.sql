#sql("findList")
  SELECT
    a.*,
    b.name AS agentName,
    b.id AS agentId,
    b.username AS agentUserName,
    b.address AS agentAddress,
    b.weixin AS agentWeixin,
    b.mobile AS agentMobile,
    d.name AS agentTypeName,
    d.first_recharge AS firstRecharge,
    d.bail AS bail,
    (SELECT c.name FROM agent c WHERE c.id=a.parent_id) AS parentName
  FROM
    agent_brand a
  LEFT JOIN
    agent b
  ON
    a.agent_id = b.id
  LEFT JOIN
  	agent_type d
  ON
  	a.agent_type = d.id
  WHERE
  a.del_flag = 0
  
  #if(agentId!=null)
  	AND  a.agent_id = #para(agentId)
  #end
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
  #if(agentType!=null)
  	AND  a.agent_type = #para(agentType)
  #end
   #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
	ORDER BY a.create_date desc
#end

#sql("findChildList")
  SELECT
    a.agent AS agentId,  
    b.name AS agentName,
    b.weixin     AS agentWeixin,
    b.address    AS agentAddress,
    b.mobile    AS agentMobile,
    c.name AS brandName
  FROM
    agent_brand a
  LEFT JOIN
    agent b
  ON
    a.agent = b.id
  LEFT JOIN
  	brand c
  ON
  	a.brand = c.id
  WHERE
  a.del_flag = 0
  AND 
  b.auth_status = 0
   #if(agent!=null)
  	AND  b.parent_id = #para(agent)
  #end 
   #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
	ORDER BY a.create_date desc
#end

#sql("findMaxCode")
	SELECT
		max(a.code)
	FROM
		agent_brand a
	WHERE
	1=1
	#if(agentType!=null)
	AND	a.agent_type = #para(agentType)
	#end	
	#if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
    #end
#end

#sql("findByAgentId")
	SELECT
		a.*,
		b.name AS agentTypeName
	FROM
		agent_brand a
	LEFT JOIN
        agent_type b
    ON
    a.agent_type = b.id
	WHERE
	a.auth_status=1
	AND 
	a.del_flag = 0
	#if(agentId!=null)
  	AND  a.agent_id = #para(agentId)
    #end
#end