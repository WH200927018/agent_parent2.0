#sql("findList")
  SELECT
    a.*,
    b.name AS typeName
  FROM
   agent_rebate_config_params a
  LEFT JOIN 
    agent_type b
  ON 
    a.config_id=b.id
  WHERE
  a.del_flag = 0
  
  #if(configId!=null)
  	AND  a.config_id = #para(configId)
  #end
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date asc
#end