#sql("findList")
  SELECT
    a.*,
    b.name AS agentName,
    b.code AS agentCode,
    b.username AS agentUserName,
    c.name AS brandName,
    d.name AS agentTypeName
  FROM
    agent_invite a
  LEFT JOIN
    agent b
  ON
    a.agent = b.id
  LEFT JOIN
  	brand c
  ON
  	a.brand = c.id
  LEFT JOIN
  	agent_type d
  ON
  	a.agent_type = d.id
  WHERE
  a.del_flag = 0
  
  #if(agent!=null)
  	AND  a.agent = #para(agent)
  #end
  #if(brand!=null)
  	AND  a.brand = #para(brand)
  #end
  #if(agentType!=null)
  	AND  a.agent_type = #para(agentType)
  #end
  
  #if(level!=null)
  		AND a.level = #para(level)
  	#end
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end	
	ORDER BY a.create_date desc
#end

#sql("getEntity")
	SELECT 
		a.*
	FROM 
		agent_invite a
 	WHERE
  		a.del_flag = 0
  	#if(agent!=null)	
		AND a.agent = #para(agent)
	#end
	
 	#if(brand!=null)
  		AND  a.brand = #para(brand)
  	#end
  	
  	#if(agentType!=null)
  		AND  a.agent_type = #para(agentType)
  	#end
  	
  	#if(level!=null)
  		AND a.level = #para(level)
  	#end
  	
	ORDER BY a.create_date desc
#end

