#sql("findList")
  SELECT
    a.*,
    b.name AS agentName
  FROM
    agent_bond a
  LEFT JOIN
    agent b
  ON
    a.agent_id = b.id
  WHERE
  a.del_flag = 0
  
  #if(agent!=null)
  	AND  a.agent_id = #para(agent)
  #end
  #if(status!=null)
  	AND  a.audit_status = #para(status)
  #end
    
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end