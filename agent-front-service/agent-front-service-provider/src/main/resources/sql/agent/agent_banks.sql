#sql("findList")
  SELECT
    a.*,
    b.name AS username
  FROM
    agent_banks a
  LEFT JOIN
    agent b
  ON
    a.agent = b.id  
  WHERE
  a.del_flag = 0
  
  #if(agentBanksId!=null)
  	AND  a.id = #para(agentBanksId)
  #end
  
  #if(type!=null)
  	AND  a.type = #para(type)
  #end
  
  #if(agent!=null)
  	AND  a.agent = #para(agent)
  #end
  
  #if(isCompany!=null)
  	AND  a.is_company = #para(isCompany)
  #end
    
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end