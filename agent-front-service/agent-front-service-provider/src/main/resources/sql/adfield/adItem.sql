#sql("findList")
  SELECT
    a.*,
    b.name AS itemName
  FROM
    ad_item a
  LEFT JOIN
    ad_field b
  ON
    a.ad_field = b.id
  WHERE
  a.del_flag = 0
  #if(name!=null)
  	AND  a.name like #para(name)
  #end
    #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
  	ORDER BY a.create_date desc
#end

