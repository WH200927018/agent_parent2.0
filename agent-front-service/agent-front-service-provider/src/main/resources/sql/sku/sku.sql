#sql("findList")
  SELECT
    a.*   
  FROM
    sku a
  WHERE
  a.del_flag = 0
  
  #if(name!=null)
  	AND  a.name LIKE #para(name)
  #end
  #if(productId!=null)
  	AND  a.product_id = #para(productId)
  #end
  #if(sn!=null)
  	AND  a.sn LIKE #para(sn)
  #end
  #if(skuId!=null)
  	AND  a.id = #para(skuId)
  #end
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end

#sql("findByAgent")
  SELECT
    a.*,
    (SELECT b.agency_price FROM  agent_price b WHERE a.id=b.sku_id AND b.type_id=(SELECT e.agent_type FROM agent_brand e WHERE e.agent_id =#para(agentId) AND e.brand_id=a.brand_id AND e.del_flag=0) )AS skuPrice,
    (SELECT d.unit FROM product d WHERE d.id=a.product_id )  AS skuUnit
  FROM
    sku a
  WHERE
  a.del_flag = 0
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
  
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end