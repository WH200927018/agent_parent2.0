#sql("findList")
  SELECT
    *
  FROM
    sku_spec_item a
  WHERE
  a.del_flag = 0
  
  #if(skuId!=null)
  	AND  a.sku_id = #para(skuId)
  #end
  #if(specItemId!=null)
  	AND  a.spec_item_id = #para(specItemId)
  #end
  #if(specId!=null)
  	AND  a.spec_id = #para(specId)
  #end
  #if(skuSpecItemId!=null)
  	AND  a.id = #para(skuSpecItemId)
  #end
  
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end

#sql("findSkuList")
  SELECT
    a.*,
    b.name AS skuName,
    b.price AS skuPrice,
    b.image AS skuImage,
    b.price AS skuPrice,
    c.agency_price AS agentPrice,
    c.type_id AS agentType
  FROM
    sku_spec_item a
  LEFT JOIN 
  	sku b 
  on 
  	a.sku_id = b.id 
  LEFT JOIN 
  	agent_price c 
  on 
  	b.id = c.sku_id
  WHERE
  a.del_flag = 0
  
  #if(skuId!=null)
  	AND  a.sku_id = #para(skuId)
  #end
  #if(specItemId!=null)
  	AND  a.spec_item_id = #para(specItemId)
  #end
  #if(specId!=null)
  	AND  a.spec_id = #para(specId)
  #end
  #if(skuSpecItemId!=null)
  	AND  a.id = #para(skuSpecItemId)
  #end
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
	ORDER BY a.create_date desc
#end


