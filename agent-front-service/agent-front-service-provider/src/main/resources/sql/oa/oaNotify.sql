#sql("findList")
  SELECT
    a.*,
    (SELECT COUNT(b.id) FROM oa_notify_record b WHERE b.oa_notify_id =a.id AND b.read_flag=1) AS notifyRecord,
    (SELECT COUNT(b.id) FROM oa_notify_record b WHERE b.oa_notify_id =a.id ) AS notifyTotal
  FROM
    oa_notify a
  WHERE
  a.del_flag = 0
  
  #if(title!=null)
  	AND  a.title LIKE #para(title)
  #end
  #if(type!=null)
  	AND  a.type = #para(type)
  #end
  #if(status!=null)
  	AND  a.status = #para(status)
  #end
  #if(oaNotifyId!=null)
  	AND  a.id = #para(oaNotifyId)
  #end
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end
    
  #if(brandId!=null)
  	AND  a.brand_id = #para(brandId)
  #end
  ORDER BY a.create_date desc
#end