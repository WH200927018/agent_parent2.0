package com.tipu.agent.front.service.provider;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.HashKit;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.ICallback;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentRebateConfigParamsService;
import com.tipu.agent.front.service.api.AgentRewardService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.SettingValService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentExtend;
import com.tipu.agent.front.service.entity.model.AgentRebateConfigParams;
import com.tipu.agent.front.service.entity.model.AgentReward;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.Sku;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jboot.utils.StringUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Bean
@Singleton
@JbootrpcService
public class AgentServiceImpl extends JbootServiceBase<Agent> implements AgentService {

	@Override
	public Page<Agent> findPage(Agent data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", data.getName());
		}
		if (StrKit.notBlank(data.getWeixin())) {
			cond.set("weixin", data.getWeixin());
		}
		if (StrKit.notBlank(data.getMobile())) {
			cond.set("mobile", data.getMobile());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getStr("brandId"))) {
			cond.set("brandId", data.get("brandId"));
		}
		if (StrKit.notBlank(data.getStr("auditStatus"))) {
			cond.set("auditStatus", data.get("auditStatus"));
		}
		if (StrKit.notBlank(data.getStr("parentId"))) {
			cond.set("parentId", data.get("parentId"));
		}
		if (StrKit.notBlank(data.getStr("agentType"))) {
			cond.set("agentType", data.get("agentType"));
		}
		SqlPara sp = Db.getSqlPara("agent.findList", cond);
		Page<Agent> list = DAO.paginate(pageNumber, pageSize, sp);
		return list;
	}

	@Cacheable(name = CacheKey.CACHE_AGENT)
	@Override
	public List<Agent> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Agent> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<Agent> findAll(String companyId, String brandId) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(companyId)) {
			cond.set("companyId", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			cond.set("brandId", brandId);
		}
		SqlPara sp = Db.getSqlPara("agent.findAgentList", cond);
		List<Agent> list = DAO.find(sp);
		return list;
	}

	@Override
	public List<Agent> findList(Agent agent) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(agent.getUsername())) {
			cond.set("username", agent.getUsername());
		}
		if (StrKit.notBlank(agent.getName())) {
			cond.set("name", agent.getName());
		}
		if (StrKit.notBlank(agent.getWeixin())) {
			cond.set("weixin", agent.getWeixin());
		}
		if (StrKit.notBlank(agent.getMobile())) {
			cond.set("mobile", agent.getMobile());
		}
		if (StrKit.notBlank(agent.getIdNo())) {
			cond.set("idNo", agent.getIdNo());
		}
		if (StrKit.notBlank(agent.getOpenId())) {
			cond.set("openId", agent.getOpenId());
		}
		if (StrKit.notBlank(agent.getCompanyId())) {
			cond.set("companyId", agent.getCompanyId());
		}
		if (StrKit.notBlank(agent.getStr("brandId"))) {
			cond.set("brandId", agent.get("brandId"));
		}
		SqlPara sp = Db.getSqlPara("agent.findList", cond);
		List<Agent> list = DAO.find(sp);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENT);
	}

	@Override
	public int updateAgentDeposit(Agent agent, int type) {
		boolean ok = false;
		if (0 == type) {// 增加余额
			final String sql = "UPDATE agent SET agent_deposit = agent_deposit+" + agent.getAgentDeposit()
					+ " WHERE (agent_deposit+" + agent.getAgentDeposit() + ")>=0 AND id = '" + agent.getId() + "'";
			ok = Db.tx(new IAtom() {
				@Override
				public boolean run() throws SQLException {
					if (Db.update(sql) > 0) {
						return true;
					} else {
						return false;
					}
				}
			});
		} else {// 减少余额
			final String sql = "UPDATE agent SET agent_deposit = agent_deposit-" + agent.getAgentDeposit()
					+ " WHERE (agent_deposit-" + agent.getAgentDeposit() + ")>=0 AND id = '" + agent.getId() + "'";
			ok = Db.tx(new IAtom() {
				@Override
				public boolean run() throws SQLException {
					if (Db.update(sql) > 0) {
						return true;
					} else {
						return false;
					}
				}
			});
		}

		return ok == true ? 1 : -1;

	}

	@Override
	public int updateAgentReward(Agent agent, int type) {
		String sql = "UPDATE agent SET agent_reward = ";
		if (0 == type) {// 增加奖励金
			sql += "agent_reward+" + agent.getAgentReward() + " WHERE (agent_reward+" + agent.getAgentReward()
					+ ")>=0 AND id = '" + agent.getId() + "'";
		} else {// 减少奖励金
			sql += "agent_reward-" + agent.getAgentReward() + " WHERE (agent_reward-" + agent.getAgentReward()
					+ ")>=0 AND id = '" + agent.getId() + "'";
		}
		return Db.update(sql);
	}

	/**
	 * 扣款，更新成功返回对象,否则为NULL
	 */
	@Override
	public Agent subDeposit(String id, BigDecimal money) {
		int number = Db.update("update agent a  " + "set a.agent_deposit = a.agent_deposit - ?  "
				+ "where ((a.agent_deposit - ?) > -1) and id = ?", money, money, id);
		if (number > 0) {
			return DAO.findById(id);
		} else {
			return null;
		}
	}

	/**
	 * 加奖金,更新成功返回对象,否则为NULL
	 */
	@Override
	public Agent addReward(final String id, final BigDecimal money) {
		int number = Db.update("update agent a  " + "set a.agent_reward = a.agent_reward + ?  " + "where id = ?", money,
				id);
		if (number > 0) {
			return DAO.findById(id);
		} else {
			return null;
		}

	}

	/**
	 * 扣款 更新成功返回对象,否则为NULL
	 * 
	 * @param id
	 * @param money
	 * @return
	 */
	@Override
	public Agent addDeposit(final String id, final BigDecimal money) {
		int number = Db.update("update agent a  " + "set a.agent_deposit = a.agent_deposit + ?  " + "where id = ?",
				money, id);
		if (number > 0) {
			return DAO.findById(id);
		} else {
			return null;
		}
	}

	/**
	 * 更新奖金
	 */
	@Override
	public Agent subReward(String id, BigDecimal money) {
		int number = Db.update("update agent a  " + "set a.agent_reward = a.agent_reward - ?  "
				+ "where ((a.agent_reward - ?) > -1) and id = ?", money, money, id);
		if (number > 0) {
			return DAO.findById(id);
		} else {
			return null;
		}
	}

	/**
	 * nma系统中的修改密码 HashKit.sha256("");
	 */
	@Override
	public Ret updateAgencyPassword(Agent agent, String oldPassword, String newPassword, String renewPassWord) {
		if (StrKit.isBlank(oldPassword)) {
			return Ret.fail("msg", "原密码不能为空");
		}
		if (StrKit.isBlank(newPassword)) {
			return Ret.fail("msg", "新密码不能为空");
		}
		if (StrKit.isBlank(renewPassWord)) {
			return Ret.fail("msg", "确认新密码不能为空");
		}
		if (newPassword.length() < 6) {
			return Ret.fail("msg", "新密码长度不能小于 6");
		}

		String password = agent.getPassword();// 从数据库里取出原来的密码
		String newPasswordEncrypt = HashKit.sha256(oldPassword); // 将用户输入的原来的密码加密后和 数据库里的密码进行对比
		if (!newPasswordEncrypt.equals(password)) {
			return Ret.fail("msg", "原密码不正确，请重新输入");
		}
		if (!renewPassWord.equals(newPassword)) {
			return Ret.fail("msg", "新密码与确认新密码不一致");
		}
		renewPassWord = HashKit.sha256(renewPassWord);
		int result = Db.update("update agent set password = ?  where id = ? and del_flag = 0 ", renewPassWord,
				agent.getId());
		if (result > 0) {
			return Ret.ok("msg", "密码更新成功");
		} else {
			return Ret.fail("msg", "未找到账号，请联系管理员");
		}
	}

	@Override
	public int updateFrontAgentReward(Agent agent, int type) {
		String sql = "UPDATE agent SET agent_reward = ";
		if (0 == type) {// 增加奖励金
			sql += "agent_reward+" + agent.getAgentReward() + " WHERE (agent_reward+" + agent.getAgentReward()
					+ ")>=0 AND id = '" + agent.getId() + "'";
		} else {// 减少奖励金,可以为负数
			sql += "agent_reward-" + agent.getAgentReward() + " WHERE  id = '" + agent.getId() + "'";
		}
		return Db.update(sql);
	}

	@Override
	public List<Record> findRecord(String sql) {
		List<Record> list = Db.find(sql);
		return list;
	}

	@Override
	public Agent findByPhone(String phone, String companyId) {
		return DAO.findFirst("select * from Agent where mobile = ? and company_id = ?", phone, companyId);
	}

	public boolean isApply(String companyId, String brandId, String phone) {
		Agent agent = DAO.findFirst(" select * from agent a join agent_brand b\r\n" + " on a.id = b.agent_id \r\n"
				+ " where a.company_id =  ? and b.brand_id = ? and a.mobile = ? ", companyId, brandId, phone);
		return agent != null ? true : false;
	}

	public Agent applyAgent(final Agent agent, final String companyId, final String brandId, final String agentTypeId,
			final String parentId) {
		String id = StringUtils.uuid();
		agent.setUsername(agent.getMobile());
		agent.setIsFristAuth("0");
		agent.setDelFlag("0");
		agent.setFrozen("0");
		agent.setCompanyId(companyId);
		agent.setId(id);
		boolean execStatus = Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				// TODO Auto-generated method stub
				if (agent.save()) {
					AgentExtend agentExtend = new AgentExtend();
					agentExtend.setAgentId(agent.getId());
					agentExtend.setCreateBy(agent.getName());
					agentExtend.setCreateDate(new Date());
					agentExtend.setUpdateBy(agent.getName());
					agentExtend.setUpdateDate(new Date());
					agentExtend.setDelFlag("0");
					agentExtend.setId(StringUtils.uuid());
					if (agentExtend.save()) {
						AgentBrand agentBrand = new AgentBrand();
						agentBrand.setId(StringUtils.uuid());
						agentBrand.setAgentId(agent.getId());
						agentBrand.setAuthStatus("0");
						agentBrand.setAgentType(agentTypeId);
						agentBrand.setCreateBy(agent.getName());
						agentBrand.setBrandId(brandId);
						agentBrand.setAuditRootStatus("0");
						agentBrand.setAuditStatus("0");
						agentBrand.setParentId(parentId);
						agentBrand.setAuditAgentId(parentId);
						agentBrand.setCreateDate(new Date());
						agentBrand.setUpdateBy(agent.getName());
						agentBrand.setUpdateDate(new Date());
						agentBrand.setDelFlag("0");
						if (agentBrand.save()) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
		});
		if (execStatus) {
			return agent;
		} else {
			return null;
		}
	}

	@Override
	public List<Agent> findAgentList(Agent agent) {
		return DAO.find("select * from Agent where id !=? AND username = ? and company_id = ?", agent.getId(),
				agent.getUsername(), agent.getCompanyId());
	}

	@Override
	public Agent findAgent(String searchName) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(searchName)) {
			columns.eq("username", searchName);
		}
		columns.eq("del_flag", "0");
		Agent agent = DAO.findFirstByColumns(columns);
		return agent;
	}

	public Page<Agent> findAgentsByStatus(int pageNumber, int pageSize, String brandId, String agentId,
			String auditStatus) {
		return DAO.paginate(pageNumber, pageSize, "select * ", " from agent a join agent_brand b  on a.id = b.agent_id "
				+ " where a.is_frist_auth = ? and b.brand_id = ? and b.audit_status = ? and a.del_flag = ? and b.audit_agent_id = ?",
				"0", brandId, auditStatus, "0", agentId);
	}

	public Page<Agent> findRelativesByType(int pageNumber, int pageSize, String agentId, String type, String brandId) {
		if ("1".equals(type)) {
			return DAO.paginate(pageNumber, pageSize, "select a.*, c.name as typeName ",
					" from agent a join agent_brand b  on a.id = b.agent_id  left join agent_type c on b.agent_type = c.id"
							+ " where b.parent_id = ? and  b.brand_id = ? ",
					agentId, brandId);
		} else {
			return DAO.paginate(pageNumber, pageSize, "select a.*, c.name as typeName  ",
					" from agent a join agent_brand b  on a.id = b.agent_id left join agent_type c on b.agent_type = c.id"
							+ " where a.id = ? and b.brand_id = ?",
					agentId, brandId);
		}
	}

	@Override
	public int reduceAgentDepositAndStock(Agent agent, List<Sku> skuList, String productType) {
		boolean ok = false;
		final String sql = "UPDATE agent SET agent_deposit = agent_deposit-" + agent.getAgentDeposit()
				+ " WHERE (agent_deposit-" + agent.getAgentDeposit() + ")>=0 AND id = '" + agent.getId() + "'";
		if (productType.equals("1")) {// 减少实体库存

			ok = Db.tx(new IAtom() {
				@Override
				public boolean run() throws SQLException {
					for (Sku sku : skuList) {
						final String stockSql = "UPDATE sku SET stock = stock-" + sku.getProductNumer()
								+ " WHERE (stock-" + sku.getProductNumer() + ")>=0 AND id = '" + sku.getId() + "'";

						if (Db.update(stockSql) <= 0) {
							return false;
						}
					}
					int i = Db.update(sql);
					if (i <= 0) {
						return false;
					}
					return true;

				}
			});

		} else {// 减少云库存

			ok = Db.tx(new IAtom() {
				@Override
				public boolean run() throws SQLException {
					for (Sku sku : skuList) {
						final String stockSql = "UPDATE sku SET virtual_stock = virtual_stock-" + sku.getProductNumer()
								+ " WHERE (virtual_stock-" + sku.getProductNumer() + ")>=0 AND id = '" + sku.getId()
								+ "'";
						if (Db.update(stockSql) <= 0) {
							return false;
						}
					}
					if (Db.update(sql) <= 0) {
						return false;
					}
					return true;
				}

			});

		}
		return ok == true ? 1 : -1;
	}

	@Override
	public Agent findByPhoneOrWx(String data, String companyId) {
		return DAO.findFirst("select * from Agent where (mobile = ? OR weixin=?) and company_id = ?", data, data,
				companyId);
	}

	/**
	 * 同级返利
	 */
	@Override
	public int updateAgentReward(Agent agent, AgentBrand agentBrand, Orders order, AgentType agentType,
			List<AgentBrand> agentBrandList) {
		boolean ok = false;
		AgentRebateConfigParamsService configParamsService = Jboot.service(AgentRebateConfigParamsService.class);
		AgentRewardService agentRewardService = Jboot.service(AgentRewardService.class);
		AgentService agentService = Jboot.service(AgentService.class);
		ok = Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				AgentRebateConfigParams config = new AgentRebateConfigParams();
				config.setCompanyId(agent.getCompanyId());
				config.setBrandId(agentBrand.getBrandId());
				config.setConfigId(agentType.getId());
				config = configParamsService.findByConfigId(config);
				if (config != null) {
					String params=config.getConfigVal().replaceAll("<p>", "").replaceAll("</p>", "");
					JSONArray jsonObject = JSONArray.fromObject(params);
					BigDecimal lr = new BigDecimal(0);
					if (agentBrandList.size() > jsonObject.size()) {// 上级层数大于
						for (int i = 0; i < jsonObject.size(); i++) {
							String parentId = agentBrandList.get(i).getAgentId();
							if (!parentId.equals("0")) {// 不是总公司账号
								JSONObject param = JSONObject.fromObject(jsonObject.get(i));
								lr = order.getBuySum().multiply(new BigDecimal(String.valueOf(param.get("val"))));
								AgentReward wsAgencyReward = new AgentReward();
								wsAgencyReward.setId(StrKit.getRandomUUID());
								wsAgencyReward.setCreateDate(new Date());
								wsAgencyReward.setDelFlag("0");
								wsAgencyReward.setRewardType("2"); // 奖励类型（4.奖励扣款 （订单取消奖励没有）1.个人代理差价 2.同级8%提成 3 团队季度奖)
								wsAgencyReward.setOrderId(order.getId());
								wsAgencyReward.setAgentId(parentId);
								wsAgencyReward.setBrandId(agentBrand.getBrandId());
								wsAgencyReward.setRemarks("");
								wsAgencyReward.setRewardMoney(lr);
								if (!agentRewardService.save(wsAgencyReward)) {
									return false;
								}
								Agent ag = agentService.findById(parentId);
								BigDecimal agentReward = ag.getAgentReward().add(lr);
								ag.setAgentReward(agentReward);
								if (!agentService.update(ag)) {
									return false;
								}
							}
						}
					} else {
						for (int i = 0; i < agentBrandList.size(); i++) {
							String parentId = agentBrandList.get(i).getAgentId();
							if (!parentId.equals("0")) {// 不是总公司账号
								JSONObject param = JSONObject.fromObject(jsonObject.get(i));
								lr = order.getBuySum().multiply(new BigDecimal(String.valueOf(param.get("val"))));
								AgentReward wsAgencyReward = new AgentReward();
								wsAgencyReward.setId(StrKit.getRandomUUID());
								wsAgencyReward.setCreateDate(new Date());
								wsAgencyReward.setDelFlag("0");
								wsAgencyReward.setRewardType("2"); // 奖励类型（4.奖励扣款 （订单取消奖励没有）1.个人代理差价 2.同级8%提成 3 团队季度奖)
								wsAgencyReward.setOrderId(order.getId());
								wsAgencyReward.setAgentId(parentId);
								wsAgencyReward.setBrandId(agentBrand.getBrandId());
								wsAgencyReward.setRemarks("");
								wsAgencyReward.setRewardMoney(lr);
								if (!agentRewardService.save(wsAgencyReward)) {
									return false;
								}
								Agent ag = agentService.findById(parentId);
								BigDecimal agentReward = ag.getAgentReward().add(lr);
								ag.setAgentReward(agentReward);
								if (!agentService.update(ag)) {
									return false;
								}
							}
						}
					}
				} else {
					return false;
				}
				return true;

			}
		});
		return ok == true ? 1 : -1;

	}

}