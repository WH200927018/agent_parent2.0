package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.SkuService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.Sku;
import io.jboot.service.JbootServiceBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;
/**
 * 
 * @author wh
 *
 */
@Bean
@Singleton
@JbootrpcService
public class SkuServiceImpl extends JbootServiceBase<Sku> implements SkuService {

	@Override
	public Page<Sku> findPage(Sku data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getSn())) {
			cond.set("sn", "%" + data.getSn() + "%");
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("skuId", data.getId());
		}
		if (StrKit.notBlank(data.getProductId())) {
			cond.set("productId", data.getProductId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("sku.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<Sku> findList(Sku data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getSn())) {
			cond.set("sn", "%" + data.getSn() + "%");
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("skuId", data.getId());
		}
		if (StrKit.notBlank(data.getProductId())) {
			cond.set("productId", data.getProductId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		if (StrKit.notBlank(data.getProductType())) {
			cond.set("productTypeId", data.getProductType());
		}
		SqlPara sp = Db.getSqlPara("sku.findList", cond);
		List<Sku> SkuList = DAO.find(sp);
		return SkuList;
	}
	
	@Override
	public List<Sku> findByAgent(Agent agent,String brandId) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(agent.getCompanyId())) {
			cond.set("companyId", agent.getCompanyId());
		}
		if (StrKit.notBlank(agent.getId())) {
			cond.set("agentId", agent.getId());
		}
		if (StrKit.notBlank(brandId)) {
			cond.set("brandId", brandId);
		}
		SqlPara sp = Db.getSqlPara("sku.findByAgent", cond);
		List<Sku> SkuList = DAO.find(sp);
		return SkuList;
	}
	
	@Cacheable(name = CacheKey.CACHE_SKU)
	@Override
	public List<Sku> findAll() {
		Columns columns = Columns.create();		
		columns.eq("del_flag", "0");
		List<Sku> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<Sku> findAll(String companyId,String brandId) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<Sku> list = DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_SKU);
	}

	@Override
	public int updateStock(Integer stock, Integer stockType, String id) {
		String sql = "UPDATE sku SET stock = ";
		if (0 == stockType) {// 增加库存
			sql += "stock+" + stock + " WHERE (stock+" + stock + ")>=0 AND id = '" + id + "'";
		} else {// 减少库存
			sql += "stock-" + stock + " WHERE (stock-" + stock + ")>=0 AND id = '" + id + "'";
		}
		return Db.update(sql);
	}

	@Override
	public List<Sku> findListByProductIds(String productIds) {
		String sql = "SELECT * FROM sku WHERE del_flag= '0' ";
		if (StrKit.notBlank(productIds)) {
			String[] productId = productIds.split(",");
			if (productId.length > 0) {
				sql += " AND product_id IN (";
				for (int i = 0; i < productId.length; i++) {
					if (StrKit.notBlank(productId[i])) {
						if (i < productId.length - 1) {
							sql += productId[i] + ",";
						} else {
							sql += productId[i];
						}
					}
				}
				sql += " )";
			}
		}
		return DAO.find(sql);
	}

	/**
	 * 检查库存是否充足
	 */
	@Override
	public List<Sku> subStoreByList(final List<Sku> wsProducts,String productType) {
		final List<Sku> list = new ArrayList<Sku>();
		Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				for (int i = 0; i < wsProducts.size(); i++) {
					Sku p = wsProducts.get(i);
					int number =0;
					if(productType.equals("1")) {
						number = Db.update(
								"update sku p  " + "set p.stock = p.stock - ?  "
										+ " where ((p.stock - ?) > -1) and p.id = ?",
								p.getProductNumer(), p.getProductNumer(), p.getId());
					}else {
						number = Db.update(
								"update sku p  " + "set p.virtual_stock = p.virtual_stock - ?  "
										+ " where ((p.virtual_stock - ?) > -1) and p.id = ?",
								p.getProductNumer(), p.getProductNumer(), p.getId());
					}
					
					if (number == 0) {
						list.add(p);
					}
				}
				if (list.size() > 0) {
					return false;
				} else {
					return true;
				}
			}
		});
		return list;
	}

	@Override
	public int updateVirtuaStock(Integer stock, Integer stockType, String id) {
		String sql = "UPDATE sku SET virtual_stock = ";
		if (0 == stockType) {// 增加库存
			sql += "virtual_stock+" + stock + " WHERE (virtual_stock+" + stock + ")>=0 AND id = '" + id + "'";
		} else {// 减少库存
			sql += "virtual_stock-" + stock + " WHERE (virtual_stock-" + stock + ")>=0 AND id = '" + id + "'";
		}
		return Db.update(sql);
	}

}