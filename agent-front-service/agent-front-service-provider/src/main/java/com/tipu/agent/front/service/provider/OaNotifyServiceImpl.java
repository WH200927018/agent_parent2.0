package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.OaNotifyService;
import com.tipu.agent.front.service.entity.model.AgentBond;
import com.tipu.agent.front.service.entity.model.OaNotify;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class OaNotifyServiceImpl extends JbootServiceBase<OaNotify> implements OaNotifyService {

	@Override
	public Page<OaNotify> findPage(OaNotify data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getTitle())) {
			cond.set("title", "%" + data.getTitle() + "%");
		}
		if (StrKit.notBlank(data.getType())) {
			cond.set("type", data.getType());
		}
		if (StrKit.notBlank(data.getStatus())) {
			cond.set("status", data.getStatus());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("oaNotify.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<OaNotify> findList(OaNotify data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getTitle())) {
			cond.set("title", "%" + data.getTitle() + "%");
		}
		if (StrKit.notBlank(data.getStatus())) {
			cond.set("status", data.getStatus());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("oaNotifyId", data.getId());
		}
		if (StrKit.notBlank(data.getType())) {
			cond.set("type", data.getType());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("oaNotify.findList", cond);
		List<OaNotify> oaNotifyList = DAO.find(sp);
		return oaNotifyList;
	}

	@Cacheable(name = CacheKey.CACHE_OANOTIFY)
	@Override
	public List<OaNotify> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<OaNotify> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_OANOTIFY);
	}

}