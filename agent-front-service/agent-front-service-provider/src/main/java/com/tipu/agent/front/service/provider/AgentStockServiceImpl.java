package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.AgentStockService;
import com.tipu.agent.front.service.entity.model.AgentStock;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentStockServiceImpl extends JbootServiceBase<AgentStock> implements AgentStockService {

	@Override
	public Page<AgentStock> findPage(AgentStock data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public List<AgentStock> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentStock> list = DAO.findListByColumns(columns);
		return list;
	}

	/**
	 * 查询所有
	 * 
	 * @param data
	 * @return
	 */
	@Override
	public List<AgentStock> findList(AgentStock data) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			columns.eq("agent_id", data.getAgentId());
		}
		if (StrKit.notBlank(data.getStockType())) {
			columns.eq("stock_type", data.getStockType());
		}
		if (StrKit.notBlank(data.getStr("startDate"))) {
			columns.ge("create_date", data.getStr("startDate"));
		}
		if (StrKit.notBlank(data.getStr("endDate"))) {
			columns.le("create_date", data.getStr("endDate"));
		}
		if (StrKit.notBlank(data.getStr("now"))) {
			columns.like("create_date", "%"+data.getStr("now")+"%");
		}
		columns.eq("del_flag", "0");
		List<AgentStock> list = DAO.findListByColumns(columns);
		return list;
	}

}