package com.tipu.agent.front.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.ShoppingRecordService;
import com.tipu.agent.front.service.entity.model.ShoppingCart;
import com.tipu.agent.front.service.entity.model.ShoppingRecord;
import io.jboot.service.JbootServiceBase;

import java.util.List;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class ShoppingRecordServiceImpl extends JbootServiceBase<ShoppingRecord> implements ShoppingRecordService {

	@Override
	public Page<ShoppingRecord> findPage(ShoppingRecord data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getAgencyId())) {
			columns.eq("agency_id", data.getAgencyId());
		}
		if (StrKit.notBlank(data.getOrderId())) {
			columns.eq("order_id", data.getOrderId());
		}
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}

	@Override
	public List<ShoppingRecord> findList(ShoppingRecord data) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getAgencyId())) {
			columns.eq("agency_id", data.getAgencyId());
		}
		if (StrKit.notBlank(data.getOrderId())) {
			columns.eq("order_id", data.getOrderId());
		}
		if (StrKit.notBlank(data.getType())) {
			columns.eq("type", data.getType());
		}
		columns.eq("del_flag", "0");
		List<ShoppingRecord> list = DAO.findListByColumns(columns);
		return list;
	}

}