package com.tipu.agent.front.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.front.service.api.OrderProductService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.OrderProduct;
import io.jboot.service.JbootServiceBase;

import java.util.List;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class OrderProductServiceImpl extends JbootServiceBase<OrderProduct> implements OrderProductService {

	@Override
	public Page<OrderProduct> findPage(OrderProduct data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();

        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}

	@Override
	public List<OrderProduct> findList(OrderProduct orderProduct) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(orderProduct.getId())) {
			cond.set("id", orderProduct.getId());
        }
		if (StrKit.notBlank(orderProduct.getOrderId())) {
			cond.set("orderId", orderProduct.getOrderId());
		}
		SqlPara sp = Db.getSqlPara("orderProduct.findList",cond);
		List<OrderProduct> list = DAO.find(sp);
		return list;
	}
	
	@Override
	public List<OrderProduct> findOrderProductByOrderId(String id) {
		Kv cond = Kv.create();
		cond.set("orderId", id);
		SqlPara sp = Db.getSqlPara("orderProduct.findOrderProductByOrderId",cond);
		List<OrderProduct> list = DAO.find(sp);
		return list;
	}

}