package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.SettingService;
import com.tipu.agent.front.service.entity.model.Setting;
import io.jboot.service.JbootServiceBase;
import java.util.List;
import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class SettingServiceImpl extends JbootServiceBase<Setting> implements SettingService {

	@Override
	public Page<Setting> findPage(Setting data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();       
		if (StrKit.notBlank(data.getSysKey())) {
			columns.eq("sys_key", data.getSysKey());
		}
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		return DAO.paginateByColumns(pageNumber, pageSize, columns);
	}

	@Override
	public List<Setting> findList(Setting data) {
		Columns columns = Columns.create();       
		if (StrKit.notBlank(data.getSysKey())) {
			columns.eq("sys_key", data.getSysKey());
		}
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		List<Setting> settingList = DAO.findListByColumns(columns);
		return settingList;
	}
	
    @Override
    public Setting getSettingBySysKey(Setting data) {
        Columns columns = Columns.create();       
		if (StrKit.notBlank(data.getSysKey())) {
			columns.eq("sys_key", data.getSysKey());
		}
        Setting setting = DAO.findFirstByColumns(columns);
        return setting;
    }

	@Cacheable(name = CacheKey.SETTING)
	@Override
	public List<Setting> findAll() {
		Columns columns = Columns.create();
		List<Setting> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.SETTING);
	}

}
