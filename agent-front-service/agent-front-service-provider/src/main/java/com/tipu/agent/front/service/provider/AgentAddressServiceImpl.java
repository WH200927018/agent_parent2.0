package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentAddressService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAddress;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentAddressServiceImpl extends JbootServiceBase<AgentAddress> implements AgentAddressService {

	@Override
	public Page<AgentAddress> findPage(AgentAddress data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgentId())) {
			cond.set("agentId", data.getAgentId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentAddress.findList",cond);
		Page<AgentAddress> list = DAO.paginate(pageNumber, pageSize, sp);
        return list;
	}
	
	@Cacheable(name = CacheKey.CACHE_AGENTADDRESS)
	@Override
	public List<AgentAddress> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentAddress> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<AgentAddress> findAll(String companyId,String brandId){
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id",brandId);
		}
		columns.eq("del_flag", "0");
		List<AgentAddress> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<AgentAddress> findList(AgentAddress data){
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getAgentId())) {
            columns.eq("agent_id", data.getAgentId());
        }
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		columns.eq("del_flag", "0");
		List<AgentAddress> list = 	DAO.findListByColumns(columns,"is_default DESC");
		return list;
	}
		
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTADDRESS);
    }

}