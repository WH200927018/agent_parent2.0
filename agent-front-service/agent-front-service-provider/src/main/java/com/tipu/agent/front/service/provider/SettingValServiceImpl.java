package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.SettingValService;
import com.tipu.agent.front.service.entity.model.SettingVal;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class SettingValServiceImpl extends JbootServiceBase<SettingVal> implements SettingValService {

	@Override
	public Page<SettingVal> findPage(SettingVal data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();       
		if (StrKit.notBlank(data.getSettingId())) {
			columns.eq("setting_id", data.getSettingId());
		} 
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		} 
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		return DAO.paginateByColumns(pageNumber, pageSize, columns);
	}

	@Override
	public List<SettingVal> findList(SettingVal data) {
		Columns columns = Columns.create();       
		if (StrKit.notBlank(data.getSettingId())) {
			columns.eq("setting_id", data.getSettingId());
		} 
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		List<SettingVal> settingList = DAO.findListByColumns(columns);
		return settingList;
	}
	
    @Override
    public SettingVal getSettingBySysKey(SettingVal data) {
        Columns columns = Columns.create();       
		if (StrKit.notBlank(data.getSettingId())) {
			columns.eq("setting_id", data.getSettingId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		columns.eq("enable", "1");
        SettingVal setting = DAO.findFirstByColumns(columns);
        return setting;
    }

	@Override
	public List<SettingVal> findAll() {
		Columns columns = Columns.create();
		List<SettingVal> list = DAO.findListByColumns(columns);
		return list;
	}

}