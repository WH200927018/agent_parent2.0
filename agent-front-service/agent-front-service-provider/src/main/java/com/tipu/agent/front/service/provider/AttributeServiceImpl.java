package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AttributeService;
import com.tipu.agent.front.service.entity.model.Attribute;
import com.tipu.agent.front.service.entity.model.Attribute;

import io.jboot.service.JbootServiceBase;

import java.util.List;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AttributeServiceImpl extends JbootServiceBase<Attribute> implements AttributeService {

	@Override
	public Page<Attribute> findPage(Attribute data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("attributeId", data.getId());
		}
		SqlPara sp = Db.getSqlPara("attribute.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<Attribute> findList(Attribute data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("attributeId", data.getId());
		}
		SqlPara sp = Db.getSqlPara("attribute.findList", cond);
		List<Attribute> AttributeList = DAO.find(sp);
		return AttributeList;
	}

	@Cacheable(name = CacheKey.CACHE_ATTRIBUTE)
	@Override
	public List<Attribute> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Attribute> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_ATTRIBUTE);
	}

}