package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentType;
import io.jboot.service.JbootServiceBase;
import java.util.List;
import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AgentTypeServiceImpl extends JbootServiceBase<AgentType> implements AgentTypeService {

	@Override
	public Page<AgentType> findPage(AgentType data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
          cond.set("name", "%"+data.getName()+"%");
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentType.findList",cond);
		Page<AgentType> list = DAO.paginate(pageNumber, pageSize, sp);
        return list;
	}
	
	public List<AgentType> findByDelfalg(AgentBrand agentBrand) {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentType> list = DAO.findListByColumns(columns);
		return list;
	}
	
	@Cacheable(name = CacheKey.CACHE_AGENTTYPE)
	@Override
	public List<AgentType> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentType> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<AgentType> findAll(String companyId,String brandId){
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<AgentType> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTTYPE);
    }
	
	@Override
	public List<AgentType> findAllBySort(AgentType agentType) {
		Kv cond = Kv.create();
		if (agentType.getSort() !=null) {
          cond.set("sort", agentType.getSort());
		}
		if (StrKit.notBlank(agentType.getCompanyId())) {
			cond.set("companyId", agentType.getCompanyId());
		}
		if (StrKit.notBlank(agentType.getBrandId())) {
			cond.set("brandId", agentType.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentType.findList",cond);
		List<AgentType> list = DAO.find(sp);
		return list;
	}

	public  List<AgentType> findCurrentGradeList(String brandId,String companyId,int grade){
		return DAO.find("select * from agent_type where brand_id = ? and company_id = ? and del_flag=? and grade>= ?", brandId,companyId,"0",grade);
	}
	
	@Override
	public List<AgentType> findAllByGrade(AgentType agentType) {
		Kv cond = Kv.create();
		if (agentType.getGrade() !=null) {
          cond.set("grade", agentType.getGrade());
		}
		if (StrKit.notBlank(agentType.getCompanyId())) {
			cond.set("companyId", agentType.getCompanyId());
		}
		if (StrKit.notBlank(agentType.getBrandId())) {
			cond.set("brandId", agentType.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentType.findList",cond);
		List<AgentType> list = DAO.find(sp);
		return list;
	}

}