package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.StrKit;
import com.tipu.agent.front.service.api.OaNotifyRecordService;
import com.tipu.agent.front.service.entity.model.OaNotifyRecord;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class OaNotifyRecordServiceImpl extends JbootServiceBase<OaNotifyRecord> implements OaNotifyRecordService {
	
	@Override
	public List<OaNotifyRecord> findList(OaNotifyRecord data){
		Columns columns = Columns.create();
		if(StrKit.notBlank(data.getAgentId())){
			columns.eq("agent_id", data.getAgentId());
		}
		if(StrKit.notBlank(data.getOaNotifyId())){
			columns.eq("oa_notify_id", data.getOaNotifyId());
		}
		List<OaNotifyRecord> list =DAO.findListByColumns(columns);
		return list;
	}


}