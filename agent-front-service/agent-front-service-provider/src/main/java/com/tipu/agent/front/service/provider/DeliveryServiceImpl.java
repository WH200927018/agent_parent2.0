package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.DeliveryService;
import com.tipu.agent.front.service.entity.model.Delivery;
import com.tipu.agent.front.service.entity.model.DeliveryItem;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class DeliveryServiceImpl extends JbootServiceBase<Delivery> implements DeliveryService {

	@Override
	public Page<Delivery> findPage(Delivery data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgentSend())) {
          cond.set("agentSend", "%"+data.getAgentSend()+"%");
		}
		if (StrKit.notBlank(data.getAgentReceive())) {
	          cond.set("agentReceive", "%"+data.getAgentReceive()+"%");
		}
		if (StrKit.notBlank(data.getDeliveryNo())) {
	          cond.set("deliveryNo", data.getDeliveryNo());
		}
		if (StrKit.notBlank(data.getDeliveryStatus())) {
	          cond.set("deliveryStatus", data.getDeliveryStatus());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("delivery.findList",cond);
		Page<Delivery> list = DAO.paginate(pageNumber, pageSize, sp);
		return list; 
	}

	@Override
	public List<Delivery> findList(Delivery data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getDeliveryNo())) {
			cond.set("deliveryNo", data.getDeliveryNo());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("delivery.findList", cond);
		return DAO.find(sp);
	}
	
	@Cacheable(name = CacheKey.CACHE_DELIVERY)
	@Override
	public List<Delivery> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Delivery> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<Delivery> findAll(String companyId,String brandId){
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id",brandId);
		}
		columns.eq("del_flag", "0");
		List<Delivery> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_DELIVERY);
    }
	
	@Override
	public String getMaxNo(){
		SqlPara sp = Db.getSqlPara("delivery.getMaxNo");
		Delivery delivery = DAO.findFirst(sp);
		String maxNo = delivery.getStr("max(a.delivery_no)");
		return maxNo;
	}

	@Override
	public void batchInsertList(List<DeliveryItem> deliveryItemList) {
		Db.batchSave(deliveryItemList, deliveryItemList.size());
	}
	

	@Override
	public List<Delivery> findDelivery(Delivery data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getDeliveryNo())) {
			cond.set("deliveryNo", data.getDeliveryNo());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("delivery.findDelivery", cond);
		return DAO.find(sp);
	}
}