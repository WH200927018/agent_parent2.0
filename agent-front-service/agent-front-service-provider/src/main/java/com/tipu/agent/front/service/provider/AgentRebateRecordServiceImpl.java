package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.AgentRebateRecordService;
import com.tipu.agent.front.service.entity.model.AgentRebateRecord;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentRebateRecordServiceImpl extends JbootServiceBase<AgentRebateRecord> implements AgentRebateRecordService {

	@Override
	public Page<AgentRebateRecord> findPage(AgentRebateRecord data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public List<AgentRebateRecord> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentRebateRecord> list = DAO.findListByColumns(columns);
		return list;
	}

}