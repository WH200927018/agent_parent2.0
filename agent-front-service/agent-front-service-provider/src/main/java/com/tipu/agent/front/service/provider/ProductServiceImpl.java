package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.entity.model.Product;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class ProductServiceImpl extends JbootServiceBase<Product> implements ProductService {

	@Override
	public Page<Product> findPage(Product data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
          cond.set("name", "%"+data.getName()+"%");
		}
		if (StrKit.notBlank(data.getSn())) {
	          cond.set("sn", "%"+data.getSn()+"%");
		}
		if(data.getIsMarketable() != null){
			cond.set("isMarketable", data.getIsMarketable());
		}
		if (StrKit.notBlank(data.getProductType())) {
	          cond.set("productType", "%"+data.getProductType()+"%");
		}
		if (StrKit.notBlank(data.getProductBrand())) {
	          cond.set("productBrand", "%"+data.getProductBrand()+"%");
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("product.findList",cond);
		Page<Product> list = DAO.paginate(pageNumber, pageSize, sp);
		return list;
	}
	
	@Override
	public List<Product> findList(Product data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
          cond.set("name", "%"+data.getName()+"%");
		}
		if (StrKit.notBlank(data.getSn())) {
	          cond.set("sn", "%"+data.getSn()+"%");
		}
		if(data.getIsMarketable() != null){
			cond.set("isMarketable", data.getIsMarketable());
		}
		if (StrKit.notBlank(data.getProductType())) {
	          cond.set("productType", "%"+data.getProductType()+"%");
		}
		if (StrKit.notBlank(data.getProductBrand())) {
	          cond.set("productBrand", "%"+data.getProductBrand()+"%");
		}
		if (StrKit.notBlank(data.getIsTop())) {
	          cond.set("isTop", data.getIsTop());
		}
		if (StrKit.notBlank(data.getIsShow())) {
	          cond.set("isShow", data.getIsShow());
		}
		if (data.getCode() != null) {
	          cond.set("code", data.getCode());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("product.findList",cond);
		List<Product> list = DAO.find(sp);
		return list;
	}
	
	/**
	 * 查询商品最大条码
	 */
	@Override
	public Product findLastCode(){
		SqlPara sp = Db.getSqlPara("product.findLastCode");
		Product product = DAO.findFirst(sp);
		return product;
	}

	/**
	 * 根据编码查询产品
	 */
	@Override
	@Cacheable(name=Product.PRODUCT_CODE_CACHE, key="#(productCode)")
	public Product findByCode(@Named("productCode")String productCode) {
		Columns columns = Columns.create();
		columns.eq("code", productCode);
		return DAO.findFirstByColumns(columns);
	}
	
	@Cacheable(name = CacheKey.CACHE_PRODUCT)
	@Override
	public List<Product> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Product> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<Product> findAll(String companyId,String brandId){
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<Product> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_PRODUCT);
    }

	@Override
	public int updateStock(Integer stock, Integer stockType,String id) {
		String sql="UPDATE product SET stock = ";
		if(0==stockType){//增加库存
			sql+="stock+"+stock+" WHERE (stock+"+stock+")>=0 AND id = '"+id+"'";
		}else{//减少库存
			sql+="stock-"+stock+" WHERE (stock-"+stock+")>=0 AND id = '"+id+"'";
		}
		return Db.update(sql);
	}

}