package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentPriceService;
import com.tipu.agent.front.service.entity.model.AgentPrice;
import io.jboot.service.JbootServiceBase;
import java.util.List;
import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AgentPriceServiceImpl extends JbootServiceBase<AgentPrice> implements AgentPriceService {

	@Override
	public Page<AgentPrice> findPage(AgentPrice data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getId())) {
			cond.set("agentPriceId", data.getId());
		}
		if (StrKit.notBlank(data.getSkuId())) {
			cond.set("skuId", data.getSkuId());
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("agentTypeId", data.getTypeId());
		}
		SqlPara sp = Db.getSqlPara("agentPrice.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<AgentPrice> findList(AgentPrice data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getId())) {
			cond.set("agentPriceId", data.getId());
		}
		if (StrKit.notBlank(data.getSkuId())) {
			cond.set("skuId", data.getSkuId());
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("agentTypeId", data.getTypeId());
		}
		SqlPara sp = Db.getSqlPara("agentPrice.findList", cond);
		List<AgentPrice> AgentPriceList = DAO.find(sp);
		return AgentPriceList;
	}

	@Cacheable(name = CacheKey.CACHE_AGENTPRICE)
	@Override
	public List<AgentPrice> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentPrice> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTPRICE);
	}

	@Override
	public AgentPrice findBySkuAndAgentType(String skuId, String agentType) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(skuId)) {
			columns.eq("sku_id", skuId);
		}
		if (StrKit.notBlank(agentType)) {
			columns.eq("type_id", agentType);
		}
		columns.eq("del_flag", "0");
		AgentPrice agentPrice = DAO.findFirstByColumns(columns);
		return agentPrice;
	}

}