package com.tipu.agent.front.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.AgentExtendService;
import com.tipu.agent.front.service.entity.model.AgentExtend;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AgentExtendServiceImpl extends JbootServiceBase<AgentExtend> implements AgentExtendService {

	@Override
	public Page<AgentExtend> findPage(AgentExtend data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getAgentId())) {
			columns.eq("agent_id", data.getAgentId());
		}
        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}


	@Override
	public int updateByAgentId(AgentExtend data) {
	    return Db.update("update  agent_extend ex  " + 
				"set ex.buz_pic=?, " + 
				"ex.id_neg_pic=?, " + 
				"ex.id_pos_pic = ? " + 
				"where ex.agent_id = ? ", data.getBuzPic(),data.getIdNegPic(),data.getIdPosPic(),data.getAgentId());
	}
}