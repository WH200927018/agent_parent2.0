package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentBondService;
import com.tipu.agent.front.service.entity.model.AgentBond;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentBondServiceImpl extends JbootServiceBase<AgentBond> implements AgentBondService {

	@Override
	public Page<AgentBond> findPage(AgentBond data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgent())) {
	          cond.set("agent", data.getAgent());
		}	
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentBond.findList",cond);
        return DAO.paginate(pageNumber, pageSize, sp);
	}
	
	@Override
	public List<AgentBond> findList(AgentBond data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgent())) {
	          cond.set("agent", data.getAgent());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentBond.findList",cond);
		List<AgentBond> agentBondList = DAO.find(sp);
		return agentBondList;
	}

	/**
	 * 查询所有 （缓存）
	 */
	@Cacheable(name = CacheKey.CACHE_AGENTBOND)
	@Override
	public List<AgentBond> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentBond> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	/**
	 * 清除缓存
	 */
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTBOND);
    }

}