package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Column;
import io.jboot.db.model.Columns;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.SkuSpecItemService;
import com.tipu.agent.front.service.entity.model.Sku;
import com.tipu.agent.front.service.entity.model.SkuSpecItem;
import io.jboot.service.JbootServiceBase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class SkuSpecItemServiceImpl extends JbootServiceBase<SkuSpecItem> implements SkuSpecItemService {

	@Override
	public Page<SkuSpecItem> findPage(SkuSpecItem data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getSkuId())) {
			cond.set("skuId", data.getSkuId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("skuSpecItemId", data.getId());
		}
		if (StrKit.notBlank(data.getSpecId())) {
			cond.set("specId", data.getSpecId());
		}
		if (StrKit.notBlank(data.getSpecItemId())) {
			cond.set("specItemId", data.getSpecItemId());
		}
		SqlPara sp = Db.getSqlPara("skuSpecItem.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<SkuSpecItem> findList(SkuSpecItem data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getSkuId())) {
			cond.set("skuId", data.getSkuId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("skuSpecItemId", data.getId());
		}
		if (StrKit.notBlank(data.getSpecId())) {
			cond.set("specId", data.getSpecId());
		}
		if (StrKit.notBlank(data.getSpecItemId())) {
			cond.set("specItemId", data.getSpecItemId());
		}
		SqlPara sp = Db.getSqlPara("skuSpecItem.findList", cond);
		List<SkuSpecItem> list = DAO.find(sp);
		return list;
	}
	
	@Override
	public List<SkuSpecItem> findSkuList(SkuSpecItem data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getSkuId())) {
			cond.set("skuId", data.getSkuId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("skuSpecItemId", data.getId());
		}
		if (StrKit.notBlank(data.getSpecId())) {
			cond.set("specId", data.getSpecId());
		}
		if (StrKit.notBlank(data.getSpecItemId())) {
			cond.set("specItemId", data.getSpecItemId());
		}
		SqlPara sp = Db.getSqlPara("skuSpecItem.findSkuList", cond);
		List<SkuSpecItem> list = DAO.find(sp);
		return list;
	}
	
	@Override
	public List<SkuSpecItem> findSkuIds(String specItemIds){
		String sql = "SELECT * FROM sku_spec_item WHERE del_flag= '0' ";
        if (StrKit.notBlank(specItemIds)) {
            String[] specItemId = specItemIds.split(",");
            if (specItemId.length > 0) {
                sql += " AND spec_item_id IN (";
                for (int i = 0; i < specItemId.length; i++) {
                    if (StrKit.notBlank(specItemId[i])) {
                        if (i < specItemId.length - 1) {
                            sql += "\""+specItemId[i]+"\"" + ",";
                        } else {
                            sql += "\""+specItemId[i]+"\"";
                        }
                    }
                }
                sql += " )";
            }
        }
        return DAO.find(sql);
	}
	

	@Cacheable(name = CacheKey.CACHE_SKUSPECITEM)
	@Override
	public List<SkuSpecItem> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<SkuSpecItem> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_SKUSPECITEM);
	}

	@Override
	public List<SkuSpecItem> findListBySkuId(SkuSpecItem data) {
		List<SkuSpecItem> skuSpecItemList = new ArrayList<SkuSpecItem>();
		if (StrKit.notBlank(data.getSkuId())) {
			String sql = "SELECT * FROM sku_spec_item WHERE del_flag = 0 AND sku_id IN (";
			String[] skuIds = data.getSkuId().split(",");
			for (int i = 0; i < skuIds.length; i++) {
				if (StrKit.notBlank(skuIds[i])) {
					if (i < skuIds.length - 1) {
						sql += "'"+skuIds[i] + "',";
					} else {
						sql += "'"+skuIds[i]+"'";
					}
				}
			}
			sql += ")";
			skuSpecItemList = DAO.find(sql);
		}
		return skuSpecItemList;
	}

}