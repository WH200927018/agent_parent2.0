package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AttributeItemService;
import com.tipu.agent.front.service.entity.model.AttributeItem;
import io.jboot.service.JbootServiceBase;
import java.util.List;
import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AttributeItemServiceImpl extends JbootServiceBase<AttributeItem> implements AttributeItemService {

	@Override
	public Page<AttributeItem> findPage(AttributeItem data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("attributeItemId", data.getId());
		}
		SqlPara sp = Db.getSqlPara("attributeItem.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<AttributeItem> findList(AttributeItem data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("attributeItemId", data.getId());
		}
		SqlPara sp = Db.getSqlPara("attributeItem.findList", cond);
		List<AttributeItem> AttributeItemList = DAO.find(sp);
		return AttributeItemList;
	}

	@Cacheable(name = CacheKey.CACHE_ATTRIBUTEITEM)
	@Override
	public List<AttributeItem> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AttributeItem> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_ATTRIBUTEITEM);
	}

}