package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.SpecItemService;
import com.tipu.agent.front.service.entity.model.SpecItem;
import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class SpecItemServiceImpl extends JbootServiceBase<SpecItem> implements SpecItemService {

	@Override
	public Page<SpecItem> findPage(SpecItem data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getSpecId())) {
			cond.set("specId", data.getSpecId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("specItemId", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("specItem.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<SpecItem> findList(SpecItem data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getSpecId())) {
			cond.set("specId", data.getSpecId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("specItemId", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("specItem.findList", cond);
		List<SpecItem> SpecItemList = DAO.find(sp);
		return SpecItemList;
	}

	@Cacheable(name = CacheKey.CACHE_SPECITEM)
	@Override
	public List<SpecItem> findAll() {
		Columns columns = Columns.create();		
		columns.eq("del_flag", "0");
		List<SpecItem> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<SpecItem> findAll(String companyId,String brandId) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<SpecItem> list = DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_SPECITEM);
	}

}