package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentAuthService;
import com.tipu.agent.front.service.entity.model.AgentAuth;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentAuthServiceImpl extends JbootServiceBase<AgentAuth> implements AgentAuthService {

	@Override
	public Page<AgentAuth> findPage(AgentAuth data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgentId())) {
          cond.set("agentId", data.getAgentId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentAuth.findList",cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}
	
	@Cacheable(name = CacheKey.CACHE_AGENTAUTH)
	@Override
	public List<AgentAuth> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentAuth> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	/**
	 * 查询
	 * @param data
	 * @return
	 */
	@Override
	public List<AgentAuth> findList(AgentAuth data){
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getId())) {
          cond.set("id", data.getId());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			cond.set("agentId", data.getAgentId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentAuth.findList",cond);
		List<AgentAuth> list = DAO.find(sp);
		return list;
	}
	
	/**
	 * 查询最新授权状态
	 * @param date
	 * @return
	 */
	@Override
	public List<AgentAuth> findListUpToDate(AgentAuth data){
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgentId())) {
          cond.set("agentId", data.getAgentId());
		}	
		SqlPara sp = Db.getSqlPara("agentAuth.findListUpToDate",cond);
		List<AgentAuth> list = DAO.find(sp);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTAUTH);
    }
	
	@Override
	public int updateAgentValidity(String date) {
		String sql="UPDATE agent_auth SET auth_status = '2' WHERE del_flag='0' AND validity_date <? AND audit_status= '1'";	
		return Db.update(sql,date);
	}

}