package com.tipu.agent.front.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.AgentAuthTempletService;
import com.tipu.agent.front.service.entity.model.AgentAuthTemplet;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AgentAuthTempletServiceImpl extends JbootServiceBase<AgentAuthTemplet> implements AgentAuthTempletService {

	@Override
	public Page<AgentAuthTemplet> findPage(AgentAuthTemplet data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();

        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}

	@Override
	public AgentAuthTemplet findByApplyTypeAndBrandId(String applyType, String brandId) {
		Columns columns = Columns.create();
        if(StrKit.notBlank(applyType)) {
        	columns.eq("apply_type", applyType);
        }
        if(StrKit.notBlank(brandId)) {
        	columns.eq("brand_id", brandId);
        }
        return DAO.findFirstByColumns(columns);
		
	}

}