package com.tipu.agent.front.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.AgentRewardService;
import com.tipu.agent.front.service.entity.model.AgentReward;
import io.jboot.service.JbootServiceBase;

import java.util.List;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AgentRewardServiceImpl extends JbootServiceBase<AgentReward> implements AgentRewardService {

	@Override
	public Page<AgentReward> findPage(AgentReward data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getAgentId())) {
            columns.like("agent_id", data.getAgentId());
        }
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}

	@Override
	public List<AgentReward> findList(AgentReward agencyReward) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(agencyReward.getOrderId())) {
			columns.eq("order_id", agencyReward.getOrderId());
		}
		if (StrKit.notBlank(agencyReward.getId())) {
			columns.eq("id", agencyReward.getId());
		}
		if (StrKit.notBlank(agencyReward.getAgentId())) {
			columns.eq("agent_id", agencyReward.getAgentId());
		}
		if (StrKit.notBlank(agencyReward.getRewardType())) {
			columns.eq("reward_type", agencyReward.getRewardType());
		}
		if (StrKit.notBlank(agencyReward.getBrandId())) {
			columns.eq("brand_id", agencyReward.getBrandId());
		}
		if (StrKit.notBlank(agencyReward.getStr("now"))) {
			columns.like("create_date", "%"+agencyReward.getStr("now")+"%");
		}
		columns.eq("del_flag", "0");
		List<AgentReward> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<AgentReward> findGiveList(AgentReward agentReward) {
		String sql="SELECT a.* FROM agent_reward a WHERE a.del_flag= '0' AND a.brand_id= ? AND  exists (select b.id from orders b where b.order_creater_id = ? AND b.id=a.order_id) ";
		List<AgentReward> list = DAO.find(sql,agentReward.getBrandId(),agentReward.getAgentId());
		return list;
	}

}