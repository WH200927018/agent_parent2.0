package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentInviteService;
import com.tipu.agent.front.service.entity.model.AgentInvite;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentInviteServiceImpl extends JbootServiceBase<AgentInvite> implements AgentInviteService {

	@Override
	public Page<AgentInvite> findPage(AgentInvite data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgent())) {
          cond.set("agent", data.getAgent());
		}
		if (StrKit.notBlank(data.getBrand())) {
	          cond.set("brand", data.getBrand());
		}
		if (StrKit.notBlank(data.getAgentType())) {
	          cond.set("agentType", data.getAgentType());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		SqlPara sp = Db.getSqlPara("agentInvite.findList",cond);
		Page<AgentInvite> list = DAO.paginate(pageNumber, pageSize, sp);
        return list;
	}

	@Override
	public List<AgentInvite> findList(AgentInvite searchInvite){
		Kv cond = Kv.create();
		if (StrKit.notBlank(searchInvite.getAgent())) {
          cond.set("agent", searchInvite.getAgent());
		}
		if (StrKit.notBlank(searchInvite.getBrand())) {
	          cond.set("brand", searchInvite.getBrand());
		}
		if (StrKit.notBlank(searchInvite.getAgentType())) {
	          cond.set("agentType", searchInvite.getAgentType());
		}
		if (StrKit.notBlank(searchInvite.getCompanyId())) {
			cond.set("companyId", searchInvite.getCompanyId());
		}
		SqlPara sp = Db.getSqlPara("agentInvite.findList",cond);
		List<AgentInvite> list = DAO.find(sp);
		return list;
	}
	
	@Override
	public AgentInvite getEntity(AgentInvite searchInvite) {
		Kv cond = Kv.create();
		cond.set("agent",searchInvite.getAgent());
		cond.set("agentType",searchInvite.getAgentType());
		cond.set("level",searchInvite.getLevel());
		SqlPara sp = Db.getSqlPara("agentInvite.getEntity",cond);
		AgentInvite agentInvite = DAO.findFirst(sp);
		return agentInvite;
	}
	
	@Cacheable(name = CacheKey.CACHE_AGENTINVITE)
	@Override
	public List<AgentInvite> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentInvite> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTINVITE);
    }

}