package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentLoginLogService;
import com.tipu.agent.front.service.entity.model.AgentLoginLog;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentLoginLogServiceImpl extends JbootServiceBase<AgentLoginLog> implements AgentLoginLogService {

	@Override
	public Page<AgentLoginLog> findPage(AgentLoginLog data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgent())) {
	          cond.set("agent", data.getAgent());
		}						
		SqlPara sp = Db.getSqlPara("agentLoginLog.findList",cond);
        return DAO.paginate(pageNumber, pageSize, sp);
	}
	
	@Cacheable(name = CacheKey.CACHE_AGENTLOGINLOG)
	@Override
	public List<AgentLoginLog> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentLoginLog> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTLOGINLOG);
    }

}