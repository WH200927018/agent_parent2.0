package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.SpecService;
import com.tipu.agent.front.service.entity.model.Spec;
import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class SpecServiceImpl extends JbootServiceBase<Spec> implements SpecService {

	@Override
	public Page<Spec> findPage(Spec data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getGroupId())) {
			cond.set("groupId", data.getGroupId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("specId", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("spec.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<Spec> findList(Spec data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getGroupId())) {
			cond.set("groupId", data.getGroupId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("specId", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("spec.findList", cond);
		List<Spec> SpecList = DAO.find(sp);
		return SpecList;
	}
	
	@Override
	public List<Spec> findSpecItemList(Spec data){
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getGroupId())) {
			cond.set("groupId", data.getGroupId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("id", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("spec.findSpecItemList", cond);
		List<Spec> SpecList = DAO.find(sp);
		return SpecList;
	}

	@Cacheable(name = CacheKey.CACHE_SPEC)
	@Override
	public List<Spec> findAll() {
		Columns columns = Columns.create();		
		columns.eq("del_flag", "0");
		List<Spec> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<Spec> findAll(String companyId,String brandId) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<Spec> list = DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_SPEC);
	}

}