package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.ProductType;
import com.tipu.agent.front.service.entity.model.Sku;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class ProductTypeServiceImpl extends JbootServiceBase<ProductType> implements ProductTypeService {

	@Override
	public Page<ProductType> findPage(ProductType data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getName())) {
            columns.like("name", "%"+data.getName()+"%");
        }
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public List<ProductType> findAllBySort(String companyId,String brandId) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
        return DAO.findListByColumns(columns, "sort");
	}
	
	@Cacheable(name = CacheKey.CACHE_PRODUCTTYPE)
	@Override
	public List<ProductType> findAll(){
		Columns columns = Columns.create();		
		columns.eq("del_flag", "0");
		List<ProductType> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<ProductType> findAll(String companyId,String brandId){
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<ProductType> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_PRODUCTTYPE);
    }

	@Override
	public List<ProductType> findByAgent(Agent agent) {
		String sql="SELECT a.* FROM product_type a WHERE a.company_id=? AND exists (select b.brand_id from agent_brand b where b.agent_id =? and b.brand_id=a.brand_id)";
		List<ProductType> productTypeList = DAO.find(sql,agent.getCompanyId(),agent.getId());
		return productTypeList;
	}

}