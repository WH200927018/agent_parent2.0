package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.AgentTakeStockService;
import com.tipu.agent.front.service.entity.model.AgentTakeStock;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentTakeStockServiceImpl extends JbootServiceBase<AgentTakeStock> implements AgentTakeStockService {

	@Override
	public Page<AgentTakeStock> findPage(AgentTakeStock data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public List<AgentTakeStock> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentTakeStock> list = DAO.findListByColumns(columns);
		return list;
	}

	/**
	 * 查询所有
	 * 
	 * @param data
	 * @return
	 */
	@Override
	public List<AgentTakeStock> findList(AgentTakeStock data) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			columns.eq("agent_id", data.getAgentId());
		}
		if (StrKit.notBlank(data.getOrderId())) {
			columns.eq("order_id", data.getOrderId());
		}
		if (StrKit.notBlank(data.getStr("startDate"))) {
			columns.ge("create_date", data.getStr("startDate"));
		}
		if (StrKit.notBlank(data.getStr("endDate"))) {
			columns.le("create_date", data.getStr("endDate"));
		}
		if (StrKit.notBlank(data.getStr("now"))) {
			columns.like("create_date", "%"+data.getStr("now")+"%");
		}
		columns.eq("del_flag", "0");
		List<AgentTakeStock> list = DAO.findListByColumns(columns);
		return list;
	}

}