package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.SpecGroupService;
import com.tipu.agent.front.service.entity.model.SpecGroup;
import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class SpecGroupServiceImpl extends JbootServiceBase<SpecGroup> implements SpecGroupService {

	@Override
	public Page<SpecGroup> findPage(SpecGroup data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("specGroupId", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("specGroup.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<SpecGroup> findList(SpecGroup data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getTypeId())) {
			cond.set("typeId", data.getTypeId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("specGroupId", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("specGroup.findList", cond);
		List<SpecGroup> SpecGroupList = DAO.find(sp);
		return SpecGroupList;
	}

	@Cacheable(name = CacheKey.CACHE_SPECGROUP)
	@Override
	public List<SpecGroup> findAll() {
		Columns columns = Columns.create();		
		columns.eq("del_flag", "0");
		List<SpecGroup> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<SpecGroup> findAll(String companyId,String brandId) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<SpecGroup> list = DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_SPECGROUP);
	}

}