package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AdItemService;
import com.tipu.agent.front.service.entity.model.AdItem;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AdItemServiceImpl extends JbootServiceBase<AdItem> implements AdItemService {

	/**
	 * 分页查询
	 */
	@Override
	public Page<AdItem> findPage(AdItem data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
          cond.set("name", "%"+data.getName()+"%");
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("adItem.findList",cond);
		Page<AdItem> list = DAO.paginate(pageNumber, pageSize, sp);
		return list;
	}
	
	/**
	 * 查询所有
	 */
	@Cacheable(name = CacheKey.CACHE_ADITEM)
	@Override
	public List<AdItem> findAll(){
		Kv cond = Kv.create();
		SqlPara sp = Db.getSqlPara("adItem.findList",cond);
		List<AdItem> list = DAO.find(sp);
		return list;
	}
	
	@Override
	public List<AdItem> findAll(String companyId, String brandId){
		Kv cond = Kv.create();
		if (StrKit.notBlank(companyId)) {
			cond.set("companyId", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			cond.set("brandId", brandId);
		}
		SqlPara sp = Db.getSqlPara("adItem.findList",cond);
		List<AdItem> list = DAO.find(sp);
		return list;
	}
	
	/**
	 * 删除缓存
	 */
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_ADITEM);
    }

}