package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentBanksService;
import com.tipu.agent.front.service.entity.model.AgentBanks;
import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentBanksServiceImpl extends JbootServiceBase<AgentBanks> implements AgentBanksService {

	@Override
	public Page<AgentBanks> findPage(AgentBanks data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getId())) {
			cond.set("agentBanksId", data.getId());
		}
		if (StrKit.notBlank(data.getType())) {
			cond.set("type", data.getType());
		}
		if (StrKit.notBlank(data.getAgent())) {
			cond.set("agent", data.getAgent());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentBanks.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<AgentBanks> findList(AgentBanks data) {
		Kv cond = Kv.create();		
		if (StrKit.notBlank(data.getId())) {
			cond.set("agentBanksId", data.getId());
		}
		if (StrKit.notBlank(data.getType())) {
			cond.set("bank_type", data.getType());
		}
		if (StrKit.notBlank(data.getAgent())) {
			cond.set("agent", data.getAgent());
		}
		if (StrKit.notBlank(data.getIsCompany())) {
			cond.set("isCompany", data.getIsCompany());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentBanks.findList", cond);
		List<AgentBanks> AgentBanksList = DAO.find(sp);
		return AgentBanksList;
	}

	@Cacheable(name = CacheKey.CACHE_AGENTBANKS)
	@Override
	public List<AgentBanks> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentBanks> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<AgentBanks> findAll(String companyId,String brandId) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id",brandId);
		}
		columns.eq("del_flag", "0");
		List<AgentBanks> list = DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTBANKS);
	}

}