package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.SkuAttributeItemService;
import com.tipu.agent.front.service.entity.model.SkuAttributeItem;
import io.jboot.service.JbootServiceBase;
import java.util.List;
import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class SkuAttributeItemServiceImpl extends JbootServiceBase<SkuAttributeItem> implements SkuAttributeItemService {

	@Override
	public Page<SkuAttributeItem> findPage(SkuAttributeItem data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getSkuId())) {
			cond.set("skuId", data.getSkuId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("skuAttributeItemId", data.getId());
		}
		if (StrKit.notBlank(data.getAttributeId())) {
			cond.set("attributeId", data.getAttributeId());
		}
		if (StrKit.notBlank(data.getAttributeItemId())) {
			cond.set("attributeItemId", data.getAttributeItemId());
		}
		SqlPara sp = Db.getSqlPara("skuAttributeItem.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<SkuAttributeItem> findList(SkuAttributeItem data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getSkuId())) {
			cond.set("skuId", data.getSkuId());
		}
		if (StrKit.notBlank(data.getId())) {
			cond.set("skuAttributeItemId", data.getId());
		}
		if (StrKit.notBlank(data.getAttributeId())) {
			cond.set("attributeId", data.getAttributeId());
		}
		if (StrKit.notBlank(data.getAttributeItemId())) {
			cond.set("attributeItemId", data.getAttributeItemId());
		}
		SqlPara sp = Db.getSqlPara("skuAttributeItem.findList", cond);
		List<SkuAttributeItem> list = DAO.find(sp);
		return list;
	}

	@Cacheable(name = CacheKey.CACHE_SKUATTRIBUTEITEM)
	@Override
	public List<SkuAttributeItem> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<SkuAttributeItem> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_SKUATTRIBUTEITEM);
	}

}