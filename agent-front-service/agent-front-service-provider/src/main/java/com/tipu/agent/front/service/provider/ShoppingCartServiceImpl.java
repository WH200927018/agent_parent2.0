package com.tipu.agent.front.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.ShoppingCartService;
import com.tipu.agent.front.service.entity.model.ShoppingCart;
import io.jboot.service.JbootServiceBase;
import java.util.List;
import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class ShoppingCartServiceImpl extends JbootServiceBase<ShoppingCart> implements ShoppingCartService {

	@Override
	public Page<ShoppingCart> findPage(ShoppingCart data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			columns.eq("agent_id", data.getAgentId());
		}
		if (StrKit.notBlank(data.getProductId())) {
			columns.eq("product_id", data.getProductId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}

	@Override
	public List<ShoppingCart> findList(ShoppingCart data) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			columns.eq("agent_id", data.getAgentId());
		}
		if (StrKit.notBlank(data.getProductId())) {
			columns.eq("product_id", data.getProductId());
		}
		columns.eq("del_flag", "0");
		List<ShoppingCart> list = DAO.findListByColumns(columns);
		return list;
	}

}