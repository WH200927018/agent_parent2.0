package com.tipu.agent.front.service.provider;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.Orders;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class OrdersServiceImpl extends JbootServiceBase<Orders> implements OrdersService {

	@Override
	public Page<Orders> findPage(Orders data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getId())) {
			cond.set("orderId", data.getId());
		}			
		if (StrKit.notBlank(data.getAgentId())) {
			cond.set("agent", data.getAgentId());
		}
		if (StrKit.notBlank(data.getBank())) {
			cond.set("bank", data.getBank());
		}
		if (StrKit.notBlank(data.getOrderType())) {
			cond.set("orderType", data.getOrderType());
		}
		if (StrKit.notBlank(data.getOrderGroupId())) {
			cond.set("orderGroup", data.getOrderGroupId());
		}
		if (StrKit.notBlank(data.getHandleAgentId())) {
			cond.set("handleAgentId", data.getHandleAgentId());
		}
		if(data.getState() != null){
			String state = data.getState();
			if(state.equals(">1")){
				cond.set("gtState",1); //如果是大于1
			}else{
				cond.set("state",data.getState());
			}
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("order.findList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<Orders> findList(Orders data) {
		Kv cond = Kv.create();		
		if (StrKit.notBlank(data.getId())) {
			cond.set("orderId", data.getId());
		}
		if (StrKit.notBlank(data.getBank())) {
			cond.set("bank", data.getBank());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			cond.set("agent", data.getAgentId());
		}
		if (StrKit.notBlank(data.getOrderType())) {
			cond.set("orderType", data.getOrderType());
		}
		if(data.getState() != null){
			cond.set("state",data.getState());
		}
		if (StrKit.notBlank(data.getOrderGroupId())) {
			cond.set("orderGroup", data.getOrderGroupId());
		}
		if (StrKit.notBlank(data.getHandleAgentId())) {
			cond.set("handleAgentId", data.getHandleAgentId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		if (StrKit.notBlank(data.getOrderCreaterId())) {
			cond.set("orderCreater", data.getOrderCreaterId());
		}
		if (StrKit.notBlank(data.getStr("now"))) {
			cond.set("now", "%"+data.getStr("now")+"%");
		}
		SqlPara sp = Db.getSqlPara("order.findList", cond);
		List<Orders> orderList = DAO.find(sp); 
		return orderList;
	}

	@Cacheable(name = CacheKey.CACHE_ORDER)
	@Override
	public List<Orders> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Orders> list = DAO.findListByColumns(columns);
		return list;
	}
	
	/**
	 * 计算这个代理的总业绩
	 * @param agentId
	 * @return
	 */
	@Override
	public BigDecimal countMoney(String agentId){
		Kv cond = Kv.create();
		cond.set("agentId", agentId);
		SqlPara sp = Db.getSqlPara("order.countMoney", cond);
		Orders orders = DAO.findFirst(sp);
		BigDecimal countMoney = orders.getBigDecimal("SUM(buy_sum)");
		return countMoney;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_ORDER);
	}

	@Override
	public Page<Orders> findSubPage(Orders data, Integer pageNumber, Integer pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getId())) {
			cond.set("orderId", data.getId());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			cond.set("agent", data.getAgentId());
		}
		if (StrKit.notBlank(data.getBank())) {
			cond.set("bank", data.getBank());
		}
		if (StrKit.notBlank(data.getOrderType())) {
			cond.set("orderType", data.getOrderType());
		}
		if (StrKit.notBlank(data.getOrderGroupId())) {
			cond.set("orderGroup", data.getOrderGroupId());
		}
		if (StrKit.notBlank(data.getOrderCreaterId())) {
			cond.set("orderCreater", data.getOrderCreaterId());
		}
		if (StrKit.notBlank(data.getHandleAgentId())) {
			cond.set("handleAgentId", data.getHandleAgentId());
		}
		if (StrKit.notBlank(data.getState())) {
			cond.set("state", data.getState());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("order.findChildList", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	/**
	 * 通过groupId更新
	 */
	@Override
	public int updateOrderStateByGroupId(String groupId) {
		return Db.update("update orders o " + 
				"set  o.state = '-1' " +
				" where o.order_group_id = '"+ groupId+"'");
	}

	/**
	 * 获取待处理订单的总数
	 */
	@Override
	public Integer getCount(String id) {
		Orders orders =	DAO.findFirst("select count(*) as count from orders where  del_flag=0 and order_type = 1 and handle_agent_id = ? and (state = 0 or state = 1 ) order by create_date desc",id);
		Integer count = Integer.parseInt(orders.getStr("count"));
		return count;
	}

	/**
	 * 前台待处理订单sql
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@Override
	public Page<Orders> findFrontPage(Orders data, int pageNumber, int pageSize){
		Kv cond = Kv.create();
		cond.set("handleAgentId", data.getHandleAgentId());
		SqlPara sp = Db.getSqlPara("order.findOrders", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	/**
	 * 团队业绩分页
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@Override
	public Page<Orders> findAchievementPage(Orders data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgentId())) {
			cond.set("agentId", data.getAgentId());
		}
		SqlPara sp = Db.getSqlPara("order.myAchievementListPage", cond);
		return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<Record> findRecord(String sql) {
		List<Record> list = Db.find(sql);
		return list;
	}
	
	@Override
	public BigDecimal countOrdersByState(String productType,String state,String startDate,String endDate){
		String sql = "select SUM(a.buy_sum) from orders a "
					+ "LEFT JOIN order_product b on a.id=b.order_id "
					+ " WHERE a.del_flag = 0 "
					+ " AND a.order_type = 1"
 					+ " AND a.state = "+ state;
					if (StrKit.notBlank(productType)) {
			            String[] productTypeId = productType.split(",");
			            if (productTypeId.length > 0) {
			                sql += " AND product IN (";
			                for (int i = 0; i < productTypeId.length; i++) {
			                    if (StrKit.notBlank(productTypeId[i])) {
			                        if (i < productTypeId.length - 1) {
			                            sql += "\""+productTypeId[i]+"\"" + ",";
			                        } else {
			                            sql += "\""+productTypeId[i]+"\"";
			                        }
			                    }
			                }
			                sql += " )";
			            }
			        }
					if(StrKit.notBlank(startDate) && StrKit.notBlank(endDate)){
						sql += " AND a.create_date >='"+ startDate + "  00:00:00' and a.create_date<='" +endDate+" 23:59:59'";
					}
					Orders orders =	DAO.findFirst(sql);
					BigDecimal sum = orders.getBigDecimal("SUM(a.buy_sum)");
					if(sum == null){
						sum =new BigDecimal(0);
					}
		return sum;
	}
	@Override
	public List<Orders> findSubList(Orders data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getId())) {
			cond.set("orderId", data.getId());
		}
		if (StrKit.notBlank(data.getAgentId())) {
			cond.set("agent", data.getAgentId());
		}
		if (StrKit.notBlank(data.getBank())) {
			cond.set("bank", data.getBank());
		}
		if (StrKit.notBlank(data.getOrderType())) {
			cond.set("orderType", data.getOrderType());
		}
		if (StrKit.notBlank(data.getOrderGroupId())) {
			cond.set("orderGroup", data.getOrderGroupId());
		}
		if (StrKit.notBlank(data.getOrderCreaterId())) {
			cond.set("orderCreater", data.getOrderCreaterId());
		}
		if (StrKit.notBlank(data.getHandleAgentId())) {
			cond.set("handleAgentId", data.getHandleAgentId());
		}
		if (StrKit.notBlank(data.getState())) {
			cond.set("state", data.getState());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("order.findChildList", cond);
		return DAO.find(sp);
	}

	@Override
	public List<Orders> findListByAgentIds(List<AgentBrand> agentBrandList) {
		String sql = "SELECT * FROM orders WHERE del_flag= '0' AND order_type=1 AND state>0 ";
			
			if (agentBrandList.size() > 0) {
				sql += "AND brand_id= ' "+agentBrandList.get(0).getBrandId()+"' AND agent_id IN (";
				for (int i = 0; i < agentBrandList.size(); i++) {
						if (i < agentBrandList.size() - 1) {
							sql += "'"+agentBrandList.get(i).getAgentId() + "',";
						} else {
							sql += "'"+agentBrandList.get(i).getAgentId()+ "'";
						}					
				}
				sql += " )";
			}
		return DAO.find(sql);
	}

	@Override
	public List<Orders> findTransferList(Orders orders) {
		String sql="SELECT a.*,b.account_asname AS acName,b.account AS account FROM orders a LEFT JOIN agent_banks b ON a.bank =b.id WHERE a.del_flag='0' AND a.agent_id=? AND a.order_type=? ";
		return DAO.find(sql,orders.getAgentId(),orders.getOrderType());
	}

	@Override
	public Orders findByAgentIdAndGroupId(String agentId, String orderGroupId) {
		String sql="SELECT a.* FROM orders a WHERE a.del_flag='0' AND a.order_creater_id=? AND a.agent_id=? AND a.order_group_id=? ";
		return DAO.findFirst(sql,agentId,agentId,orderGroupId);
	}
}