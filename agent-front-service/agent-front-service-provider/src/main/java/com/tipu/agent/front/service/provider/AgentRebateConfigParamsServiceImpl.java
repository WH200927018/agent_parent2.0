package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.front.service.api.AgentRebateConfigParamsService;
import com.tipu.agent.front.service.entity.model.AgentRebateConfigParams;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentRebateConfigParamsServiceImpl extends JbootServiceBase<AgentRebateConfigParams> implements AgentRebateConfigParamsService {

	@Override
	public Page<AgentRebateConfigParams> findPage(AgentRebateConfigParams data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getCompanyId())) {
	          cond.set("company_id", data.getCompanyId());
		}	
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		if (StrKit.notBlank(data.getConfigId())) {
			cond.set("configId", data.getConfigId());
		}
		SqlPara sp = Db.getSqlPara("agentRebateConfigParams.findList",cond);
        return DAO.paginate(pageNumber, pageSize, sp);
	}

	@Override
	public List<AgentRebateConfigParams> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentRebateConfigParams> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<AgentRebateConfigParams> findAll(String companyId, String brandId) {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		List<AgentRebateConfigParams> list = DAO.findListByColumns(columns);
		return list;
	}

	/**
	 * 查询
	 * 
	 * @param data
	 * @return
	 */
	@Override
	public List<AgentRebateConfigParams> findList(AgentRebateConfigParams data) {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		List<AgentRebateConfigParams> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public AgentRebateConfigParams findByConfigId(AgentRebateConfigParams data) {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		if (StrKit.notBlank(data.getConfigId())) {
			columns.eq("config_id", data.getConfigId());
		}
		AgentRebateConfigParams config = DAO.findFirstByColumns(columns);
		return config;
	}




}