package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.StrKit;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.ProductImagesService;
import com.tipu.agent.front.service.entity.model.ProductImages;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class ProductImagesServiceImpl extends JbootServiceBase<ProductImages> implements ProductImagesService {

	@Override
	public List<ProductImages> findList(ProductImages productImages) {
		Columns columns = Columns.create();		
		if (StrKit.notBlank(productImages.getProductId())) {
            columns.eq("product_id", productImages.getProductId());
        }
		if (StrKit.notBlank(productImages.getId())) {
			columns.eq("id", productImages.getId());
		}
		if (StrKit.notBlank(productImages.getUrl())) {
			columns.eq("url", productImages.getUrl());
		}
		columns.eq("del_flag", "0");		
		return DAO.findListByColumns(columns,"create_date");
	}
	
	@Cacheable(name = CacheKey.CACHE_PRODUCTIMAGES)
	@Override
	public List<ProductImages> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<ProductImages> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_PRODUCTIMAGES);
    }

		

}