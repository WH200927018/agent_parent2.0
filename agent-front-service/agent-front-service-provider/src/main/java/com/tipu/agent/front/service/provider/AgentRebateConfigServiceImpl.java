package com.tipu.agent.front.service.provider;

import java.util.List;
import javax.inject.Singleton;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.api.AgentRebateConfigService;
import com.tipu.agent.front.service.entity.model.AgentRebateConfig;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentRebateConfigServiceImpl extends JbootServiceBase<AgentRebateConfig> implements AgentRebateConfigService {

	@Override
	public Page<AgentRebateConfig> findPage(AgentRebateConfig data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public List<AgentRebateConfig> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentRebateConfig> list = DAO.findListByColumns(columns);
		return list;
	}

}