package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AdFieldService;
import com.tipu.agent.front.service.entity.model.AdField;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AdFieldServiceImpl extends JbootServiceBase<AdField> implements AdFieldService {

	@Override
	public Page<AdField> findPage(AdField data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getName())) {
			columns.like("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Cacheable(name = CacheKey.CACHE_ADFIELD)
	@Override
	public List<AdField> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AdField> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<AdField> findAll(String companyId, String brandId) {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		List<AdField> list = DAO.findListByColumns(columns);
		return list;
	}

	/**
	 * 查询所有
	 * 
	 * @param data
	 * @return
	 */
	@Override
	public List<AdField> findList(AdField data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getIsShow())) {
			cond.set("isShow", data.getIsShow());
		}
		if (StrKit.notBlank(data.getPosType())) {
			cond.set("posType", data.getPosType());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("adfield.findList", cond);
		List<AdField> list = DAO.find(sp);
		return list;
	}

	/**
	 * 根据栏位是否显示首页和 显示页 来查询 这个栏目中有几个广告 找出图片或视频
	 * 
	 * @param data
	 * @return
	 */
	@Override
	public List<AdField> findShowAdItem(AdField data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%" + data.getName() + "%");
		}
		if (StrKit.notBlank(data.getIsShow())) {
			cond.set("isShow", data.getIsShow());
		}
		if (StrKit.notBlank(data.getPosType())) {
			cond.set("posType", data.getPosType());
		}
		if (data.getAdType() != null) {
			cond.set("adType", data.getAdType());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("adfield.findShowAdItem", cond);
		List<AdField> list = DAO.find(sp);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_ADFIELD);
	}

}