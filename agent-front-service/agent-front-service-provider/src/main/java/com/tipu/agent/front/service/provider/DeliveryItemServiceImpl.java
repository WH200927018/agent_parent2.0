package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.DeliveryItemService;
import com.tipu.agent.front.service.entity.model.DeliveryItem;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class DeliveryItemServiceImpl extends JbootServiceBase<DeliveryItem> implements DeliveryItemService {

	@Override
	public Page<DeliveryItem> findPage(DeliveryItem data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getDelivery())) {
          cond.set("delivery", data.getDelivery());
		}
		if (StrKit.notBlank(data.getProduct())) {
	          cond.set("product", data.getProduct());
		}
		if (StrKit.notBlank(data.getAntiCode())) {
	          cond.set("antiCode", data.getAntiCode());
		}
		if (StrKit.notBlank(data.getLogisticsCode())) {
	          cond.set("logisticsCode", data.getLogisticsCode());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			cond.set("companyId", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("deliveryItem.findList",cond);
		Page<DeliveryItem> list = DAO.paginate(pageNumber, pageSize, sp);
		return list; 
	}
	
	@Cacheable(name = CacheKey.CACHE_DELIVERYITEM)
	@Override
	public List<DeliveryItem> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<DeliveryItem> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_DELIVERYITEM);
    }

	@Override
	public List<DeliveryItem> findList(DeliveryItem deliveryItem) {
		Columns columns = Columns.create();
		if(StrKit.notBlank(deliveryItem.getDelivery())){
			columns.eq("delivery", deliveryItem.getDelivery());
		}
		if (StrKit.notBlank(deliveryItem.getCompanyId())) {
			columns.eq("company_id", deliveryItem.getCompanyId());
		}
		if (StrKit.notBlank(deliveryItem.getBrandId())) {
			columns.eq("brand_id",deliveryItem.getBrandId());
		}
		columns.eq("del_flag", "0");
		List<DeliveryItem> list = 	DAO.findListByColumns(columns);
		return list;
	}

}