package com.tipu.agent.front.service.provider.app;

import io.jboot.Jboot;

/**
 * 服务启动入口 
 * @author Rlax
 *
 */
public class FrontServiceApplication {
    public static void main(String [] args){
        Jboot.run(args);
    }
}
