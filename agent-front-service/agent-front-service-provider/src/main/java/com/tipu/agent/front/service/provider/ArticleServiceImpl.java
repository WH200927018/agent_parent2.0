package com.tipu.agent.front.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.ArticleService;
import com.tipu.agent.front.service.entity.model.AgentLoginLog;
import com.tipu.agent.front.service.entity.model.Article;
import io.jboot.service.JbootServiceBase;

import java.util.List;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class ArticleServiceImpl extends JbootServiceBase<Article> implements ArticleService {

	@Override
	public Page<Article> findPage(Article data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getTitle())) {
			columns.like("title", "%"+data.getTitle()+"%");
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			columns.eq("brand_id", data.getBrandId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}
	
	@Cacheable(name = CacheKey.CACHE_ARTICLE)
	@Override
	public List<Article> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Article> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<Article> findAll(String companyId, String brandId){
		Columns columns = Columns.create();
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		columns.eq("del_flag", "0");
		List<Article> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_ARTICLE);
    }

}