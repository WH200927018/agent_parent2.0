package com.tipu.agent.front.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AgentBrandServiceImpl extends JbootServiceBase<AgentBrand> implements AgentBrandService {

	@Override
	public Page<AgentBrand> findPage(AgentBrand data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getAgentType())) {
			cond.set("agentType", data.getAgentType());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentBrand.findList", cond);
		Page<AgentBrand> list = DAO.paginate(pageNumber, pageSize, sp);
		return list;
	}

	public List<AgentBrand> findList(AgentBrand agentBrand) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(agentBrand.getAgentType())) {
			columns.eq("agent_type", agentBrand.getAgentType());
		}
		if (StrKit.notBlank(agentBrand.getBrandId())) {
			columns.eq("brand_id", agentBrand.getBrandId());
		}
		if (StrKit.notBlank(agentBrand.getParentId())) {
			columns.eq("parent_id", agentBrand.getParentId());
		}
		if (StrKit.notBlank(agentBrand.getAuditStatus())) {
			columns.eq("audit_status", agentBrand.getAuditStatus());
		}
		if (StrKit.notBlank(agentBrand.getAgentId())) {
			columns.eq("agent_id", agentBrand.getAgentId());
		}
		columns.eq("del_flag", "0");
		return DAO.findListByColumns(columns);
	}

	@Override
	public List<AgentBrand> findAgentBrandList(AgentBrand agentBrand) {
		Kv cond = Kv.create();

		if (StrKit.notBlank(agentBrand.getBrandId())) {
			cond.set("brandId", agentBrand.getBrandId());
		}
		SqlPara sp = Db.getSqlPara("agentBrand.findList", cond);
		return DAO.find(sp);
	}

	@Override
	public List<AgentBrand> findChildList(Agent agent) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(agent.getId())) {
			cond.set("agent", agent.getId());
		}
		SqlPara sp = Db.getSqlPara("agentBrand.findChildList", cond);
		return DAO.find(sp);
	}

	@Cacheable(name = CacheKey.CACHE_AGENTBRAND)
	@Override
	public List<AgentBrand> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AgentBrand> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public void refreshCache() {
		Jboot.me().getCache().removeAll(CacheKey.CACHE_AGENTBRAND);
	}

	/**
	 * 查询最大编号
	 */
	@Override
	public String findMaxCode(AgentBrand data) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("brandId", data.getBrandId());
		}
		if (StrKit.notBlank(data.getBrandId())) {
			cond.set("agentType", data.getAgentType());
		}
		SqlPara sp = Db.getSqlPara("agentBrand.findMaxCode", cond);
		AgentBrand agentBrand = DAO.findFirst(sp);
		String agentlevel = agentBrand.getStr("max(a.code)");
		return agentlevel;
	}

	@Override
	public AgentBrand findByAgentAndBrand(String agentId, String brandId) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(brandId)) {
			columns.eq("brand_id", brandId);
		}
		if (StrKit.notBlank(brandId)) {
			columns.eq("agent_id", agentId);
		}
		columns.eq("auth_status", "1");
		columns.eq("del_flag", "0");
		AgentBrand agentBrand = DAO.findFirstByColumns(columns);
		return agentBrand;
	}
	
	
	@Override
	public List<AgentBrand> findListByAgentIdAndAuthStatus(String agentId,String authStatus) {
		return  DAO.find("select * from agent_brand where agent_id = ? and del_flag = ? and auth_status = ?", agentId,"0",authStatus);
	}
	

	@Override
	public List<AgentBrand> findByAgentId(String agentId) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(agentId)) {
			cond.set("agentId", agentId);
		}
		SqlPara sp = Db.getSqlPara("agentBrand.findByAgentId", cond);
		return DAO.find(sp);
	}
}