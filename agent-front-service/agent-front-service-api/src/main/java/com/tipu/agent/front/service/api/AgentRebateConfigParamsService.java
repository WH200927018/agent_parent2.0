package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.AgentRebateConfigParams;

import java.util.List;

public interface AgentRebateConfigParamsService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<AgentRebateConfigParams> findPage(AgentRebateConfigParams data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public AgentRebateConfigParams findById(Object id);


    /**
     * find all model
     *
     * @return all <AgentRebateConfigParams
     */
    public List<AgentRebateConfigParams> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(AgentRebateConfigParams model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(AgentRebateConfigParams model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(AgentRebateConfigParams model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(AgentRebateConfigParams model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

    public List<AgentRebateConfigParams> findAll(String companyId,String brandId);
    
    public List<AgentRebateConfigParams> findList(AgentRebateConfigParams data);

	public AgentRebateConfigParams findByConfigId(AgentRebateConfigParams data);
}