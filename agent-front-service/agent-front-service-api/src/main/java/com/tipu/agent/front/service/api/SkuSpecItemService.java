package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.Sku;
import com.tipu.agent.front.service.entity.model.SkuSpecItem;

import java.util.List;

public interface SkuSpecItemService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<SkuSpecItem> findPage(SkuSpecItem data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public SkuSpecItem findById(Object id);


    /**
     * find all model
     *
     * @return all <SkuSpecItem
     */
    public List<SkuSpecItem> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(SkuSpecItem model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(SkuSpecItem model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(SkuSpecItem model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(SkuSpecItem model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
    
    /**
     * 清除缓存
     */
	public void refreshCache();
	
    /**
     * 根据条件查询
     * @param data
     * @return
     */
	public List<SkuSpecItem> findList(SkuSpecItem data);

	/**
     * 根据条件查询
     * @param data
     * @return
     */	
	public List<SkuSpecItem> findListBySkuId(SkuSpecItem data);

	public List<SkuSpecItem> findSkuList(SkuSpecItem data);

	public List<SkuSpecItem> findSkuIds(String specItemIds);
}