package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.Delivery;
import com.tipu.agent.front.service.entity.model.DeliveryItem;

import java.util.List;

public interface DeliveryService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<Delivery> findPage(Delivery data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Delivery findById(Object id);


    /**
     * find all model
     *
     * @return all <Delivery
     */
    public List<Delivery> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Delivery model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(Delivery model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(Delivery model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Delivery model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

    /**
     * 查询订单
     * @param delivery
     * @return
     */
	public List<Delivery> findList(Delivery delivery);

	public String getMaxNo();

	public void batchInsertList(List<DeliveryItem> deliveryItemList);

	public void refreshCache();

	 /**
     * 查询订单详情
     * @param delivery
     * @return
     */
	public List<Delivery> findDelivery(Delivery delivery);

	public List<Delivery> findAll(String companyId, String brandId);
}