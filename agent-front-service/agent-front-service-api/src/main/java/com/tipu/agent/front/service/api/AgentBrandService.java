package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;

import java.util.List;

public interface AgentBrandService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<AgentBrand> findPage(AgentBrand data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public AgentBrand findById(Object id);


    /**
     * find all model
     *
     * @return all <AgentBrand
     */
    public List<AgentBrand> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(AgentBrand model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(AgentBrand model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(AgentBrand model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(AgentBrand model);
    
    /**
     * 查询所有代理商品牌
     * @param agentBrand
     * @return
     */
    public List<AgentBrand> findList(AgentBrand agentBrand);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

    /**
     * 查询微信代理商信息
     * @param agentBrand
     * @return
     */
	List<AgentBrand> findAgentBrandList(AgentBrand agentBrand);

	/**
	 * 根据父id查询代理申请
	 * @param agent
	 * @return
	 */
	List<AgentBrand> findChildList(Agent agent);

	public void refreshCache();

	public String findMaxCode(AgentBrand data);

	public AgentBrand findByAgentAndBrand(String agentId, String brandId);

	public List<AgentBrand> findByAgentId(String agentId);
    
	/**
	 * 根据代理ID查询所有代理品牌
	 * */
	public List<AgentBrand> findListByAgentIdAndAuthStatus(String agentId,String authStatus);
	
}