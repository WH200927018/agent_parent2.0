package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.ProductType;

import java.util.List;

public interface ProductTypeService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<ProductType> findPage(ProductType data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public ProductType findById(Object id);


    /**
     * find all model
     *
     * @return all <ProductType
     */
    public List<ProductType> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(ProductType model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(ProductType model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(ProductType model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(ProductType model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
    
    /**
     * 排序查询商品类型
     * @return
     */
    public List<ProductType> findAll(String companyId, String brandId);

	public void refreshCache();

	public List<ProductType> findAllBySort(String companyId, String brandId);

	/**
	 * 根据代理商信息查询商品类型
	 * @param agent
	 * @return
	 */
	public List<ProductType> findByAgent(Agent agent);

}