package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentType;

import java.util.List;

public interface AgentTypeService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<AgentType> findPage(AgentType data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public AgentType findById(Object id);


    /**
     * find all model
     *
     * @return all <AgentType
     */
    public List<AgentType> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(AgentType model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(AgentType model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(AgentType model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(AgentType model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
    
    /**
     * 根据删除标志位查询代理商类型
     * @param i 0-未删除；1-已删除
     * @return
     */
	public List<AgentType> findByDelfalg(AgentBrand agentBrand);

	/**
	 * 刷新缓存
	 */
	public void refreshCache();
    
	/**
	 * 根据代理商等级查询
	 * @param agentType
	 * @return
	 */
	public List<AgentType> findAllBySort(AgentType agentType);

	public List<AgentType> findAll(String companyId, String brandId);
	
	public  List<AgentType> findCurrentGradeList(String brandId,String companyId,int grade);

	public List<AgentType> findAllByGrade(AgentType data);
}