package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.Orders;

import java.math.BigDecimal;
import java.util.List;

public interface OrdersService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<Orders> findPage(Orders data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Orders findById(Object id);


    /**
     * find all model
     *
     * @return all <Order
     */
    public List<Orders> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Orders model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(Orders model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(Orders model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Orders model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

	public void refreshCache();

	public List<Orders> findList(Orders data);

	/**
	 * 计算这个代理的总业绩
	 * @param agentId
	 * @return
	 */
	public BigDecimal countMoney(String agentId);
	
	/**
	 * 查询下级订单
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Orders> findSubPage(Orders data, Integer pageIndex, Integer pageNumber);

	public int updateOrderStateByGroupId(String orderGroupId);

	public Integer getCount(String id);

	public Page<Orders> findFrontPage(Orders data, int pageNumber, int pageSize);

	public Page<Orders> findAchievementPage(Orders data, int pageNumber, int pageSize);

	public List<Record> findRecord(String sql);

	public BigDecimal countOrdersByState(String productType, String state, String startDate, String endDate);

	public List<Orders> findSubList(Orders data);

	public List<Orders> findListByAgentIds(List<AgentBrand> agentBrandList);

	public List<Orders> findTransferList(Orders orders);

	public Orders findByAgentIdAndGroupId(String agentId, String orderGroupId);
}