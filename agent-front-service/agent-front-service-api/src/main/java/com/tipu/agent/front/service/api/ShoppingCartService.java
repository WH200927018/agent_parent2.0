package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.ShoppingCart;
import com.tipu.agent.front.service.entity.model.Sku;

import java.util.List;

public interface ShoppingCartService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<ShoppingCart> findPage(ShoppingCart data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public ShoppingCart findById(Object id);


    /**
     * find all model
     *
     * @return all <ShoppingCart
     */
    public List<ShoppingCart> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(ShoppingCart model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(ShoppingCart model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(ShoppingCart model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(ShoppingCart model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

    /**
     * 根据条件查询
     * @param data
     * @return
     */
	public List<ShoppingCart> findList(ShoppingCart data);
}