package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.Product;

import java.util.List;

public interface ProductService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<Product> findPage(Product data, int pageNumber, int pageSize);

	/**
	 * 根据产品编码查找产品对象
	 * @param productCode
	 * @return
	 */
	public Product findByCode(String productCode);
	
    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Product findById(Object id);


    /**
     * find all model
     *
     * @return all <Product
     */
    public List<Product> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Product model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(Product model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(Product model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Product model);
    
    /**
     * 查询最大条码
     * @return
     */
    public Product findLastCode();


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
   
    public List<Product> findList(Product data);
    
    /**
     * 清除缓存
     */
	public void refreshCache();
    
	/**
	 * 修改库存
	 * @param stock
	 * @param stockType
	 * @return
	 */
	public int updateStock(Integer stock, Integer stockType,String id);

	public List<Product> findAll(String companyId, String brandId);

}