package com.tipu.agent.front.service.api;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.Sku;

import java.math.BigDecimal;
import java.util.List;

public interface AgentService {

	/**
	 * 分页查询
	 * 
	 * @param data
	 *            查询参数对象
	 * @param pageNumber
	 *            页数
	 * @param pageSize
	 *            分页大小
	 * @return 分页
	 */
	public Page<Agent> findPage(Agent data, int pageNumber, int pageSize);

	/**
	 * find model by primary key
	 *
	 * @param id
	 * @return
	 */
	public Agent findById(Object id);

	/**
	 * find all model
	 *
	 * @return all <Agent
	 */
	public List<Agent> findAll();

	/**
	 * delete model by primary key
	 *
	 * @param id
	 * @return success
	 */
	public boolean deleteById(Object id);

	/**
	 * delete model
	 *
	 * @param model
	 * @return
	 */
	public boolean delete(Agent model);

	/**
	 * save model to database
	 *
	 * @param model
	 * @return
	 */
	public boolean save(Agent model);

	/**
	 * save or update model
	 *
	 * @param model
	 * @return if save or update success
	 */
	public boolean saveOrUpdate(Agent model);

	/**
	 * update data model
	 *
	 * @param model
	 * @return
	 */
	public boolean update(Agent model);

	public void join(Page<? extends Model> page, String joinOnField);

	public void join(Page<? extends Model> page, String joinOnField, String[] attrs);

	public void join(Page<? extends Model> page, String joinOnField, String joinName);

	public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);

	public void join(List<? extends Model> models, String joinOnField);

	public void join(List<? extends Model> models, String joinOnField, String[] attrs);

	public void join(List<? extends Model> models, String joinOnField, String joinName);

	public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);

	public void join(Model model, String joinOnField);

	public void join(Model model, String joinOnField, String[] attrs);

	public void join(Model model, String joinOnField, String joinName);

	public void join(Model model, String joinOnField, String joinName, String[] attrs);

	public void keep(Model model, String... attrs);

	public void keep(List<? extends Model> models, String... attrs);

	public List<Agent> findList(Agent checkAgent);

	public void refreshCache();

	/**
	 * 修改余额
	 * 
	 * @param agent
	 * @param type
	 */
	public int updateAgentDeposit(Agent agent, int type);

	/**
	 * 修改奖励金
	 * 
	 * @param agent
	 * @param type
	 */
	public int updateAgentReward(Agent agent, int type);

	public Agent subDeposit(String id, BigDecimal orderBuySum);

	public Agent addReward(final String id, final BigDecimal money);

	public Agent addDeposit(String id, BigDecimal money);

	public Agent subReward(String id, BigDecimal money);

	public int updateFrontAgentReward(Agent handleAgent, int i);

	public Ret updateAgencyPassword(Agent agent, String oldPassword, String newPassword, String renewPassWord);

	public List<Record> findRecord(String sql);

	public List<Agent> findAll(String companyId, String brandId);

	
	public Agent findByPhone(String phone, String companyId);

	public Agent applyAgent(final Agent agent, final String companyId, final String brandId, final String agentTypeId,
			final String parentId);

	public boolean isApply(String companyId, String brandId, String phone);

	public List<Agent> findAgentList(Agent agent);

	public Agent findAgent(String searchName);
	
	public Page<Agent> findAgentsByStatus(int pageNumber , int pageSize ,String brandId,String agentId,String auditStatus);
	
	public  Page<Agent> findRelativesByType(int pageNumber , int pageSize , String agentId,String type,String brandId);

	public int reduceAgentDepositAndStock(Agent agent, List<Sku> skuList, String productType);

	public Agent findByPhoneOrWx(String data, String companyId);

	public int updateAgentReward(Agent agent, AgentBrand agentBrand, Orders order, AgentType agentType,List<AgentBrand> agentBrandList);

}