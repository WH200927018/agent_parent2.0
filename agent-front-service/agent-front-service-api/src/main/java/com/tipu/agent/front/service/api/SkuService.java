package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.Sku;
import java.util.List;

public interface SkuService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<Sku> findPage(Sku data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Sku findById(Object id);


    /**
     * find all model
     *
     * @return all <Sku
     */
    public List<Sku> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Sku model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(Sku model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(Sku model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Sku model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);
    
    /**
     * 清除缓存
     */
	public void refreshCache();
	
    /**
     * 根据条件查询
     * @param data
     * @return
     */
	public List<Sku> findList(Sku data);
	
	/**
	 * 根据商品id查询
	 * @param data
	 * @return
	 */
	public List<Sku> findListByProductIds(String productIds);
    
	/**
	 * 修改库存
	 * @param stock
	 * @param stockType
	 * @param id
	 * @return
	 */
	public int updateStock(Integer stock, Integer stockType, String id);

	/**
	 * 查询是库存是否充足
	 * @param productIds
	 * @return
	 */
	public List<Sku> subStoreByList(final List<Sku> productIds,String productType);

	public List<Sku> findAll(String companyId, String brandId);

	public List<Sku> findByAgent(Agent agent, String brandId);

	public int updateVirtuaStock(Integer virtual_stock, Integer stockType, String id);

}