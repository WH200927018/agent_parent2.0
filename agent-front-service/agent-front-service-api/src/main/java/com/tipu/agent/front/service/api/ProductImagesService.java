package com.tipu.agent.front.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.front.service.entity.model.ProductImages;

import java.util.List;

public interface ProductImagesService  {


    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public ProductImages findById(Object id);


    /**
     * find all model
     *
     * @return all <ProductImages
     */
    public List<ProductImages> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(ProductImages model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(ProductImages model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(ProductImages model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(ProductImages model);
    


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

   /**
    * 查询轮播图片
    * @param id
    * @return
    */
	public List<ProductImages> findList(ProductImages productImages);

	public void refreshCache();


}