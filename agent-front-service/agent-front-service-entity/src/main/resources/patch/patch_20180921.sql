ALTER TABLE `agent`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属公司id' AFTER `store_url`;

ALTER TABLE `sys_brand`
ADD COLUMN `code`  int(255) NULL DEFAULT 1 COMMENT ' 品牌编号' AFTER `company_id`;

ALTER TABLE `agent`
DROP COLUMN `validity_date`,
DROP COLUMN `apply_type`,
DROP COLUMN `store_type`,
DROP COLUMN `store_name`,
DROP COLUMN `store_url`;

ALTER TABLE `agent_brand`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属公司id' AFTER `del_flag`;

ALTER TABLE `agent_auth`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `del_flag`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `agent_bond`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `status`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `agent_banks`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `is_company`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `agent_address`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `agent_name`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;