ALTER TABLE `ad_field`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `pos_type`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `ad_item`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `remarks`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `article`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `del_flag`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `oa_notify`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `del_flag`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `setting`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `enable`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `oa_notify`
MODIFY COLUMN `create_by`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '创建者' AFTER `status`,
MODIFY COLUMN `create_date`  datetime NULL COMMENT '创建时间' AFTER `create_by`,
MODIFY COLUMN `update_by`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '更新者' AFTER `create_date`,
MODIFY COLUMN `update_date`  datetime NULL COMMENT '更新时间' AFTER `update_by`,
MODIFY COLUMN `del_flag`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记' AFTER `remarks`;