ALTER TABLE `product`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `package_spec`;

ALTER TABLE `sku`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `product_type`;

ALTER TABLE `product_type`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `sort`;

ALTER TABLE `spec`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `group_id`;

ALTER TABLE `spec_group`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `sort`;

ALTER TABLE `spec_item`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `sort`;

ALTER TABLE `sku_spec_item`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `sku_id`;

ALTER TABLE `product`
ADD COLUMN `brand_id`  varchar(64) NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `sku`
ADD COLUMN `brand_id`  varchar(64) NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `product_type`
ADD COLUMN `brand_id`  varchar(64) NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `spec`
ADD COLUMN `brand_id`  varchar(64) NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `spec_group`
ADD COLUMN `brand_id`  varchar(64) NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `spec_item`
ADD COLUMN `brand_id`  varchar(64) NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `sku_spec_item`
ADD COLUMN `brand_id`  varchar(64) NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `agent_type`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `sort`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;
