ALTER TABLE `agent_invite`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `share_url`;

ALTER TABLE `orders`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `product_unit`;

ALTER TABLE `delivery`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `export_records`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;

ALTER TABLE `delivery_item`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司ID' AFTER `credit`,
ADD COLUMN `brand_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌id' AFTER `company_id`;