ALTER TABLE `agent`
DROP COLUMN `validity_date`,
DROP COLUMN `apply_type`,
DROP COLUMN `store_type`,
DROP COLUMN `store_name`,
DROP COLUMN `store_url`;

/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : agent-base-back

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-09-07 19:35:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agent_auth
-- ----------------------------
DROP TABLE IF EXISTS `agent_auth`;
CREATE TABLE `agent_auth` (
  `id` varchar(64) DEFAULT NULL COMMENT '主键',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `agent_id` varchar(64) DEFAULT NULL COMMENT '代理商id',
  `validity_date` datetime DEFAULT NULL COMMENT '授权有效期',
  `auth_pic` varchar(255) DEFAULT NULL COMMENT '授权书',
  `auth_status` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '是否审核(-1：不通过，0：待处理，1：通过)',
  `apply_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '申请类型',
  `store_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '店铺类型',
  `store_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '店铺名称',
  `store_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '店铺地址',
  `pay_img` varchar(255) DEFAULT NULL COMMENT '付款凭证'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='代理商授权';



ALTER TABLE `agent`
DROP COLUMN `validity_date`,
DROP COLUMN `apply_type`,
DROP COLUMN `store_type`,
DROP COLUMN `store_name`,
DROP COLUMN `store_url`;


/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : agent-base

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-09-10 16:10:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for weixin
-- ----------------------------
DROP TABLE IF EXISTS `weixin`;
CREATE TABLE `weixin` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(64) DEFAULT NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `wx_no` varchar(255) DEFAULT NULL COMMENT '微信号',
  `wx_name` varchar(255) DEFAULT NULL COMMENT '微信名称',
  `wx_type` varchar(255) DEFAULT NULL COMMENT '微信类型 0-订阅号 1-服务好 2-企业号',
  `wx_token` varchar(255) DEFAULT NULL COMMENT '微信Token',
  `wx_appid` varchar(255) DEFAULT NULL COMMENT '微信APPID',
  `wx_appsecret` varchar(255) DEFAULT NULL COMMENT '微信APPSecret',
  `wx_qrurl` varchar(255) DEFAULT NULL COMMENT '微信二维码',
  `wx_heading_url` varchar(255) DEFAULT NULL COMMENT '微信头像',
  `wx_mchid` varchar(255) DEFAULT NULL COMMENT '微信商户号',
  `wx_paykey` varchar(255) DEFAULT NULL COMMENT '微信商户密钥',
  `wx_oauth_url` varchar(255) DEFAULT NULL COMMENT '网页2.0授权地址,需要加http',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for weixin_menu
-- ----------------------------
DROP TABLE IF EXISTS `weixin_menu`;
CREATE TABLE `weixin_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(64) DEFAULT NULL COMMENT '父id',
  `wx_id` bigint(20) DEFAULT NULL COMMENT '微信表主键',
  `wx_menu_name` varchar(111) DEFAULT NULL COMMENT '菜单名称',
  `wx_menu_operation` varchar(255) DEFAULT NULL COMMENT '菜单操作',
  `wx_menu_level` int(11) DEFAULT NULL COMMENT '菜单级别（只有两级）',
  `wx_menu_sort` int(11) DEFAULT NULL COMMENT '排序',
  `wx_menu_type` varchar(64) DEFAULT '' COMMENT '菜单类型',
  `wx_menu_media_id` varchar(64) DEFAULT NULL COMMENT '调用新增永久素材接口返回的合法media_id',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(64) DEFAULT NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='微信菜单';