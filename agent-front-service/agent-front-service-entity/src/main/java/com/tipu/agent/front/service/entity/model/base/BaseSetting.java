package com.tipu.agent.front.service.entity.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 系统配置项
 */
@SuppressWarnings("serial")
public abstract class BaseSetting<M extends BaseSetting<M>> extends JbootModel<M> implements IBean {

	public void setId(java.lang.String id) {
		set("id", id);
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public void setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}
	
	public void setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
	}
	
	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public void setSysKey(java.lang.String sysKey) {
		set("sys_key", sysKey);
	}
	
	public java.lang.String getSysKey() {
		return getStr("sys_key");
	}
	
	public void setSysType(java.lang.String sysType) {
		set("sys_type", sysType);
	}
	
	public java.lang.String getSysType() {
		return getStr("sys_type");
	}
	
		
	public void setRemarks(java.lang.String remarks) {
		set("remarks", remarks);
	}
	
	public java.lang.String getRemarks() {
		return getStr("remarks");
	}
	
}