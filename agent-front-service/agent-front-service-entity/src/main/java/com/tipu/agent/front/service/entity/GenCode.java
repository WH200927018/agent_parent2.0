package com.tipu.agent.front.service.entity;

import com.tipu.agent.admin.base.gencode.model.AppModelGenerator;

/**
 * 代码生成
 * @author Rlax
 *
 */
public class GenCode {

    public static void main(String[] args) {
        AppModelGenerator.doGenerate();
    }
}
