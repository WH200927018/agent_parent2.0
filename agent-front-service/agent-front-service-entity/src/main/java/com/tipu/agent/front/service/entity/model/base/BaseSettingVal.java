package com.tipu.agent.front.service.entity.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseSettingVal<M extends BaseSettingVal<M>> extends JbootModel<M> implements IBean {

	public void setId(java.lang.String id) {
		set("id", id);
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public void setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public void setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
	}
	
	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public void setSettingId(java.lang.String settingId) {
		set("setting_id", settingId);
	}
	
	public java.lang.String getSettingId() {
		return getStr("setting_id");
	}

	public void setSysType(java.lang.String sysType) {
		set("sys_type", sysType);
	}
	
	public java.lang.String getSysType() {
		return getStr("sys_type");
	}

	public void setCompanyId(java.lang.String companyId) {
		set("company_id", companyId);
	}
	
	public java.lang.String getCompanyId() {
		return getStr("company_id");
	}
	
	public void setBrandId(java.lang.String brandId) {
		set("brand_id", brandId);
	}
	
	public java.lang.String getBrandId() {
		return getStr("brand_id");
	}
	
	public void setEnable(java.lang.String enable) {
		set("enable", enable);
	}
	
	public java.lang.String getEnable() {
		return getStr("enable");
	}
		
	public void setRemarks(java.lang.String remarks) {
		set("remarks", remarks);
	}
	
	public java.lang.String getRemarks() {
		return getStr("remarks");
	}

	
	public void setVal(java.lang.String val) {
		set("val", val);
	}
	
	public java.lang.String getVal() {
		return getStr("val");
	}
}
