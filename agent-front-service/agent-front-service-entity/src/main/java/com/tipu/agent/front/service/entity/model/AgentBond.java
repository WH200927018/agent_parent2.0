package com.tipu.agent.front.service.entity.model;

import com.tipu.agent.front.service.entity.model.base.BaseAgentBond;

import io.jboot.db.annotation.Table;

/**
 * 代理商保证金
 * @author hulin
 *
 */
@Table(tableName = "agent_bond", primaryKey = "id")
public class AgentBond extends BaseAgentBond<AgentBond> {
	
}
