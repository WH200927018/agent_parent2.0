package com.tipu.agent.front.service.entity.model;

import io.jboot.db.annotation.Table;
import com.tipu.agent.front.service.entity.model.base.BaseSkuSpecItem;

/**
 * Generated by Jboot.
 */
@Table(tableName = "sku_spec_item", primaryKey = "id")
public class SkuSpecItem extends BaseSkuSpecItem<SkuSpecItem> {
	
}
