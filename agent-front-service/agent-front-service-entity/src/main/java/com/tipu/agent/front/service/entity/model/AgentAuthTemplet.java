package com.tipu.agent.front.service.entity.model;

import io.jboot.db.annotation.Table;
import com.tipu.agent.front.service.entity.model.base.BaseAgentAuthTemplet;

/**
 * Generated by Jboot.
 */
@Table(tableName = "agent_auth_templet", primaryKey = "id")
public class AgentAuthTemplet extends BaseAgentAuthTemplet<AgentAuthTemplet> {
	
}
