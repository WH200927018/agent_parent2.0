package com.tipu.agent.front.service.entity.model;

import io.jboot.db.annotation.Table;
import com.tipu.agent.front.service.entity.model.base.BaseAgentInvite;

/**
 * Generated by Jboot.
 */
@Table(tableName = "agent_invite", primaryKey = "id")
public class AgentInvite extends BaseAgentInvite<AgentInvite> {
	
}
