package com.tipu.agent.front.service.entity.model;

import io.jboot.db.annotation.Table;
import com.tipu.agent.front.service.entity.model.base.BaseAgentExtend;

/**
 * Generated by Jboot.
 */
@Table(tableName = "agent_extend", primaryKey = "id")
public class AgentExtend extends BaseAgentExtend<AgentExtend> {
	
}
