package com.tipu.agent.front.service.entity.model;

import com.tipu.agent.front.service.entity.model.base.BaseAgentAddress;

import io.jboot.db.annotation.Table;

/**
 * 代理商常用地址
 * @author wh
 *
 */
@Table(tableName = "agent_address", primaryKey = "id")
public class AgentAddress extends BaseAgentAddress<AgentAddress>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4449830726343692392L;

}
