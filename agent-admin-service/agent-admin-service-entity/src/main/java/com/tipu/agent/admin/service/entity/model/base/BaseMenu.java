package com.tipu.agent.admin.service.entity.model.base;
import java.util.List;

import com.jfinal.plugin.activerecord.IBean;
import com.tipu.agent.admin.service.entity.model.Button;

import io.jboot.db.model.JbootModel;

/**
 * 
 * @author Javen
 * 2016年5月30日
 */
@SuppressWarnings("serial")
public abstract class BaseMenu<M extends BaseMenu<M>> extends JbootModel<M> implements IBean {
	public void setButton(List<Button> button) {
		set("button", button);
	}
	
	public List<Button> getButton() {
		return get("button");
	}
	
}
