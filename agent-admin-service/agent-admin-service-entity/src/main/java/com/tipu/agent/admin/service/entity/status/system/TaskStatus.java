package com.tipu.agent.admin.service.entity.status.system;

import com.tipu.agent.admin.base.common.BaseStatus;

/**
 * 任务状态类
 * @author think
 *
 */
public class TaskStatus extends BaseStatus {

	public final static String UNRUN = "0";
	public final static String RUNNING = "1";
	
	public TaskStatus() {
		add(UNRUN, "未运行");
		add(RUNNING, "正在运行");
	}
	
	private static TaskStatus me;
	
	public static TaskStatus me() {
		if(me == null) {
			me = new TaskStatus();
		}
		return me;
	}
	
}
