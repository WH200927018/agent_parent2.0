package com.tipu.agent.admin.service.entity.model;

/**
 * 
 * @author Javen
 * 2016年5月30日
 */
public class ClickButton extends Button{
	
	public void setKey(java.lang.String key) {
		set("key", key);
	}
	
	public java.lang.String getKey() {
		return getStr("key");
	}
	
}
