package com.tipu.agent.admin.service.entity.model;

/**
 * 
 * @author Javen
 * 2016年5月30日
 */
public class MediaButton extends Button{
	
	public void setMedia(java.lang.String media) {
		set("media_id", media);
	}
	
	public java.lang.String getMedia() {
		return getStr("media_id");
	}
	
}
