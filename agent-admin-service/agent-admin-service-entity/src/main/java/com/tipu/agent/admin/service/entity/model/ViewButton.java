package com.tipu.agent.admin.service.entity.model;
/**
 * @author Javen
 * 2016年5月30日
 */
public class ViewButton extends Button{
	
	public void setUrl(java.lang.String url) {
		set("url", url);
	}
	
	public java.lang.String getUrl() {
		return getStr("url");
	}
	
	
}
