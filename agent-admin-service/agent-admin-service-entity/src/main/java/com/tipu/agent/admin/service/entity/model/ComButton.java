package com.tipu.agent.admin.service.entity.model;

import java.util.List;

/**
 * 
 * @author Javen
 * 2016年5月30日
 */
public class ComButton extends Button {
	
	public void setSubButton(List<Button> sub_button) {
		set("sub_button", sub_button);
	}
	
	public List<Button> getSubButton() {
		return get("sub_button");
	}
	
}
