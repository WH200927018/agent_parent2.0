package com.tipu.agent.admin.service.entity.model.base;

import com.jfinal.plugin.activerecord.IBean;

import io.jboot.db.model.JbootModel;

/**
 * 按钮的基类
 * @author Javen
 * 2016年5月30日
 */
@SuppressWarnings("serial")
public abstract class BaseButton<M extends BaseButton<M>> extends JbootModel<M> implements IBean {

	public void setType(java.lang.String type) {
		set("type", type);
	}
	
	public java.lang.String getType() {
		return getStr("type");
	}
	
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}
	
}