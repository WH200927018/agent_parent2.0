/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : agent-admin

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-09-12 16:23:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_brand
-- ----------------------------
DROP TABLE IF EXISTS `sys_brand`;
CREATE TABLE `sys_brand` (
  `id` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '主键',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `remarks` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注信息',
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '名称',
  `logo_pic` varchar(255) DEFAULT NULL COMMENT 'logo',
  `company_id` varchar(64) DEFAULT NULL COMMENT '所属公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='品牌';

-- ----------------------------
-- Table structure for sys_company
-- ----------------------------
DROP TABLE IF EXISTS `sys_company`;
CREATE TABLE `sys_company` (
  `id` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '主键',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `remarks` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注信息',
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '名称',
  `logo_pic` varchar(255) DEFAULT NULL COMMENT 'logo',
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `home_url` varchar(255) DEFAULT NULL COMMENT '网址',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='公司';


ALTER TABLE `sys_user`
ADD COLUMN `company_id`  varchar(64) NULL COMMENT '所属公司id' AFTER `note`;

ALTER TABLE `sys_role`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属公司id' AFTER `note`;
