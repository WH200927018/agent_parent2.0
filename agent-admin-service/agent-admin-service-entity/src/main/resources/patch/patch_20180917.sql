ALTER TABLE `weixin_menu`
ADD COLUMN `company_id`  varchar(64) NULL COMMENT '所属公司id' AFTER `wx_menu_level`;

ALTER TABLE `weixin`
ADD COLUMN `company_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属公司id' AFTER `wx_oauth_url`;