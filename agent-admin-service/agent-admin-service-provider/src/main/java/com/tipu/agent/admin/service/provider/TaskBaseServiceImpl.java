package com.tipu.agent.admin.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.service.api.TaskBaseService;
import com.tipu.agent.admin.service.entity.model.TaskBase;

import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class TaskBaseServiceImpl extends JbootServiceBase<TaskBase> implements TaskBaseService {

	@Override
	public Page<TaskBase> findPage(TaskBase taskBase, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if(StrKit.notBlank(taskBase.getName())) {
			columns.like("name", "%"+taskBase.getName()+"%");
		}
        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "id asc");
	}

	@Override
	public boolean hasTask(String name) {
		return findByName(name) != null;
	}

	@Override
	public TaskBase findByName(String name) {
		return DAO.findFirstByColumn("name", name);
	}

}