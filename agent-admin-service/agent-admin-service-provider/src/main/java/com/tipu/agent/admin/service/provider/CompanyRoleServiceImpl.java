package com.tipu.agent.admin.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.admin.service.api.CompanyRoleService;
import com.tipu.agent.admin.service.entity.model.CompanyRole;
import com.tipu.agent.admin.service.entity.model.UserRole;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class CompanyRoleServiceImpl extends JbootServiceBase<CompanyRole> implements CompanyRoleService {

	@Override
	public Page<CompanyRole> findPage(CompanyRole data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getRoleId())) {
			columns.eq("role_id", data.getRoleId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public List<CompanyRole> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<CompanyRole> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<CompanyRole> findList(CompanyRole data) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getId())) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if (StrKit.notBlank(data.getRoleId())) {
			columns.eq("role_id", data.getRoleId());
		}
		columns.eq("del_flag", "0");
		List<CompanyRole> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public int[] batchSave(List<CompanyRole> list) {
		return Db.batchSave(list, list.size());
	}

	@Override
	public int deleteByCompanyId(String companyId) {
		return Db.delete("delete from sys_company_role where company_id = ?", companyId);
	}

	@Override
	public List<CompanyRole> findByCompanyId(String companyId) {
		return DAO.find("SELECT * FROM sys_company_role  WHERE company_id= ? ", companyId);
	}

	@Override
	public CompanyRole findByCompanyIdAndRoleId(String companyId, String roleId) {
		return DAO.findFirst("SELECT * FROM sys_company_role WHERE company_id= ? AND role_id=? ", companyId, roleId);
	}
	
	@Override
	public int deleteByCompanyIdAndRoleId(String companyId, String roleId) {
		return Db.delete("DELETE FROM sys_company_role WHERE company_id= ? AND role_id=? ", companyId, roleId);
	}

	@Override
	public int deleteByRoleId(String roleId) {
		return Db.delete("DELETE FROM sys_company_role WHERE role_id=? ", roleId);
		
	}
}