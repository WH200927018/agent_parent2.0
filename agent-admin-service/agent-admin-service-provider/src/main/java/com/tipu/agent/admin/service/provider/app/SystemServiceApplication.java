package com.tipu.agent.admin.service.provider.app;

import io.jboot.Jboot;

/**
 * 服务启动入口
 * @author Rlax
 *
 */
public class SystemServiceApplication {
    public static void main(String [] args){
        Jboot.run(args);
    }
}
