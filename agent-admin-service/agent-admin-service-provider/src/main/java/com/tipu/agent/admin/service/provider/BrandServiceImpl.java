package com.tipu.agent.admin.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.entity.model.Brand;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class BrandServiceImpl extends JbootServiceBase<Brand> implements BrandService {

	@Override
	public Page<Brand> findPage(Brand data, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(data.getName())) {
			cond.set("name", "%"+data.getName()+"%");
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			if (!data.getCompanyId().equals("0")) {
				cond.set("companyId", data.getCompanyId());
			}
		}
		SqlPara sp = Db.getSqlPara("system-brand.findList",cond);
		Page<Brand> list = DAO.paginate(pageNumber, pageSize, sp);
        return list;
	}
	
	@Cacheable(name = CacheKey.CACHE_SYS_BRAND)
	@Override
	public List<Brand> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Brand> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<Brand> findList(Brand data){
		Columns columns = Columns.create();
		if(StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if(data.getCode()!=null) {
			columns.eq("code", data.getCode());
		}
		columns.eq("del_flag", "0");
		List<Brand> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_SYS_BRAND);
    }
	
	@Override
	public int findByCode(Brand data){
		Columns columns = Columns.create();
		if(StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		if(data.getCode()!=null) {
			columns.eq("code", data.getCode());
		}
		if(StrKit.notBlank(data.getId())) {//排除自己
			columns.ne("id", data.getId());
		}
		columns.eq("del_flag", "0");
		List<Brand> list = 	DAO.findListByColumns(columns);
		return list.size();
	}

	
	@Override
	public List<Brand> findByIds(List<String> ids){
		StringBuffer buffer= new StringBuffer("select * from sys_brand where id in (");
		for (String id : ids) {
			buffer.append("'"+id+"',");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		return DAO.find(buffer.toString());
	}

}