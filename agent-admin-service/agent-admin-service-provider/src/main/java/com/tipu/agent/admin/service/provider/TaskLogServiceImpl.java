package com.tipu.agent.admin.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;

import com.tipu.agent.admin.service.api.TaskLogService;
import com.tipu.agent.admin.service.entity.model.TaskLog;

import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class TaskLogServiceImpl extends JbootServiceBase<TaskLog> implements TaskLogService {

}