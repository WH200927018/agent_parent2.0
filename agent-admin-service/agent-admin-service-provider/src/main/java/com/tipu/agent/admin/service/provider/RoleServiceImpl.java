package com.tipu.agent.admin.service.provider;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.service.api.CompanyRoleService;
import com.tipu.agent.admin.service.api.RoleResService;
import com.tipu.agent.admin.service.api.RoleService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.Company;
import com.tipu.agent.admin.service.entity.model.CompanyRole;
import com.tipu.agent.admin.service.entity.model.Role;
import com.tipu.agent.admin.service.entity.model.RoleRes;
import com.tipu.agent.admin.service.entity.status.system.RoleStatus;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Bean
@Singleton
@JbootrpcService
public class RoleServiceImpl extends JbootServiceBase<Role> implements RoleService {

	@Inject
	private RoleResService roleResService;

	@Inject
	private CompanyRoleService companyRoleService;

	@Override
	public Page<Role> findPage(Role sysRole, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
		if (StrKit.notBlank(sysRole.getName())) {
			cond.set("name", "%" + sysRole.getName() + "%");
		}
		if (StrKit.notBlank(sysRole.getCompanyId())) {
			if (!sysRole.getCompanyId().equals("0")) {
				cond.set("companyId", sysRole.getCompanyId());
			}
		}
		SqlPara sp = Db.getSqlPara("system-role.findList", cond);
		Page<Role> list = DAO.paginate(pageNumber, pageSize, sp);
		return list;
	}

	@Override
	public List<Role> findByUserName(String name) {
		SqlPara sp = Db.getSqlPara("system-role.findByUserName");
		sp.addPara(name);
		return DAO.find(sp);
	}

	@Override
	public boolean auth(Long id, String resIds) {
		List<RoleRes> roleResList = new ArrayList<RoleRes>();

		return Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				roleResService.deleteByRoleId(id);

				if (StrKit.notBlank(resIds)) {
					String[] ress = resIds.split(",");

					for (String resId : ress) {
						RoleRes roleRes = new RoleRes();
						roleRes.setRoleId(id);
						roleRes.setResId(Long.parseLong(resId));
						roleResList.add(roleRes);
					}

					int[] rets = Db.batchSave(roleResList, roleResList.size());
					for (int ret : rets) {
						if (ret < 1) {
							return false;
						}
					}
				}

				return true;
			}
		});
	}

	@Override
	public List<Role> findByStatusUsed() {
		return DAO.findListByColumn("status", RoleStatus.USED);
	}

	@Override
	public List<Role> findList(Role data) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		List<Role> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public boolean saveRole(Role sysRole, String companyId) {
		return Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				if (!sysRole.save()) {
					return false;
				}

				if (StrKit.notBlank(companyId)) {
					CompanyRole companyRole = new CompanyRole();
					companyRole.setCompanyId(companyId);
					companyRole.setRoleId(sysRole.getId() + "");
					companyRole.setCreateBy(sysRole.getLastUpdAcct());
					companyRole.setCreateDate(sysRole.getLastUpdTime());
					companyRole.setRoleName(sysRole.getName());
					companyRole.setDelFlag("0");
					if (!companyRole.save()) {
						return false;
					}
				}

				return true;
			}
		});
	}

	@Override
	public boolean updateRole(Role sysRole, String companyId) {
		return Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				if (!sysRole.update()) {
					return false;
				}

				if (StrKit.notBlank(companyId)) {
					companyRoleService.deleteByRoleId(sysRole.getId() + "");
					CompanyRole companyRole = new CompanyRole();
					companyRole.setCompanyId(companyId);
					companyRole.setRoleId(sysRole.getId() + "");
					companyRole.setCreateBy(sysRole.getLastUpdAcct());
					companyRole.setCreateDate(sysRole.getLastUpdTime());
					companyRole.setRoleName(sysRole.getName());
					companyRole.setDelFlag("0");
					if (!companyRole.save()) {
						return false;
					}
				}

				return true;
			}
		});
	}

	@Override
	public boolean deleteRoleById(Long roleId) {
		return Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				Role sysRole = new Role();
				sysRole.setId(roleId);
				if (!sysRole.delete()) {
					return false;
				}
				String sql = "DELETE FROM sys_company_role WHERE role_id= ?";
				if (Db.delete(sql, roleId) < 0) {
					return false;
				}
				return true;
			}
		});
	}

	@Override
	public List<Role> findByStatusUsedAndType() {
		Columns columns = Columns.create();
		columns.eq("status", "1");
		columns.eq("type", "0");
		List<Role> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public boolean saveRoleAndCompany(Role sysRole, List<Company> companyList) {
		return Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				if (!sysRole.save()) {
					return false;
				}

				if (companyList.size() > 0) {
					for (Company company : companyList) {
						CompanyRole companyRole = new CompanyRole();
						companyRole.setCompanyId(company.getId());
						companyRole.setRoleId(sysRole.getId() + "");
						companyRole.setCreateBy(sysRole.getLastUpdAcct());
						companyRole.setCreateDate(sysRole.getLastUpdTime());
						companyRole.setRoleName(sysRole.getName());
						companyRole.setDelFlag("0");
						if (!companyRole.save()) {
							return false;
						}
					}
				}
				return true;
			}
		});
	}
}