package com.tipu.agent.admin.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.tipu.agent.admin.service.api.UserRoleService;
import com.tipu.agent.admin.service.entity.model.UserRole;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;
import java.util.List;

@Bean
@Singleton
@JbootrpcService
public class UserRoleServiceImpl extends JbootServiceBase<UserRole> implements UserRoleService {

    @Override
    public int deleteByUserId(Long userId) {
        return Db.update("delete from sys_user_role where user_id = ?", userId);
    }

    @Override
    public int[] batchSave(List<UserRole> list) {
        return Db.batchSave(list, list.size());
    }
    
    @Override
    public List<UserRole> findByRoleId(Long roleId) {
        return DAO.find("SELECT * from sys_user_role where role_id = ?", roleId);
    }
}