package com.tipu.agent.admin.service.provider;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.admin.service.api.CompanyRoleService;
import com.tipu.agent.admin.service.api.CompanyService;
import com.tipu.agent.admin.service.api.UserRoleService;
import com.tipu.agent.admin.service.entity.model.Company;
import com.tipu.agent.admin.service.entity.model.CompanyRole;
import com.tipu.agent.admin.service.entity.model.Role;
import com.tipu.agent.admin.service.entity.model.User;
import com.tipu.agent.admin.service.entity.model.UserRole;
import com.tipu.agent.admin.service.entity.status.system.UserOnlineStatus;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class CompanyServiceImpl extends JbootServiceBase<Company> implements CompanyService {

	@Inject
	private CompanyRoleService companyRoleService;
	
	@Override
	public Page<Company> findPage(Company data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getName())) {
            columns.like("name", "%"+data.getName()+"%");
        }
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}
	
	@Cacheable(name = CacheKey.CACHE_COMPANY)
	@Override
	public List<Company> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");		
		List<Company> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
	public List<Company> findList(Company data){
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getId())) {
            columns.eq("id", data.getId());
        }
		if (StrKit.notBlank(data.getName())) {
			columns.eq("name", data.getName());
		}
		columns.eq("del_flag", "0");
		List<Company> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache()                        {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        Jboot.me().getCache().removeAll(CacheKey.CACHE_COMPANY);
    }
	
	@Override
	public int countCompany(Company data){
		String sql="SELECT id FROM sys_company WHERE name=? AND id !=? AND del_flag ='0'";		
		return DAO.find(sql,data.getName(),data.getId()).size();
	}
	
	@Override
	public boolean saveCompany(Company data, List<Role> roleList) {

		return Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				if (!data.save()) {
					return false;
				}

				if (roleList.size() >0) {
					List<CompanyRole> list = new ArrayList<CompanyRole>();
					for (Role role : roleList) {
						CompanyRole companyRole = new CompanyRole();
						companyRole.setId(StrKit.getRandomUUID());
						companyRole.setCompanyId(data.getId());
						companyRole.setRoleId(role.getId()+"");
						companyRole.setCreateBy(data.getCreateBy());
						companyRole.setCreateDate(new Date());
						companyRole.setDelFlag("0");
						list.add(companyRole);
					}
					int[] rets = companyRoleService.batchSave(list);

					for (int ret : rets) {
						if (ret < 1) {
							return false;
						}
					}
				}
				return true;
			}
		});
	}

}