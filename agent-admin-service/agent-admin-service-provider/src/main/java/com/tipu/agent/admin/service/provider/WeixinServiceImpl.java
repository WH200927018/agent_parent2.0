package com.tipu.agent.admin.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.admin.service.api.WeixinService;
import com.tipu.agent.admin.service.entity.model.Weixin;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class WeixinServiceImpl extends JbootServiceBase<Weixin> implements WeixinService {

	@Override
	public Page<Weixin> findPage(Weixin data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getWxName())) {
			columns.eq("wx_name", data.getWxName());
		}
		if (StrKit.notBlank(data.getWxAppId())) {
			columns.eq("wx_appid", data.getWxAppId());
		}
		if (StrKit.notBlank(data.getWxNo())) {
			columns.eq("wx_no", data.getWxNo());
		}
		if (data.getId() != null) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public List<Weixin> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Weixin> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<Weixin> findList(Weixin data) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(data.getWxName())) {
			columns.eq("wx_name", data.getWxName());
		}
		if (StrKit.notBlank(data.getWxAppId())) {
			columns.eq("wx_appid", data.getWxAppId());
		}
		if (StrKit.notBlank(data.getWxNo())) {
			columns.eq("wx_no", data.getWxNo());
		}
		if (data.getId() != null) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		columns.eq("del_flag", "0");
		List<Weixin> list = DAO.findListByColumns(columns);
		return list;
	}

}