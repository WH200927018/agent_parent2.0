package com.tipu.agent.admin.service.provider.ge;

import com.tipu.agent.admin.base.gencode.serviceimpl.AppServiceImplGenerator;

/**
 * 代码生成
 * @author Rlax
 *
 */
public class GenCode {

    public static void main(String[] args) {
        AppServiceImplGenerator.doGenerate();
    }
}
