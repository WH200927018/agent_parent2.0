package com.tipu.agent.admin.service.provider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.admin.base.common.ZTree;
import com.tipu.agent.admin.service.api.WeixinMenuService;
import com.tipu.agent.admin.service.entity.model.Button;
import com.tipu.agent.admin.service.entity.model.ClickButton;
import com.tipu.agent.admin.service.entity.model.ComButton;
import com.tipu.agent.admin.service.entity.model.MediaButton;
import com.tipu.agent.admin.service.entity.model.Menu;
import com.tipu.agent.admin.service.entity.model.ViewButton;
import com.tipu.agent.admin.service.entity.model.Weixin;
import com.tipu.agent.admin.service.entity.model.WeixinMenu;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class WeixinMenuServiceImpl extends JbootServiceBase<WeixinMenu> implements WeixinMenuService {

	@Override
	public Page<WeixinMenu> findPage(WeixinMenu data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		if (data.getId() != null) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getPid())) {
			columns.eq("pid", data.getPid());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		columns.eq("del_flag", "0");
		return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "wx_menu_sort ASC");
	}

	@Override
	public List<WeixinMenu> findAll() {
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<WeixinMenu> list = DAO.findListByColumns(columns);
		return list;
	}

	@Override
	public List<WeixinMenu> findList(WeixinMenu data) {
		Columns columns = Columns.create();
		if (data.getId() != null) {
			columns.eq("id", data.getId());
		}
		if (StrKit.notBlank(data.getPid())) {
			columns.eq("pid", data.getPid());
		}
		if (StrKit.notBlank(data.getCompanyId())) {
			columns.eq("company_id", data.getCompanyId());
		}
		columns.eq("del_flag", "0");
		List<WeixinMenu> list = DAO.findListByColumns(columns, "wx_menu_sort ASC");
		return list;
	}

	@Override
	public List<ZTree> findTree(String companyId) {
		List<ZTree> zList = new ArrayList<ZTree>();
		// 查询微信
		String sql = "SELECT * FROM weixin WHERE del_flag ='0' AND company_id = ? ORDER BY create_date DESC";
		List<Record> weixinList = Db.find(sql, companyId);
		if (weixinList.size() > 0) {
			for (Record weixin : weixinList) {
				ZTree ztree = new ZTree(weixin.get("id"), weixin.get("wx_name"), "0");
				zList.add(ztree);
			}
		}
		Columns columns = Columns.create();// 查询微信菜单
		if (StrKit.notBlank(companyId)) {
			columns.eq("company_id", companyId);
		}
		columns.eq("del_flag", "0");
		List<WeixinMenu> weixinMenuList = DAO.findListByColumns(columns, "pid desc, wx_menu_sort asc");
		if (weixinMenuList.size() > 0) {
			for (WeixinMenu weixinMenu : weixinMenuList) {
				ZTree ztree = new ZTree(weixinMenu.getId() + "", weixinMenu.getWxMenuName(), weixinMenu.getPid());
				zList.add(ztree);
			}
		}
		return zList;
	}

	@Override
	public Menu getMenuByAppid(String appid) {
		Menu menu = new Menu();
		// 查询微信
		String sql = "SELECT a.* FROM weixin_menu a,weixin b WHERE a.del_flag ='0' AND b.wx_appid= ? AND a.pid=b.id ORDER BY a.wx_menu_sort ASC";
		List<WeixinMenu> weixinMenuList = DAO.find(sql, appid);
		if (weixinMenuList.size() > 0) {
			String wxMenuName = "";// 菜单名称
			String wxMenuType = "";// 菜单类型
			String wxMenuOperation = "";// 菜单操作
			List<Button> firstButtonList = new ArrayList<Button>();// 一级菜单
			for (WeixinMenu weixinMenu : weixinMenuList) {
				List<Button> secondButtonList = new ArrayList<Button>();// 二级菜单
				wxMenuName = weixinMenu.getWxMenuName();
				wxMenuType = weixinMenu.getWxMenuType();
				wxMenuOperation = weixinMenu.getWxMenuOperation();
				if (wxMenuType.equals("sub_button")) {// 菜单
					ComButton mainBtn = new ComButton();
					mainBtn.setName(wxMenuName);
					WeixinMenu wxMenu = new WeixinMenu();
					wxMenu.setPid(weixinMenu.getId() + "");
					List<WeixinMenu> secondMenuList = findList(wxMenu);// 二级菜单
					if (secondMenuList.size() > 0) {
						for (WeixinMenu secondMenu : secondMenuList) {
							wxMenuName = secondMenu.getWxMenuName();
							wxMenuType = secondMenu.getWxMenuType();
							wxMenuOperation = secondMenu.getWxMenuOperation();
							if (wxMenuType.equals("view")) {// 链接
								ViewButton btn = new ViewButton();
								btn.setName(wxMenuName);
								btn.setType(wxMenuType);
								btn.setUrl(wxMenuOperation);
								secondButtonList.add(btn);
							} else if (wxMenuType.equals("media_id") || wxMenuType.equals("view_limited")) {// 图文消息
								MediaButton btn = new MediaButton();
								btn.setName(wxMenuName);
								btn.setType(wxMenuType);
								btn.setMedia(wxMenuOperation);
								secondButtonList.add(btn);
							} else {// 其他
								ClickButton btn = new ClickButton();
								btn.setName(wxMenuName);
								btn.setType(wxMenuType);
								btn.setKey(wxMenuOperation);
								secondButtonList.add(btn);
							}
						}
					}
					mainBtn.setSubButton(secondButtonList);
					firstButtonList.add(mainBtn);
				} else if (wxMenuType.equals("view")) {// 链接
					ViewButton btn = new ViewButton();
					btn.setName(wxMenuName);
					btn.setType(wxMenuType);
					btn.setUrl(wxMenuOperation);
					firstButtonList.add(btn);
				} else if (wxMenuType.equals("media_id") || weixinMenu.getWxMenuType().equals("view_limited")) {// 图文消息
					MediaButton btn = new MediaButton();
					btn.setName(wxMenuName);
					btn.setType(wxMenuType);
					btn.setMedia(wxMenuOperation);
					firstButtonList.add(btn);
				} else {// 其他
					ClickButton btn = new ClickButton();
					btn.setName(wxMenuName);
					btn.setType(wxMenuType);
					btn.setKey(wxMenuOperation);
					firstButtonList.add(btn);
				}

			}
			menu.setButton(firstButtonList);
		}
		return menu;

	}

	@Override
	public int countMenu(String pid, Integer wxMenulevel) {
		String sql = "SELECT id FROM weixin_menu WHERE del_flag ='0' AND pid=? AND wx_menu_level=? ";
		List<WeixinMenu> weixinMenuList = DAO.find(sql, pid, wxMenulevel);
		return weixinMenuList.size();
	}

}