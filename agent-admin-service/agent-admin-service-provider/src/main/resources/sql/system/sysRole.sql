#sql("findByUserName")
  SELECT
    b.*
  FROM
    sys_role b,
    sys_user d,
    sys_user_role e
  WHERE
    b.id = e.role_id
    AND d.id = e.user_id
    AND d.`name` = ?
#end

#sql("findList")
 SELECT
    a.*,  
    b.name AS companyName
  FROM
    sys_role a
  LEFT JOIN
    sys_company b
  ON
    a.company_id = b.id
  WHERE
   1=1
  #if(name!=null)
  	AND  a.name LIKE #para(name)
  #end 
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end 
	ORDER BY a.seq asc
#end