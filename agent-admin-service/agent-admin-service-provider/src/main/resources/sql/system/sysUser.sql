#sql("findList")
 SELECT
    a.*,  
    b.name AS companyName
  FROM
    sys_user a
  LEFT JOIN
    sys_company b
  ON
    a.company_id = b.id
  WHERE
  1=1

  #if(name!=null)
  	AND  a.name LIKE #para(name)
  #end 
  #if(phone!=null)
  	AND  a.phone LIKE #para(phone)
  #end 
  #if(companyId!=null)
  	AND  a.company_id = #para(companyId)
  #end 
	ORDER BY a.id desc
#end