package com.tipu.agent.admin.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.service.entity.model.CompanyRole;

import java.util.List;

public interface CompanyRoleService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<CompanyRole> findPage(CompanyRole data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public CompanyRole findById(Object id);


    /**
     * find all model
     *
     * @return all <CompanyRole
     */
    public List<CompanyRole> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(CompanyRole model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(CompanyRole model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(CompanyRole model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(CompanyRole model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

	public List<CompanyRole> findList(CompanyRole CompanyRole);

	public int[] batchSave(List<CompanyRole> list);

	public int deleteByCompanyId(String id);

	public List<CompanyRole> findByCompanyId(String companyId);

	public CompanyRole findByCompanyIdAndRoleId(String companyId, String roleId);

	public int deleteByCompanyIdAndRoleId(String companyId, String roleId);

	public int deleteByRoleId(String string);


}