package com.tipu.agent.admin.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.service.entity.model.TaskBase;

import java.util.List;

public interface TaskBaseService  {

	/**
     * 分页查询定时任务信息
     * @param taskBase
     * @return
     */
    public Page<TaskBase> findPage(TaskBase taskBase, int pageNumber, int pageSize);
    
    /**
     * 任务是否存在
     * @param name
     * @return 存在返回-true，否则返回false
     */
    public boolean hasTask(String name);

    /**
     * 根据名称查询任务信息
     * @param name
     * @return
     */
    public TaskBase findByName(String name);
	
    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public TaskBase findById(Object id);


    /**
     * find all model
     *
     * @return all <TaskBase
     */
    public List<TaskBase> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(TaskBase model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(TaskBase model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(TaskBase model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(TaskBase model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

}