package com.tipu.agent.admin.service.api; 

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.ZTree;
import com.tipu.agent.admin.service.entity.model.Menu;
import com.tipu.agent.admin.service.entity.model.WeixinMenu;
import java.util.List;

public interface WeixinMenuService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<WeixinMenu> findPage(WeixinMenu data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public WeixinMenu findById(Object id);


    /**
     * find all model
     *
     * @return all <Brand
     */
    public List<WeixinMenu> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(WeixinMenu model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(WeixinMenu model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(WeixinMenu model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(WeixinMenu model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

	public List<WeixinMenu> findList(WeixinMenu weixinMenu);

	public List<ZTree> findTree(String companyId);

	/**
	 * 获取自定义菜单
	 * @return
	 */
	public Menu getMenuByAppid(String appid);

	/**
	 * 查询菜单数量
	 * @param pid
	 * @param wxMenulevel
	 * @return
	 */
	public int countMenu(String pid, Integer wxMenulevel);
	
}