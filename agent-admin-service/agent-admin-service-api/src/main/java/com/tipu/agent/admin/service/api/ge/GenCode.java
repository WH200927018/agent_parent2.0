package com.tipu.agent.admin.service.api.ge;

import com.tipu.agent.admin.base.gencode.service.AppServiceGenerator;

/**
 * 代码生成
 * @author Rlax
 *
 */
public class GenCode {

    public static void main(String[] args) {
        AppServiceGenerator.doGenerate();
    }
}
