package com.tipu.agent.anticode.service.provider;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.anticode.service.api.AnticodeConfigService;
import com.tipu.agent.anticode.service.entity.model.AnticodeConfig;

import io.jboot.service.JbootServiceBase;

import java.util.List;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class AnticodeConfigServiceImpl extends JbootServiceBase<AnticodeConfig> implements AnticodeConfigService {

	@Override
	public Page<AnticodeConfig> findPage(AnticodeConfig data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();
		
		if(StrKit.notBlank(data.getConfigNo())) {
        	columns.eq("config_no", data.getConfigNo());
        }
        
        if(StrKit.notBlank(data.getBrandId())) {
        	columns.eq("brand_id", data.getBrandId());
        }
        
        if(StrKit.notBlank(data.getProductId())) {
        	columns.eq("product_id", data.getProductId());
        }
        
        if(StrKit.notBlank(data.getAntiRuleId())) {
        	columns.eq("anti_rule_id", data.getAntiRuleId());
        }
//		Kv cond = Kv.create();
//        
//        SqlPara sp = Db.getSqlPara("agent-anticode.findList", cond);
//		return DAO.paginate(pageNumber, pageSize, sp);
        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList(), "create_date desc");
	}

	@Override
	public AnticodeConfig findByLogisticsCode(String logisticsCode) {
		Columns columns = Columns.create();
		
		if(StrKit.notBlank(logisticsCode)) {
        	columns.le("start_big", logisticsCode);
        	columns.ge("end_big", logisticsCode);
        }
		
		return DAO.findFirstByColumns(columns);
	}
	
	@Cacheable(name = CacheKey.CACHE_ANTICODECONFIG)
	@Override
	public List<AnticodeConfig> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AnticodeConfig> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_ANTICODECONFIG);
    }

}