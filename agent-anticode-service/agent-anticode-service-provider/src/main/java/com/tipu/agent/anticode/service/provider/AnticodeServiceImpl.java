package com.tipu.agent.anticode.service.provider;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.kit.Kv;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.admin.base.util.AESUtil;
import com.tipu.agent.admin.base.util.DateUtils;
import com.tipu.agent.admin.base.util.Encodes;
import com.tipu.agent.anticode.service.api.AnticodeService;
import com.tipu.agent.anticode.service.entity.model.AntiRuleRecord;
import com.tipu.agent.anticode.service.entity.model.Anticode;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jboot.utils.StringUtils;

@Bean
@Singleton
@JbootrpcService
public class AnticodeServiceImpl extends JbootServiceBase<Anticode> implements AnticodeService {

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());

	public static final String NUMBERCHAR = "0123456789";
	
	@Override
	public Page<Anticode> findPage(Anticode data, String configNo, int pageNumber, int pageSize) {
		Kv cond = Kv.create();
        cond.set("configNo", configNo);
        
        SqlPara sp = Db.getSqlPara("agent-anticode.findList", cond);
		
		return DAO.paginate(pageNumber, pageSize, sp);
//        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}
	
	@Override
	public void saveGenCodes(String configId, String configNo, Integer channel, String qrcodeUrl, String ruleId, String brandId,
			String productId, Integer bigStart, Integer bigEnd, Integer smallSize, Integer productCode,
			Integer brandCode, List<AntiRuleRecord> recordList) {
		String savePath = PathKit.getWebRootPath() + File.separator + "anticode";
		StringBuffer writer = new StringBuffer();
		File folder = new File(savePath);
		if(!folder.exists()) {
			folder.mkdirs();
		}
		String configPath = savePath + File.separator + configNo;
		folder = new File(configPath);
		if(folder.exists()) {
			return ;
		}
		String fileName = "anticode_" + productCode + "_" + channel + "_" + configNo + ".txt";
		try {
			FileWriter fw = new FileWriter(savePath + File.separator +fileName);
			
			List<Record> codes = new ArrayList<Record>();
			Date nowDate = new Date();
			// 处理防伪数据-----end
			for (int i = bigStart + 1; i <= bigEnd; i++) {
				String antiCode = randomCode();
				String logisCode = brandCode.toString() + productCode.toString();
				logisCode += addZero(bigEnd.toString().length(), i);
				Anticode newAnticode = new Anticode();
				String bigId = StringUtils.uuid();
				newAnticode.setId(bigId);
				newAnticode.setCreateDate(nowDate);
				newAnticode.setBrandId(brandId);
				newAnticode.setProductId(productId);
				newAnticode.setAntiRuleId(ruleId);
				newAnticode.setAntiCode(Encodes.entryptPassword(antiCode));
				newAnticode.setLogisticsCode(logisCode);
				newAnticode.setType(1);
				newAnticode.setConfigId(configId);
				codes.add(newAnticode.toRecord());
				StringBuilder bigBuilder = new StringBuilder();
				bigBuilder.append(logisCode).append(",").append(qrcodeUrl).append("?rand=").append(productCode).append("&id=").append(URLEncoder.encode(AESUtil.aesEncode(bigId + "_" + antiCode.substring(0, 12) + "_" + configNo, productCode), "UTF-8"));
				if(smallSize > 0) {
					for (int j = 1; j <= smallSize; j++) {
						String antiCodeSmall = randomCode();
						String logisCodeSmall = logisCode + "-" + addZero(smallSize.toString().length(), j);
						Anticode smallAnticode = new Anticode();
						String smallId = StringUtils.uuid();
						smallAnticode.setId(smallId);
						smallAnticode.setCreateDate(nowDate);
						smallAnticode.setBrandId(brandId);
						smallAnticode.setProductId(productId);
						smallAnticode.setAntiRuleId(ruleId);
						smallAnticode.setAntiCode(Encodes.entryptPassword(antiCodeSmall));
						smallAnticode.setLogisticsCode(logisCodeSmall);
						smallAnticode.setType(3);
						smallAnticode.setConfigId(configId);
						codes.add(smallAnticode.toRecord());
						writer.append(bigBuilder)
							.append(",").append(logisCodeSmall)
							.append(",").append(antiCodeSmall)
							.append(",").append(qrcodeUrl).append("?rand=").append(productCode).append("&id=").append(URLEncoder.encode(AESUtil.aesEncode(smallId + "_" + antiCodeSmall.substring(0, 12) + "_" + configNo, productCode), "UTF-8")).append("\r\n");
					}
				}
				logger.info("-------写入"+logisCode+"-------");
			}
			logger.info("---------刷新到文件---------");
			fw.write(writer.toString());
			fw.flush();
			fw.close();
			writer = null;
			logger.info("--------------防伪码数据文件生成完成----------");
			// 分批次插入，每次1万条，多线程执行
			if(codes.size() <= 10000) {
				batchSave(configNo, codes);
			} else {
				int num = codes.size() / 10000;
				int left = codes.size() % 10000;
				for(int i = 0; i<num; i++) {
//					List<Anticode> subCodes = codes.subList(i*10000, i*10000+10000);
					List<Record> subCodes = new ArrayList<Record>();
					for(int j=i*10000; j<i*10000+10000; j++) {
						subCodes.add(codes.get(j));
					}
					generateAntiCode(i, configNo, subCodes);
				}
				if(left > 0) {
//					List<Anticode> subCodes = codes.subList(codes.size() - left, codes.size());
					List<Record> subCodes = new ArrayList<Record>();
					for(int j=codes.size() - left; j<codes.size(); j++) {
						subCodes.add(codes.get(j));
					}
					generateAntiCode(num, configNo, subCodes);
				}
			}
			
			for (AntiRuleRecord record : recordList) {
				record.setStartBig(bigEnd);
				record.update();
			}
			
			logger.info("----------防伪码生成结束--------");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveNewTable(String configNo) {
		Db.update("create table anticode_"+configNo+" like anticode");
	}

	@Override
	public void saveAll(String configNo, List<Record> records) {
		Db.batchSave("anticode_"+configNo, records, records.size());
	}
	
	/**
	 * 生成16位随机数
	 * @return
	 */
	private String randomCode() {
		StringBuffer sb = new StringBuffer();  
        Random random = new Random();  
        for (int i = 0; i < 16; i++) {  
            sb.append(NUMBERCHAR.charAt(random.nextInt(NUMBERCHAR.length())));  
        }
        return sb.toString();
	}
	
	private String addZero(int size, Integer number) {
		String numberStr = number.toString();
		String result = "";
		for(int i=numberStr.length(); i<size; i++) {
			result += "0";
		}
		return result += numberStr;
	}
	
	/**
	 * 循环生成防伪码
	 */
	private void generateAntiCode(int count, String configNo, List<Record> anticodeList){
		try {
			Map<String, Object> eventParam = new HashMap<String, Object>();
			eventParam.put("count", count);
			eventParam.put("codes", anticodeList);
			eventParam.put("configNo", configNo);
			Jboot.sendEvent("anticodeGenEvent", eventParam);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * 批量保存
	 * @param anticodeList 防伪码
	 */
	private void batchSave(String configNo, final List<Record> anticodeList) {
		Db.batchSave("anticode_"+configNo, anticodeList, anticodeList.size());
	}

	@Override
	public Anticode findByEncodeId(String encodeId, Integer productCode) {
		logger.info("Anticode parse: [productCode" + productCode + "][id: " + encodeId + "]");
		if(null != productCode && StrKit.notBlank(encodeId)) {
			String[] contentArray = AESUtil.aesDecode(encodeId, productCode).split("_");
			Anticode anticode = new Anticode();
			anticode.setId(contentArray[0]);
			anticode.setAntiCode(contentArray[1]);
			anticode.setConfigNo(contentArray[2]);
			
			String configNo = contentArray[2];
			try {
				DateUtils.parseDate(configNo, "yyyyMMddHHmmss");
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
			Record record = Db.findById("anticode_"+contentArray[2], contentArray[0]);
			if(null != record) {
				anticode.setLogisticsCode(record.getStr("logistics_code"));
				anticode.setAgentId(record.getStr("agent_id"));
				anticode.setBrandId(record.getStr("brand_id"));
				anticode.setProductId(record.getStr("product_id"));
				return anticode;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public Anticode findByAnticode(String plainAnticode, String id, String configNo) {
		logger.info("Anticode check: [plainAnticode" + plainAnticode + "][id: " + id + "][configNo: " + configNo + "]");
		if(StrKit.notBlank(plainAnticode) && StrKit.notBlank(id) && StrKit.notBlank(configNo)) {
			try {
				DateUtils.parseDate(configNo, "yyyyMMddHHmmss");
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
			Anticode anticode = new Anticode();
			Record record = Db.findById("anticode_"+configNo, id);
			if(null != record && Encodes.validatePassword(plainAnticode, record.getStr("anti_code"))) {
				anticode.setLogisticsCode(record.getStr("logistics_code"));
				anticode.setAgentId(record.getStr("agent_id"));
				anticode.setId(record.getStr("id"));
				anticode.setProductId(record.getStr("product_id"));
				anticode.setBrandId(record.getStr("brand_id"));
				anticode.setConfigId(record.getStr("config_id"));
				anticode.setConfigNo(record.getStr("config_id"));
				anticode.setAntiRuleId(record.getStr("anti_rule_id"));
				anticode.setCreateDate(record.getDate("create_date"));
				anticode.setAntiCode(record.getStr("anti_code"));
				anticode.setType(record.getInt("type"));
				return anticode;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public Anticode findByLogisticsCode(String logisticsCode, String productId) {
		if(StrKit.notBlank(logisticsCode)) {
			String checkCode = logisticsCode;
			String configNo = "";
//			String productId = "";
//			if(logisticsCode.startsWith("11")) {
//				productId = "2057367439e54bd18382e0daf2a7bd2f";
//			} else if(logisticsCode.startsWith("12")) {
//				productId = "2f9d3b4be75547d6ab3106aec0e1ec5a";
//			} else if(logisticsCode.startsWith("13")) {
//				productId = "5e717a7d16a148d3bafb86156e832ffd";
//			} else if(logisticsCode.startsWith("14")) {
//				productId = "2e928ae4e49a40c8a4f51d5c96735d5b";
//			}
			if(logisticsCode.contains("-")) {
				// 小码
				checkCode = logisticsCode.split("-")[0].substring(2);
				Record record = Db.findFirst("select config_no from anticode_config where start_big<=? and end_big>=? and product_id=? limit 1", checkCode, checkCode, productId);
				if(null != record) {
					configNo = record.getStr("config_no");
				}
			} else {
				// 箱码
				checkCode = logisticsCode.substring(2);
				Record record = Db.findFirst("select config_no from anticode_config where start_big<=? and end_big>=? and product_id=? limit 1", checkCode, checkCode, productId);
				if(null != record) {
					configNo = record.getStr("config_no");
				}
			}
			if(StrKit.notBlank(configNo)) {
				Anticode anticode = new Anticode();
				Record record = Db.findFirst("select * from anticode_"+configNo + " where logistics_code=? limit 1", logisticsCode);
				if(null != record) {
					anticode.setLogisticsCode(record.getStr("logistics_code"));
					anticode.setAgentId(record.getStr("agent_id"));
					anticode.setId(record.getStr("id"));
					anticode.setProductId(record.getStr("product_id"));
					anticode.setBrandId(record.getStr("brand_id"));
					anticode.setConfigId(record.getStr("config_id"));
					anticode.setConfigNo(record.getStr("config_id"));
					anticode.setAntiRuleId(record.getStr("anti_rule_id"));
					anticode.setCreateDate(record.getDate("create_date"));
					anticode.setAntiCode(record.getStr("anti_code"));
					anticode.setType(record.getInt("type"));
					return anticode;
				} else {
					return null;
				}
			} else {
				return null;
			}
			
			
		}
		return null;
	}

	@Override
	public void updateDelivery(Map<String, String> codeProductMap, String agentId) {
		if(StrKit.notBlank(agentId)) {
			Set<String> logisCodes = codeProductMap.keySet();
			for (String code : logisCodes) {
				String checkCode = code;
				String productId = codeProductMap.get(code);
//				if(code.startsWith("11")) {
//					productId = "2057367439e54bd18382e0daf2a7bd2f";
//				} else if(code.startsWith("12")) {
//					productId = "2f9d3b4be75547d6ab3106aec0e1ec5a";
//				} else if(code.startsWith("13")) {
//					productId = "5e717a7d16a148d3bafb86156e832ffd";
//				} else if(code.startsWith("14")) {
//					productId = "2e928ae4e49a40c8a4f51d5c96735d5b";
//				}
				if(code.contains("-")) {
					// 小码
					checkCode = code.split("-")[0];
					checkCode = code.split("-")[0].substring(2);
					Record record = Db.findFirst("select config_no from anticode_config where start_big<=? and end_big>=? and product_id=? limit 1", checkCode, checkCode, productId);
					if(null != record) {
						String configNo = record.getStr("config_no");
						// 更新小码对应的代理信息
						Db.update("update anticode_"+configNo+" set agent_id=? where logistics_code=?", agentId, code);
					}
				} else {
					// 箱码
					checkCode = code.substring(2);
					Record record = Db.findFirst("select config_no from anticode_config where start_big<=? and end_big>=? and product_id=? limit 1", checkCode, checkCode, productId);
					if(null != record) {
						String configNo = record.getStr("config_no");
						// 更新小码对应的代理信息
						Object [] para = new Object[]{agentId, code, code + "-%"};
						Db.update("update anticode_"+configNo+" set agent_id=? where logistics_code=? or logistics_code like ?", para);
					}
				}
			}
		}
	}
	
	@Cacheable(name = CacheKey.CACHE_ANTICODE)
	@Override
	public List<Anticode> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<Anticode> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_ANTICODE);
    }

	@Override
	public Anticode findByAnticodeAndConfig(String configNo, String anticode) {
		logger.info("Anticode check: [anticode: " + anticode + "][configNo: " + configNo + "]");
		if(StrKit.notBlank(anticode) && StrKit.notBlank(configNo)) {
			if(configNo.length() == 14) {
				try {
					DateUtils.parseDate(configNo, "yyyyMMddHHmmss");
				} catch (ParseException e) {
					e.printStackTrace();
					return null;
				}
			}
			Anticode entity = new Anticode();
			Record record = Db.findFirst("select * from anticode_"+configNo + " where anti_code=?", anticode);
			if(null != record) {
				entity.setLogisticsCode(record.getStr("logistics_code"));
				entity.setAgentId(record.getStr("agent_id"));
				entity.setId(record.getStr("id"));
				entity.setProductId(record.getStr("product_id"));
				entity.setBrandId(record.getStr("brand_id"));
//				entity.setConfigId(record.getStr("config_id"));
				entity.setConfigNo(record.getStr("config_id"));
				entity.setAntiRuleId(record.getStr("anti_rule_id"));
				entity.setCreateDate(record.getDate("create_date"));
				entity.setAntiCode(record.getStr("anti_code"));
				entity.setType(record.getInt("type"));
				return entity;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

}