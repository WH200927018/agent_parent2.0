package com.tipu.agent.anticode.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.anticode.service.api.AntiRuleRecordService;
import com.tipu.agent.anticode.service.entity.model.AntiRuleRecord;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AntiRuleRecordServiceImpl extends JbootServiceBase<AntiRuleRecord> implements AntiRuleRecordService {

	@Override
	public List<AntiRuleRecord> findList(AntiRuleRecord ruleRecord) {
		Columns columns = Columns.create();
		if (StrKit.notBlank(ruleRecord.getProductId())) {
            columns.eq("product_id", ruleRecord.getProductId());
        }
		if (StrKit.notBlank(ruleRecord.getBrandId())) {
            columns.eq("brand_id", ruleRecord.getBrandId());
        }
		if (StrKit.notBlank(ruleRecord.getAntiRuleId())) {
            columns.eq("anti_rule_id", ruleRecord.getAntiRuleId());
        }
		columns.eq("del_flag", "0");
		return DAO.findListByColumns(columns);
	}
	
	@Override
	public Page<AntiRuleRecord> findPage(AntiRuleRecord data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();

        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}

	@Cacheable(name = CacheKey.CACHE_ANTIRULERECORD)
	@Override
	public List<AntiRuleRecord> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AntiRuleRecord> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_ANTIRULERECORD);
    }
}