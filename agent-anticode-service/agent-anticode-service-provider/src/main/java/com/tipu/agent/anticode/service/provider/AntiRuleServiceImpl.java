package com.tipu.agent.anticode.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.anticode.service.api.AntiRuleService;
import com.tipu.agent.anticode.service.entity.model.AntiRule;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AntiRuleServiceImpl extends JbootServiceBase<AntiRule> implements AntiRuleService {

	@Override
	public Page<AntiRule> findPage(AntiRule data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();

        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}
	
	@Cacheable(name = CacheKey.CACHE_ANTIRULE)
	@Override
	public List<AntiRule> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AntiRule> list =  DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_ANTIRULE);
    }

}