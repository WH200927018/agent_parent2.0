package com.tipu.agent.anticode.service.provider;

import java.util.List;

import javax.inject.Singleton;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.anticode.service.api.AnticodeSearchLogService;
import com.tipu.agent.anticode.service.entity.model.AnticodeSearchLog;

import io.jboot.Jboot;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.cache.annotation.Cacheable;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;

@Bean
@Singleton
@JbootrpcService
public class AnticodeSearchLogServiceImpl extends JbootServiceBase<AnticodeSearchLog> implements AnticodeSearchLogService {

	@Override
	public Page<AnticodeSearchLog> findPage(AnticodeSearchLog data, int pageNumber, int pageSize) {
		Columns columns = Columns.create();

		if(data.getCreateDate() != null) {
			columns.ge("create_date", data.getCreateDate());
		}
		
		if(data.getUpdateDate() != null) {
			columns.le("create_date", data.getUpdateDate());
		}
		
		if(StrKit.notBlank(data.getProductId())) {
        	columns.eq("product_id", data.getProductId());
        }
		if(StrKit.notBlank(data.getMobile())) {
			columns.eq("mobile", data.getMobile());
        }
        
        if(StrKit.notBlank(data.getLogisticsCode())) {
        	columns.eq("logistics_code", data.getLogisticsCode());
        }
        
        if(StrKit.notBlank(data.getAnticodeContent())) {
        	columns.eq("anticode_content", data.getAnticodeContent());
        }
        
        if(data.getSellerChannel() != null) {
        	columns.eq("seller_channel", data.getSellerChannel());
        }
        
        if(data.getFirstFake() != null) {
        	columns.eq("first_fake", data.getFirstFake());
        }
        
        if(data.getFake() != null) {
        	columns.eq("fake", data.getFake());
        }
		
        return DAO.paginateByColumns(pageNumber, pageSize, columns.getList());
	}
	
	@Cacheable(name = CacheKey.CACHE_ANTICODESEARCHLOG)
	@Override
	public List<AnticodeSearchLog> findAll(){
		Columns columns = Columns.create();
		columns.eq("del_flag", "0");
		List<AnticodeSearchLog> list = 	DAO.findListByColumns(columns);
		return list;
	}
	
	@Override
    public void refreshCache() {
        Jboot.me().getCache().removeAll(CacheKey.CACHE_ANTICODESEARCHLOG);
    }

}