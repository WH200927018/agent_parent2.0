package com.tipu.agent.anticode.service.codegen.provider;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.anticode.service.codegen.api.AnticodeGenService;

import io.jboot.event.JbootEvent;
import io.jboot.event.JbootEventListener;
import io.jboot.event.annotation.EventConfig;

/**
 * 防伪码生成事件
 * @author think
 *
 */
@EventConfig(action = {"anticodeGenEvent"}, async = true)
public class AnticodeGenListener implements JbootEventListener {

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	private AnticodeGenService anticodeService;
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(JbootEvent event) {
		Map<String, Object> data = event.getData();
		List<Record> codes = (List<Record>) data.get("codes");
		logger.info("开始保存第" + data.get("count") + "万条记录!");
		anticodeService.saveAll(data.get("configNo").toString(), codes);
		logger.info("结束保存第" + data.get("count") + "万条记录!");
		
	}

}
