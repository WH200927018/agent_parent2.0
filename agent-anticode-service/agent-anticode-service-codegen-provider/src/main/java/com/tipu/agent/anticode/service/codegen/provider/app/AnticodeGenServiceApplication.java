package com.tipu.agent.anticode.service.codegen.provider.app;

import io.jboot.Jboot;

/**
 * 服务启动入口
 * @author Rlax
 *
 */
public class AnticodeGenServiceApplication {
    public static void main(String [] args){
        Jboot.run(args);
    }
}
