package com.tipu.agent.anticode.service.vccodegen.provider;


import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;

import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.admin.base.util.AESUtil;
import com.tipu.agent.admin.base.util.Encodes;
import com.tipu.agent.anticode.service.codegen.api.AnticodeGenService;
import com.tipu.agent.anticode.service.entity.model.AntiRuleRecord;
import com.tipu.agent.anticode.service.entity.model.Anticode;
import io.jboot.service.JbootServiceBase;
import io.jboot.utils.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Bean
@Singleton
@JbootrpcService
public class AnticodeServiceImpl extends JbootServiceBase<Anticode> implements AnticodeGenService {

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());

	public static final String NUMBERCHAR = "0123456789";
	
	@Override
	public void saveGenCodes(String configId, String configNo, String productSn, Integer codeLength, Integer channel, String qrcodeUrl, String ruleId, String brandId,
			String productId, Integer bigStart, Integer bigEnd, Integer smallSize, Integer productCode,
			Integer brandCode, List<AntiRuleRecord> recordList) {
		String savePath = PathKit.getWebRootPath() + File.separator + "anticode";
//		StringBuffer writer = new StringBuffer();
		File folder = new File(savePath);
		if(!folder.exists()) {
			folder.mkdirs();
		}
		String configPath = savePath + File.separator + configNo;
		folder = new File(configPath);
		if(!folder.exists()) {
			folder.mkdirs();
		}  else {
			return ;
		}
		String fileName = "anticode_" + productCode + "_" + channel + "_" + configNo + ".txt";
		try {
			FileWriter fw = new FileWriter(savePath + File.separator +fileName);
			BufferedWriter bufferedWriter = new BufferedWriter(fw);
			List<Record> codes = new ArrayList<Record>();
			Date nowDate = new Date();
			// 处理防伪数据-----end
			for (int i = bigStart + 1; i <= bigEnd; i++) {
				String antiCode = randomCode();
				String logisCode = brandCode.toString() + productCode.toString();
				logisCode += addZero(bigEnd.toString().length(), i);
				Anticode newAnticode = new Anticode();
				String bigId = StringUtils.uuid();
				newAnticode.setId(bigId);
				newAnticode.setCreateDate(nowDate);
				newAnticode.setBrandId(brandId);
				newAnticode.setProductId(productId);
				newAnticode.setAntiRuleId(ruleId);
				newAnticode.setAntiCode(antiCode);
				newAnticode.setLogisticsCode(logisCode);
				newAnticode.setType(1);
				newAnticode.setConfigId(configId);
				codes.add(newAnticode.toRecord());
				StringBuilder bigBuilder = new StringBuilder();
				bigBuilder.append(qrcodeUrl).append("?configNo=").append(configNo).append("&no=").append(antiCode);
				if(smallSize > 0) {
					for (int j = 1; j <= smallSize; j++) {
						String antiCodeSmall = randomCode();
						String logisCodeSmall = logisCode + "-" + addZero(smallSize.toString().length(), j);
						Anticode smallAnticode = new Anticode();
						String smallId = StringUtils.uuid();
						smallAnticode.setId(smallId);
						smallAnticode.setCreateDate(nowDate);
						smallAnticode.setBrandId(brandId);
						smallAnticode.setProductId(productId);
						smallAnticode.setAntiRuleId(ruleId);
						smallAnticode.setAntiCode(antiCodeSmall);
						smallAnticode.setLogisticsCode(logisCodeSmall);
						smallAnticode.setType(3);
						smallAnticode.setConfigId(configId);
						codes.add(smallAnticode.toRecord());
						
						StringBuilder lineBuilder = new StringBuilder(bigBuilder);
						
						bufferedWriter.write(lineBuilder.append(",").append(qrcodeUrl).append("?configNo=").append(configNo).append("&no=").append(antiCodeSmall).append("\r\n").toString());
					}
				}
				logger.info("-------写入"+logisCode+"-------");
			}
			logger.info("---------刷新到文件---------");
//			fw.write(writer.toString());
			bufferedWriter.flush();
			fw.close();
			bufferedWriter.close();
//			writer = null;
			logger.info("--------------防伪码数据文件生成完成----------");
			// 分批次插入，每次1万条，多线程执行
			if(codes.size() <= 10000) {
				batchSave(configNo, codes);
			} else {
				int num = codes.size() / 10000;
				int left = codes.size() % 10000;
				for(int i = 0; i<num; i++) {
					List<Record> subCodes = codes.subList(i*10000, i*10000+10000);
//					List<Record> subCodes = new ArrayList<Record>();
//					for(int j=i*10000; j<i*10000+10000; j++) {
//						subCodes.add(codes.get(j));
//					}
					generateAntiCode(i, configNo, subCodes);
				}
				if(left > 0) {
					List<Record> subCodes = codes.subList(codes.size() - left, codes.size());
//					List<Record> subCodes = new ArrayList<Record>();
//					for(int j=codes.size() - left; j<codes.size(); j++) {
//						subCodes.add(codes.get(j));
//					}
					generateAntiCode(num, configNo, subCodes);
				}
			}
			
			for (AntiRuleRecord record : recordList) {
				record.setStartBig(bigEnd);
				record.setUpdateDate(new Date());
//				record.update();
				Db.update("anti_rule_record", record.toRecord());
			}
			
			logger.info("----------防伪码生成结束--------");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveAll(String configNo, List<Record> records) {
		Db.batchSave("anticode_"+configNo, records, records.size());
	}
	
	/**
	 * 生成16位随机数
	 * @return
	 */
	private String randomCode() {
		StringBuffer sb = new StringBuffer("1657");  
        Random random = new Random();
        for (int i = 0; i < 8; i++) {  
            sb.append(NUMBERCHAR.charAt(random.nextInt(NUMBERCHAR.length())));  
        }
        return sb.toString();
	}
	
	private String addZero(int size, Integer number) {
		String numberStr = number.toString();
		String result = "";
		for(int i=numberStr.length(); i<size; i++) {
			result += "0";
		}
		return result += numberStr;
	}
	
	/**
	 * 循环生成防伪码
	 */
	private void generateAntiCode(int count, String configNo, List<Record> anticodeList){
//		try {
//			Map<String, Object> eventParam = new HashMap<String, Object>();
//			eventParam.put("count", count);
//			eventParam.put("codes", anticodeList);
//			eventParam.put("configNo", configNo);
//			Jboot.sendEvent("anticodeGenEvent", eventParam);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		logger.info("开始保存第" + count + "万条记录!");
		Db.batchSave("anticode_"+configNo, anticodeList, anticodeList.size());
		logger.info("结束保存第" + count + "万条记录!");
	}

	/**
	 * 批量保存
	 * @param anticodeList 防伪码
	 */
	private void batchSave(String configNo, final List<Record> anticodeList) {
		Db.batchSave("anticode_"+configNo, anticodeList, anticodeList.size());
	}

}