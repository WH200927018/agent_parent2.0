package com.tipu.agent.anticode.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.anticode.service.entity.model.AntiRuleRecord;
import com.tipu.agent.anticode.service.entity.model.Anticode;

import java.util.List;
import java.util.Map;

public interface AnticodeService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param configNo 配置批号
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<Anticode> findPage(Anticode data, String configNo, int pageNumber, int pageSize);
	
	/**
	 * 保存新表
	 * @param configNo 批次号
	 */
	public void saveNewTable(String configNo);
	
	/**
	 * 生成防伪码
	 * @param configId 批次ID
	 * @param configNo 批号
	 * @param channel 渠道
	 * @param savePath 保存路径
	 * @param qrcodeUrl 二维码地址路径
	 * @param ruleId 规则ID
	 * @param brandId 品牌ID
	 * @param productId 产品ID
	 * @param bigStart 大标起始值
	 * @param bigEnd 大标结束值
	 * @param smallSize 小标个数
	 * @param productCode 产品编码
	 * @param brandCode 品牌编码
	 * @param recordList 记录列表
	 */
	public void saveGenCodes(String configId, String configNo, Integer channel, String qrcodeUrl, String ruleId, String brandId, String productId, Integer bigStart, Integer bigEnd, Integer smallSize, Integer productCode, Integer brandCode, List<AntiRuleRecord> recordList);
	
	/**
	 * 批量保存
	 * @param configNo
	 * @param anticodeList
	 */
	public void saveAll(String configNo, List<Record> anticodeList);
	
	/**
	 * 根据编码后的ID查找
	 * @param encodeId
	 * @param productCode
	 */
	public Anticode findByEncodeId(String encodeId, Integer productCode);
	
	/**
	 * 根据防伪码查找
	 * @param plainAnticode 明文防伪码
	 * @param id ID
	 * @param configNo 批号
	 */
	public Anticode findByAnticode(String plainAnticode, String id, String configNo);
	
	/**
	 * 根据批号和物流码查找防伪码信息
	 * @param logisticsCode 物流码
	 * @param productId 产品ID
	 * @return
	 */
	public Anticode findByLogisticsCode(String logisticsCode, String productId);
	
	/**
	 * 批号
	 * @param configNo 批号
	 * @param anticode 防伪码
	 * @return
	 */
	public Anticode findByAnticodeAndConfig(String configNo, String anticode);
	
	/**
	 * 更新发货信息
	 * @param codeProductMap 物流码对应的map
	 * @param agentId 代理ID
	 */
	public void updateDelivery(Map<String, String> codeProductMap, String agentId);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Anticode findById(Object id);


    /**
     * find all model
     *
     * @return all <Anticode
     */
    public List<Anticode> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Anticode model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(Anticode model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(Anticode model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Anticode model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

	public void refreshCache();

}