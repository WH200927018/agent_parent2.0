package com.tipu.agent.anticode.service.api;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.anticode.service.entity.model.AnticodeConfig;

import java.util.List;

public interface AnticodeConfigService  {

	/**
	 * 分页查询
	 * @param data 查询参数对象
	 * @param pageNumber 页数
	 * @param pageSize 分页大小
	 * @return 分页
	 */
	public Page<AnticodeConfig> findPage(AnticodeConfig data, int pageNumber, int pageSize);

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public AnticodeConfig findById(Object id);
    
    /**
     * 根据物流码查找防伪配置
     * @param logisticsCode 物流码
     * @return
     */
    public AnticodeConfig findByLogisticsCode(String logisticsCode);


    /**
     * find all model
     *
     * @return all <AnticodeConfig
     */
    public List<AnticodeConfig> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(AnticodeConfig model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(AnticodeConfig model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(AnticodeConfig model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(AnticodeConfig model);


    public void join(Page<? extends Model> page, String joinOnField);
    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);
    public void join(Page<? extends Model> page, String joinOnField, String joinName);
    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField);
    public void join(List<? extends Model> models, String joinOnField, String[] attrs);
    public void join(List<? extends Model> models, String joinOnField, String joinName);
    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);
    public void join(Model model, String joinOnField);
    public void join(Model model, String joinOnField, String[] attrs);
    public void join(Model model, String joinOnField, String joinName);
    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);
    public void keep(List<? extends Model> models, String... attrs);

	public void refreshCache();
}