package com.tipu.agent.anticode.service.entity.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAnticodeSearchLog<M extends BaseAnticodeSearchLog<M>> extends JbootModel<M> implements IBean {

	public void setId(java.lang.String id) {
		set("id", id);
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public void setCreateBy(java.lang.String createBy) {
		set("create_by", createBy);
	}
	
	public java.lang.String getCreateBy() {
		return getStr("create_by");
	}

	public void setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public void setUpdateBy(java.lang.String updateBy) {
		set("update_by", updateBy);
	}
	
	public java.lang.String getUpdateBy() {
		return getStr("update_by");
	}

	public void setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
	}
	
	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public void setProductId(java.lang.String productId) {
		set("product_id", productId);
	}
	
	public java.lang.String getProductId() {
		return getStr("product_id");
	}

	public void setAnticodeContent(java.lang.String anticodeContent) {
		set("anticode_content", anticodeContent);
	}
	
	public java.lang.String getAnticodeContent() {
		return getStr("anticode_content");
	}

	public void setLogisticsCode(java.lang.String logisticsCode) {
		set("logistics_code", logisticsCode);
	}
	
	public java.lang.String getLogisticsCode() {
		return getStr("logistics_code");
	}

	public void setIp(java.lang.String ip) {
		set("ip", ip);
	}
	
	public java.lang.String getIp() {
		return getStr("ip");
	}

	public void setLat(java.lang.String lat) {
		set("lat", lat);
	}
	
	public java.lang.String getLat() {
		return getStr("lat");
	}

	public void setLng(java.lang.String lng) {
		set("lng", lng);
	}
	
	public java.lang.String getLng() {
		return getStr("lng");
	}

	public void setAddress(java.lang.String address) {
		set("address", address);
	}
	
	public java.lang.String getAddress() {
		return getStr("address");
	}

	public void setFormatAddress(java.lang.String formatAddress) {
		set("format_address", formatAddress);
	}
	
	public java.lang.String getFormatAddress() {
		return getStr("format_address");
	}

	public void setNation(java.lang.String nation) {
		set("nation", nation);
	}
	
	public java.lang.String getNation() {
		return getStr("nation");
	}

	public void setProvince(java.lang.String province) {
		set("province", province);
	}
	
	public java.lang.String getProvince() {
		return getStr("province");
	}

	public void setCity(java.lang.String city) {
		set("city", city);
	}
	
	public java.lang.String getCity() {
		return getStr("city");
	}

	public void setDistrict(java.lang.String district) {
		set("district", district);
	}
	
	public java.lang.String getDistrict() {
		return getStr("district");
	}

	public void setStreet(java.lang.String street) {
		set("street", street);
	}
	
	public java.lang.String getStreet() {
		return getStr("street");
	}

	public void setStreetNumber(java.lang.String streetNumber) {
		set("street_number", streetNumber);
	}
	
	public java.lang.String getStreetNumber() {
		return getStr("street_number");
	}

	public void setMobile(java.lang.String mobile) {
		set("mobile", mobile);
	}
	
	public java.lang.String getMobile() {
		return getStr("mobile");
	}

	public void setMsg(java.lang.String msg) {
		set("msg", msg);
	}
	
	public java.lang.String getMsg() {
		return getStr("msg");
	}

	public void setVerifyCode(java.lang.String verifyCode) {
		set("verify_code", verifyCode);
	}
	
	public java.lang.String getVerifyCode() {
		return getStr("verify_code");
	}

	public void setVerifyDate(java.util.Date verifyDate) {
		set("verify_date", verifyDate);
	}
	
	public java.util.Date getVerifyDate() {
		return get("verify_date");
	}

	public void setFirstFake(java.lang.Integer firstFake) {
		set("first_fake", firstFake);
	}
	
	public java.lang.Integer getFirstFake() {
		return getInt("first_fake");
	}

	public void setFake(java.lang.Integer fake) {
		set("fake", fake);
	}
	
	public java.lang.Integer getFake() {
		return getInt("fake");
	}

	public void setHasReply(java.lang.Integer hasReply) {
		set("has_reply", hasReply);
	}
	
	public java.lang.Integer getHasReply() {
		return getInt("has_reply");
	}

	public void setReplyDate(java.util.Date replyDate) {
		set("reply_date", replyDate);
	}
	
	public java.util.Date getReplyDate() {
		return get("reply_date");
	}

	public void setReplyContent(java.lang.String replyContent) {
		set("reply_content", replyContent);
	}
	
	public java.lang.String getReplyContent() {
		return getStr("reply_content");
	}

	public void setReplyCount(java.lang.Integer replyCount) {
		set("reply_count", replyCount);
	}
	
	public java.lang.Integer getReplyCount() {
		return getInt("reply_count");
	}

	public void setSellerChannel(java.lang.Integer sellerChannel) {
		set("seller_channel", sellerChannel);
	}
	
	public java.lang.Integer getSellerChannel() {
		return getInt("seller_channel");
	}

	public void setSellerName(java.lang.String sellerName) {
		set("seller_name", sellerName);
	}
	
	public java.lang.String getSellerName() {
		return getStr("seller_name");
	}

	public void setSellerWechat(java.lang.String sellerWechat) {
		set("seller_wechat", sellerWechat);
	}
	
	public java.lang.String getSellerWechat() {
		return getStr("seller_wechat");
	}

	public void setSellerMobile(java.lang.String sellerMobile) {
		set("seller_mobile", sellerMobile);
	}
	
	public java.lang.String getSellerMobile() {
		return getStr("seller_mobile");
	}

	public void setSellerLink(java.lang.String sellerLink) {
		set("seller_link", sellerLink);
	}
	
	public java.lang.String getSellerLink() {
		return getStr("seller_link");
	}

	public void setCityData(java.lang.String cityData) {
		set("city_data", cityData);
	}
	
	public java.lang.String getCityData() {
		return getStr("city_data");
	}

	public void setRemarks(java.lang.String remarks) {
		set("remarks", remarks);
	}
	
	public java.lang.String getRemarks() {
		return getStr("remarks");
	}

	public void setDelFlag(java.lang.String delFlag) {
		set("del_flag", delFlag);
	}
	
	public java.lang.String getDelFlag() {
		return getStr("del_flag");
	}

}
