package com.tipu.agent.anticode.service.entity.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAnticode<M extends BaseAnticode<M>> extends JbootModel<M> implements IBean {

	public void setId(java.lang.String id) {
		set("id", id);
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public void setCreateBy(java.lang.String createBy) {
		set("create_by", createBy);
	}
	
	public java.lang.String getCreateBy() {
		return getStr("create_by");
	}

	public void setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
	}
	
	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public void setUpdateBy(java.lang.String updateBy) {
		set("update_by", updateBy);
	}
	
	public java.lang.String getUpdateBy() {
		return getStr("update_by");
	}

	public void setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
	}
	
	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public void setConfigId(java.lang.String configId) {
		set("config_id", configId);
	}
	
	public java.lang.String getConfigId() {
		return getStr("config_id");
	}

	public void setBrandId(java.lang.String brandId) {
		set("brand_id", brandId);
	}
	
	public java.lang.String getBrandId() {
		return getStr("brand_id");
	}

	public void setProductId(java.lang.String productId) {
		set("product_id", productId);
	}
	
	public java.lang.String getProductId() {
		return getStr("product_id");
	}

	public void setAntiRuleId(java.lang.String antiRuleId) {
		set("anti_rule_id", antiRuleId);
	}
	
	public java.lang.String getAntiRuleId() {
		return getStr("anti_rule_id");
	}

	public void setAntiCode(java.lang.String antiCode) {
		set("anti_code", antiCode);
	}
	
	public java.lang.String getAntiCode() {
		return getStr("anti_code");
	}

	public void setLogisticsCode(java.lang.String logisticsCode) {
		set("logistics_code", logisticsCode);
	}
	
	public java.lang.String getLogisticsCode() {
		return getStr("logistics_code");
	}

	public void setAgentId(java.lang.String agentId) {
		set("agent_id", agentId);
	}
	
	public java.lang.String getAgentId() {
		return getStr("agent_id");
	}

	public void setType(java.lang.Integer type) {
		set("type", type);
	}
	
	public java.lang.Integer getType() {
		return getInt("type");
	}

	public void setRemarks(java.lang.String remarks) {
		set("remarks", remarks);
	}
	
	public java.lang.String getRemarks() {
		return getStr("remarks");
	}

	public void setDelFlag(java.lang.String delFlag) {
		set("del_flag", delFlag);
	}
	
	public java.lang.String getDelFlag() {
		return getStr("del_flag");
	}

}
