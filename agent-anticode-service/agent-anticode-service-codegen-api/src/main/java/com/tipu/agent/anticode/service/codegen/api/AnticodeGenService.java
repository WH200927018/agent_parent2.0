package com.tipu.agent.anticode.service.codegen.api;

import java.util.List;

import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.anticode.service.entity.model.AntiRuleRecord;

public interface AnticodeGenService  {

	/**
	 * 生成防伪码
	 * @param configId 批次ID
	 * @param configNo 批号
	 * @param productSn 产品序列号
	 * @param codeLength 防伪码长度
	 * @param channel 渠道
	 * @param savePath 保存路径
	 * @param qrcodeUrl 二维码地址路径
	 * @param ruleId 规则ID
	 * @param brandId 品牌ID
	 * @param productId 产品ID
	 * @param bigStart 大标起始值
	 * @param bigEnd 大标结束值
	 * @param smallSize 小标个数
	 * @param productCode 产品编码
	 * @param brandCode 品牌编码
	 * @param recordList 记录列表
	 */
	public void saveGenCodes(String configId, String configNo, String productSn, Integer codeLength, Integer channel, String qrcodeUrl, String ruleId, String brandId, String productId, Integer bigStart, Integer bigEnd, Integer smallSize, Integer productCode, Integer brandCode, List<AntiRuleRecord> recordList);
	
	/**
	 * 批量保存
	 * @param configNo
	 * @param anticodeList
	 */
	public void saveAll(String configNo, List<Record> anticodeList);

}