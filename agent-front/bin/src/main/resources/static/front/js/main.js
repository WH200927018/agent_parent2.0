// JavaScript Document
$(document).ready(function(){
	$(".case_list p a:last-child").css("margin-right","0");
	$(".case_type p:nth-child(2)").css("margin-bottom","59px");
	
	
	//导航
	$(".case_type p a").click(function(){
	  $(".case_type p a").removeClass("on");
		$(this).addClass("on");
	});
	
	(function(){
	  var time = null;
	  var list = $("#navlist");
	  var box = $("#navbox");
	  var lista = list.find("a");
	  
	  for(var i=0,j=lista.length;i<j;i++){
		  if(lista[i].className == "now"){
			  var olda = i;
		  }
	  }
	  
	  var box_show = function(hei){
		  box.stop().animate({
			  height:hei,
			  opacity:1
		  },500);
	  }
	  
	  var box_hide = function(){
		  box.stop().animate({
			  height:0,
			  opacity:1
		  },500);
	  }
	  
	  lista.hover(function(){
		  lista.removeClass("now");
		  $(this).addClass("now");
		  clearTimeout(time);
		  var index = list.find("a").index($(this));
		  box.find(".cont").hide().eq(index).show();
		  var _height = box.find(".cont").eq(index).height();
		  box_show(_height)
	  },function(){
		  time = setTimeout(function(){	
			  box.find(".cont").hide();
			  box_hide();
		  },50);
		  lista.removeClass("now");
		  lista.eq(olda).addClass("now");
	  });
	  
	  box.find(".cont").hover(function(){
		  var _index = box.find(".cont").index($(this));
		  lista.removeClass("now");
		  lista.eq(_index).addClass("now");
		  clearTimeout(time);
		  $(this).show();
		  var _height = $(this).height();
		  box_show(_height);
	  },function(){
		  time = setTimeout(function(){		
			  $(this).hide();
			  box_hide();
		  },50);
		  lista.removeClass("now");
		  lista.eq(olda).addClass("now");
	  });
  
  })();
	

	//幻灯片
	jQuery(".banner").slide({mainCell:".bd ul",effect:"fold",autoPlay:true,delayTime:700});
	
	
	
});