package com.tipu.agent.front.utils;

public class ErrorConst {
	//登录错误 没有此账号
	public final static String LOGIN_NOT_ACCOUNT = "100000";
	//登录错误 账号状态异常
	public final static String LOGIN_ACCOUNT_STATE_EXCEPT= "100001";
	//登录错误 账号未来授权登录
	public final static String LOGIN_ACCOUNT_NOT_AUTH= "100002";
	//登录错误 账号密码不正确
	public final static String LOGIN_PWD_ERROR= "100003";
	//登录超時
	public final static String LOGIN_TIME_OUT= "100004";
	//token不存在
	public final static String LOGIN_TOKEN_NOT_EXISTS= "100005";
	//库存不足
	public static final String SHOP_UNDERSTOCL = "100006";
	//货款余额不足
	public static final String SHOP_PAYMENT_INSUFFICIENT = "100007";
	//代理商已存在
	public static final String APPLY_AGENT_EXISTS = "100008";
	//代理商已存在
	public static final String APPLY_AGENT_BRAND_REG_EXISTS = "100009";
	//验证码为空
	public static final String APPLY_CODE_IS_NULL = "100010";
	//验证码不正确
	public static final String APPLY_CODE_ERROR = "100011";
	//更新失败
	public static final String APPLY_EXTENDS_MOD_ERROR = "100012";
	//两次密码不一致
	//未找到对应代理信息
	public static final String APPLY_IS_NOT_AGENT = "100013";
	public static final String MY_UPDATEPWD_INCONFORMITY = "100013";
	//账户状态异常 没加入任何品牌
	public static final String LOGIN_IS_NOT_BRAND = "100014";
	
	
	
	//系统更新异常
	public static final String SERVICE_ADD_EXCEPTION = "600001";
	public static final String SERVICE_MOD_EXCEPTION = "600002";
	public static final String SERVICE_DEL_EXCEPTION = "600003";
	public static final String SERVICE_SELECT_EXCEPTION = "600004";

} 
