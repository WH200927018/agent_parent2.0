package com.tipu.agent.front.controller.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.Ret;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.util.IpKit;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.front.service.api.AdFieldService;
import com.tipu.agent.front.service.api.AdItemService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.entity.model.AdField;
import com.tipu.agent.front.service.entity.model.AdItem;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Product;
import com.tipu.agent.front.service.entity.model.ProductType;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * index首页
 * 
 * @author hulin
 *
 */
@RequestMapping("/")
public class IndexController extends BaseController {

	// 前台底部导航传值，常量
	private static String classHidden = "";

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AdItemService adItemService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AdFieldService adFieldService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;

	/**
	 * index
	 */
	public void index() {
		List<AdItem> adItemList = adItemService.findAll();// 轮播图片
//		List<ProductType> productTypeList = productTypeService.findAllBySort();// 商品类型
		AdField adField = new AdField();
		adField.setIsShow("1");
		adField.setPosType("1");
		List<AdField> adFieldList = adFieldService.findList(adField);// 栏目
		List<AdField> adFieldItemList = adFieldService.findShowAdItem(adField);
		if(adFieldList.size()>0){//去除没有轮播图片的栏目
			for(int i=adFieldList.size()-1;i>=0;i--){
				int count=0;
				for(AdItem adItem:adItemList){
					if(adFieldList.get(i).getId().equals(adItem.getAdField())){
						count++;
					}
				}
				if(count==0){
					adFieldList.remove(i);
				}
			}
		}
		
		
		Product product = new Product();
		product.setIsTop("1");
		List<Product> productList = productService.findList(product);
		setAttr("productList", productList);
		setAttr("adItemList", adItemList);
		setAttr("adFieldItemList", adFieldItemList);
//		setAttr("productTypeList", productTypeList);
		setAttr("adFieldList",adFieldList);
		render("index.html");
	}
	 

	/**
	 * 传值（前台底部是公共模块，每次点击底部的时候就要让那个模块高亮显示） 首页：1 产品：2 查询：3 代理商：4
	 * 
	 * @return
	 */
	public void pathValue(String id) {
		classHidden = "";
		Ret ret = new Ret();
		ret.set("msg", id);
		classHidden = id;
		renderJson(ret);
	}

	/**
	 * ajax异步请求值 如果点击首页，从常量里获取1，保持首页的字体高亮显示 如果点击产品，从常量里获取2，保持产品的字体高亮显示
	 * 如果点击查询，从常量里获取3，保持查询的字体高亮显示 如果点击代理商，从常量里获取4，保持代理商的字体高亮显示
	 * 
	 * @param id
	 */
	public void addClass(String url) {
		classHidden = "1";
		if (url.contains("/product")) {
			classHidden = "2";
		} else if (url.contains("/search")) {
			classHidden = "3";
		} else if (url.contains("/agent")) {
			classHidden = "4";
		}
		String id = classHidden;
		Ret ret = new Ret();
		ret.set("msg", id);
		renderJson(ret);
	}

	/**
	 * ajax异步请求值 展现栏目信息
	 */
	public void getColumns() {
		List<AdItem> adItemList = adItemService.findAll();// 轮播图片
		List<AdField> adFieldList = adFieldService.findAll();// 栏目
		Ret ret = new Ret();
		ret.set("adItemList", adItemList);
		ret.set("adFieldList", adFieldList);
		renderJson(ret);
	}
}
