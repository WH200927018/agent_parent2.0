/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.tipu.agent.front.controller.agent;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.util.DateUtils;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.api.CompanyService;
import com.tipu.agent.admin.service.api.DataService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.Company;
import com.tipu.agent.front.controller.support.LoginInterceptor;
import com.tipu.agent.front.service.api.AgentAuthService;
import com.tipu.agent.front.service.api.AgentBondService;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentExtendService;
import com.tipu.agent.front.service.api.AgentInviteService;
import com.tipu.agent.front.service.api.AgentRewardService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentStockService;
import com.tipu.agent.front.service.api.AgentTakeStockService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.DeliveryService;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAuth;
import com.tipu.agent.front.service.entity.model.AgentBond;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentExtend;
import com.tipu.agent.front.service.entity.model.AgentReward;
import com.tipu.agent.front.service.entity.model.AgentStock;
import com.tipu.agent.front.service.entity.model.AgentTakeStock;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.utils.AjaxResut;
import com.tipu.agent.front.utils.ErrorConst;

import io.jboot.Jboot;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.utils.StringUtils;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.cors.EnableCORS;

/**
 * 代理商后台
 * 
 * @author hulin
 *
 */
@RequestMapping(value = "/agent")
// @Before(LoginInterceptor.class)
public class AgentFrontController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
	private DataService dataApi;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private DeliveryService deliveryService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentInviteService agentInviteService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentExtendService agentExtendService;

	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
	private BrandService brandService;

	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
	private CompanyService companyService;

	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
	private DataService dataService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAuthService agentAuthService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBondService agentBondService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentRewardService agentRewardService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrdersService orderService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentStockService agentStockService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTakeStockService agentTakeStockService;

	/**
	 * 根据ID获取品牌
	 */
	public void findBrandInfo() {
		String id = getPara("id");
		Brand brand = brandService.findById(id);
		renderJson(AjaxResut.toSuccess(brand));
	}

	/**
	 * 根据ID获取公司信息
	 */
	public void findCompanyInfo() {
		String id = getPara("id");
		Company company = companyService.findById(id);
		renderJson(AjaxResut.toSuccess(company));
	}

	/**
	 * 根据ID获取级别信息
	 */
	public void findAgentTypeInfo() {
		String id = getPara("id");
		AgentType agentType = agentTypeService.findById(id);
		renderJson(AjaxResut.toSuccess(agentType));
	}

	/**
	 * 申请新代理
	 */
	public void applyAgent() {
		// 创建新的代理
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String code = reqObject.getString("code");
		String phone = reqObject.getString("mobile");
		String checkCode = Jboot.me().getCache().get(com.tipu.agent.front.utils.ServiceConst.APPLY_ID_CODE, phone);
		if (StringUtils.isNotEmpty(code) == false) {
			renderJson(AjaxResut.toError(ErrorConst.APPLY_CODE_IS_NULL));
			return;
		}
		if (checkCode == null || checkCode.equals(code) == false) {
			renderJson(AjaxResut.toError(ErrorConst.APPLY_CODE_ERROR));
			return;
		}
		Agent params = new Agent();
		params.setBirthday(DateUtils.formatDate(reqObject.getDate("birthday")));
		params.setBizNo(reqObject.getString("bizNo"));
		params.setAddress(reqObject.getString("address"));
		params.setAgentProp(reqObject.getString("agentProp"));
		params.setIdNo(reqObject.getString("idNo"));
		params.setCompanyId(reqObject.getString("companyId"));
		params.setAgentType(reqObject.getString("agentType"));
		params.setBrandId(reqObject.getString("brandId"));
		params.setWeixin(reqObject.getString("weixin"));
		params.setPassword(reqObject.getString("password"));
		params.setMobile(reqObject.getString("mobile"));
		params.setGender(reqObject.getString("gender"));
		params.setName(reqObject.getString("name"));
		params.setParentId(reqObject.getString("parentId"));
		params.setAgentType(reqObject.getString("agentType"));
		// 查询是否存在账户
		Agent agent = agentService.findByPhone(params.getMobile(), params.getCompanyId());
		if (agent == null) {
			// 检查是否加入过该品牌
			if (agentService.isApply(params.getCompanyId(), params.getBrandId(), params.getMobile()) == false) {
				Agent retuVal = agentService.applyAgent(params, params.getCompanyId(), params.getBrandId(),
						params.getAgentType(), params.getParentId());
				renderJson(AjaxResut.toSuccess(retuVal));
			} else {
				renderJson(AjaxResut.toError(ErrorConst.APPLY_AGENT_BRAND_REG_EXISTS));
			}
		} else {
			// 代理商已经存在
			renderJson(AjaxResut.toError(ErrorConst.APPLY_AGENT_EXISTS));
		}
	}

	/***
	 * 更新证件照片
	 */
	public void updateAgentExtendsInfo() {
		// dataService.
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String id = reqObject.getString("id");
		String idPosPic = reqObject.getString("id_pos_pic");
		String idNegPic = reqObject.getString("id_neg_pic");
		String buzPic = reqObject.getString("buz_pic");
		AgentExtend agentExtends = new AgentExtend();
		agentExtends.setIdPosPic(idPosPic);
		agentExtends.setIdNegPic(idNegPic);
		agentExtends.setBuzPic(buzPic);
		agentExtends.setAgentId(id);
		int num = agentExtendService.updateByAgentId(agentExtends);
		if (num > 0) {
			renderJson(AjaxResut.toSuccess(null));
		} else {
			renderJson(AjaxResut.toError(ErrorConst.APPLY_EXTENDS_MOD_ERROR));
		}
	}

	public void applyAuth() {
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String agentId = reqObject.getString("agentId");
		String brandId = reqObject.getString("brandId");
		String payImg = reqObject.getString("payImg");
		String storeName = reqObject.getString("storeName");
		String applyType = reqObject.getString("applyType");
		String storeUrl = reqObject.getString("storeUrl");
		String storeType = reqObject.getString("storeType");
		AgentAuth agentAuht = new AgentAuth();
		agentAuht.setId(StringUtils.uuid());
		agentAuht.setBrandId(brandId);
		agentAuht.setPayImg(payImg);
		agentAuht.setStoreUrl(storeUrl);
		agentAuht.setStoreType(storeType);
		agentAuht.setStoreName(storeName);
		agentAuht.setAgentId(agentId);
		agentAuht.setStoreUrl(storeUrl);
		agentAuht.setApplyType(applyType);
		agentAuht.setAuditStatus("0");
		agentAuht.setCreateDate(new Date());
		agentAuht.setUpdateDate(new Date());
		agentAuht.setDelFlag("0");
		boolean status = agentAuthService.save(agentAuht);
		if (status) {
			renderJson(AjaxResut.toSuccess(agentAuht));
		} else {
			renderJson(AjaxResut.toError(ErrorConst.APPLY_EXTENDS_MOD_ERROR));
		}
	}

	public void applyBond() {
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String sendAccountType = reqObject.getString("send_account_type");
		String sendAccountNo = reqObject.getString("send_account_no");
		String receiveAccountPrice = reqObject.getString("receive_account_price");
		String receiveAccountName = reqObject.getString("receive_account_name");
		String receiveAccount = reqObject.getString("receive_account");
		String payImg = reqObject.getString("pay_img");
		String agentId = reqObject.getString("agent_id");
		String brandId = reqObject.getString("brand_id");
		AgentBond agentBond = new AgentBond();
		agentBond.setId(StringUtils.uuid());
		agentBond.setSendAccountNo(sendAccountNo);
		agentBond.setSendAccountType(sendAccountType);
		agentBond.setReceiveAccount(receiveAccount);
		agentBond.setReceiveAccountName(receiveAccountName);
		agentBond.setReceiveAccountPrice(receiveAccountPrice);
		agentBond.setPayImg(payImg);
		agentBond.setAgent(agentId);
		agentBond.setBrandId(brandId);
		agentBond.setAuditStatus("0");
		agentBond.setCreateDate(new Date());
		agentBond.setUpdateDate(new Date());
		agentBond.setDelFlag("0");
		boolean status = agentBondService.save(agentBond);
		if (status) {
			renderJson(AjaxResut.toSuccess(agentBond));
		} else {
			renderJson(AjaxResut.toError(ErrorConst.APPLY_EXTENDS_MOD_ERROR));
		}
	}

	public void findAgentByPhone() {
		String phone = getPara("phone");
		String companyId = getPara("companyId");
		Agent agent = agentService.findByPhone(phone, companyId);
		if (agent != null) {
			renderJson(AjaxResut.toSuccess(agent));
		} else {
			renderJson(AjaxResut.toError(ErrorConst.APPLY_IS_NOT_AGENT));
		}
	}

	/**
	 * 根据品牌获取用户奖励信息
	 * 
	 * @throws ParseException
	 */
	@EnableCORS
	@Before(LoginInterceptor.class)
	public void getRewardInfo() throws ParseException {
		String brandId = getPara("brandId");
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Ret ret = new Ret();
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM", Locale.UK);
		// 当前日期
		String now = formatter.format(currentTime);
		// 查询获得的总奖励金
		BigDecimal receiveReward = new BigDecimal(0);
		AgentReward agentReward = new AgentReward();
		agentReward.setAgentId(agent.getId());
		agentReward.setBrandId(brandId);
		List<AgentReward> receiveRewardList = agentRewardService.findList(agentReward);
		if (receiveRewardList.size() > 0) {
			for (AgentReward reward : receiveRewardList) {
				if (reward.getRewardType().equals("4")) {// （4.奖励扣款 （订单取消奖励没有）1.个人代理差价 2.同级8%提成 3 团队季度奖)
					receiveReward = receiveReward.subtract(reward.getRewardMoney());
				} else {
					receiveReward = receiveReward.add(reward.getRewardMoney());
				}
			}
		}
		// 当月获得的总奖励金
		BigDecimal nowReceiveReward = new BigDecimal(0);
		agentReward.set("now", now);
		receiveRewardList = agentRewardService.findList(agentReward);
		if (receiveRewardList.size() > 0) {
			for (AgentReward reward : receiveRewardList) {
				if (reward.getRewardType().equals("4")) {// （4.奖励扣款 （订单取消奖励没有）1.个人代理差价 2.同级8%提成 3 团队季度奖)
					nowReceiveReward = nowReceiveReward.subtract(reward.getRewardMoney());
				} else {
					nowReceiveReward = nowReceiveReward.add(reward.getRewardMoney());
				}
			}
		}
		// 给出的总奖励金
		BigDecimal giveReward = new BigDecimal(0);
		List<AgentReward> giveRewardList = agentRewardService.findGiveList(agentReward);
		if (giveRewardList.size() > 0) {
			for (AgentReward reward : giveRewardList) {
				if (reward.getRewardType().equals("4")) {// （4.奖励扣款 （订单取消奖励没有）1.个人代理差价 2.同级8%提成 3 团队季度奖)
					giveReward = giveReward.subtract(reward.getRewardMoney());
				} else {
					giveReward = giveReward.add(reward.getRewardMoney());
				}
			}
		}
		ret.set("now", now);
		ret.set("receiveReward", receiveReward);
		ret.set("giveReward", giveReward);
		ret.set("nowReceiveReward", nowReceiveReward);
		renderJson(AjaxResut.toSuccess(ret));

	}

	/****
	 * 审核代理商列表
	 */
	public void auditList() {
		String brandId = getPara("brandId");
		String agentId = getPara("agentId");
		String auditStatus = getPara("auditStatus");
		int pageNumber = getParaToInt("pageNumber");
		Page<Agent> list = agentService.findAgentsByStatus(pageNumber,
				com.tipu.agent.front.utils.ServiceConst.SERVICE_PAGE_SIZE, brandId, agentId, auditStatus);
		renderJson(AjaxResut.toSuccess(list));
	}

	public void auditCommit() {
		String id = getPara("id");
		String status = getPara("status");
		AgentBrand agentBrand = new AgentBrand();
		agentBrand.setId(id);
		agentBrand.setAuditStatus(status);
		if (agentBrandService.update(agentBrand)) {
			renderJson(AjaxResut.toSuccess(null));
		} else {
			renderJson(AjaxResut.toError(ErrorConst.SERVICE_MOD_EXCEPTION));
		}
	}

	// 我的上下级
	public void myRelatives() {
		String token = getPara("token");
		String brandId = getPara("brandId");
		String type = getPara("type");
		int pageNumber = getParaToInt("pageNumber");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		if ("0".equals(type)) {
			String id = "";
			List<AgentBrand> brands = agent.getBrands();
			if (brands != null && brands.size() > 0) {
				for (int i = 0; i < brands.size(); i++) {
					AgentBrand brand = brands.get(i);
					if (brand.getBrandId().equals(brandId)) {
						id = brand.getParentId();
					}
				}
			}
			Page<Agent> list = agentService.findRelativesByType(pageNumber,
					com.tipu.agent.front.utils.ServiceConst.SERVICE_PAGE_SIZE, id, type, brandId);
			renderJson(AjaxResut.toSuccess(list));
		} else {
			Page<Agent> list = agentService.findRelativesByType(pageNumber,
					com.tipu.agent.front.utils.ServiceConst.SERVICE_PAGE_SIZE, agent.getId(), type, brandId);
			renderJson(AjaxResut.toSuccess(list));
		}
	}

	/**
	 * 根据品牌获取我的业绩信息
	 * 
	 * @throws ParseException
	 */
	@EnableCORS
	@Before(LoginInterceptor.class)
	public void getAchievementInfo() throws ParseException {
		String brandId = getPara("brandId");
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Ret ret = new Ret();
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM", Locale.UK);
		// 当前日期
		String now = formatter.format(currentTime);
		List<AgentBrand> agentBrandList = new ArrayList<AgentBrand>();
		agentBrandList = getSubInfo(brandId, agent.getId(), agentBrandList);
		AgentBrand agentBrand = new AgentBrand();
		agentBrand.setAuditStatus("1");
		agentBrand.setBrandId(brandId);
		agentBrand.setAgentId(agent.getId());
		// 当前品牌信息
		Brand brand = brandService.findById(brandId);
		List<AgentBrand> list = agentBrandService.findList(agentBrand);
		if (list.size() > 0) {
			agentBrandList.add(list.get(0));
			ret.set("auth_pic", agent.getAvatar());
			ret.set("brandName", brand.getName());
			ret.set("agentTypeName", agentTypeService.findById(list.get(0).getAgentType()).getName());
		}
		// 直属下级个数
		agentBrand = new AgentBrand();
		agentBrand.setAuditStatus("1");
		agentBrand.setBrandId(brandId);
		agentBrand.setParentId(agent.getId());
		List<AgentBrand> subList = agentBrandService.findList(agentBrand);
		Integer mySubSize = subList.size();
		// 团队个数
		Integer teamSize = agentBrandList.size();

		// 当月个人业绩
		BigDecimal myAvhievement = new BigDecimal(0);
		Orders order = new Orders();
		order.setBrandId(brandId);
		order.set("now", now);
		order.setOrderCreaterId(agent.getId());
		List<Orders> myOrderList = orderService.findList(order);
		if (myOrderList.size() > 0) {
			for (Orders myOrder : myOrderList) {
				myAvhievement = myAvhievement.add(myOrder.getBuySum());
			}
		}
		// 当月团队业绩
		BigDecimal teamAvhievement = new BigDecimal(0);
		List<Orders> teamOrderList = orderService.findListByAgentIds(agentBrandList);
		if (teamOrderList.size() > 0) {
			for (Orders teamOrder : teamOrderList) {
				teamAvhievement = teamAvhievement.add(teamOrder.getBuySum());
			}
		}
		ret.set("now", now);
		ret.set("mySubSize", mySubSize);
		ret.set("teamSize", teamSize);
		ret.set("myAvhievement", myAvhievement);
		ret.set("teamAvhievement", teamAvhievement);
		renderJson(AjaxResut.toSuccess(ret));
	}

	/**
	 * 递归查询该品牌团队所有代理
	 * 
	 * @throws ParseException
	 */
	public List<AgentBrand> getSubInfo(String brandId, String parentId, List<AgentBrand> agentBrandList) {
		AgentBrand agentBrand = new AgentBrand();
		agentBrand.setAuditStatus("1");
		agentBrand.setBrandId(brandId);
		agentBrand.setParentId(parentId);
		List<AgentBrand> list = agentBrandService.findList(agentBrand);
		if (list.size() > 0) {
			for (AgentBrand data : list) {
				agentBrandList.add(data);
				getSubInfo(data.getBrandId(), data.getAgentId(), agentBrandList);
			}
		}
		return agentBrandList;
	}

	/**
	 * 获取我的仓库信息
	 * 
	 * @throws ParseException
	 */
	/**
	 * @throws ParseException
	 */
	@EnableCORS
	@Before(LoginInterceptor.class)
	public void getStockInfo() throws ParseException {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		agent = agentService.findById(agent.getId());
		Ret ret = new Ret();
		Date currentTime = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// 获取当前周7天的日期
		String monday = getTimeInterval(currentTime);
		String tuesday = getDateAfter(monday, 1);
		String wednesday = getDateAfter(monday, 2);
		String thursday = getDateAfter(monday, 3);
		String friday = getDateAfter(monday, 4);
		String saturday = getDateAfter(monday, 5);
		String sunday = getDateAfter(monday, 6);
		List<AgentStock> agentStockList = getStockNum(agent, monday, sunday);
		List<AgentTakeStock> agentTakeStockList = takeStockNum(agent, monday, sunday);
		// 当前周的周一进货和提货数据
		Integer mondayGetNum = 0;
		Integer mondayTakeNum = 0;
		// 当前周的周二进货和提货数据
		Integer tuesdayGetNum = 0;
		Integer tuesdayTakeNum = 0;
		// 当前周的周三进货和提货数据
		Integer wednesdayGetNum = 0;
		Integer wednesdayTakeNum = 0;
		// 当前周的周四进货和提货数据
		Integer thursdayGetNum = 0;
		Integer thursdayTakeNum = 0;
		// 当前周的周五进货和提货数据
		Integer fridayGetNum = 0;
		Integer fridayTakeNum = 0;
		// 当前周的周六进货和提货数据
		Integer saturdayGetNum = 0;
		Integer saturdayTakeNum = 0;
		// 当前周的周日进货和提货数据
		Integer sundayGetNum = 0;
		Integer sundayTakeNum = 0;
		if (agentStockList.size() > 0) {// 进货数据
			String createDate = "";
			for (AgentStock agentStock : agentStockList) {
				createDate = sdf.format(agentStock.getCreateDate());
				if (monday.equals(createDate)) {
					mondayGetNum += agentStock.getStockNumber();
				} else if (tuesday.equals(createDate)) {
					tuesdayGetNum += agentStock.getStockNumber();
				} else if (wednesday.equals(createDate)) {
					wednesdayGetNum += agentStock.getStockNumber();
				} else if (thursday.equals(createDate)) {
					thursdayGetNum += agentStock.getStockNumber();
				} else if (friday.equals(createDate)) {
					fridayGetNum += agentStock.getStockNumber();
				} else if (saturday.equals(createDate)) {
					saturdayGetNum += agentStock.getStockNumber();
				} else if (sunday.equals(createDate)) {
					sundayGetNum += agentStock.getStockNumber();
				}
			}
		}

		if (agentTakeStockList.size() > 0) {// 提货数据
			String createDate = "";
			for (AgentTakeStock agentTakeStock : agentTakeStockList) {
				createDate = sdf.format(agentTakeStock.getCreateDate());
				if (monday.equals(createDate)) {
					mondayTakeNum += agentTakeStock.getStockNumber();
				} else if (tuesday.equals(createDate)) {
					tuesdayTakeNum += agentTakeStock.getStockNumber();
				} else if (wednesday.equals(createDate)) {
					wednesdayTakeNum += agentTakeStock.getStockNumber();
				} else if (thursday.equals(createDate)) {
					thursdayTakeNum += agentTakeStock.getStockNumber();
				} else if (friday.equals(createDate)) {
					fridayTakeNum += agentTakeStock.getStockNumber();
				} else if (saturday.equals(createDate)) {
					saturdayTakeNum += agentTakeStock.getStockNumber();
				} else if (sunday.equals(createDate)) {
					sundayTakeNum += agentTakeStock.getStockNumber();
				}
			}
		}

		ret.set("actual_stock", agent.getActualStock());
		ret.set("virtual_stock", agent.getVirtualStock());
		ret.set("mondayGetNum", mondayGetNum);
		ret.set("mondayTakeNum", mondayTakeNum);
		ret.set("tuesdayGetNum", tuesdayGetNum);
		ret.set("tuesdayTakeNum", tuesdayTakeNum);
		ret.set("wednesdayGetNum", wednesdayGetNum);
		ret.set("wednesdayTakeNum", wednesdayTakeNum);
		ret.set("thursdayGetNum", thursdayGetNum);
		ret.set("thursdayTakeNum", thursdayTakeNum);
		ret.set("fridayGetNum", fridayGetNum);
		ret.set("fridayTakeNum", fridayTakeNum);
		ret.set("saturdayGetNum", saturdayGetNum);
		ret.set("saturdayTakeNum", saturdayTakeNum);
		ret.set("sundayGetNum", sundayGetNum);
		ret.set("sundayTakeNum", sundayTakeNum);
		renderJson(AjaxResut.toSuccess(ret));
	}

	/**
	 * 获得当前日期的进货信息
	 * 
	 * @param agent
	 * @param date
	 * @return
	 */
	public List<AgentStock> getStockNum(Agent agent, String startDate, String endDate) {
		// 当前日期的进货数据
		AgentStock agentStock = new AgentStock();
		agentStock.setCompanyId(agent.getCompanyId());
		agentStock.setAgentId(agent.getId());
		agentStock.set("startDate", startDate);
		agentStock.set("endDate", endDate);
		List<AgentStock> agentStockList = agentStockService.findList(agentStock);
		return agentStockList;
	}

	/**
	 * 获得当前日期的提货信息
	 * 
	 * @param agent
	 * @param date
	 * @return
	 */
	public List<AgentTakeStock> takeStockNum(Agent agent, String startDate, String endDate) {
		// 当前日期的提货数据
		AgentTakeStock agentTakeStock = new AgentTakeStock();
		agentTakeStock.setCompanyId(agent.getCompanyId());
		agentTakeStock.setAgentId(agent.getId());
		agentTakeStock.set("startDate", startDate);
		agentTakeStock.set("endDate", endDate);
		List<AgentTakeStock> agentTakeStockList = agentTakeStockService.findList(agentTakeStock);
		return agentTakeStockList;
	}

	/**
	 * 根据当前日期获得所在周的日期区间（周一日期）
	 * 
	 * @return
	 * @author
	 * @throws ParseException
	 */
	public String getTimeInterval(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		// System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期
		// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		// 获得当前日期是一个星期的第几天
		int day = cal.get(Calendar.DAY_OF_WEEK);
		// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String imptimeBegin = sdf.format(cal.getTime());
		// System.out.println("所在周星期一的日期：" + imptimeBegin);
		// cal.add(Calendar.DATE, 6);
		// String imptimeEnd = sdf.format(cal.getTime());
		// System.out.println("所在周星期日的日期：" + imptimeEnd);
		return imptimeBegin;
	}

	/**
	 * 得到几天后的时间
	 * 
	 * @param d
	 * @param day
	 * @return
	 * @throws ParseException
	 */
	public static String getDateAfter(String d, int day) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar now = Calendar.getInstance();
		now.setTime(sdf.parse(d));
		now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
		return sdf.format(now.getTime());
	}

	/**
	 * 获取实体库存明细
	 * 
	 * @throws ParseException
	 */
	@EnableCORS
	@Before(LoginInterceptor.class)
	public void getActualStockDetails() throws ParseException {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Ret ret = new Ret();
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM", Locale.UK);
		// 当前日期
		String now = formatter.format(currentTime);
		AgentStock agentStock = new AgentStock();
		agentStock.set("now", now);
		agentStock.setAgentId(agent.getId());
		agentStock.setCompanyId(agent.getCompanyId());
		agentStock.setStockType("1");// 1 实际库存 2虚拟库存
		List<AgentStock> agentStockList = agentStockService.findList(agentStock);
		BigDecimal total = new BigDecimal(0);
		if (agentStockList.size() > 0) {
			for (AgentStock data : agentStockList) {
				total = total.add(data.getPurchasePrice().multiply(new BigDecimal(data.getStockNumber())));
			}
		}
		ret.set("total", total);
		ret.set("agentStockList", agentStockList);
		renderJson(AjaxResut.toSuccess(ret));
	}

	/**
	 * 获取云库存明细
	 * 
	 * @throws ParseException
	 */
	@EnableCORS
	@Before(LoginInterceptor.class)
	public void getVirtualStockDetails() throws ParseException {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Ret ret = new Ret();
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM", Locale.UK);
		// 当前日期
		String now = formatter.format(currentTime);
		AgentStock agentStock = new AgentStock();
		agentStock.set("now", now);
		agentStock.setAgentId(agent.getId());
		agentStock.setCompanyId(agent.getCompanyId());
		agentStock.setStockType("2");// 1 实际库存 2虚拟库存
		List<AgentStock> agentStockList = agentStockService.findList(agentStock);
		BigDecimal getTotal = new BigDecimal(0);
		if (agentStockList.size() > 0) {
			for (AgentStock data : agentStockList) {
				getTotal = getTotal.add(data.getPurchasePrice().multiply(new BigDecimal(data.getStockNumber())));
			}
		}
		AgentTakeStock agentTakeStock = new AgentTakeStock();
		agentTakeStock.set("now", now);
		agentTakeStock.setAgentId(agent.getId());
		agentTakeStock.setCompanyId(agent.getCompanyId());
		List<AgentTakeStock> agentTakeStockList = agentTakeStockService.findList(agentTakeStock);
		BigDecimal takeTotal = new BigDecimal(0);
		if (agentTakeStockList.size() > 0) {
			for (AgentTakeStock data : agentTakeStockList) {
				takeTotal = takeTotal.add(data.getPurchasePrice().multiply(new BigDecimal(data.getStockNumber())));
			}
		}
		ret.set("getTotal", getTotal);
		ret.set("agentStockList", agentStockList);
		ret.set("takeTotal", takeTotal);
		ret.set("agentTakeStockList", agentTakeStockList);
		renderJson(AjaxResut.toSuccess(ret));
	}

	/**
	 * 获取云库存商品列表
	 * 
	 * @throws ParseException
	 */
	@EnableCORS
	@Before(LoginInterceptor.class)
	public void getVirtualStockList() throws ParseException {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentStock agentStock = new AgentStock();
		agentStock.setAgentId(agent.getId());
		agentStock.setCompanyId(agent.getCompanyId());
		agentStock.setStockType("2");// 1 实际库存 2虚拟库存
		List<AgentStock> agentStockList = agentStockService.findList(agentStock);
		AgentTakeStock agentTakeStock = new AgentTakeStock();
		agentTakeStock.setAgentId(agent.getId());
		agentTakeStock.setCompanyId(agent.getCompanyId());
		List<AgentTakeStock> agentTakeStockList = agentTakeStockService.findList(agentTakeStock);
		if (agentStockList.size() > 0 && agentTakeStockList.size() > 0) {
			for (AgentStock getStock : agentStockList) {// 计算库存
				for (AgentTakeStock takeStock : agentTakeStockList) {
					if (getStock.getId().equals(takeStock.getAgentStockId())) {
						getStock.setStockNumber(getStock.getStockNumber() - takeStock.getStockNumber());
					}
				}
			}
			for (int i = agentStockList.size() - 1; i >= 0; i--) {// 去除库存为0的
				if (agentStockList.get(i).getStockNumber() < 1) {
					agentStockList.remove(i);
				}
			}
		}
		renderJson(AjaxResut.toSuccess(agentStockList));
	}

	/****
	 * 代理商查询
	 */
	@EnableCORS
	public void agentQuery() {
		String data = getPara("data");
		String companyId = getPara("companyId");
		Agent agent=agentService.findByPhoneOrWx(data, companyId);
		Ret ret=new Ret();
		if(agent !=null) {
			List<AgentBrand> agentBrandList = agentBrandService.findByAgentId(agent.getId());
			List<Brand> brandList = brandService.findAll();
			if (brandList.size() > 0 && agentBrandList.size() > 0) {
				for (Brand brand : brandList) {
					for (AgentBrand agentBrand : agentBrandList) {
						if (brand.getId().equals(agentBrand.getBrandId())) {
							agentBrand.set("name", brand.getName());
							agentBrand.set("logo_pic", brand.getLogoPic());
						}
					}
				}
			}
			ret.set("agent", agent);
			ret.set("agentBrandList", agentBrandList);
			renderJson(AjaxResut.toSuccess(ret));
		}else {
			renderJson(AjaxResut.toError(""));
		}
		
		
	}
}