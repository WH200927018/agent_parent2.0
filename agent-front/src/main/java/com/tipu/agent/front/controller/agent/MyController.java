package com.tipu.agent.front.controller.agent;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.QrcodeApi;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.front.controller.common.OrderNumberGenerator;
import com.tipu.agent.front.controller.support.LoginInterceptor;
import com.tipu.agent.front.service.api.AgentAddressService;
import com.tipu.agent.front.service.api.AgentAuthService;
import com.tipu.agent.front.service.api.AgentBanksService;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentInviteService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.OrderProductService;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAddress;
import com.tipu.agent.front.service.entity.model.AgentAuth;
import com.tipu.agent.front.service.entity.model.AgentBanks;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentInvite;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.OrderProduct;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.ProductType;
import com.tipu.agent.front.utils.AjaxResut;
import com.tipu.agent.front.utils.ErrorConst;

import io.jboot.Jboot;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.cors.EnableCORS;

/**
 * 前台的进货
 * 
 * @author wh
 *
 */
@RequestMapping(value = "/agent/my")
@Before(LoginInterceptor.class)
public class MyController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAddressService agentAddressService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBanksService agentBanksService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAuthService agentAuthService;

	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
	private BrandService brandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrdersService orderService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrderProductService orderProductService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentInviteService agentInviteService;

	/**
	 * 获取代理商信息
	 */
	@EnableCORS
	public void getAgentInfo() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		agent = agentService.findById(agent.getId());
		renderJson(AjaxResut.toSuccess(agent));
	}

	/**
	 * 获取代理商授权品牌
	 */
	@EnableCORS
	public void getAgentBrandList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		List<AgentBrand> agentBrandList = agentBrandService.findByAgentId(agent.getId());
		List<Brand> brandList = brandService.findAll();
		if (brandList.size() > 0 && agentBrandList.size() > 0) {
			for (Brand brand : brandList) {
				for (AgentBrand agentBrand : agentBrandList) {
					if (brand.getId().equals(agentBrand.getBrandId())) {
						agentBrand.set("name", brand.getName());
						agentBrand.set("logo_pic", brand.getLogoPic());
					}
				}
			}
		}
		renderJson(AjaxResut.toSuccess(agentBrandList));
	}

	/**
	 * 获取代理商授权
	 */
	@EnableCORS
	public void getAgentAuthList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentAuth data = new AgentAuth();
		data.setAgentId(agent.getId());
		List<AgentAuth> agentAuthList = agentAuthService.findList(data);
		renderJson(AjaxResut.toSuccess(agentAuthList));
	}

	/**
	 * 修改登录密码
	 */
	@EnableCORS
	public void updatePwd() {
		String token = getPara("token");
		String oldPwd = getPara("oldPwd");
		String newPwdOne = getPara("newPwdOne");
		String newPwdTwo = getPara("newPwdTwo");
		if (!newPwdOne.equals(newPwdTwo)) {
			renderJson(AjaxResut.toError(ErrorConst.MY_UPDATEPWD_INCONFORMITY));
			return;
		}
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		agent = agentService.findById(agent.getId());
		if (!agent.getPassword().equals(oldPwd)) {
			renderJson(AjaxResut.toError(ErrorConst.LOGIN_PWD_ERROR));
			return;
		}
		String id = agent.getId();
		agent = new Agent();
		agent.setId(id);
		agent.setPassword(newPwdTwo);
		if (agentService.update(agent)) {
			Jboot.me().getCache().remove(LoginController.LOGIN_TOKEN, token);
		}
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 退出登录
	 */
	@EnableCORS
	public void logOut() {
		String token = getPara("token");
		Jboot.me().getCache().remove(LoginController.LOGIN_TOKEN, token);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 常用地址列表
	 */
	@EnableCORS
	public void getAddressList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentAddress agentAddress = new AgentAddress();
		agentAddress.setAgentId(agent.getId());
		agentAddress.setCompanyId(agent.getCompanyId());
		List<AgentAddress> addressList = agentAddressService.findList(agentAddress);
		renderJson(AjaxResut.toSuccess(addressList));
	}

	/**
	 * 根据id获取常用地址
	 */
	@EnableCORS
	public void getAddressByAddressId() {
		String addressId = getPara("addressId");
		AgentAddress address = agentAddressService.findById(addressId);
		renderJson(AjaxResut.toSuccess(address));
	}

	/**
	 * 根据id删除常用地址
	 */
	@EnableCORS
	public void deleteAddress() {
		String addressId = getPara("addressId");
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentAddress agentAddress = new AgentAddress();
		agentAddress.setDelFlag("1");
		agentAddress.setId(addressId);
		agentAddress.setUpdateBy(agent.getId());
		agentAddress.setUpdateDate(new Date());
		agentAddressService.update(agentAddress);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 新增地址
	 */
	@EnableCORS
	public void submitAddress() {
		String token = getPara("token");
		String name = getPara("name");
		String phone = getPara("phone");
		String myAddress = getPara("myAddress");
		String detail = getPara("detail");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentAddress agentAddress = new AgentAddress();
		agentAddress.setAgentName(name);
		agentAddress.setAgentPhone(phone);
		agentAddress.setAgentAddress(detail);
		agentAddress.setAddressArea(myAddress);
		agentAddress.setAgentId(agent.getId());
		agentAddress.setCompanyId(agent.getCompanyId());
		agentAddress.setCreateBy(agent.getId());
		agentAddress.setCreateDate(new Date());
		agentAddress.setIsDefault(0);
		agentAddress.setDelFlag("0");
		agentAddressService.save(agentAddress);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 修改地址
	 */
	@EnableCORS
	public void updateAddress() {
		String addressId = getPara("addressId");
		String name = getPara("name");
		String phone = getPara("phone");
		String myAddress = getPara("myAddress");
		String detail = getPara("detail");
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentAddress agentAddress = new AgentAddress();
		agentAddress.setAgentName(name);
		agentAddress.setAgentPhone(phone);
		agentAddress.setAgentAddress(detail);
		agentAddress.setAddressArea(myAddress);
		agentAddress.setId(addressId);
		agentAddress.setUpdateBy(agent.getId());
		agentAddress.setUpdateDate(new Date());
		agentAddressService.update(agentAddress);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 修改默认地址
	 */
	@EnableCORS
	public void updateDefault() {
		String addressId = getPara("addressId");
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentAddress agentAddress = new AgentAddress();
		agentAddress.setAgentId(agent.getId());
		agentAddress.setCompanyId(agent.getCompanyId());
		List<AgentAddress> addressList = agentAddressService.findList(agentAddress);
		if (addressList.size() > 0) {
			for (AgentAddress address : addressList) {
				if (!address.getId().equals(addressId) && 1 == address.getIsDefault()) {
					address.setIsDefault(0);
					address.setUpdateBy(agent.getId());
					address.setUpdateDate(new Date());
					agentAddressService.update(address);
				}
			}
		}
		agentAddress = new AgentAddress();
		agentAddress.setId(addressId);
		agentAddress.setIsDefault(1);
		agentAddress.setUpdateBy(agent.getId());
		agentAddress.setUpdateDate(new Date());
		agentAddressService.update(agentAddress);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 常用银行卡列表
	 */
	@EnableCORS
	public void getBankList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentBanks agentBanks = new AgentBanks();
		agentBanks.setAgent(agent.getId());
		agentBanks.setCompanyId(agent.getCompanyId());
		List<AgentBanks> bankList = agentBanksService.findList(agentBanks);
		renderJson(AjaxResut.toSuccess(bankList));
	}

	/**
	 * 公司银行卡列表
	 */
	@EnableCORS
	public void getCompanyBankList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentBanks agentBanks = new AgentBanks();
		agentBanks.setCompanyId(agent.getCompanyId());
		agentBanks.setIsCompany("1");
		List<AgentBanks> bankList = agentBanksService.findList(agentBanks);
		renderJson(AjaxResut.toSuccess(bankList));
	}

	/**
	 * 新增银行卡
	 */
	@EnableCORS
	public void addBank() {
		String token = getPara("token");
		String account = getPara("account");
		String bankName = getPara("bankName");
		String accountAsname = getPara("accountAsname");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentBanks agentBanks = new AgentBanks();
		agentBanks.setAccount(account);
		agentBanks.setAccountAsname(accountAsname);
		agentBanks.setBankName(bankName);
		agentBanks.setAgent(agent.getId());
		agentBanks.setCompanyId(agent.getCompanyId());
		agentBanks.setCreateBy(agent.getId());
		agentBanks.setCreateDate(new Date());
		agentBanks.setIsCompany("0");
		agentBanks.setDelFlag("0");
		agentBanksService.save(agentBanks);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 根据id删除常用银行卡
	 */
	@EnableCORS
	public void deleteBank() {
		String bankId = getPara("bankId");
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentBanks agentBanks = new AgentBanks();
		agentBanks.setDelFlag("1");
		agentBanks.setId(bankId);
		agentBanks.setUpdateBy(agent.getId());
		agentBanks.setUpdateDate(new Date());
		agentBanksService.update(agentBanks);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 我要充值
	 */
	@EnableCORS
	public void addTransfer() {
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String bankId = reqObject.getString("bankId");
		String money = reqObject.getString("money");
		String imageUrl = reqObject.getString("imageUrl");
		String token = reqObject.getString("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		String number = OrderNumberGenerator.get();
		Orders orders = new Orders();
		orders.setId(number);
		orders.setCreateDate(new Date());
		orders.setDelFlag("0");
		orders.setOrderGroupId(StrKit.getRandomUUID());
		orders.setUpdateDate(new Date());
		// 订单处理人
		orders.setHandleAgentId("0");
		BigDecimal buySum = new BigDecimal(money);
		orders.setBuySum(buySum);
		orders.setOrderType("4"); // 4是充值
		orders.setPayment(bankId);
		orders.setBank(bankId);
		orders.setAgentId(agent.getId());
		orders.setAgentName(agent.getName());
		orders.setState("0");
		orders.setProductType("3"); // 3是虚拟货币
		orders.setPaymentCert(imageUrl);
		orders.setCompanyId(agent.getCompanyId());
		orderService.save(orders);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 获取充值记录
	 */
	@EnableCORS
	public void getTransferList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Orders orders = new Orders();
		orders.setOrderType("4"); // 4是充值
		orders.setAgentId(agent.getId());
		orders.setProductType("3"); // 3是虚拟货币
		orders.setCompanyId(agent.getCompanyId());
		List<Orders>orderList=orderService.findTransferList(orders);
		renderJson(AjaxResut.toSuccess(orderList));
	}
	
	/**
	 * 获取我的订单
	 */
	@EnableCORS
	public void getMyOrdersList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Orders data = new Orders();
		data.setOrderType("1");
		data.setOrderCreaterId(agent.getId());
		data.setAgentId(agent.getId());
		List<Orders> orderList = orderService.findList(data);
		renderJson(AjaxResut.toSuccess(orderList));
	}

	/**
	 * 获取订单详情
	 */
	public void getOrderdetails() {
		String orderId = getPara("orderId");
		Orders order = orderService.findById(orderId);
		List<OrderProduct>orderProductList=orderProductService.findOrderProductByOrderId(orderId);
		Ret ret= new Ret();
		ret.set("order", order);
		ret.set("orderProductList", orderProductList);	
		renderJson(AjaxResut.toSuccess(ret));
	}

	/**
	 * 获取下级订单
	 */
	@EnableCORS
	public void getSubOrdersList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Orders data = new Orders();
		data.setOrderType("1");
		data.setOrderCreaterId(agent.getId());
		List<Orders> orderList = orderService.findSubList(data);
		renderJson(AjaxResut.toSuccess(orderList));
	}

	/**
	 * 订单-确认收货
	 */
	@EnableCORS
	public void confirmGoods() {
		String token = getPara("token");
		String orderId = getPara("orderId");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Orders data = new Orders();
		data.setId(orderId);
		data.setState("4");// -1 拒绝 0 待处理 1审核通过 2 待发货 3待收货 4结束
		data.setUpdateBy(agent.getId());
		data.setUpdateDate(new Date());
		orderService.update(data);
		renderJson(AjaxResut.toSuccess(""));
	}

	/**
	 * 获取代理商类型
	 */
	@EnableCORS
	public void getAgentTypeList() {
		String token = getPara("token");
		String agentType = getPara("agentType");
		String brandId = getPara("brandId");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		AgentType oldData = agentTypeService.findById(agentType);
		AgentType data = new AgentType();
		data.setCompanyId(agent.getCompanyId());
		data.setBrandId(brandId);
		data.setGrade(oldData.getGrade());
		List<AgentType> agentTypeList = agentTypeService.findAllByGrade(data);
		renderJson(AjaxResut.toSuccess(agentTypeList));
	}
	
	/**
	 * 更新头像
	 */
	@EnableCORS
	public void submitAvatar() {
		String token = getPara("token");
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String imageUrl = reqObject.getString("imageUrl");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		String agentId=agent.getId();
		agent =new Agent();
		agent.setId(agentId);
		agent.setAvatar(imageUrl);
		agentService.update(agent);
		agent=agentService.findById(agentId);
		List<AgentBrand> brands = agentBrandService.findListByAgentIdAndAuthStatus(agent.getId(),"1");
		agent.setBrands(brands);
		renderJson(AjaxResut.toSuccess(agent));
	}

	
}
