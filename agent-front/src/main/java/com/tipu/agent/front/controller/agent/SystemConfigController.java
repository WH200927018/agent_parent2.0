package com.tipu.agent.front.controller.agent;

import org.apache.commons.lang.RandomStringUtils;

import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.front.utils.AjaxResut;
import com.tipu.agent.front.utils.ServiceConst;

import io.jboot.Jboot;
import io.jboot.web.controller.annotation.RequestMapping;


@RequestMapping(value = "/sysconfig")
public class SystemConfigController extends BaseController{

	
	
	
	public void getIdCode() {
		String phone = getPara("phone");
		String code = RandomStringUtils.random(5, "0123456789");
		Jboot.me().getCache().put(ServiceConst.APPLY_ID_CODE, phone, code,1000*60*60);
		renderJson(AjaxResut.toSuccess(code));
	}
}
