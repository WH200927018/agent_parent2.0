package com.tipu.agent.front;

import io.jboot.Jboot;

/**
 * 服务启动入口
 * @author Rlax
 *
 */
public class FrontApplication {
    public static void main(String [] args){
        Jboot.run(args);
    }
}
