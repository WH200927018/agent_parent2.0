/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.tipu.agent.front.controller.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.front.controller.support.LoginInterceptor;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.utils.AjaxResut;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 代理商后台
 * 
 * @author hulin
 *
 */
@RequestMapping(value = "/brand")
@Before(LoginInterceptor.class)
public class AgentBrandController extends BaseController {

	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;
	 
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;
	
	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
	private BrandService brandService;
	
	/**
	 * 获取品牌信息 和 对应级别信息
	 */
	public void getBrandAndTypes() {
		String id = getPara("id");
		AgentBrand agentBrand = agentBrandService.findById(id);
		String companyId = getPara("companyId");
		String typeId = agentBrand.getAgentType();
		AgentType agentType = agentTypeService.findById(typeId);
		Brand brand = brandService.findById(agentBrand.getBrandId());
		List<AgentType> agentTypes = agentTypeService.findCurrentGradeList(agentBrand.getBrandId(),companyId,agentType.getGrade());
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("brand", brand);
		result.put("agentTypes", agentTypes);	
		renderJson(AjaxResut.toSuccess(result));
	}
	
	public void getType() {
		String id = getPara("id");
		AgentType agentType = agentTypeService.findById(id);
		renderJson(AjaxResut.toSuccess(agentType));
	}
	
	public void getBrands() {
		JSONObject reqObject  = JSONObject.parseObject(getBodyString());
		String ids = reqObject.getString("ids");
		String [] idsSplit = ids.split(",");
		List<String> idsList= new ArrayList<String>();
		for (String idSplit : idsSplit) {
			idsList.add(idSplit);
		}
		List<Brand>  brands = brandService.findByIds(idsList);
		renderJson(AjaxResut.toSuccess(brands));
	}

}