package com.tipu.agent.front.controller.common;

import java.util.HashMap;

import com.jfinal.kit.JsonKit;

import io.jboot.web.render.JbootJsonRender;

public class TiPuRender extends JbootJsonRender{

	public void render() {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
		response.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
		super.render();
	}
	
	
	public TiPuRender() {
		super();
    }

    public TiPuRender(String key, Object value) {
    	super(key,value);
    }

    public TiPuRender(String[] attrs) {
    	super(attrs);
    }

    public TiPuRender(String jsonText) {
    	super(jsonText);
    }

    public TiPuRender(Object object) {
    	super(object);
    }

}
