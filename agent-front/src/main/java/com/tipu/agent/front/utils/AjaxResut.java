package com.tipu.agent.front.utils;

public class AjaxResut {
	private String errorNo = "0";
	private String errorInfo = "";
	private Object data;
	
	private AjaxResut() {
		
	}
	
	public String getErrorNo() {
		return errorNo;
	}
	public void setErrorNo(String errorNo) {
		this.errorNo = errorNo;
	}
	public String getErrorInfo() {
		return errorInfo;
	}
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	public static AjaxResut toError(String errorNo,Object data) {
		AjaxResut result = new AjaxResut();
		result.setData(data);
		result.setErrorNo(errorNo);
		return result;
	}
	
	public static AjaxResut toError(String errorNo,String errorInfo,Object data) {
		AjaxResut result = new AjaxResut();
		result.setData(data);
		result.setErrorNo(errorNo);
		result.setErrorInfo(errorInfo);
		return result;
	}
	
	public static AjaxResut toError(String errorNo,String errorInfo) {
		AjaxResut result = new AjaxResut();
		result.setErrorNo(errorNo);
		result.setErrorInfo(errorInfo);
		return result;
	}
	
	public static AjaxResut toError(String errorNo) {
		AjaxResut result = new AjaxResut();
		result.setErrorNo(errorNo);
		return result;
	}
	
	public static AjaxResut toSuccess(Object data) {
		AjaxResut result = new AjaxResut();
		result.setData(data);
		return result;
	}
}
