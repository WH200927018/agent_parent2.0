/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.tipu.agent.front.controller.agent;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.weixin.sdk.api.AccessTokenApi;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.TemplateMsgApi;
import com.jfinal.weixin.sdk.utils.HttpUtils;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.front.controller.common.OrderNumberGenerator;
import com.tipu.agent.front.controller.support.LoginInterceptor;
import com.tipu.agent.front.service.api.AgentAddressService;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentPriceService;
import com.tipu.agent.front.service.api.AgentRebateConfigParamsService;
import com.tipu.agent.front.service.api.AgentRewardService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentStockService;
import com.tipu.agent.front.service.api.AgentTakeStockService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.OrderProductService;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.api.ProductImagesService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.api.SettingValService;
import com.tipu.agent.front.service.api.ShoppingCartService;
import com.tipu.agent.front.service.api.ShoppingRecordService;
import com.tipu.agent.front.service.api.SkuService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAddress;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentPrice;
import com.tipu.agent.front.service.entity.model.AgentReward;
import com.tipu.agent.front.service.entity.model.AgentStock;
import com.tipu.agent.front.service.entity.model.AgentTakeStock;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.OrderProduct;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.Product;
import com.tipu.agent.front.service.entity.model.ProductImages;
import com.tipu.agent.front.service.entity.model.ProductType;
import com.tipu.agent.front.service.entity.model.SettingVal;
import com.tipu.agent.front.service.entity.model.ShoppingCart;
import com.tipu.agent.front.service.entity.model.ShoppingRecord;
import com.tipu.agent.front.service.entity.model.Sku;
import com.tipu.agent.front.service.entity.model.base.BaseAgentPrice;
import com.tipu.agent.front.utils.AjaxResut;
import com.tipu.agent.front.utils.ErrorConst;

import io.jboot.Jboot;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.cors.EnableCORS;
import io.undertow.util.PortableConcurrentDirectDeque;

/**
 * 前台的进货
 * 
 * @author wh
 *
 */
@RequestMapping(value = "/agent/order")
@Before(LoginInterceptor.class)
public class OrderController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SkuService skuService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentPriceService agentPriceService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ShoppingCartService shoppingCartService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAddressService agentAddressService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrdersService ordersService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrderProductService orderProductService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ShoppingRecordService shoppingRecordService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentRewardService agentRewardService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductImagesService productImagesService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SettingValService settingValService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentStockService agentStockService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTakeStockService agentTakeStockService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentRebateConfigParamsService agentRebateConfigParamsService;

	/**
	 * 进货
	 */
	@EnableCORS
	public void findProductType() {
		String token = getPara("token");
		String brandId = getPara("brandId");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		List<ProductType> productTypeList = productTypeService.findAll(agent.getCompanyId(), brandId);
		renderJson(productTypeList);
	}

	/**
	 * 商品详细
	 */
	@EnableCORS
	public void findProductDeatil() {
		String token = getPara("token");
		String brandId = getPara("brandId");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		renderJson(skuService.findByAgent(agent, brandId));
	}

	/**
	 * 前台商城结算
	 */
	@EnableCORS
	public void submitOrders() {
		AjaxResut result = null;
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		String carProducts = reqObject.getString("skuIds");
		String orderPayment = reqObject.getString("orderPayment");// 付款方式
		String addressId = reqObject.getString("addressId");
		String numbers = reqObject.getString("buyNums");
		String remarks = reqObject.getString("remarks");
		String brandId = reqObject.getString("brandId");
		String imageUrl = reqObject.getString("imageUrl");
		String productType = reqObject.getString("productType");// 订单货物类型（1实体仓库2云仓库 3.虚拟货币）
		String[] ids = carProducts.split(",");
		String[] nums = numbers.split(",");
		List<Sku> skuList = new ArrayList<Sku>();
		Sku sku = new Sku();
		BigDecimal buySums = new BigDecimal(0);
		Integer buyNum = 0;
		for (int i = 0; i < ids.length; i++) {
			sku = skuService.findById(ids[i]);
			sku.setProductNumer(Integer.valueOf(nums[i]));
			skuList.add(sku);
			AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agent.getId(), sku.getBrandId());
			AgentPrice agentPrice = agentPriceService.findBySkuAndAgentType(sku.getId(), agentBrand.getAgentType());
			buySums = buySums.add(new BigDecimal(nums[i]).multiply(new BigDecimal(agentPrice.getAgencyPrice())));
			buyNum += Integer.valueOf(nums[i]);
		}
		agent.setAgentDeposit(buySums);
		if (orderPayment.equals("0")) {// 货款余额付费
			if (agentService.reduceAgentDepositAndStock(agent, skuList, productType) < 0) {
				// 货款余额不足或库存不足
				result = AjaxResut.toError(ErrorConst.SHOP_PAYMENT_INSUFFICIENT);
				renderJson(result);
				return;
			}
		} else {
			// 检验库存是否充足
			List<Sku> list = skuService.subStoreByList(skuList, productType);
			if (list.size() > 0) {
				// 库存不足
				result = AjaxResut.toError(ErrorConst.SHOP_UNDERSTOCL);
				renderJson(result);
				return;
			}
		}
		String number = OrderNumberGenerator.get();
		Product product = productService.findById(sku.getProductId());
		AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agent.getId(), sku.getBrandId());
		Orders order = new Orders();
		order.setId(number);
		order.setOrderCreaterId(agent.getId());
		order.setCreateDate(new Date());
		order.setOrderGroupId(StrKit.getRandomUUID());
		order.setBuySum(buySums);
		order.setBuyNums(buyNum);
		order.setProductUnit(product.getUnit());
		order.setUpdateDate(new Date());
		order.setProductName(sku.getName());
		order.setRemarks(remarks);
		// 董事
		boolean flag = agentType(agentBrand);// 判断这个等级是否为最顶级
		if (flag == true) {
			order.setHandleAgentId("0");
		} else {
			order.setHandleAgentId(agentBrand.getParentId());
		}
		order.setDelFlag("0");
		AgentAddress address = agentAddressService.findById(addressId);
		order.setCompanyId(agent.getCompanyId());
		order.setBrandId(product.getBrandId());
		order.setReceiveAddress(address.getAddressArea() + address.getAgentAddress());
		order.setReceiveName(address.getAgentName());
		order.setReceiverPhone(address.getAgentPhone());
		order.setAgentId(agent.getId());
		order.setAgentName(agent.getName());
		order.setProductImg(sku.getImage());
		order.setPayment(orderPayment);// 支付方式 0货款 1现金
		if (productType.equals("1")) {
			order.setPaymentCert(imageUrl);
			order.setState("0");// 订单状态 -1 拒绝 0 待处理 1审核通过 2 待发货 3待收货 4结束
		} else {
			order.setState("4");// 订单状态 -1 拒绝 0 待处理 1审核通过 2 待发货 3待收货 4结束
		}
		order.setOrderType("1");// 订单类型(1.普通订单 2提货订单 3. 提现订单 4 董事充值订单 5 保证金订单 )
		order.setProductType(productType);// 订单货物类型 （2云仓库1 实体仓库 3.虚拟货币）
		ordersService.save(order);
		for (int i = 0; i < ids.length; i++) {
			sku = skuList.get(i);
			// 创建订单
			if (ids[i].equals(sku.getId())) {
				AgentPrice agentPrice = agentPriceService.findBySkuAndAgentType(sku.getId(), agentBrand.getAgentType());
				product = productService.findById(sku.getProductId());
				BigDecimal buySum = new BigDecimal(nums[i]).multiply(new BigDecimal(agentPrice.getAgencyPrice()));
				// 创建产品
				OrderProduct wpo = new OrderProduct();
				wpo.setId(StrKit.getRandomUUID());
				wpo.setProductImg(sku.getImage());
				wpo.setBuyNum(nums[i].toString());
				wpo.setOrderId(number);
				wpo.setCreateDate(new Date());
				wpo.setDelFlag("0");
				wpo.setBuyUprice(agentPrice.getAgencyPrice());
				wpo.setBuySum(buySum.toString());
				wpo.setProduct(sku.getId());
				wpo.setPackageSpec(Integer.valueOf(product.getPackageSpec()));
				wpo.setProductUnit(product.getUnit());
				orderProductService.save(wpo);
				if (!productType.equals("1")) {// 云库存计算所有差价返利
					// 更新实际库存总数
					Agent newAgent = new Agent();
					agent = agentService.findById(agent.getId());
					Integer virtualStock = agent.getVirtualStock() + Integer.valueOf(nums[i]);
					newAgent.setVirtualStock(virtualStock);
					newAgent.setId(agent.getId());
					agentService.update(newAgent);
					// 新增代理商库存信息
					AgentStock agentStock = new AgentStock();
					agentStock.setAgentId(agent.getId());
					agentStock.setCompanyId(agent.getCompanyId());
					agentStock.setOrderId(number);
					agentStock.setPurchasePrice(new BigDecimal(agentPrice.getAgencyPrice()));
					agentStock.setSkuId(sku.getId());
					agentStock.setSkuName(sku.getName());
					agentStock.setSkuPrice(sku.getPrice());
					agentStock.setStockNumber(Integer.valueOf(nums[i]));
					agentStock.setStockType(productType);
					agentStock.setCreateDate(new Date());
					agentStock.setCreateBy(agent.getId());
					agentStock.setProductInfo(sku.getImage());
					agentStock.setDelFlag("0");
					agentStockService.save(agentStock);
					List<AgentBrand> agentBrandList = new ArrayList<AgentBrand>();
					agentBrandList = getSupInfo(brandId, agentBrand.getParentId(), agentBrandList);
					if (agentBrandList.size() > 0) {
						BigDecimal lr = new BigDecimal(0);
						for (AgentBrand agenBrand : agentBrandList) {
							if (agenBrand.getAgentId().equals("0") == false) {
								AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
								if (agentType.getIsProfit().equals("1")) {
									AgentPrice agenPrice = agentPriceService.findBySkuAndAgentType(sku.getId(),
											agenBrand.getAgentType());
									lr = new BigDecimal(agenPrice.getAgencyPrice()).multiply(new BigDecimal(nums[i]))
											.multiply(agentType.getProfit());
									AgentReward wsAgencyReward = new AgentReward();
									wsAgencyReward.setId(StrKit.getRandomUUID());
									wsAgencyReward.setCreateDate(new Date());
									wsAgencyReward.setDelFlag("0");
									wsAgencyReward.setRewardType("1"); // 奖励类型（4.奖励扣款 （订单取消奖励没有）1.个人代理差价 2.同级8%提成 3
																		// 团队季度奖)
									wsAgencyReward.setOrderId(order.getId());
									wsAgencyReward.setAgentId(agenBrand.getAgentId());
									wsAgencyReward.setBrandId(agenBrand.getBrandId());
									wsAgencyReward.setRemarks("");
									wsAgencyReward.setRewardMoney(lr);
									agentRewardService.save(wsAgencyReward);
									// 加差价奖金
									agentService.addReward(agenBrand.getAgentId(), lr);
								}
							}
						}
					}
				}
			}
		}
		if (!agentBrand.getParentId().equals("0")) {
			AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
			List<AgentBrand> agentBrandList = new ArrayList<AgentBrand>();
			agentBrandList = getSupInfo(brandId, agentBrand.getParentId(), agentBrandList);
			agentService.updateAgentReward(agent, agentBrand, order, agentType, agentBrandList);
		}
		// 创建消费记录
		ShoppingRecord record = new ShoppingRecord();
		record.setId(StrKit.getRandomUUID());
		record.setCreateDate(new Date());
		record.setOrderId(order.getId());
		record.setShoppingPrice(buySums.toString());
		record.setType("1"); // 消费类型 是6 订单退款 5奖励退款 4打款 3 收到货款 2提取 还是 1购买商品
		record.setAgencyId(agent.getId());
		record.setDelFlag("0");
		record.setRemarks("进货消费:" + buySums);
		record.setAgencyBalance(agent.getAgentDeposit().toString());
		shoppingRecordService.save(record);
		result = AjaxResut.toSuccess("0");
		renderJson(result);
	}

	/**
	 * 判断代理商级别是否为根节点 根据grade字段判断是否为1， 最顶层
	 * 
	 * @return
	 */
	public boolean agentType(AgentBrand agentBrand) {
		AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
		boolean flag = false;
		if (agentType.getGrade() == 1) {
			flag = true;
		}
		return flag;
	}

	/**
	 * 递归查询该品牌下所有上级代理
	 * 
	 * @throws ParseException
	 */
	public List<AgentBrand> getSupInfo(String brandId, String agentId, List<AgentBrand> agentBrandList) {
		AgentBrand agentBrand = new AgentBrand();
		agentBrand.setAuthStatus("1");
		agentBrand.setBrandId(brandId);
		agentBrand.setAgentId(agentId);
		List<AgentBrand> list = agentBrandService.findList(agentBrand);
		if (list.size() > 0) {
			for (AgentBrand data : list) {
				agentBrandList.add(data);
				getSupInfo(data.getBrandId(), data.getParentId(), agentBrandList);
			}
		}
		return agentBrandList;
	}

	/**
	 * 获取待审核订单
	 */
	@EnableCORS
	public void getAuditOrdersList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Orders data = new Orders();
		data.setOrderType("1");
		data.setState("0");
		data.setHandleAgentId(agent.getId());
		List<Orders> orderList = ordersService.findList(data);
		renderJson(AjaxResut.toSuccess(orderList));
	}

	/**
	 * 前台提货结算
	 */
	@EnableCORS
	public void takeGoods() {
		AjaxResut result = null;
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		String carProducts = reqObject.getString("stockIds");
		String addressId = reqObject.getString("addressId");
		String numbers = reqObject.getString("buyNums");
		String remarks = reqObject.getString("remarks");
		String[] ids = carProducts.split(",");
		String[] nums = numbers.split(",");
		List<AgentStock> stockList = new ArrayList<AgentStock>();
		AgentStock agentStock = new AgentStock();
		BigDecimal buySums = new BigDecimal(0);
		Integer buyNum = 0;
		for (int i = 0; i < ids.length; i++) {
			agentStock = agentStockService.findById(ids[i]);
			stockList.add(agentStock);
			buySums = buySums.add(new BigDecimal(nums[i]).multiply(agentStock.getPurchasePrice()));
			buyNum += Integer.valueOf(nums[i]);
		}
		Sku sku = skuService.findById(agentStock.getSkuId());
		Product product = productService.findById(sku.getProductId());
		String number = OrderNumberGenerator.get();
		Orders order = new Orders();
		order.setId(number);
		order.setOrderCreaterId(agent.getId());
		order.setCreateDate(new Date());
		order.setOrderGroupId(StrKit.getRandomUUID());
		order.setBuySum(buySums);
		order.setBuyNums(buyNum);
		order.setProductUnit(product.getUnit());
		order.setUpdateDate(new Date());
		order.setProductName(sku.getName());
		order.setRemarks(remarks);
		order.setHandleAgentId("0");
		order.setDelFlag("0");
		AgentAddress address = agentAddressService.findById(addressId);
		order.setCompanyId(agent.getCompanyId());
		order.setBrandId(product.getBrandId());
		order.setReceiveAddress(address.getAddressArea() + address.getAgentAddress());
		order.setReceiveName(address.getAgentName());
		order.setReceiverPhone(address.getAgentPhone());
		order.setAgentId(agent.getId());
		order.setAgentName(agent.getName());
		order.setProductImg(sku.getImage());
		order.setState("0");// 订单状态 -1 拒绝 0 待处理 1审核通过 2 待发货 3待收货 4结束
		order.setOrderType("2");// 订单类型(1.普通订单 2提货订单 3. 提现订单 4 董事充值订单 5 保证金订单 )
		order.setProductType("1");// 订单货物类型 （2云仓库1 实体仓库 3.虚拟货币）
		ordersService.save(order);
		for (int i = 0; i < ids.length; i++) {
			agentStock = stockList.get(i);
			sku = skuService.findById(agentStock.getSkuId());
			product = productService.findById(sku.getProductId());
			// 创建订单
			BigDecimal buySum = new BigDecimal(nums[i]).multiply(agentStock.getPurchasePrice());

			// 创建产品
			OrderProduct wpo = new OrderProduct();
			wpo.setId(StrKit.getRandomUUID());
			wpo.setProductImg(sku.getImage());
			wpo.setBuyNum(nums[i].toString());
			wpo.setOrderId(number);
			wpo.setCreateDate(new Date());
			wpo.setDelFlag("0");
			wpo.setBuyUprice(agentStock.getPurchasePrice() + "");
			wpo.setBuySum(buySum.toString());
			wpo.setProduct(sku.getId());
			wpo.setPackageSpec(Integer.valueOf(product.getPackageSpec()));
			wpo.setProductUnit(product.getUnit());
			orderProductService.save(wpo);
			// 新增代理商提货信息
			AgentTakeStock agentTakeStock = new AgentTakeStock();
			agentTakeStock.setAgentId(agent.getId());
			agentTakeStock.setCompanyId(agent.getCompanyId());
			agentTakeStock.setOrderId(number);
			agentTakeStock.setPurchasePrice(agentStock.getPurchasePrice());
			agentTakeStock.setSkuId(agentStock.getSkuId());
			agentTakeStock.setSkuName(agentStock.getSkuName());
			agentTakeStock.setSkuPrice(agentStock.getSkuPrice());
			agentTakeStock.setStockNumber(Integer.valueOf(nums[i]));
			agentTakeStock.setCreateDate(new Date());
			agentTakeStock.setCreateBy(agent.getId());
			agentTakeStock.setProductInfo(sku.getImage());
			agentTakeStock.setAgentStockId(agentStock.getId());
			agentTakeStock.setDelFlag("0");
			agentTakeStockService.save(agentTakeStock);
		}
		// 创建消费记录
		ShoppingRecord record = new ShoppingRecord();
		record.setId(StrKit.getRandomUUID());
		record.setCreateDate(new Date());
		record.setOrderId(order.getId());
		record.setShoppingPrice(buySums.toString());
		record.setType("1"); // 消费类型 是6 订单退款 5奖励退款 4打款 3 收到货款 2提取 还是 1购买商品
		record.setAgencyId(agent.getId());
		record.setDelFlag("0");
		record.setRemarks("进货消费:" + buySums);
		record.setAgencyBalance(agent.getAgentDeposit().toString());
		shoppingRecordService.save(record);
		result = AjaxResut.toSuccess("0");
		renderJson(result);
	}

	/**
	 * 获取我的提货订单
	 */
	@EnableCORS
	public void getStockOrdersList() {
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Orders data = new Orders();
		data.setOrderType("2");
		data.setOrderCreaterId(agent.getId());
		List<Orders> orderList = ordersService.findList(data);
		renderJson(AjaxResut.toSuccess(orderList));
	}

	/**
	 * 下级订单审核
	 */
	@EnableCORS
	public void auditSubOrders() {
		AjaxResut result = null;
		String orderId = getPara("orderId");
		String token = getPara("token");
		Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN, token);
		Orders oldOrder = ordersService.findById(orderId);
		oldOrder.setState("1");// 订单状态 -1 拒绝 0 待处理 1审核通过 2 待发货 3待收货 4结束
		ordersService.update(oldOrder);
		List<OrderProduct> orderProductList = orderProductService.findOrderProductByOrderId(orderId);
		List<Sku> skuList = new ArrayList<Sku>();
		Sku sku = new Sku();
		BigDecimal buySums = new BigDecimal(0);
		Integer buyNum = 0;
		if (orderProductList.size() > 0) {
			for (OrderProduct op : orderProductList) {
				sku = skuService.findById(op.getProduct());
				sku.setProductNumer(Integer.valueOf(op.getBuyNum()));
				skuList.add(sku);
				AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agent.getId(), sku.getBrandId());
				AgentPrice agentPrice = agentPriceService.findBySkuAndAgentType(sku.getId(), agentBrand.getAgentType());
				buySums = buySums
						.add(new BigDecimal(op.getBuyNum()).multiply(new BigDecimal(agentPrice.getAgencyPrice())));
				buyNum += Integer.valueOf(op.getBuyNum());
			}
		}
		String number = OrderNumberGenerator.get();
		Product product = productService.findById(sku.getProductId());
		AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agent.getId(), sku.getBrandId());
		// 差价返利
		BigDecimal lr = new BigDecimal(0);
		AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
		if (agentType.getIsProfit().equals("1")) {
			lr = buySums.multiply(agentType.getProfit());
			AgentReward wsAgencyReward = new AgentReward();
			wsAgencyReward.setId(StrKit.getRandomUUID());
			wsAgencyReward.setCreateDate(new Date());
			wsAgencyReward.setDelFlag("0");
			wsAgencyReward.setRewardType("2"); // 奖励类型（4.奖励扣款 （订单取消奖励没有）1.个人代理差价 2.同级8%提成 3 团队季度奖)
			wsAgencyReward.setOrderId(oldOrder.getId());
			wsAgencyReward.setAgentId(agent.getId());
			wsAgencyReward.setBrandId(agentBrand.getBrandId());
			wsAgencyReward.setRemarks("");
			wsAgencyReward.setRewardMoney(lr);
			agentRewardService.save(wsAgencyReward);
			// 加奖金
			agentService.addReward(agent.getId(), lr);
		}
		// 创建订单
		Orders order = new Orders();
		order.setId(number);
		order.setOrderCreaterId(agent.getId());
		order.setCreateDate(new Date());
		order.setOrderGroupId(oldOrder.getOrderGroupId());
		order.setBuySum(buySums);
		order.setBuyNums(buyNum);
		order.setProductUnit(product.getUnit());
		order.setUpdateDate(new Date());
		order.setProductName(sku.getName());
		order.setRemarks(oldOrder.getRemarks());
		// 董事
		boolean flag = agentType(agentBrand);// 判断这个等级是否为最顶级
		if (flag == true) {
			order.setHandleAgentId("0");
		} else {
			order.setHandleAgentId(agentBrand.getParentId());
		}
		order.setDelFlag("0");
		order.setCompanyId(agent.getCompanyId());
		order.setBrandId(product.getBrandId());
		order.setReceiveAddress(oldOrder.getReceiveAddress());
		order.setReceiveName(oldOrder.getReceiveName());
		order.setReceiverPhone(oldOrder.getReceiverPhone());
		order.setAgentId(agent.getId());
		order.setAgentName(agent.getName());
		order.setProductImg(sku.getImage());
		order.setPayment(oldOrder.getPayment());// 支付方式 0货款 1现金
		order.setPaymentCert(oldOrder.getPaymentCert());
		order.setState("0");// 订单状态 -1 拒绝 0 待处理 1审核通过 2 待发货 3待收货 4结束
		order.setOrderType("1");// 订单类型(1.普通订单 2提货订单 3. 提现订单 4 董事充值订单 5 保证金订单 )
		order.setProductType("1");// 订单货物类型 （2云仓库1 实体仓库 3.虚拟货币）
		ordersService.save(order);
		if (orderProductList.size() > 0) {
			OrderProduct orderP = new OrderProduct();
			for (int i = 0; i < orderProductList.size(); i++) {
				orderP = orderProductList.get(i);
				sku = skuList.get(i);
				// 创建订单
				if (orderP.getProduct().equals(sku.getId())) {
					AgentPrice agentPrice = agentPriceService.findBySkuAndAgentType(sku.getId(),
							agentBrand.getAgentType());
					product = productService.findById(sku.getProductId());
					BigDecimal buySum = new BigDecimal(orderP.getBuyNum())
							.multiply(new BigDecimal(agentPrice.getAgencyPrice()));
					// 创建产品
					OrderProduct wpo = new OrderProduct();
					wpo.setId(StrKit.getRandomUUID());
					wpo.setProductImg(sku.getImage());
					wpo.setBuyNum(orderP.getBuyNum());
					wpo.setOrderId(number);
					wpo.setCreateDate(new Date());
					wpo.setDelFlag("0");
					wpo.setBuyUprice(agentPrice.getAgencyPrice());
					wpo.setBuySum(buySum.toString());
					wpo.setProduct(sku.getId());
					wpo.setPackageSpec(Integer.valueOf(product.getPackageSpec()));
					wpo.setProductUnit(product.getUnit());
					orderProductService.save(wpo);
				}
			}
		}
		// 创建消费记录
		ShoppingRecord record = new ShoppingRecord();
		record.setId(StrKit.getRandomUUID());
		record.setCreateDate(new Date());
		record.setOrderId(order.getId());
		record.setShoppingPrice(buySums.toString());
		record.setType("3"); // 消费类型 是6 订单退款 5奖励退款 4打款 3 收到货款 2提取 还是 1购买商品
		record.setAgencyId(agent.getId());
		record.setDelFlag("0");
		record.setRemarks("收到货款:" + buySums);
		record.setAgencyBalance(agent.getAgentDeposit().toString());
		shoppingRecordService.save(record);
		result = AjaxResut.toSuccess("0");
		renderJson(result);
	}
}