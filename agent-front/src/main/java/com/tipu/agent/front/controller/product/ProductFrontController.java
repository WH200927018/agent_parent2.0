/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.tipu.agent.front.controller.product;

import java.util.ArrayList;
import java.util.List;

import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.front.service.api.ProductImagesService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.entity.model.Product;
import com.tipu.agent.front.service.entity.model.ProductImages;
import com.tipu.agent.front.service.entity.model.ProductType;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;


/**
 * 前台商品 product
 * @author hulin
 *
 */
@RequestMapping(value = "/product")
public class ProductFrontController extends BaseController {
	
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductImagesService productImagesService;

	/**
	 * 商品列表页面
	 * @param model
	 * @return
	 */
	public void index() {
		Product product = new Product();
		product.setIsShow("1");
		List<Product> productLst = productService.findList(product);
		List<ProductType> productTypeLst = productTypeService.findAll();
		setAttr("productLst", productLst);
		setAttr("productTypeLst", productTypeLst);
		render("product_list.html");
	}	
	
	/**
	 * 产品具体信息
	 */
	public void detail(){
		String id = getPara("id");
		Product product = productService.findById(id);
		ProductImages productImages=new ProductImages();
		productImages.setProductId(id);
		List<ProductImages> productImagesList=productImagesService.findList(productImages);		
		setAttr("data", product).setAttr("productImagesList", productImagesList);;
		render("product_detail.html");
	}
	
}