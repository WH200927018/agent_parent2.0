package com.tipu.agent.front.controller.common;

import com.jfinal.render.ErrorRender;
import com.jfinal.render.Render;

import io.jboot.web.render.JbootRenderFactory;

public class TiPuRenderFactory extends JbootRenderFactory {
	@Override
	public Render getRender(String view) {
		return super.getRender(view);
	}

	@Override
	public Render getErrorRender(int errorCode) {
		return new ErrorRender(errorCode, constants.getErrorView(errorCode));
	}

	@Override
	public Render getJsonRender() {
		return new TiPuRender();
	}

	@Override
	public Render getJsonRender(String key, Object value) {
		return new TiPuRender(key, value);
	}

	@Override
	public Render getJsonRender(String[] attrs) {
		return new TiPuRender(attrs);
	}

	@Override
	public Render getJsonRender(String jsonText) {
		return new TiPuRender(jsonText);
	}

	@Override
	public Render getJsonRender(Object object) {
		return new TiPuRender(object);
	}

}
