package com.tipu.agent.front.utils;



/**
 * 服务常量
 * @author Rlax
 *
 */
public final class ServiceConst {  
    public final static String APPLY_ID_CODE= "apply_id_code";    
    
    public final static int SERVICE_PAGE_SIZE = 12;
}
