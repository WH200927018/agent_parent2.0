package com.tipu.agent.front.controller.support;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.front.controller.agent.LoginController;
import com.tipu.agent.front.controller.view.LoginModel;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.utils.AjaxResut;
import com.tipu.agent.front.utils.ErrorConst;

import io.jboot.Jboot;
import io.jboot.core.rpc.annotation.JbootrpcService;

/**
 * 前台登录拦截
 * 
 * @author
 *
 */
public class LoginInterceptor implements Interceptor {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	AgentService agentService;

	@Override
	public void intercept(Invocation inv) {
		String token =inv.getController().getPara("token");
		if(token==null) {
			inv.getController().renderJson(AjaxResut.toError(ErrorConst.LOGIN_TOKEN_NOT_EXISTS));
			return;
		}else {
			Agent agent = Jboot.me().getCache().get(LoginController.LOGIN_TOKEN,token);
			if(agent==null) {
				inv.getController().renderJson(AjaxResut.toError(ErrorConst.LOGIN_TIME_OUT));
				return;
			}
		}
		inv.invoke();
	}

}
