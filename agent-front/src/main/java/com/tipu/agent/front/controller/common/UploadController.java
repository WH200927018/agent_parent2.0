package com.tipu.agent.front.controller.common;

import java.io.File;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Clear;
import com.jfinal.kit.Ret;
import com.jfinal.upload.UploadFile;
import com.tipu.agent.admin.base.util.OssUtil;
import com.tipu.agent.admin.base.web.base.BaseController;

import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.cors.EnableCORS;

/**
 * 前台文件上传
 * 所有文件上传的接口都从这里走
 * @author hulin
 *
 */
@RequestMapping("/front/common/upload") 
public class UploadController extends BaseController {
	
	
	/**
	 * 代理商保证金凭证
	 */
	public void idsUpload(){
		String selfPath = "ids/pic/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 充值截图
	 */
	@EnableCORS
	public void recharge(){
		String selfPath = "recharge/";
		commonUploadOss(selfPath);
	}
	/**
	 * 公共上传
	 * @param selfPath
	 * 			阿里云自定义路径
	 */
	public void commonUploadOss(String selfPath){
		UploadFile uploadFile = getFile();
		File file = uploadFile.getFile(); 
        JSONObject data = new JSONObject();
        String path = "";
        if (uploadFile != null) {
        	try {
        		String fileName = uploadFile.getFileName();
                String extentionName = fileName.substring(fileName.lastIndexOf(".")); // 后缀名
                String newName = UUID.randomUUID().toString() + "_"+ extentionName;// 新名   
                String fileDivName = UUID.randomUUID().toString();
                String relativePath = File.separator+ selfPath +File.separator+fileDivName;   //相对路径
                String fileDiv = uploadFile.getUploadPath()+relativePath;
                File temp = new File(fileDiv);
                if(temp.exists()==false) {
    				 temp.mkdirs();				
                }
                String savePath = fileDiv+ File.separator + newName;
                String ossrelativePath = selfPath +savePath.substring(savePath.lastIndexOf(File.separator) + 1);
                uploadFile.getFile().renameTo(new File(fileDiv+ File.separator + newName)); // 重命名并上传文件
                path = OssUtil.upload(ossrelativePath, savePath); //传阿里云
                //返回任意数据即代表上传成功
                data.put("src", path);
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				file.delete();
			}
        }
       // renderHtml("12312313");
        renderJson(Ret.ok().set("code", "0").set("data", data).set("path", path).set("message", "上传成功"));
	}

}
