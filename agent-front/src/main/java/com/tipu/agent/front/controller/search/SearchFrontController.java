/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.tipu.agent.front.controller.search;


import java.util.List;

import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.util.Encodes;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.DataService;
import com.tipu.agent.front.service.api.AdFieldService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.entity.model.AdField;
import com.tipu.agent.front.service.entity.model.Agent;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 前台查询
 * 
 * @author hulin
 *
 */
@RequestMapping(value = "/search")
public class SearchFrontController extends BaseController {
	
	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
    private DataService dataApi;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AdFieldService adFieldService;

	
	/**
	 * 跳转到查询页
	 */
	public void index(){
		
		render("search_index.html");
	}
	
	/**
	 * 跳转到代理商查询
	 */
	public void searchAgent(){
		AdField adField = new AdField();
		adField.setIsShow("1");
		adField.setPosType("3");//3是代理商页
		List<AdField> adFieldItemList = adFieldService.findShowAdItem(adField);
		setAttr("adFieldItemList", adFieldItemList);
		render("agent/agent.html");
	}
	
	public void searchAgentIndex(String searchName){
		setAttr("searchName", searchName);
		render("agent/agent_info.html");
	}
	
	/**
	 * 代理商查询
	 */
	public void searchAgentInfo(String searchName){
		Agent agent = agentService.findAgent(searchName);
		if(agent != null) {
			String weixin = agent.getWeixin();
			String mobile = agent.getMobile();
			if(StrKit.notBlank(weixin)) {
				weixin = weixin.length() > 4 ? weixin.substring(0, weixin.length()-4) + "****" : "****";
			}
			
			if(StrKit.notBlank(mobile)) {
				mobile = mobile.length() > 4 ? mobile.substring(0, mobile.length()-4) + "****" : "****";
			}
			agent.setMobile(mobile);
			agent.setWeixin(weixin);
		}
		Ret ret = new Ret();
		ret.set("agent", agent);
		renderJson(ret);
	}
	
	/**
	 * 跳转到防伪查询
	 */
	public void searchAnticode(){
		
		Integer needInput = getParaToInt("needInput"); //==0的时候 是输入后四位
		Integer rand = getParaToInt("rand"); //产品编码
		String id = getPara("id");//编码后的防伪码ID
		Integer code = getParaToInt("code");
		String anticode = getPara("anticode");
		String configNo = getPara("configNo");
		setAttr("encodeId", Encodes.urlEncode(id));
		setAttr("anticode", anticode);
		setAttr("rand", rand);
		setAttr("id", id);
		setAttr("needInput", needInput);
		setAttr("code", code);
		setAttr("configNo", configNo);
		AdField adField = new AdField();
		adField.setIsShow("1");
		adField.setPosType("4");//4是防伪
		List<AdField> adFieldItemList = adFieldService.findShowAdItem(adField);
		setAttr("adFieldItemList", adFieldItemList);
		render("anticode/anticode.html");
	}
	
	/**
	 * 防伪查询结果
	 */
	public void searchAnticodeResult(){
		render("anticode/anticode_info.html");
	}

}