package com.tipu.agent.front.controller.view;

import com.tipu.agent.front.service.entity.model.Agent;

public class LoginModel {
	private String token;
	private long timeOut;
	private Agent agent;
	
	public LoginModel(String token, long timeOut, Agent agent) {
		super();
		this.token = token;
		this.timeOut = timeOut;
		this.agent = agent;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public long getTimeOut() {
		return timeOut;
	}
	public void setTimeOut(long timeOut) {
		this.timeOut = timeOut;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	
}
