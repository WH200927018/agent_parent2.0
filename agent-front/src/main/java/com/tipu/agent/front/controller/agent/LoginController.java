package com.tipu.agent.front.controller.agent;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.front.controller.view.LoginModel;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.utils.AjaxResut;
import com.tipu.agent.front.utils.ErrorConst;

import io.jboot.Jboot;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.utils.StringUtils;
import io.jboot.web.controller.annotation.RequestMapping;

@RequestMapping(value = "/login")
public class LoginController  extends BaseController {

	public static final String LOGIN_TOKEN = "login_token";
		
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	public void loginOut() {
		
	}
	
	/***
	 * 代理商登录
	 */
	public void loginCommit() {
		String phone = getPara("phone");
		String pwd = getPara("pwd");
		String companyId = getPara("companyId");
		Agent agent = agentService.findByPhone(phone, companyId);
		AjaxResut result = null;
		if(agent==null) {
			//没有该账号
			result = AjaxResut.toError(ErrorConst.LOGIN_NOT_ACCOUNT);
		}else {
			//系统未搜权
			if("0".equals(agent.getIsFristAuth())){
				result = AjaxResut.toError(ErrorConst.LOGIN_ACCOUNT_NOT_AUTH);
			}else {
				if(pwd.equals(agent.getPassword())==false) {
					result = AjaxResut.toError(ErrorConst.LOGIN_PWD_ERROR);
				}else{
					if("1".equals(agent.getFrozen())) {
						result = AjaxResut.toError(ErrorConst.LOGIN_ACCOUNT_STATE_EXCEPT);
					}else {
						//获取对应的品牌信息
						List<AgentBrand> brands = agentBrandService.findListByAgentIdAndAuthStatus(agent.getId(),"1");
						if(brands==null||brands.size()==0) {
							result = AjaxResut.toError(ErrorConst.LOGIN_IS_NOT_BRAND);
						}else {				
							agent.setBrands(brands);
							String token = StringUtils.uuid();
							int loginTime = 1000*60*60*24;
							long timeOut = new Date().getTime() +loginTime;
							Jboot.me().getCache().put(LOGIN_TOKEN, token, agent,loginTime);
							LoginModel model = new LoginModel(token, timeOut, agent);
							result = AjaxResut.toSuccess(model);
						}
					}		
				}
			}
		}
		renderJson(result);
	}
}
