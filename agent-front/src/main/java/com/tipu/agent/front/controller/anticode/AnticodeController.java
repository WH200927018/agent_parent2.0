package com.tipu.agent.front.controller.anticode;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.util.AESUtil;
import com.tipu.agent.admin.base.util.DateUtils;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.anticode.service.api.AnticodeConfigService;
import com.tipu.agent.anticode.service.api.AnticodeService;
import com.tipu.agent.anticode.service.entity.model.Anticode;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.entity.model.Product;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 前端防伪码查询控制器
 * @author think
 *
 */
@RequestMapping("/anticode")
public class AnticodeController extends BaseController {
	
	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0, timeout=30000)
	private AnticodeService anticodeService;
	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AnticodeConfigService anticodeConfigService;
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private ProductService productService;
	/**
	 * 防伪码解析(不比较用户输入防伪码正确与否)
	 * @param rand 产品编码
	 * @param id 编码后的防伪码ID
	 */
	public void parse(Integer rand, String id) {
		logger.info("Anticode parse: [rand:"+rand+"][id:"+id+"]");
		Anticode anticode = anticodeService.findByEncodeId(id, rand);
		renderJson(anticode);
	}
	
	/**
	 * 解析新的防伪
	 * @param ip IP地址
	 * @param bid BID编码
	 */
	public void parseNew(String ip, String bid) {
		logger.info("Anticode parse new: [ip: "+ip+"][bid: "+bid+"]");
		if(StrKit.notBlank(ip) && StrKit.notBlank(bid)) {
			if(bid.length() != 24) {
				renderJson();
			} else {
//				String productSn = bid.substring(0, 4);
				String configNo = bid.substring(4, 8); // 4位批号
				String anticode = bid.substring(8); // 16位防伪码
				Anticode resultEntity = anticodeService.findByAnticodeAndConfig(configNo, anticode);
				renderJson(resultEntity);
			}
			
		} else {
			renderJson();
		}
	}
	
	/**
	 * 防伪码查询(比较用户输入防伪码正确与否)
	 * @param ip 用户IP
	 * @param encodeId 编码后的防伪码ID
	 * @param configNo 批次号
	 * @param anticode 16位完整防伪码
	 * @param rand 产品编码
	 */
	public void search(String ip, String encodeId, String anticode, Integer rand) {
		logger.info("Anticode check: [ip: " + ip + "][rand: " + rand + "][encodeId: " + encodeId + "][anticode:" + anticode + "]");
		if(null != rand && StrKit.notBlank(encodeId)) {
			String decodeContent = AESUtil.aesDecode(encodeId, rand);
			logger.info("[decoded:" + decodeContent + "]");
			String[] contentArray = decodeContent.split("_");
			String parseConfigNo = contentArray[2];
			Anticode entity = new Anticode();
			entity.setId(contentArray[0]);
			entity.setAntiCode(anticode);
			entity.setConfigNo(parseConfigNo);
			
			
			try {
				DateUtils.parseDate(parseConfigNo, "yyyyMMddHHmmss");
			} catch (ParseException e) {
				e.printStackTrace();
				renderJson();
				return ;
			}
			Anticode resultEntity = anticodeService.findByAnticode(anticode, contentArray[0], parseConfigNo);
			renderJson(resultEntity);
		} else {
			renderJson();
		}
		
	}
	
	/**
	 * 防伪码查询(比较用户输入防伪码正确与否)
	 * @param ip 用户IP
	 * @param configNo 批次号
	 * @param anticode 16位完整防伪码
	 */
	public void searchAnticode(String ip, String configNo, String anticode) {
		logger.info("Anticode check: [ip: " + ip + "][configNo: " + configNo + "][anticode: " + anticode + "]");
		if(StrKit.notBlank(configNo) && StrKit.notBlank(anticode)) {
			Anticode resultEntity = anticodeService.findByAnticodeAndConfig(configNo, anticode);
			renderJson(resultEntity);
		} else {
			renderJson();
		}
		
	}
	
	/**
	 * 根据箱码查询防伪码信息
	 * @param ip IP地址
	 * @param logisticsCode 物流码
	 */
	public void delivery(String ip, String logisticsCode) {
		logger.info("Anticode delivery: [ip: " + ip + "][logisticsCode: " + logisticsCode + "]");
		String code = String.valueOf(logisticsCode.charAt(1));
		Product product = productService.findByCode(code);
		Anticode anticode = anticodeService.findByLogisticsCode(logisticsCode, product.getId());
		renderJson(anticode);
	}
	
	/**
	 * 根据箱码对应的代理信息
	 * @param ip IP地址
	 * @param codes 物流码
	 * @param agentId 代理ID
	 */
	public void deliveryUpdate(String ip, String codes, String agentId) {
		logger.info("Anticode delivery: [ip: " + ip + "][logisticsCodes: " + codes + "][agentId: " + agentId + "]");
		Map<String, String> codeProductMap = new HashMap<String, String>();
		if(StrKit.notBlank(codes)) {
			String[] codeArray = codes.split(",");
			for (int i = 0; i < codeArray.length; i++) {
				String code = String.valueOf(codeArray[i].charAt(1));
				Product product = productService.findByCode(code);
				codeProductMap.put(codeArray[i], product.getId());
			}
		}
		anticodeService.updateDelivery(codeProductMap, agentId);
		renderJson();
	}
	
}
