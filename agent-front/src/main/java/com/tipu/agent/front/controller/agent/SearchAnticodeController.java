/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.tipu.agent.front.controller.agent;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.weixin.sdk.api.JsTicket;
import com.jfinal.weixin.sdk.api.JsTicketApi;
import com.jfinal.weixin.sdk.api.JsTicketApi.JsApiType;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.util.WeiXinUtils;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.anticode.service.api.AnticodeService;
import com.tipu.agent.anticode.service.entity.model.Anticode;
import com.tipu.agent.front.controller.support.LoginInterceptor;
import com.tipu.agent.front.service.api.AgentLoginLogService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.utils.AjaxResut;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.cors.EnableCORS;

/**
 * 防伪查询
 * 
 * @author hulin
 *
 */
@RequestMapping(value = "/f")
public class SearchAnticodeController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_SYSTEM, version = ServiceConst.VERSION_1_0)
	private BrandService brandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentLoginLogService agentLoginLogService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;

	@JbootrpcService(group = ServiceConst.SERVICE_ANTICODE, version = ServiceConst.VERSION_1_0, timeout = 30000)
	private AnticodeService anticodeService;

	private static String frontUrl = PropKit.get("jboot.frontUrl");
	private static String appfrontUrl = PropKit.get("jboot.appfrontUrl");

	/**
	 * 页面进入扫一扫引导页
	 */
	@EnableCORS
	public void anticode() {
		JSONObject reqObject = JSONObject.parseObject(getBodyString());
		String url = reqObject.getString("url");
		JsTicket ticket = JsTicketApi.getTicket(JsApiType.jsapi);
		Map<String, String> map = WeiXinUtils.sign(ticket.getTicket(),url);
		Ret ret = new Ret();
		ret.set("timestamp", map.get("timestamp"));
		ret.set("nonceStr", map.get("nonceStr"));
		ret.set("signature", map.get("signature"));
		ret.set("appId", ApiConfigKit.getAppId());
		renderJson(AjaxResut.toSuccess(ret));
	}

	/**
	 * 扫一扫
	 */
	@EnableCORS
	public void index() {
		HttpServletRequest request = getRequest();
		String url = request.getRequestURL().toString();
		if (url.contains(frontUrl)) {
			String bid = getPara("bid");
			if (StrKit.notBlank(bid)) {
				if (bid.length() != 24) {
					renderJson(AjaxResut.toError(""));
					return;
				} else {
					Ret ret = new Ret();
					// String productSn = bid.substring(0, 4);
					String configNo = bid.substring(4, 8); // 4位批号
					String anticode = bid.substring(8); // 16位防伪码
					Anticode resultEntity = anticodeService.findByAnticodeAndConfig(configNo, anticode);
					if (null != resultEntity) {
						ret.set("anticode", resultEntity.getAntiCode().substring(0, 12));
						ret.set("bid", bid);
					} else {
						// 如果查不到 防伪 跳入非法界面
						renderJson(AjaxResut.toError(""));
						return;
					}

					JsTicket ticket = JsTicketApi.getTicket(JsApiType.jsapi);
					Map<String, String> map = WeiXinUtils.sign(ticket.getTicket(), request.getRequestURL().toString());
					ret.set("timestamp", map.get("timestamp"));
					ret.set("nonceStr", map.get("nonceStr"));
					ret.set("signature", map.get("signature"));
					ret.set("appId", ApiConfigKit.getAppId());
					String shareUrl = appfrontUrl + "/security?bid=" + bid;
					ret.set("shareUrl", shareUrl);
					renderJson(AjaxResut.toSuccess(ret));
					return;
				}
			}
		} else {
			renderJson(AjaxResut.toError(""));
			return;
		}
	}

}