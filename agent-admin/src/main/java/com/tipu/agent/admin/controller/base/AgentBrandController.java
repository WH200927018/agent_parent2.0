package com.tipu.agent.admin.controller.base;

import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.util.CommonUtils;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.SettingValService;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.api.DataService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAuth;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.SettingVal;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.Data;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AgentBrand管理
 * @author wh
 *
 */
@RequestMapping("/base/agent_brand")
public class AgentBrandController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AgentService agentService;
	
	@JbootrpcService
	private BrandService brandService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService
	private DataService dataApi;
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SettingValService settingService;
	
	/**
     * index
     */
    public void index() {
    	String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<Agent> agentList = agentService.findAll(companyId, ""); //代理商
    	List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);//代理商级别
    	setAttr("agentList", agentList);
    	setAttr("agentTypeList", agentTypeList);
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        AgentBrand data = new AgentBrand();
        if(StrKit.notBlank(getPara("agent"))){
        	data.setAgentId(getPara("agent"));
        }
        if(StrKit.notBlank(getPara("brand"))){
        	data.setBrandId(getPara("brand"));
        }
        if(StrKit.notBlank(getPara("agentType"))){
        	data.setAgentType(getPara("agentType"));
        }
        String companyId=AuthUtils.getLoginUser().getCompanyId();
//		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
        Page<AgentBrand> dataPage = agentBrandService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<AgentBrand>(dataPage));
    }

    /**
     * add
     */
    public void add() {
    	String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<Agent> agentList = agentService.findAll(companyId, ""); //代理商
    	List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);//代理商级别
    	setAttr("agentList", agentList);
    	setAttr("agentTypeList", agentTypeList);
        render("add.html");
    }

    /**
     * 保存提交
     * @throws ParseException 
     * @throws IOException 
     * @throws FontFormatException 
     */
    public void postAdd() throws ParseException, FontFormatException, IOException {
        AgentBrand data = getBean(AgentBrand.class, "data");
        data.setCreateDate(new Date());
        data.setAuthTime(new Date());
        data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
        data.setDelFlag("0");
        data.setAuditRootStatus(data.getAuditStatus());
        data.setAuditAgentId("0");
        data.setAuditType("0");
        String companyId=AuthUtils.getLoginUser().getCompanyId();
//		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
       
     // 生成编号 无查询某一个类别的最大编号
     		String agentLevel = agentTypeService.findById(data.getAgentType()).getGrade().toString();
     		if (StrKit.notBlank(agentLevel)) {
     			String pre = "";
     			if (agentLevel.equals("1")) {
     				pre = "A";
     			} else if (agentLevel.equals("2")) {
     				pre = "B";
     			} else if (agentLevel.equals("3")) {
     				pre = "C";
     			} else if (agentLevel.equals("4")) {
     				pre = "D";
     			} else if (agentLevel.equals("5")) {
     				pre = "E";
     			} else if (agentLevel.equals("6")) {
     				pre = "F";
     			} else if (agentLevel.equals("7")) {
     				pre = "G";
     			}
     			String maxCode = agentBrandService.findMaxCode(data);
     			if (StrKit.notBlank(maxCode)) {
     				maxCode = maxCode.replace(pre, "");
     				Integer code = Integer.parseInt(maxCode) + 1;
     				maxCode = pre + code;
     			} else {
     				maxCode = pre + "10001";
     			}
     			data.setCode(maxCode);
     		}
     		
        if (!agentBrandService.save(data)) {
            throw new BusinessException("保存失败");
        }
        agentBrandService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        AgentBrand data = agentBrandService.findById(id);
        String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<Agent> agentList = agentService.findAll(companyId, ""); //代理商
    	List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);//代理商级别
    	setAttr("agentList", agentList);
    	setAttr("agentTypeList", agentTypeList);
        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     * @throws ParseException 
     * @throws IOException 
     * @throws FontFormatException 
     */
    public void postUpdate() throws ParseException, FontFormatException, IOException {
        AgentBrand data = getBean(AgentBrand.class, "data");
        data.setAuditRootStatus(data.getAuditStatus());
        data.setUpdateDate(new Date());
        data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
        AgentBrand oldData =agentBrandService.findById(data.getId());
        if ( oldData== null) {
            throw new BusinessException("数据不存在");
        }
        if (!agentBrandService.update(data)) {
            throw new BusinessException("修改失败");
        }
        agentBrandService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
    	String id = getPara("id");
    	AgentBrand agentBrand = new AgentBrand();
    	agentBrand.setId(id);
    	agentBrand.setDelFlag("1");
        if (!agentBrandService.update(agentBrand)) {
            throw new BusinessException("删除失败");
        }
        agentBrandService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }
    
    /**
	 * 更新有效期时间
	 * 
	 * @throws ParseException
	 */
	public void updateValidityDate() throws ParseException {
		String id = getPara("id");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		AgentBrand data = agentBrandService.findById(id);
		if (data == null) {
			throw new BusinessException("数据不存在");
		}
		SettingVal setting = new SettingVal();
		setting.setSettingId("is_limit");
		setting.setCompanyId(companyId);
		setting.setBrandId(brandId);
		setting = settingService.getSettingBySysKey(setting);
		if (setting != null) {
			if ("1".equals(setting.getEnable())) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Integer num = 30 * Integer.parseInt(setting.getVal());
				String validityDate = getDay(format.format(new Date()), num);
				data.setExpireTime(format.parse(validityDate));// 添加有效期时间
				data.setUpdateDate(new Date());
				data.setAuthStatus("1");
				data.setAuditRootStatus("1");
				data.setAuditStatus("1");
				data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
				agentBrandService.update(data);
				agentBrandService.refreshCache();
			} else {
				throw new BusinessException("代理商有效期设置未启用!");
			}
		} else {
			throw new BusinessException("代理商有效期设置未创建!");
		}
		renderJson(RestResult.buildSuccess());
	}
    
	/**
	 * 求后num天
	 * 
	 * @param day
	 * @return
	 */
	public String getDay(String day, int num) {
		Calendar c = Calendar.getInstance();
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(day);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day1 = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day1 + num);

		String dayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
		return dayAfter;
	}
}