package com.tipu.agent.admin.controller.anticode;

import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.anticode.service.api.AntiRuleService;
import com.tipu.agent.anticode.service.entity.model.AntiRule;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AntiRule管理
 * @author think
 *
 */
@RequestMapping("/anticode/anti_rule")
public class AntiRuleController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AntiRuleService antiRuleService;
	
	/**
     * index
     */
    public void index() {
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        AntiRule data = new AntiRule();

        Page<AntiRule> dataPage = antiRuleService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<AntiRule>(dataPage));
    }

    /**
     * add
     */
    public void add() {
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        AntiRule data = getBean(AntiRule.class, "data");

        if (!antiRuleService.save(data)) {
            throw new BusinessException("保存失败");
        }
        antiRuleService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        AntiRule data = antiRuleService.findById(id);

        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        AntiRule data = getBean(AntiRule.class, "data");

        if (antiRuleService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!antiRuleService.update(data)) {
            throw new BusinessException("修改失败");
        }
        antiRuleService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
        String id = getPara("id");
        if (!antiRuleService.deleteById(id)) {
            throw new BusinessException("删除失败");
        }
        antiRuleService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}