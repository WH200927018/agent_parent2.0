package com.tipu.agent.admin.controller.base;

import java.util.Date;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.ArticleService;
import com.tipu.agent.front.service.entity.model.Article;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Article管理
 * @author think
 *
 */
@RequestMapping("/base/article")
public class ArticleController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private ArticleService articleService;
	
	/**
     * index
     */
    public void index() {
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        Article data = new Article();
        if(StrKit.notBlank(getPara("title"))){
        	data.setTitle(getPara("title"));
        }
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
        Page<Article> dataPage = articleService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<Article>(dataPage));
    }

    /**
     * add
     */
    public void add() {
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        Article data = getBean(Article.class, "data");
        if (StrKit.isBlank(data.getContent())) {
			throw new BusinessException("内容不能为空！");
		}
        data.setCreateDate(new Date());
        data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
        data.setDelFlag("0");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
        if (!articleService.save(data)) {
            throw new BusinessException("保存失败");
        }
        articleService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        Article data = articleService.findById(id);
        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        Article data = getBean(Article.class, "data");
        if (StrKit.isBlank(data.getContent())) {
			throw new BusinessException("内容不能为空！");
		}
        data.setUpdateDate(new Date());
        data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
        if (articleService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!articleService.update(data)) {
            throw new BusinessException("修改失败");
        }
        articleService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
        String id = getPara("id");
        Article article = new Article();
        article.setId(id);
        article.setDelFlag("1");
        if (!articleService.update(article)) {
            throw new BusinessException("删除失败");
        }
        articleService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}