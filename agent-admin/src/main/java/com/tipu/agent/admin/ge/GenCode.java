package com.tipu.agent.admin.ge;

import com.tipu.agent.admin.base.gencode.controller.AppControllerGenerator;

/**
 * 代码生成
 * @author Rlax
 *
 */
public class GenCode {

    public static void main(String[] args) {
        AppControllerGenerator.doGenerate();
    }
}
