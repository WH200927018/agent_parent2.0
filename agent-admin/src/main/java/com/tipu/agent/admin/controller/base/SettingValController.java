package com.tipu.agent.admin.controller.base;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.front.service.api.SettingService;
import com.tipu.agent.front.service.entity.model.Setting;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.SettingValService;
import com.tipu.agent.front.service.entity.model.SettingVal;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * SystemConfig管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/settingVal")
public class SettingValController extends BaseController {
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SettingValService settingValService;
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SettingService settingService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		SettingVal data = new SettingVal();
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<SettingVal> dataPage = settingValService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<SettingVal>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {	
		setAttr("settingList", settingService.findAll()).render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		SettingVal data = getBean(SettingVal.class, "data");
		String uuid = StrKit.getRandomUUID();
		data.setId(uuid);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		if (!settingValService.save(data)) {
			throw new BusinessException("保存失败");
		}
		
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		SettingVal data = settingValService.findById(id);
		setAttr("settingList", settingService.findAll());
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		SettingVal data = getBean(SettingVal.class, "data");
		SettingVal oldData = settingValService.findById(data.getId());
		if (oldData == null) {
			throw new BusinessException("数据不存在");
		}
		if (!settingValService.update(data)) {
			throw new BusinessException("修改失败");
		}
		
		renderJson(RestResult.buildSuccess());
	}
	
	/**
	 * 更新开关
	 */
	public void updateSysValue(){
		String id = getPara("id");
		String enable = getPara("enable");
		if(StrKit.notBlank(id) && StrKit.notBlank(enable)){
			SettingVal data = settingValService.findById(id);
			data.setEnable(enable);
			if (!settingValService.update(data)) {
				throw new BusinessException("修改失败");
			}
			
			renderJson(RestResult.buildSuccess());
		}
	}

//	/**
//	 * 删除
//	 */
//	@NotNullPara({ "id" })
//	public void delete() {
//		String id = getPara("id");
//		Setting systemConfig = new Setting();
//		systemConfig.setId(id);
//		if (!settingValService.update(systemConfig)) {
//			throw new BusinessException("删除失败");
//		}
//		
//		renderJson(RestResult.buildSuccess());
//	}
	
}
