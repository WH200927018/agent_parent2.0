package com.tipu.agent.admin.controller.system;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.api.CompanyRoleService;
import com.tipu.agent.admin.service.api.CompanyService;
import com.tipu.agent.admin.service.api.RoleService;
import com.tipu.agent.admin.service.api.UserService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.Company;
import com.tipu.agent.admin.service.entity.model.CompanyRole;
import com.tipu.agent.admin.service.entity.model.Role;
import com.tipu.agent.admin.service.entity.model.User;
import com.tipu.agent.admin.support.auth.AuthUtils;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Company管理
 * 
 * @author think
 *
 */
@RequestMapping("/system/company")
public class CompanyController extends BaseController {

	@JbootrpcService
	private CompanyService companyService;

	@JbootrpcService
	private BrandService brandService;

	@JbootrpcService
	private RoleService roleService;

	@JbootrpcService
	private UserService userService;

	@JbootrpcService
	private CompanyRoleService companyRoleService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		Company data = new Company();
		data.setName(getPara("name"));
		Page<Company> dataPage = companyService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Company>(dataPage));
	}

	/**
	 * 查看logo
	 */
	public void findlogo() {
		String id = getPara("id");
		Ret ret = new Ret();
		Company data = companyService.findById(id);
		String img = data.getLogoPic();
		if (StrKit.notBlank(img)) {
			ret.set("isOk", true);
			ret.set("img", img);
		} else {
			ret.set("isOk", false);
		}
		renderJson(ret);
	}

	/**
	 * add
	 */
	public void add() {
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Company data = getBean(Company.class, "data");
		Company company = new Company();
		company.setName(data.getName());
		List<Company> companyList = companyService.findList(company);
		if (companyList.size() > 0) {
			throw new BusinessException("公司名称不能重复");
		}
		String homeUrl = data.getHomeUrl();
		if (homeUrl.indexOf("http") == -1) {
			homeUrl = "http://" + homeUrl;
			data.setHomeUrl(homeUrl);
		}
		List<Role> roleList = roleService.findByStatusUsedAndType();
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		if (!companyService.saveCompany(data, roleList)) {
			throw new BusinessException("保存失败");
		}
		companyService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		Company data = companyService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Company data = getBean(Company.class, "data");
		int count = companyService.countCompany(data);
		if (count > 0) {
			throw new BusinessException("公司名称不能重复");
		}
		String homeUrl = data.getHomeUrl();
		if (homeUrl.indexOf("http") == -1) {
			homeUrl = "http://" + homeUrl;
			data.setHomeUrl(homeUrl);
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (companyService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!companyService.update(data)) {
			throw new BusinessException("修改失败");
		}
		companyService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		// 查询品牌
		Brand brand = new Brand();
		brand.setCompanyId(id);
		List<Brand> brandList = brandService.findList(brand);
		if (brandList.size() > 0) {
			throw new BusinessException("请先删除该公司所有的品牌");
		}
		// 查询用户
		User user = new User();
		user.setCompanyId(id);
		List<User> userList = userService.findList(user);
		if (userList.size() > 0) {
			throw new BusinessException("请先删除该公司所有的用户");
		}
		Company Company = new Company();
		Company.setId(id);
		Company.setDelFlag("1");
		if (!companyService.update(Company)) {
			throw new BusinessException("删除失败");
		}
		companyService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 添加超级管理员
	 */
	public void addAdmin() {
		List<Company> companyList = companyService.findAll();
		setAttr("companyList", companyList);
		render("addAdmin.html");
	}

	/**
	 * 根据公司id查询角色
	 */
	public void findRole() {
		String companyId = getPara("companyId");
		Ret ret = new Ret();
		List<CompanyRole> companyRoleList = companyRoleService.findByCompanyId(companyId);
		if (companyRoleList.size() > 0) {
			ret.set("isOk", true);
			ret.set("companyRoleList", companyRoleList);
		} else {
			ret.set("isOk", false);
		}
		renderJson(ret);
	}

	/**
	 * 超级管理员保存提交
	 */
	public void postAddAdmin() {
		User sysUser = getBean(User.class, "user");
		Long[] roles = getParaValuesToLong("userRole");
		if (roles != null) {
			if (roles.length == 0) {
				throw new BusinessException("请选择用户角色");
			}
		} else {
			throw new BusinessException("请选择用户角色");
		}
		if (userService.hasUser(sysUser.getName())) {
			throw new BusinessException("用户名已经存在");
		}
		sysUser.setLastUpdAcct(AuthUtils.getLoginUser().getName());
		if (!userService.saveUser(sysUser, roles)) {
			throw new BusinessException("保存失败");
		}

		renderJson(RestResult.buildSuccess());
	}

}