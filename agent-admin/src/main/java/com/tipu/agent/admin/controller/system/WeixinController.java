package com.tipu.agent.admin.controller.system;

import java.util.Date;
import java.util.List;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.admin.service.api.WeixinService;
import com.tipu.agent.admin.service.entity.model.Weixin;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 微信管理
 * 
 * @author think
 *
 */
@RequestMapping("/system/weixin")
public class WeixinController extends BaseController {

	@JbootrpcService
	private WeixinService weixinService;

	/**
	 * index
	 */
	public void index() {
		List<Weixin> wxList = weixinService.findAll();
		setAttr("wxList", wxList).render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		Weixin data = new Weixin();
		if (StrKit.notBlank(getPara("name"))) {
			data.setWxName(getPara("name"));
		}
		if (StrKit.notBlank(getPara("appid"))) {
			data.setWxAppId(getPara("appid"));
		}
		if (StrKit.notBlank(getPara("wxNo"))) {
			data.setWxNo(getPara("wxNo"));
		}
		if (StrKit.notBlank(getPara("id"))) {
			data.setId(getPara("id"));
		}
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		Page<Weixin> dataPage = weixinService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Weixin>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Weixin data = getBean(Weixin.class, "data");
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		data.setDelFlag("0");
		String wxOauthUrl = data.getWxOauthUrl();
		if (wxOauthUrl.indexOf("http") == -1) {
			wxOauthUrl = "http://" + wxOauthUrl;
			data.setWxOauthUrl(wxOauthUrl);
		}
		if (!weixinService.save(data)) {
			throw new BusinessException("保存失败");
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		Weixin data = weixinService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Weixin data = getBean(Weixin.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		if (weixinService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}
		String wxOauthUrl = data.getWxOauthUrl();
		if (wxOauthUrl.indexOf("http") == -1) {
			wxOauthUrl = "http://" + wxOauthUrl;
			data.setWxOauthUrl(wxOauthUrl);
		}
		if (!weixinService.update(data)) {
			throw new BusinessException("修改失败");
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Weixin data = new Weixin();
		data.setId(id);
		data.setDelFlag("1");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!weixinService.update(data)) {
			throw new BusinessException("删除失败");
		}
		renderJson(RestResult.buildSuccess());
	}

}
