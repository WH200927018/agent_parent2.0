package com.tipu.agent.admin.controller.anticode;

import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.anticode.service.api.AntiRuleRecordService;
import com.tipu.agent.anticode.service.entity.model.AntiRuleRecord;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AntiRuleRecord管理
 * @author think
 *
 */
@RequestMapping("/anticode/anti_rule_record")
public class AntiRuleRecordController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AntiRuleRecordService antiRuleRecordService;
	
	/**
     * index
     */
    public void index() {
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        AntiRuleRecord data = new AntiRuleRecord();

        Page<AntiRuleRecord> dataPage = antiRuleRecordService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<AntiRuleRecord>(dataPage));
    }

    /**
     * add
     */
    public void add() {
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        AntiRuleRecord data = getBean(AntiRuleRecord.class, "data");

        if (!antiRuleRecordService.save(data)) {
            throw new BusinessException("保存失败");
        }
        antiRuleRecordService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        AntiRuleRecord data = antiRuleRecordService.findById(id);

        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        AntiRuleRecord data = getBean(AntiRuleRecord.class, "data");

        if (antiRuleRecordService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!antiRuleRecordService.update(data)) {
            throw new BusinessException("修改失败");
        }
        antiRuleRecordService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
    	String id = getPara("id");
        if (!antiRuleRecordService.deleteById(id)) {
            throw new BusinessException("删除失败");
        }
        antiRuleRecordService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}