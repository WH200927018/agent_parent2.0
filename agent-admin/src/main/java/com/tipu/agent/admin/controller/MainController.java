package com.tipu.agent.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.StrKit;
import com.tipu.agent.admin.base.common.CacheKey;
import com.tipu.agent.admin.base.common.Consts;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.plugin.shiro.MuitiLoginToken;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.api.UserService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.User;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.admin.validator.LoginValidator;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jsonwebtoken.Jwt;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;

/**
 * 主控制器
 * 
 * @author Rlax
 *
 */
@RequestMapping("/")
public class MainController extends BaseController {

	@JbootrpcService
	private UserService userService;

	@JbootrpcService
	private BrandService brandService;

	public void index() {
		List<Brand> brandList = new ArrayList<Brand>();
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		if (StrKit.notBlank(companyId)) {
			Brand brand = new Brand();
			brand.setCompanyId(companyId);
			brandList = brandService.findList(brand);
		}
		JbootRedis redis = Jboot.me().getRedis();
		String brandName = redis.get(companyId + "_brandName");
		setAttr("brandName", brandName).setAttr("brandList", brandList).render("index.html");
	}

	public void login() {
		if (SecurityUtils.getSubject().isAuthenticated()) {
			redirect("/");
		} else {
			render("login.html");
		}
	}

	public void captcha() {
		renderCaptcha();
	}

	@Before({ POST.class, LoginValidator.class })
	public void postLogin() {
		String loginName = getPara("loginName");
		String pwd = getPara("password");

		MuitiLoginToken token = new MuitiLoginToken(loginName, pwd);
		Subject subject = SecurityUtils.getSubject();

		RestResult<String> restResult = new RestResult<String>();
		restResult.success().setMsg("登录成功");
         
		try {
			if (!subject.isAuthenticated()) {
				token.setRememberMe(false);
				subject.login(token);
				User u = userService.findByName(loginName);
				subject.getSession(true).setAttribute(Consts.SESSION_USER, u);
			}
			if (getParaToBoolean("rememberMe") != null && getParaToBoolean("rememberMe")) {
				setCookie("loginName", loginName, 60 * 60 * 24 * 7);
			} else {
				removeCookie("loginName");
			}
			// 登录时默认第一个品牌
			List<Brand> brandList = new ArrayList<Brand>();
			String companyId = AuthUtils.getLoginUser().getCompanyId();
			if (StrKit.notBlank(companyId)) {
				Brand brand = new Brand();
				brand.setCompanyId(companyId);
				brandList = brandService.findList(brand);
				if (brandList.size() > 0) {
					brand = brandList.get(0);
					JbootRedis redis = Jboot.me().getRedis();
					redis.set(companyId + "_brandId", brand.getId());
					redis.set(companyId + "_brandName", brand.getName());
				}
			}
		} catch (UnknownAccountException une) {
			restResult.error("用户名不存在");
		} catch (LockedAccountException lae) {
			restResult.error("用户被锁定");
		} catch (IncorrectCredentialsException ine) {
			restResult.error("用户名或密码不正确");
		} catch (ExcessiveAttemptsException exe) {
			restResult.error("账户密码错误次数过多，账户已被限制登录1小时");
		} catch (Exception e) {
			e.printStackTrace();
			restResult.error("服务异常，请稍后重试");
		}

		renderJson(restResult);
	}

	/**
	 * 退出
	 */
	public void logout() {
		if (SecurityUtils.getSubject().isAuthenticated()) {
			SecurityUtils.getSubject().logout();
		}
		render("login.html");
	}

	public void welcome() {
		render("welcome.html");
	}

	/**
	 * 品牌缓存修改
	 */
	public void changeBrand() {
		String brandId = getPara("brandId");
		JbootRedis redis = Jboot.me().getRedis();
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		redis.set(companyId + "_brandId", brandId);
		Brand brand = brandService.findById(brandId);
		if (brand != null) {
			redis.set(companyId + "_brandName", brand.getName());
		}
		renderJson(RestResult.buildSuccess());
	}

}
