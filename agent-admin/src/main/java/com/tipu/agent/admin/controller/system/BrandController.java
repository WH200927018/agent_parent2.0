package com.tipu.agent.admin.controller.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.api.CompanyService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.Company;
import com.tipu.agent.admin.support.auth.AuthUtils;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Brand管理
 * 
 * @author think
 *
 */
@RequestMapping("/system/brand")
public class BrandController extends BaseController {

	@JbootrpcService
	private BrandService brandService;

	@JbootrpcService
	private CompanyService companyService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		Brand data = new Brand();
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		data.setName(getPara("name"));
		Page<Brand> dataPage = brandService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Brand>(dataPage));
	}

	/**
	 * 查看logo
	 */
	public void findlogo() {
		String id = getPara("id");
		Brand data = brandService.findById(id);
		setAttr("data", data).render("logo.html");
	}

	/**
	 * add
	 */
	public void add() {
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Brand data = getBean(Brand.class, "data");
		data.setCreateDate(new Date());
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		if (brandService.findByCode(data) > 0) {
			throw new BusinessException("品牌编号请勿重复");
		}
		if (!brandService.save(data)) {
			throw new BusinessException("保存失败");
		}
		brandService.refreshCache();
		Brand brand = new Brand();
		brand.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		List<Brand> brandList = brandService.findList(brand);
		if (brandList.size() == 1) {//新增第一个品牌时重新登录
			if (SecurityUtils.getSubject().isAuthenticated()) {
				SecurityUtils.getSubject().logout();
			}
			renderJson(RestResult.buildSuccess("true"));
		} else {
			renderJson(RestResult.buildSuccess(1));
		}
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		Brand data = brandService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Brand data = getBean(Brand.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		if (brandService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}
		if (brandService.findByCode(data) > 0) {
			throw new BusinessException("品牌编号请勿重复");
		}
		if (!brandService.update(data)) {
			throw new BusinessException("修改失败");
		}
		brandService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Brand brand = new Brand();
		brand.setId(id);
		brand.setDelFlag("1");
		if (!brandService.update(brand)) {
			throw new BusinessException("删除失败");
		}
		brandService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

}