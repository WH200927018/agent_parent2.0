package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.SpecItemService;
import com.tipu.agent.front.service.api.SpecService;
import com.tipu.agent.front.service.entity.model.Spec;
import com.tipu.agent.front.service.entity.model.SpecItem;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * SpecItem管理
 * 
 * @author think
 *
 */
@RequestMapping("/base/spec_item")
public class SpecItemController extends BaseController{

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SpecItemService specItemService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SpecService  specService;
	


	/**
	 * index
	 */
	public void index() {
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		List<Spec> specList = specService.findAll(companyId,brandId);
		setAttr("specList", specList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		SpecItem data = new SpecItem();
		if (StrKit.notBlank(getPara("name"))) {
			data.setName(getPara("name"));
		}
		if (StrKit.notBlank(getPara("specId"))) {
			data.setSpecId(getPara("specId"));
		}
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
		Page<SpecItem> dataPage = specItemService.findPage(data, pageNumber, pageSize);

		renderJson(new DataTable<SpecItem>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		List<Spec> specList = specService.findAll(companyId,brandId);
		setAttr("specList", specList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		SpecItem data = getBean(SpecItem.class, "data");
		String uuid = StrKit.getRandomUUID();
		data.setId(uuid);
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");	
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
		if (!specItemService.save(data)) {
			throw new BusinessException("保存失败");
		}
		specItemService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		List<Spec> specList = specService.findAll(companyId,brandId);
		setAttr("specList", specList);
		SpecItem data = specItemService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		SpecItem data = getBean(SpecItem.class, "data");
		SpecItem oldData = specItemService.findById(data.getId());
		if (oldData == null) {
			throw new BusinessException("数据不存在");
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!specItemService.update(data)) {
			throw new BusinessException("修改失败");
		}
		specItemService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		SpecItem specItem = new SpecItem();
		specItem.setId(id);
		specItem.setDelFlag("1");
		if (!specItemService.update(specItem)) {
			throw new BusinessException("删除失败");
		}
		specItemService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
}
