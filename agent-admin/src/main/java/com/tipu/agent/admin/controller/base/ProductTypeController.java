package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.Consts;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.entity.model.ProductType;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * ProductType管理
 * @author think
 *
 */
@RequestMapping("/base/product_type")
public class ProductTypeController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;
	
	/**
     * index
     */
    public void index() {
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        ProductType data = new ProductType();
        data.setName(getPara("name"));
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
        Page<ProductType> dataPage = productTypeService.findPage(data, pageNumber, pageSize);
        ProductType parentType = null;//上级
        for (ProductType productType : dataPage.getList()) {
        	parentType = productTypeService.findById(productType.getParentId());
        	if(parentType !=null){
        		productType.setParentName(parentType.getName());
        	}else{
        		productType.setParentName("无上级");
        	}
		}
        setAttr("data", data);
        renderJson(new DataTable<ProductType>(dataPage));
    }
    
    /**
     * 根据pid找到上级的名字
     */
    public void findById(){
    	String parentId = getPara("parentId");
    	ProductType type = null;
    	if(!parentId.equals("0")){
    		type = productTypeService.findById(parentId);
    	}
    	setAttr("type", type);
        renderJson(type);
    }
    
    /**
     * 查看icon
     */
    public void findicon(){
    	String id = getPara("id");
    	ProductType data = productTypeService.findById(id);
        setAttr("data", data).render("icon.html");
    }

    /**
     * add
     */
    public void add() {
        render("add.html");
    }
    
    /**
     * 保存提交
     */
    public void postAdd() {
        ProductType data = getBean(ProductType.class, "data");
        data.setCreateDate(new Date());
        data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
        data.setDelFlag("0");
        data.setParentId("0");
        data.setParentIds("0,");
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
        if (!productTypeService.save(data)) {
            throw new BusinessException("保存失败");
        }
        productTypeService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    
    /**
     * add下级
     */
    public void addSub() {
    	String id = getPara("id");
    	ProductType data = productTypeService.findById(id);
    	String parentId = data.getParentId();
    	String parentIds = parentId + ","+id+",";
    	setAttr("data", data);//下级
    	setAttr("parentId", id);//下级
    	setAttr("parentIds", parentIds);//下级逗号分割
        render("addsub.html");
    }
    
    /**
     * 保存提交
     */
    public void postAddSub() {
        ProductType data = getBean(ProductType.class, "data");
        data.setCreateDate(new Date());
        data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
        data.setDelFlag("0");
        data.setParentId(getPara("parentId"));
        data.setParentIds(getPara("parentIds"));
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
        if (!productTypeService.save(data)) {
            throw new BusinessException("保存失败");
        }
        productTypeService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    
    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        ProductType data = productTypeService.findById(id);

        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        ProductType data = getBean(ProductType.class, "data");
        data.setUpdateDate(new Date());
        data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
        if (productTypeService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }
        if (!productTypeService.update(data)) {
            throw new BusinessException("修改失败");
        }
        productTypeService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
    	String id = getPara("id");
    	ProductType productType = new ProductType();
    	productType.setId(id);
    	productType.setDelFlag("1");
        if (!productTypeService.update(productType)) {
            throw new BusinessException("删除失败");
        }
        productTypeService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}