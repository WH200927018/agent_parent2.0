package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentAddressService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAddress;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 常用地址管理
 * 
 * @author think
 *
 */
@RequestMapping("/base/agent_address")
public class AgentAddressController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAddressService agentAddressService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	/**
	 * index
	 */
	public void index() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AgentAddress data = new AgentAddress();
		if (StrKit.notBlank(getPara("agent"))) {
			data.setAgentId(getPara("agent"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<AgentAddress> dataPage = agentAddressService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentAddress>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList).render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		AgentAddress data = getBean(AgentAddress.class, "data");
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		if (!agentAddressService.save(data)) {
			throw new BusinessException("保存失败");
		}
		agentAddressService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		AgentAddress data = agentAddressService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		AgentAddress data = getBean(AgentAddress.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (agentAddressService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!agentAddressService.update(data)) {
			throw new BusinessException("修改失败");
		}
		agentAddressService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		AgentAddress agentAddress = new AgentAddress();
		agentAddress.setId(id);
		agentAddress.setDelFlag("1");
		if (!agentAddressService.update(agentAddress)) {
			throw new BusinessException("删除失败");
		}
		agentAddressService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
}
