package com.tipu.agent.admin.controller.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;

public class ExcelUtilController {
	
	private static final String COUNTORDERS = PathKit.getWebRootPath() + File.separator + "export\\count-orders" + File.separator ;
	public File export(String productTypeName,String productName,Map<String, String> titleData,String startDate,String endDate ) {
		String topTitle = "";
		if(StrKit.notBlank(startDate) && StrKit.notBlank(endDate)){
			topTitle = startDate+"至"+endDate+"订单统计";
		}else{
			topTitle = "订单统计";
		}
		String title=COUNTORDERS+topTitle+".xls";
	     File temp = new File(COUNTORDERS);
	     if(temp.exists()==false) {
			 temp.mkdirs();				
       }
		File file = new File(title);
		// 创建工作薄
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		HSSFCellStyle centerstyle = hssfWorkbook.createCellStyle();
		centerstyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 左右居中
		centerstyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 上下居中
		// sheet:一张表的简称
		// row:表里的行
		// 创建工作薄中的工作表
		HSSFSheet hssfSheet = hssfWorkbook.createSheet(topTitle);
		CellRangeAddress range = new CellRangeAddress(0, 0, 0, 3); //从第一列开始对具体单元格跨列
		hssfSheet.addMergedRegion(range);
		hssfSheet.setColumnWidth(1,4000);
		// 创建行
		HSSFRow row = hssfSheet.createRow(0);
		// 创建单元格，设置表头 创建列
		HSSFCell cell = null;
		// 创建标题行
		row = hssfSheet.createRow(0); //第一行
		row.setHeight((short) 300);
		//创建第一列
		cell = row.createCell(0);
		cell.setCellValue(topTitle);
		cell.setCellStyle(centerstyle);
		
		row = hssfSheet.createRow(1); 
		cell = row.createCell(0);
		cell.setCellValue("商品分类");
		cell.setCellStyle(centerstyle);
		cell = row.createCell(1);
		cell.setCellValue("商品");
		cell.setCellStyle(centerstyle);
		cell = row.createCell(2);
		cell.setCellValue("状态");
		cell = row.createCell(3);
		cell.setCellValue("总金额");
		
		range = new CellRangeAddress(2, titleData.size()+2, 0, 0);//从第3行开始对具体单元格跨行
		hssfSheet.addMergedRegion(range); 
		row = hssfSheet.createRow(2);
		cell = row.createCell(0);
		if(StrKit.notBlank(productTypeName)){
			cell.setCellValue(productTypeName);
		}else{
			if(StrKit.notBlank(productName)){
				cell.setCellValue("商品分类");
			}else{
				cell.setCellValue("所有商品分类");
			}
		}
		cell.setCellStyle(centerstyle);
		range = new CellRangeAddress(2, titleData.size()+2, 1, 1);//从第3行开始对具体单元格跨行
		hssfSheet.addMergedRegion(range);  
		cell = row.createCell(1);
		if(StrKit.notBlank(productName)){
			cell.setCellValue(productName);
		}else{
			if(StrKit.notBlank(productTypeName)){
				cell.setCellValue(productTypeName+"商品");
			}else{
				cell.setCellValue("所有商品");
			}
		}
		cell.setCellStyle(centerstyle);
		
		// 初始化索引
		int rowIndex = 2;
		int cellIndex = 2;
		double sum = 0.00;
		for (String s : titleData.keySet()) {
			cell = row.createCell(cellIndex);
			cell.setCellValue(titleData.get(s));
			cellIndex++;
			cell = row.createCell(cellIndex);
			cell.setCellValue(s);
			sum = sum + Double.parseDouble(s);
			cellIndex = 2;
			rowIndex++;
			row = hssfSheet.createRow(rowIndex); 
		}
		row = hssfSheet.createRow(rowIndex); 
		cell = row.createCell(2);
		cell.setCellValue("总计");
		cell = row.createCell(3);
		cell.setCellValue(String.valueOf(sum));
		
		try {
			FileOutputStream fileOutputStreane = new FileOutputStream(file);
			hssfWorkbook.write(fileOutputStreane);
			fileOutputStreane.flush();
			fileOutputStreane.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
}
