package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.api.AttributeItemService;
import com.tipu.agent.front.service.api.AttributeService;
import com.tipu.agent.front.service.entity.model.ProductType;
import com.tipu.agent.front.service.entity.model.Attribute;
import com.tipu.agent.front.service.entity.model.AttributeItem;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AttributeItem管理
 * 
 * @author think
 *
 */
@RequestMapping("/base/attribute_item")
public class AttributeItemController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AttributeService attributeService;
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AttributeItemService attributeItemService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;

	/**
	 * index
	 */
	public void index() {
		List<ProductType> productTypeList = productTypeService.findAll();
		setAttr("productTypeList", productTypeList);
		List<Attribute> attributeList = attributeService.findAll();
		setAttr("attributeList", attributeList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		AttributeItem data = new AttributeItem();
		if (StrKit.notBlank(getPara("name"))) {
			data.setName(getPara("name"));
		}
		if (StrKit.notBlank(getPara("typeId"))) {
			data.setTypeId(getPara("typeId"));
		}
		Page<AttributeItem> dataPage = attributeItemService.findPage(data, pageNumber, pageSize);

		renderJson(new DataTable<AttributeItem>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		List<ProductType> productTypeList = productTypeService.findAll();
		setAttr("productTypeList", productTypeList);
		List<Attribute> attributeList = attributeService.findAll();
		setAttr("attributeList", attributeList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		AttributeItem data = getBean(AttributeItem.class, "data");
		String uuid = StrKit.getRandomUUID();
		data.setId(uuid);
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		if (!attributeItemService.save(data)) {
			throw new BusinessException("保存失败");
		}
		attributeItemService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		List<ProductType> productTypeList = productTypeService.findAll();
		setAttr("productTypeList", productTypeList);
		List<Attribute> attributeList = attributeService.findAll();
		setAttr("attributeList", attributeList);
		AttributeItem data = attributeItemService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		AttributeItem data = getBean(AttributeItem.class, "data");
		AttributeItem oldData = attributeItemService.findById(data.getId());
		if (oldData == null) {
			throw new BusinessException("数据不存在");
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!attributeItemService.update(data)) {
			throw new BusinessException("修改失败");
		}
		attributeItemService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		AttributeItem AttributeItem = new AttributeItem();
		AttributeItem.setId(id);
		AttributeItem.setDelFlag("1");
		if (!attributeItemService.update(AttributeItem)) {
			throw new BusinessException("删除失败");
		}
		attributeItemService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
}
