package com.tipu.agent.admin.controller.common;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.upload.UploadFile;
import com.tipu.agent.admin.base.util.OssUtil;
import com.tipu.agent.admin.base.web.base.BaseController;

import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.cors.EnableCORS;

/**
 * 文件上传
 * 所有文件上传的接口都从这里走
 * @author hulin
 *
 */
@RequestMapping("/common/upload")
@EnableCORS
public class UploadController extends BaseController {
	
	/**
	 * 品牌logo上传
	 */
	public void brandLogo(){
        String selfPath = "brand/logo/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 商品分类图标上传
	 */
	public void productTypeIcon(){
		String selfPath = "productType/icon/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 广告图片上传
	 */
	public void adItemImg(){
		String selfPath = "adField/images/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 广告视频上传
	 */
	public void adItemVideo(){
		String selfPath = "adField/video/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 广告正文内容图片
	 */
	public void adItemContentImg(){
		String selfPath = "adItem/images/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 商品管理主图上传
	 */
	public void productMainImg(){
		String selfPath = "product/main/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 商品管理轮播图上传
	 */
	public void productCarouselImg(){
		String selfPath = "product/carousel/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 商品管理介绍正文图片上传
	 */
	public void productIntroductionImg(){
		String selfPath = "product/introduction/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 代理商图片管理
	 */
	public void agentAvatar(){
		String selfPath = "agent/auth/avatar/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 代理商保证金凭证
	 */
	public void agentBond(){
		String selfPath = "agent/bond/";
		commonUploadOss(selfPath);
	}

	/**
	 * 文章正文内容图片
	 */
	public void articleContentImg(){
		String selfPath = "article/images/";
		commonUploadOss(selfPath);
	}

	/**
	 * 品牌管理介绍正文图片上传
	 */
	public void brandIntroductionImg(){
		String selfPath = "brand/introduction/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 通告管理内容正文图片上传
	 */
	public void oaNotifyContentImg(){
		String selfPath = "oaNotify/content/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 通告管理多文件上传
	 */
	public void oaNotifyFile(){
		String selfPath = "oaNotify/file/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * 产品订单打款截图上传
	 */
	public void orderImg(){
		String selfPath = "order/img/";
		commonUploadOss(selfPath);
	}
	
	/**
	 * sku商品图片上传
	 */
	public void skuImg(){
		String selfPath = "sku/img/";
		commonUploadOss(selfPath);
	}

	/**
	 * 物流码导入txt文件上传
	 */
	public void exportPath(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(new Date());
		String selfPath = "delivery/"+date;
		UploadFile uploadFile = getFile();
		File file = uploadFile.getFile();
		JSONObject data = new JSONObject();
		String path = "";
        String fileName = "";
        String savePath = "";
        if (uploadFile != null) {
        	try {
        		fileName = uploadFile.getFileName();
                String extentionName = fileName.substring(fileName.lastIndexOf(".")); // 后缀名
                String newName = StrKit.getRandomUUID() +  extentionName;// 新名   
                String relativePath = File.separator+ selfPath +File.separator;   //相对路径
                String fileDiv = uploadFile.getUploadPath()+relativePath;
                File temp = new File(fileDiv);
                if(temp.exists()==false) {
    				 temp.mkdirs();				
                }
                savePath = fileDiv + newName;
                uploadFile.getFile().renameTo(new File(savePath)); // 重命名并上传文件
                //返回任意数据即代表上传成功
                data.put("src", savePath);
                data.put("fileName", fileName);
                data.put("relativePath", relativePath);
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				  //上传完先不删除 等待后台解析后再去删除
			}
        }
        renderJson(Ret.ok().set("code", "0").set("data", data).set("message", "上传成功"));
	}
	
	/**
	 * 公共上传
	 * @param selfPath
	 * 			阿里云自定义路径
	 */
	public void commonUploadOss(String selfPath){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(new Date());
		selfPath = selfPath + date;
		UploadFile uploadFile = getFile();
		File file = uploadFile.getFile();
        JSONObject data = new JSONObject();
        String path = "";
        String savePath = "";
        if (uploadFile != null) {
        	try {
        		String fileName = uploadFile.getFileName();
                String extentionName = fileName.substring(fileName.lastIndexOf(".")); // 后缀名
                String newName = StrKit.getRandomUUID() + extentionName;// 新名   
                String relativePath = File.separator+ selfPath +File.separator;   //相对路径
                String fileDiv = uploadFile.getUploadPath()+relativePath;
                File temp = new File(fileDiv);
                if(temp.exists()==false) {
    				 temp.mkdirs();				
                }
                savePath = fileDiv+ File.separator + newName;
                String ossrelativePath = selfPath + "/"+savePath.substring(savePath.lastIndexOf(File.separator) + 1);
                uploadFile.getFile().renameTo(new File(fileDiv+ File.separator + newName)); // 重命名并上传文件
                path = OssUtil.upload(ossrelativePath, savePath); //传阿里云
                //返回任意数据即代表上传成功
                data.put("src", path);
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				//上传完阿里云后删除本地的文件
				if(StrKit.notBlank(savePath)){
					new File(savePath).delete();
				}
			}
        }
        renderJson(Ret.ok().set("code", "0").set("data", data).set("path", path).set("message", "上传成功"));
	}

}
