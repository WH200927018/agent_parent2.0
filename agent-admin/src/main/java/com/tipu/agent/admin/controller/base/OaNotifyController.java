package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.OaNotifyRecordService;
import com.tipu.agent.front.service.api.OaNotifyService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.OaNotify;
import com.tipu.agent.front.service.entity.model.OaNotifyRecord;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * OaNotify管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/oa_notify")
public class OaNotifyController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OaNotifyService oaNotifyService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OaNotifyRecordService oaNotifyRecordService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		OaNotify data = new OaNotify();
		if (StrKit.notBlank(getPara("title"))) {
			data.setTitle(getPara("title"));
		}
		if (StrKit.notBlank(getPara("type"))) {
			data.setType(getPara("type"));
		}
		if (StrKit.notBlank(getPara("status"))) {
			data.setStatus(getPara("status"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<OaNotify> dataPage = oaNotifyService.findPage(data, pageNumber, pageSize);

		renderJson(new DataTable<OaNotify>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		OaNotify data = getBean(OaNotify.class, "data");
		if (StrKit.isBlank(data.getContent())) {
			throw new BusinessException("内容不能为空!");
		}
		OaNotifyRecord oaNotifyRecord = getBean(OaNotifyRecord.class, "data");
		String uuid = StrKit.getRandomUUID();
		data.setId(uuid);
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		oaNotifyRecord.setAgentId(data.getId());
		oaNotifyRecord.setOaNotifyId(uuid);
		oaNotifyRecord.setReadFlag("0");
		if (!oaNotifyRecordService.save(oaNotifyRecord)) {
			throw new BusinessException("保存失败");
		}
		if (!oaNotifyService.save(data)) {
			throw new BusinessException("保存失败");
		}
		oaNotifyService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		OaNotify data = oaNotifyService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		OaNotify data = getBean(OaNotify.class, "data");
		if (StrKit.isBlank(data.getContent())) {
			throw new BusinessException("内容不能为空!");
		}
		OaNotify oldOaNotify = oaNotifyService.findById(data.getId());
		if (oldOaNotify == null) {
			throw new BusinessException("数据不存在");
		}
		if ("1".equals(oldOaNotify.getStatus())) {// 已发布的无法修改
			throw new BusinessException("已发布的无法修改!");
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());

		if (!oaNotifyService.update(data)) {
			throw new BusinessException("修改失败");
		}
		oaNotifyService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		OaNotify oaNotify = new OaNotify();
		oaNotify.setId(id);
		oaNotify.setDelFlag("1");
		if (!oaNotifyService.update(oaNotify)) {
			throw new BusinessException("删除失败");
		}
		oaNotifyService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

}