package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.api.SpecGroupService;
import com.tipu.agent.front.service.api.SpecService;
import com.tipu.agent.front.service.entity.model.ProductType;
import com.tipu.agent.front.service.entity.model.Spec;
import com.tipu.agent.front.service.entity.model.SpecGroup;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * spec管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/spec")
public class SpecController extends BaseController {
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SpecService specService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SpecGroupService specGroupService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;

	/**
	 * index
	 */
	public void index() {
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		List<ProductType> productTypeList = productTypeService.findAll(companyId,brandId);
		setAttr("productTypeList", productTypeList);
		List<SpecGroup> specGroupList = specGroupService.findAll(companyId,brandId);
		setAttr("specGroupList", specGroupList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		Spec data = new Spec();
		if (StrKit.notBlank(getPara("name"))) {
			data.setName(getPara("name"));
		}
		if (StrKit.notBlank(getPara("typeId"))) {
			data.setTypeId(getPara("typeId"));
		}
		if (StrKit.notBlank(getPara("groupId"))) {
			data.setGroupId(getPara("groupId"));
		}
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
		Page<Spec> dataPage = specService.findPage(data, pageNumber, pageSize);

		renderJson(new DataTable<Spec>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		List<ProductType> productTypeList = productTypeService.findAll(companyId,brandId);
		setAttr("productTypeList", productTypeList);
		List<SpecGroup> specGroupList = specGroupService.findAll(companyId,brandId);
		setAttr("specGroupList", specGroupList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Spec data = getBean(Spec.class, "data");
		String uuid = StrKit.getRandomUUID();
		data.setId(uuid);
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
		if (!specService.save(data)) {
			throw new BusinessException("保存失败");
		}
		specService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		List<ProductType> productTypeList = productTypeService.findAll(companyId,brandId);
		setAttr("productTypeList", productTypeList);
		List<SpecGroup> specGroupList = specGroupService.findAll(companyId,brandId);
		setAttr("specGroupList", specGroupList);
		Spec data = specService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Spec data = getBean(Spec.class, "data");
		Spec oldData = specService.findById(data.getId());
		if (oldData == null) {
			throw new BusinessException("数据不存在");
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!specService.update(data)) {
			throw new BusinessException("修改失败");
		}
		specService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Spec spec = new Spec();
		spec.setId(id);
		spec.setDelFlag("1");
		if (!specService.update(spec)) {
			throw new BusinessException("删除失败");
		}
		specService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
}
