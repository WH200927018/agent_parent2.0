package com.tipu.agent.admin.controller.base;


import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentRebateConfigParamsService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.entity.model.AgentRebateConfigParams;
import com.tipu.agent.front.service.entity.model.AgentType;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * SystemConfig管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/agent_rebate_config_params")
public class AgentRebateConfigParamsController extends BaseController {
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentRebateConfigParamsService agentRebateConfigParamsService;
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		AgentRebateConfigParams data = new AgentRebateConfigParams();
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<AgentRebateConfigParams> dataPage = agentRebateConfigParamsService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentRebateConfigParams>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {	
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<AgentType>agentTypeList=agentTypeService.findAll(companyId, brandId);
		setAttr("agentTypeList", agentTypeList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		AgentRebateConfigParams data = getBean(AgentRebateConfigParams.class, "data");
		String uuid = StrKit.getRandomUUID();
		data.setId(uuid);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		data.setDelFlag("0");
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		if (!agentRebateConfigParamsService.save(data)) {
			throw new BusinessException("保存失败");
		}
		
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		AgentRebateConfigParams data = agentRebateConfigParamsService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<AgentType>agentTypeList=agentTypeService.findAll(companyId, brandId);
		setAttr("agentTypeList", agentTypeList);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		AgentRebateConfigParams data = getBean(AgentRebateConfigParams.class, "data");
		AgentRebateConfigParams oldData = agentRebateConfigParamsService.findById(data.getId());
		if (oldData == null) {
			throw new BusinessException("数据不存在");
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!agentRebateConfigParamsService.update(data)) {
			throw new BusinessException("修改失败");
		}
		
		renderJson(RestResult.buildSuccess());
	}
	

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		AgentRebateConfigParams data = new AgentRebateConfigParams();
		data.setId(id);
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("1");
		if (!agentRebateConfigParamsService.update(data)) {
			throw new BusinessException("删除失败");
		}
		
		renderJson(RestResult.buildSuccess());
	}
	
}
