package com.tipu.agent.admin.controller.system;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.entity.model.Res;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.admin.service.api.WeixinMenuService;
import com.tipu.agent.admin.service.entity.model.WeixinMenu;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 微信管理
 * 
 * @author think
 *
 */
@RequestMapping("/system/weixin_menu")
public class WeixinMenuController extends BaseController {

	@JbootrpcService
	private WeixinMenuService weixinMenuService;

	/**
	 * index
	 */
	public void index() {
		List<WeixinMenu> wxList = weixinMenuService.findAll();
		setAttr("wxList", wxList).render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		WeixinMenu data = new WeixinMenu();
		if (StrKit.notBlank(getPara("id"))) {
			data.setId(getParaToLong("id"));
		}
		if (StrKit.notBlank(getPara("pid"))) {
			data.setPid(getPara("pid"));
		}
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		Page<WeixinMenu> dataPage = weixinMenuService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<WeixinMenu>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String pid = getPara("pid");
		setAttr("pid", pid);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		WeixinMenu data = getBean(WeixinMenu.class, "data");
		String pid = data.getPid();
		if (StrKit.isBlank(pid)) {
			throw new BusinessException("请选择上级菜单");
		}
		WeixinMenu pMenu = new WeixinMenu();// 上级菜单
		if (!"0".equals(pid)) {
			pMenu = weixinMenuService.findById(pid);
			if (pMenu == null) {
				data.setWxMenuLevel(1);
			} else {
				if (pMenu.getWxMenuLevel() > 1) {
					throw new BusinessException("只能创建两级菜单");
				} else {
					data.setWxMenuLevel(2);
				}
			}
		} else {
			throw new BusinessException("请选择上级菜单");
		}
		//查询菜单数量
		int countMenu = weixinMenuService.countMenu(pid, data.getWxMenuLevel());
		if (data.getWxMenuLevel() == 1) {
			if (countMenu > 2) {
				throw new BusinessException("一级菜单最多只能创建三个");
			}
		} else if (data.getWxMenuLevel() == 2) {
			if (countMenu > 4) {
				throw new BusinessException("二级菜单最多只能创建五个");
			}
			if (data.getWxMenuType().equals("sub_button")) {
				throw new BusinessException("二级菜单不能为菜单类型");
			}
			if (pMenu != null) {
				if (!pMenu.getWxMenuType().equals("sub_button")) {
					throw new BusinessException("不是菜单类型的一级菜单不能有下级");
				}
			}
		}
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		if (!weixinMenuService.save(data)) {
			throw new BusinessException("保存失败");
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		WeixinMenu data = weixinMenuService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		WeixinMenu data = getBean(WeixinMenu.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		data.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
		if (weixinMenuService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!weixinMenuService.update(data)) {
			throw new BusinessException("修改失败");
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		Long id = getParaToLong("id");
		WeixinMenu data = new WeixinMenu();
		data.setId(id);
		data.setDelFlag("1");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!weixinMenuService.update(data)) {
			throw new BusinessException("删除失败");
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 微信菜单资源树
	 */
	public void resTree() {
		renderJson(RestResult.buildSuccess(weixinMenuService.findTree(AuthUtils.getLoginUser().getCompanyId())));
	}

	/**
	 * 微信自定义菜单测试
	 */
	public void testMenu(String appid) {
		String jsonMenu = JsonKit.toJson(weixinMenuService.getMenuByAppid(appid)).toString();
		System.out.println(jsonMenu);
		renderJson(jsonMenu);
	}

}
