package com.tipu.agent.admin.controller.base;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.core.JFinal;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.util.IpKit;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.api.DataService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.Data;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.admin.util.FileUtils;
import com.tipu.agent.anticode.service.api.AnticodeService;
import com.tipu.agent.anticode.service.entity.model.Anticode;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.DeliveryItemService;
import com.tipu.agent.front.service.api.DeliveryService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Delivery;
import com.tipu.agent.front.service.entity.model.DeliveryItem;
import com.tipu.agent.front.service.entity.model.Product;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Delivery管理
 * 
 * @author think
 *
 */
@RequestMapping("/base/delivery")
public class DeliveryController extends BaseController {

	@JbootrpcService
	private DataService dataApi;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private DeliveryService deliveryService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_ANTICODE, version = ServiceConst.VERSION_1_0)
	private AnticodeService anticodeService;

	@JbootrpcService
	private BrandService brandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private DeliveryItemService deliveryItemService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	/**
	 * index
	 */
	public void index() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		Delivery data = new Delivery();
		if (StrKit.notBlank(getPara("agentSend"))) {
			data.setAgentSend(getPara("agentSend"));
		}
		if (StrKit.notBlank(getPara("agentReceive"))) {
			data.setAgentReceive(getPara("agentReceive"));
		}
		if (StrKit.notBlank(getPara("deliveryNo"))) {
			data.setDeliveryNo(getPara("deliveryNo"));
		}
		if (StrKit.notBlank(getPara("deliveryStatus"))) {
			data.setDeliveryStatus(getPara("deliveryStatus"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<Delivery> dataPage = deliveryService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Delivery>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Delivery data = getBean(Delivery.class, "data");
		String agentSendId = data.getAgentSendId();// 从表单中获取 发货代理Id
		Agent agentSendIdEntity = agentService.findById(agentSendId); // 发货代理 entity
		List<Data> list = dataApi.getListByTypeOnUse("agent_level");
		String agentSendName = "";
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agentSendId, brandId);
		AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
		for (Data selfData : list) {
			if (selfData.getCode().equals(agentType.getGrade().toString())) {
				agentSendName = selfData.getCodeDesc();
			}
		}
		data.setAgentSend(agentSendIdEntity.getName());
		data.setAgentSendLevel(agentSendName);
		data.setDelFlag("0");
		String agentReceiveId = data.getAgentReceiveId(); // 从表单中获取收货代理id
		Agent agentReceiveIdEntity = agentService.findById(agentReceiveId); // 收货代理 entity
		String agentReceiveName = "";
		agentBrand = agentBrandService.findByAgentAndBrand(agentReceiveId, brandId);
		agentType = agentTypeService.findById(agentBrand.getAgentType());
		for (Data selfData : list) {
			if (selfData.getCode().equals(agentType.getGrade().toString())) {
				agentReceiveName = selfData.getCodeDesc();
			}
		}
		data.setAgentReceive(agentReceiveIdEntity.getName());
		data.setAgentReceiveLevel(agentReceiveName);
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		// -1 新生成 0:待发货 1：已发货
		if (!deliveryService.save(data)) {
			throw new BusinessException("保存失败");
		}
		deliveryService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		Delivery data = deliveryService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Delivery data = getBean(Delivery.class, "data");

		String agentSendId = data.getAgentSendId();// 从表单中获取 发货代理Id
		Agent agentSendIdEntity = agentService.findById(agentSendId); // 发货代理 entity
		List<Data> list = dataApi.getListByTypeOnUse("agent_level");
		String agentSendName = "";
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agentSendId, brandId);
		AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
		for (Data selfData : list) {
			if (selfData.getCode().equals(agentType.getGrade().toString())) {
				agentSendName = selfData.getCodeDesc();
			}
		}
		data.setAgentSend(agentSendIdEntity.getName());
		data.setAgentSendLevel(agentSendName);

		String agentReceiveId = data.getAgentReceiveId(); // 从表单中获取收货代理id
		Agent agentReceiveIdEntity = agentService.findById(agentReceiveId); // 收货代理 entity
		String agentReceiveName = "";
		agentBrand = agentBrandService.findByAgentAndBrand(agentReceiveId, brandId);
		agentType = agentTypeService.findById(agentBrand.getAgentType());
		for (Data selfData : list) {
			if (selfData.getCode().equals(agentType.getGrade().toString())) {
				agentReceiveName = selfData.getCodeDesc();
			}
		}
		data.setAgentReceive(agentReceiveIdEntity.getName());
		data.setAgentReceiveLevel(agentReceiveName);

		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (deliveryService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!deliveryService.update(data)) {
			throw new BusinessException("修改失败");
		}
		deliveryService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Delivery delivery = new Delivery();
		delivery.setId(id);
		delivery.setDelFlag("1"); // 删除发货单
		DeliveryItem deliveryItem = new DeliveryItem();
		deliveryItem.setDelivery(id);
		List<DeliveryItem> deliveryItemList = deliveryItemService.findList(deliveryItem);
		for (DeliveryItem deliveryItem2 : deliveryItemList) {
			deliveryItem2.setDelFlag("1");
			deliveryItem2.setDelivery(id);
			if (!deliveryItemService.update(deliveryItem2)) {
				throw new BusinessException("删除失败");
			}
		}
		if (!deliveryService.update(delivery)) {
			throw new BusinessException("删除失败");
		}
		deliveryService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 跳转导入物流码界面
	 */
	public void exportDelivery() {
		List<Agent> agentList = agentService.findAll();
		setAttr("agentList", agentList);
		render("exportDelivery.html");
	}

	/**
	 * 导入物流码操作
	 * 
	 * @throws IOException
	 */
	public void export() throws IOException {
		String agentSendId = getPara("agentSendId");
		String path = getPara("path");
		if (StrKit.notBlank(path) && StrKit.notBlank(agentSendId)) {
			File file = new File(path);
			InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
			BufferedReader br = new BufferedReader(reader);
			// 第一步 生成一个新的发货单
			Agent selfAgent = agentService.findById(agentSendId);
			String maxNo = deliveryService.getMaxNo();
			String dateNo = new SimpleDateFormat("yyyyMMdd").format(new Date()) + "0001";
			// 用代理商的id 插数据库
			Delivery delivery = new Delivery();
			String deliveryId = StrKit.getRandomUUID();
			delivery.setId(deliveryId);
			delivery.setAgentReceiveId(agentSendId);
			List<Data> list = dataApi.getListByTypeOnUse("agent_level");
			String agentLevelName = "";
			String companyId = AuthUtils.getLoginUser().getCompanyId();
			JbootRedis redis = Jboot.me().getRedis();
			String brandId = redis.get(companyId + "_brandId");
			AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agentSendId, brandId);
			AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
			for (Data selfData : list) {
				if (selfData.getCode().equals(agentType.getGrade().toString())) {
					agentLevelName = selfData.getCodeDesc();
				}
			}
			delivery.setAgentReceiveLevel(agentLevelName);
			delivery.setAgentReceive(selfAgent.getName());
			// String parentIds = selfAgent.getParentIds();
			// if(StrKit.notBlank(parentIds)) {
			// String topParentId = parentIds.split(",")[1];
			// delivery.setAgentSendId(topParentId);
			// Agent parentAgent = agentService.findById(topParentId);
			// delivery.setAgentSend(parentAgent.getName());
			// }
			// 插入发货单号 递增

			if (maxNo != null && maxNo.compareTo(dateNo) >= 0) {
				delivery.setDeliveryNo(String.valueOf((Long.parseLong(maxNo) + 1)));
			} else {
				delivery.setDeliveryNo(dateNo);
			}
			delivery.setCreateDate(new Date());
			delivery.setCreateBy(AuthUtils.getLoginUser().getId().toString());
			delivery.setDelFlag("0");
			delivery.setDeliveryStatus("0"); // -1 新生成 0:待发货 1：已发货
			String ip = IpKit.getRealIp(getRequest());
			delivery.setIp(ip);
			deliveryService.save(delivery); // save是新增

			long start = System.currentTimeMillis(); // 记录开始导入时间
			String line = "";
			List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
			Map<String, String> codeProductMap = new HashMap<String, String>();
			List<String> failCode = new ArrayList<String>(); // 失败的记录
			Integer count = 0;// 记录总数
			Integer successCount = 0; // 成功的数量
			Integer failCount = 0; // 失败的数量
			while ((line = br.readLine()) != null) {
				if (StrKit.notBlank(line)) {
					count++;
					String first = line.substring(0, 1);
					Brand brand = new Brand();
					brand.setCompanyId(companyId);
					brand.setCode(Integer.parseInt(first));
					// 第一步先去 brand表里查看 code是否为 物流码的第一位 是否有数据
					List<Brand> brandList = brandService.findList(brand);
					if (brandList.size() > 0) {
						// 第二步 去截取第二位 是否是属于product表里的code
						String second = line.substring(1, 2);
						Product product = new Product();
						product.setCode(Integer.parseInt(second));
						List<Product> productList = productService.findList(product);
						if (productList.size() > 0) {
							product = productList.get(0);
							String productId = product.getId();
							// 第三步 找出物流码是否属于 区间发货码 返回实体 如果有 说明对的，如果没有 记录失败的发货码
							Anticode anticode = anticodeService.findByLogisticsCode(line, productId);
							if (anticode != null) {
								DeliveryItem deliveryItem = new DeliveryItem();
								String deliveryItemId = StrKit.getRandomUUID();
								deliveryItem.setId(deliveryItemId);
								deliveryItem.setCreateDate(new Date());
								deliveryItem.setCreateBy(AuthUtils.getLoginUser().getId().toString());
								deliveryItem.setDelFlag("0");
								deliveryItem.setDelivery(deliveryId);
								deliveryItem.setProduct(productId);
								deliveryItem.setAntiCode(anticode.getAntiCode());
								deliveryItem.setLogisticsCode(anticode.getLogisticsCode());
								deliveryItem.setProductNum(1);
								deliveryItemList.add(deliveryItem);
								successCount++;
								codeProductMap.put(line, productId);
							} else {
								failCode.add(line);
								failCount++;
							}
						} else {
							failCode.add(line);
							failCount++;
						}
					} else {
						failCode.add(line);
						failCount++;
					}
				}
			}
			// 批量插入deliveryItemList
			if (deliveryItemList.size() > 0) {
				deliveryService.batchInsertList(deliveryItemList);
			}
			anticodeService.updateDelivery(codeProductMap, agentSendId);// 更新anticode_XXX 里发货代理商
			long end = System.currentTimeMillis(); // 记录导入结束时间

			double exportDate = end - start;
			double ms = exportDate / 1000;
			String str = String.join("<br/>", failCode.toArray(new String[failCode.size()]));
			String message = "此次导入共花费：" + ms + "ms<br/>共计：" + count + "个<br/>成功：" + successCount + "个<br/>失败："
					+ failCount + "个<br/>失败的箱码为：<br/>" + str;
			Delivery deliveryRecord = deliveryService.findById(deliveryId);
			deliveryRecord.setExportRecords(message);
			deliveryService.update(deliveryRecord);
			renderJson(RestResult.buildSuccess());
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 查看导入记录 （生成文件下载）
	 * 
	 * @throws IOException
	 */
	public void findExportRecords() throws IOException {
		String id = getPara("id");
		Delivery data = deliveryService.findById(id);
		String exportrecords = data.getExportRecords();

		String delivery_no = data.getDeliveryNo();
		String agentAgent = data.getAgentReceive();
		String fileName = delivery_no + "_" + agentAgent;
		if (StrKit.notBlank(exportrecords)) {
			String realPath = "/download/delivery/" + fileName + ".txt";
			String path = JFinal.me().getServletContext().getRealPath("/") + realPath;
			path = path.replaceAll("/", "\\\\");
			File file = new File(path);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
			}
			file.createNewFile();

			File f = new File(path);
			FileUtils.writeString(f, "");
			if (f.isFile()) {
				renderFile(f);
			}
			// write
			FileWriter fw = new FileWriter(file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			exportrecords = exportrecords.replaceAll("<br/>", "\r\n");
			bw.write(exportrecords);
			bw.flush();
			bw.close();
			fw.close();
			setAttr("fileName", fileName);
			setAttr("realPath", realPath);
			setAttr("data", data);
		} else {
			setAttr("msg", "此发货单不是导入生成");
		}
		render("findExportRecords.html");
	}

	/**
	 * 查看发货单
	 */
	@NotNullPara({ "deliveryId" })
	public void findDeliveryItem() {
		String deliveryId = getPara("deliveryId");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Delivery> deliveryList = deliveryService.findAll(companyId, brandId);
		List<Product> productList = productService.findAll(companyId, brandId);
		setAttr("deliveryList", deliveryList);
		setAttr("productList", productList);
		setAttr("deliveryId", deliveryId);
		render("findDeliveryItem.html");
	}

	/**
	 * 表格数据
	 */
	@NotNullPara({ "deliveryId" })
	public void deliveryItemTableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		DeliveryItem data = new DeliveryItem();
		if (StrKit.notBlank(getPara("deliveryId"))) {
			data.setDelivery(getPara("deliveryId"));
		}
		if (StrKit.notBlank(getPara("product"))) {
			data.setProduct(getPara("product"));
		}
		if (StrKit.notBlank(getPara("antiCode"))) {
			data.setAntiCode(getPara("antiCode"));
		}
		if (StrKit.notBlank(getPara("logisticsCode"))) {
			data.setLogisticsCode(getPara("logisticsCode"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<DeliveryItem> dataPage = deliveryItemService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<DeliveryItem>(dataPage));
	}

}