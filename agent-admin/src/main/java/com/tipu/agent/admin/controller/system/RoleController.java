package com.tipu.agent.admin.controller.system;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.plugin.shiro.ShiroCacheUtils;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.CompanyService;
import com.tipu.agent.admin.service.api.RoleService;
import com.tipu.agent.admin.service.api.UserRoleService;
import com.tipu.agent.admin.service.entity.model.Company;
import com.tipu.agent.admin.service.entity.model.CompanyRole;
import com.tipu.agent.admin.service.entity.model.Role;
import com.tipu.agent.admin.service.entity.model.UserRole;
import com.tipu.agent.admin.service.entity.status.system.RoleStatus;
import com.tipu.agent.admin.support.auth.AuthUtils;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 系统角色管理
 * 
 * @author Rlax
 *
 */
@RequestMapping("/system/role")
public class RoleController extends BaseController {

	@JbootrpcService
	private RoleService roleService;

	@JbootrpcService
	private UserRoleService userRoleService;

	@JbootrpcService
	private CompanyService companyService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * res表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		Role sysRole = new Role();
		sysRole.setName(getPara("name"));
		Page<Role> rolePage = roleService.findPage(sysRole, pageNumber, pageSize);
		renderJson(new DataTable<Role>(rolePage));
	}

	/**
	 * add
	 */
	public void add() {
		List<Company> companyList = companyService.findAll();
		setAttr("companyList", companyList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Role sysRole = getBean(Role.class, "role");
		String companyId = sysRole.getCompanyId();
		sysRole.setLastUpdAcct(AuthUtils.getLoginUser().getName());
		sysRole.setStatus(RoleStatus.USED);
		sysRole.setLastUpdTime(new Date());
		sysRole.setNote("保存系统角色");
		if ("0".equals(sysRole.getType())) {
			List<Company> companyList = companyService.findAll();
			if (!roleService.saveRoleAndCompany(sysRole, companyList)) {
				throw new BusinessException("保存失败");
			}
		} else {
			if (!roleService.saveRole(sysRole, companyId)) {
				throw new BusinessException("保存失败");
			}
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		Long id = getParaToLong("id");
		Role sysRole = roleService.findById(id);
		List<Company> companyList = companyService.findAll();
		setAttr("companyList", companyList).setAttr("role", sysRole).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Role sysRole = getBean(Role.class, "role");
		String companyId = sysRole.getCompanyId();
		sysRole.setLastUpdAcct(AuthUtils.getLoginUser().getName());
		sysRole.setLastUpdTime(new Date());
		sysRole.setNote("修改系统资源");
		if ("0".equals(sysRole.getType())) {
			if (!roleService.update(sysRole)) {
				throw new BusinessException("修改失败");
			}
		} else {
			if (!roleService.updateRole(sysRole, companyId)) {
				throw new BusinessException("修改失败");
			}
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		Long id = getParaToLong("id");
		List<UserRole> userRoleList = userRoleService.findByRoleId(id);
		if (userRoleList.size() > 0) {
			throw new BusinessException("请先删除该角色所有的用户");
		}
		if (!roleService.deleteRoleById(id)) {
			throw new BusinessException("删除失败");
		}

		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 角色赋权
	 */
	@NotNullPara({ "id" })
	public void auth() {
		Role sysRole = roleService.findById(getParaToLong("id"));
		setAttr("role", sysRole).render("auth.html");
	}

	/**
	 * 角色赋权提交
	 */
	@NotNullPara({ "id", "resIds" })
	public void postAuth() {
		String resIds = getPara("resIds");
		Long id = getParaToLong("id");

		if (!roleService.auth(id, resIds)) {
			throw new BusinessException("赋权失败");
		}

		ShiroCacheUtils.clearAuthorizationInfoAll();
		renderJson(RestResult.buildSuccess());
	}
}
