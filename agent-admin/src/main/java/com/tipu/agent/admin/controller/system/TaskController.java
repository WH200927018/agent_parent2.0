package com.tipu.agent.admin.controller.system;

import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.TaskBaseService;
import com.tipu.agent.admin.service.entity.model.TaskBase;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 任务管理
 * @author think
 *
 */
@RequestMapping("/system/task")
public class TaskController extends BaseController {

	@JbootrpcService
	private TaskBaseService taskBaseService;
	
	/**
     * index
     */
    public void index() {
        render("main.html");
    }
    
    /**
     * task表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        TaskBase taskBase = new TaskBase();
        taskBase.setName(getPara("name"));

        Page<TaskBase> taskPage = taskBaseService.findPage(taskBase, pageNumber, pageSize);
        renderJson(new DataTable<TaskBase>(taskPage));
    }
    
    /**
     * add
     */
    public void add() {
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
    	TaskBase taskBase = getBean(TaskBase.class, "task");

        if (taskBaseService.hasTask(taskBase.getName())) {
            throw new BusinessException("任务已经存在");
        }

        if (!taskBaseService.save(taskBase)) {
            throw new BusinessException("保存失败");
        }

        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        Long id = getParaToLong("id");
        TaskBase taskBase = taskBaseService.findById(id);

        setAttr("task", taskBase).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
    	TaskBase taskBase = getBean(TaskBase.class, "task");

    	TaskBase _taskBase = taskBaseService.findById(taskBase.getId());
        if (_taskBase == null) {
            throw new BusinessException("任务不存在");
        }

        if (!taskBaseService.update(taskBase)) {
            throw new BusinessException("任务修改失败");
        }
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
        Long id = getParaToLong("id");
        if (!taskBaseService.deleteById(id)) {
            throw new BusinessException("任务删除失败");
        }

        renderJson(RestResult.buildSuccess());
    }
    
}
