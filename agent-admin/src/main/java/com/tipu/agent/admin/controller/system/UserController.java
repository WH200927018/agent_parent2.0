package com.tipu.agent.admin.controller.system;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.CompanyRoleService;
import com.tipu.agent.admin.service.api.CompanyService;
import com.tipu.agent.admin.service.api.RoleService;
import com.tipu.agent.admin.service.api.UserService;
import com.tipu.agent.admin.service.entity.model.Company;
import com.tipu.agent.admin.service.entity.model.CompanyRole;
import com.tipu.agent.admin.service.entity.model.Role;
import com.tipu.agent.admin.service.entity.model.User;
import com.tipu.agent.admin.service.entity.status.system.UserStatus;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.admin.validator.system.ChangePwdValidator;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 系统用户管理
 * @author Rlax
 * 
 */
@RequestMapping("/system/user")
public class UserController extends BaseController {

    @JbootrpcService
    private UserService userService;

    @JbootrpcService
    private RoleService roleService;
    
	@JbootrpcService
	private CompanyService companyService;
	
	@JbootrpcService
	private CompanyRoleService companyRoleService;

    /**
     * index
     */
    public void index() {
        render("main.html");
    }

    /**
     * res表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        User sysUser = new User();
        sysUser.setName(getPara("name"));
        sysUser.setPhone(getPara("phone"));
        sysUser.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
        Page<User> userPage = userService.findPage(sysUser, pageNumber, pageSize);
        renderJson(new DataTable<User>(userPage));
    }

    /**
     * add
     */
    public void add() {
        List<Company> companyList = companyService.findAll();
        setAttr("companyList", companyList).render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        User sysUser = getBean(User.class, "user");
        Long[] roles = getParaValuesToLong("userRole");
        if (userService.hasUser(sysUser.getName())) {
            throw new BusinessException("用户名已经存在");
        }
        sysUser.setCompanyId(AuthUtils.getLoginUser().getCompanyId());
        sysUser.setLastUpdAcct(AuthUtils.getLoginUser().getName());
        if (!userService.saveUser(sysUser, roles)) {
            throw new BusinessException("保存失败");
        }

        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        Long id = getParaToLong("id");
        User sysUser = userService.findById(id);      
        List<CompanyRole> roleList = companyRoleService.findByCompanyId(sysUser.getCompanyId());;
        List<Role> sysRoleList = roleService.findByUserName(sysUser.getName());
        List<Company> companyList = companyService.findAll();
        setAttr("companyList", companyList).setAttr("user", sysUser).setAttr("roleList", roleList).setAttr("userRoleList", sysRoleList).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        User sysUser = getBean(User.class, "user");
        Long[] roles = getParaValuesToLong("userRole");
        User _sysUser = userService.findById(sysUser.getId());
        if (_sysUser == null) {
            throw new BusinessException("用户不存在");
        }

        sysUser.setLastUpdAcct(AuthUtils.getLoginUser().getName());

        if (!userService.updateUser(sysUser, roles)) {
            throw new BusinessException("修改失败");
        }
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
        Long id = getParaToLong("id");
        if (!userService.deleteById(id)) {
            throw new BusinessException("删除失败");
        }

        renderJson(RestResult.buildSuccess());
    }

    /**
     * 解锁用户
     */
    @NotNullPara({"id"})
    public void use() {
        Long id = getParaToLong("id");

        User sysUser = userService.findById(id);
        if (sysUser == null) {
            throw new BusinessException("用户不存在");
        }

        sysUser.setStatus(UserStatus.USED);
        sysUser.setLastUpdTime(new Date());
        sysUser.setNote("解锁系统用户");

        if (!userService.update(sysUser)) {
            throw new BusinessException("解锁失败");
        }

        renderJson(RestResult.buildSuccess());
    }

    /**
     * 锁定用户
     */
    @NotNullPara({"id"})
    public void unuse() {
        Long id = getParaToLong("id");

        User sysUser = userService.findById(id);
        if (sysUser == null) {
            throw new BusinessException("用户不存在");
        }

        sysUser.setStatus(UserStatus.LOCKED);
        sysUser.setLastUpdTime(new Date());
        sysUser.setNote("锁定系统用户");

        if (!userService.update(sysUser)) {
            throw new BusinessException("锁定失败");
        }

        renderJson(RestResult.buildSuccess());
    }

    /**
     * 个人资料
     */
    public void profile() {
        User sysUser = userService.findById(AuthUtils.getLoginUser().getId());
        setAttr("user", sysUser).render("profile.html");
    }

    /**
     * 个人资料修改提交
     */
    public void postProfile() {
        User sysUser = getBean(User.class, "user");
        if (!sysUser.getId().equals(AuthUtils.getLoginUser().getId())) {
            throw new BusinessException("无权操作");
        }

        sysUser.setLastUpdAcct(AuthUtils.getLoginUser().getName());
        sysUser.setLastUpdTime(new Date());
        sysUser.setNote("用户修改个人资料");

        if (!userService.update(sysUser)) {
            throw new BusinessException("资料修改失败");
        }

        renderJson(RestResult.buildSuccess());
    }

    /**
     * 修改密码
     */
    public void changepwd() {
        User sysUser = AuthUtils.getLoginUser();
        setAttr("user", sysUser).render("changepwd.html");
    }

    /**
     * 修改密码提交
     */
    @Before( {POST.class, ChangePwdValidator.class} )
    public void postChangepwd() {
        User sysUser = getBean(User.class, "user");
        if (!sysUser.getId().equals(AuthUtils.getLoginUser().getId())) {
            throw new BusinessException("无权操作");
        }

        String pwd = getPara("newPwd");


        String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
        SimpleHash hash = new SimpleHash("md5", pwd, salt2, 2);
        pwd = hash.toHex();
        sysUser.setPwd(pwd);
        sysUser.setSalt2(salt2);
        sysUser.setLastUpdAcct(AuthUtils.getLoginUser().getName());
        sysUser.setLastUpdTime(new Date());
        sysUser.setNote("用户修改密码");

        if (!userService.update(sysUser)) {
            throw new BusinessException("修改密码失败");
        }

        renderJson(RestResult.buildSuccess());
    }

}
