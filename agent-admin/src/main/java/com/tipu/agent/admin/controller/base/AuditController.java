package com.tipu.agent.admin.controller.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentBanksService;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentPriceService;
import com.tipu.agent.front.service.api.AgentRewardService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentStockService;
import com.tipu.agent.front.service.api.AgentTakeStockService;
import com.tipu.agent.front.service.api.OrderProductService;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.api.ShoppingRecordService;
import com.tipu.agent.front.service.api.SkuService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBanks;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentPrice;
import com.tipu.agent.front.service.entity.model.AgentReward;
import com.tipu.agent.front.service.entity.model.AgentStock;
import com.tipu.agent.front.service.entity.model.AgentTakeStock;
import com.tipu.agent.front.service.entity.model.OrderProduct;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.ShoppingRecord;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 审核管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/audit")
public class AuditController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrdersService orderService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrderProductService orderProductService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBanksService agentBanksService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ShoppingRecordService shoppingRecordService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentRewardService agencyRewardService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SkuService skuService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTakeStockService agentTakeStockService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentStockService agentStockService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentPriceService agentPriceService;

	/**
	 * 充值index
	 */
	public void rechargeIndex() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		render("recharge/main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		Orders data = new Orders();
		if (StrKit.notBlank(getPara("agent"))) {
			data.setAgentId(getPara("agent"));
		}
		if (StrKit.notBlank(getPara("state"))) {
			data.setState(getPara("state"));
		}
		if (StrKit.notBlank(getPara("id"))) {
			data.setId(getPara("id"));
		}
		if (StrKit.notBlank(getPara("type"))) {
			data.setOrderType(getPara("type"));
		}
		if (StrKit.notBlank(getPara("handleAgentId"))) {
			data.setHandleAgentId(getPara("handleAgentId"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		// JbootRedis redis = Jboot.me().getRedis();
		// String brandId = redis.get(companyId + "_brandId");
		// if (StrKit.notBlank(brandId)) {
		// data.setBrandId(brandId);
		// }
		Page<Orders> dataPage = orderService.findPage(data, pageNumber, pageSize);

		renderJson(new DataTable<Orders>(dataPage));
	}

	/**
	 * 充值审核页
	 */
	@NotNullPara({ "id" })
	public void auditRecharge() {
		String id = getPara("id");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		// JbootRedis redis = Jboot.me().getRedis();
		// String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, "");
		List<AgentBanks> agentBanksList = agentBanksService.findAll(companyId, "");
		Orders data = orderService.findById(id);
		setAttr("data", data).setAttr("agentList", agentList).setAttr("agentBanksList", agentBanksList);
		render("recharge/audit.html");
	}

	/**
	 * 充值提交
	 */
	public void postRecharge() {
		Orders order = getBean(Orders.class, "order");
		Orders data = getBean(Orders.class, "data");
		if (StrKit.isBlank(order.getState())) {
			throw new BusinessException("请先审核！");
		}
		Orders oldOrder = orderService.findById(order.getId());
		if (oldOrder == null) {
			throw new BusinessException("数据不存在");
		}
		if ("1".equals(order.getState())) {// 审核通过后代理商修改货款
			Agent agent = new Agent();
			agent.setId(data.getAgentId());
			agent.setAgentDeposit(data.getBuySum());
			int type = 0;// 0-增加;1-减少
			if (agentService.updateAgentDeposit(agent, type) <= 0) {
				throw new BusinessException("审核失败");
			}
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		order.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			order.setBrandId(brandId);
		}
		order.setUpdateDate(new Date());
		order.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!orderService.update(order)) {
			throw new BusinessException("审核失败");
		}
		Agent newAgent = agentService.findById(data.getAgentId());
		String balance = "";
		if (newAgent != null) {
			balance = newAgent.getAgentDeposit() + "";
		}
		ShoppingRecord shoppingRecord = new ShoppingRecord();
		shoppingRecord.setOrderId(order.getId());
		shoppingRecord.setRemarks("充值货款：" + data.getBuySum());
		shoppingRecord.setShoppingPrice(data.getBuySum() + "");
		shoppingRecord.setAgencyId(data.getAgentId());
		shoppingRecord.setAgencyBalance(balance);
		shoppingRecord.setType("3");
		shoppingRecord.setDelFlag("0");
		shoppingRecord.setCreateDate(new Date());
		shoppingRecord.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		if (!shoppingRecordService.save(shoppingRecord)) {
			throw new BusinessException("审核失败");
		}
		orderService.refreshCache();
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 提现index
	 */
	public void withdrawIndex() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		// JbootRedis redis = Jboot.me().getRedis();
		// String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, "");
		setAttr("agentList", agentList);
		render("withdraw/main.html");
	}

	/**
	 * 提现审核页
	 */
	@NotNullPara({ "id" })
	public void auditWithdraw() {
		String id = getPara("id");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		// JbootRedis redis = Jboot.me().getRedis();
		// String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, "");
		List<AgentBanks> agentBanksList = agentBanksService.findAll(companyId, "");
		Orders data = orderService.findById(id);
		setAttr("data", data).setAttr("agentList", agentList).setAttr("agentBanksList", agentBanksList);
		render("withdraw/audit.html");
	}

	/**
	 * 提现提交
	 */
	public void postWithdraw() {
		Orders order = getBean(Orders.class, "order");
		Orders data = getBean(Orders.class, "data");
		if (StrKit.isBlank(order.getState())) {
			throw new BusinessException("请先审核！");
		}
		Orders oldOrder = orderService.findById(order.getId());
		if (oldOrder == null) {
			throw new BusinessException("数据不存在");
		}
		if ("1".equals(order.getState())) {// 审核通过后代理商修改货款
			Agent agent = new Agent();
			agent.setId(data.getAgentId());
			agent.setAgentDeposit(data.getBuySum());
			agent.setAgentReward(data.getBuySum());
			if (agentService.updateAgentReward(agent, 1) <= 0) {// 先修改奖励金
				throw new BusinessException("奖励金不足，无法提现！");
			}
			int type = 0;// 0-增加;1-减少
			if (agentService.updateAgentDeposit(agent, type) <= 0) {// 再修改货款
				throw new BusinessException("审核失败");
			}
		}
		order.setUpdateDate(new Date());
		order.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!orderService.update(order)) {
			throw new BusinessException("审核失败");
		}
		Agent newAgent = agentService.findById(data.getAgentId());
		String balance = "";
		if (newAgent != null) {
			balance = newAgent.getAgentDeposit() + "";
		}
		ShoppingRecord shoppingRecord = new ShoppingRecord();
		shoppingRecord.setOrderId(order.getId());
		shoppingRecord.setRemarks("奖励提现现金：" + data.getBuySum());
		shoppingRecord.setShoppingPrice(data.getBuySum() + "");
		shoppingRecord.setAgencyId(data.getAgentId());
		shoppingRecord.setAgencyBalance(balance);
		shoppingRecord.setType("2");
		shoppingRecord.setDelFlag("0");
		shoppingRecord.setCreateDate(new Date());
		shoppingRecord.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		if (!shoppingRecordService.save(shoppingRecord)) {
			throw new BusinessException("审核失败");
		}
		orderService.refreshCache();
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 修改订单状态
	 */
	@NotNullPara({ "id", "state" })
	public void stateUpdate() {
		String id = getPara("id");
		String state = getPara("state");
		Orders order = orderService.findById(id);
		if (order == null) {
			throw new BusinessException("数据不存在");
		}
		Orders data = new Orders();
		data.setOrderGroupId(order.getOrderGroupId());
		List<Orders> orderList = orderService.findList(data);
		if (orderList.size() > 0) {// 修改订单组的所有订单
			for (Orders oldData : orderList) {
				oldData.setState(state);
				oldData.setUpdateDate(new Date());
				oldData.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
				if (!orderService.update(oldData)) {
					throw new BusinessException("发货失败");
				}
			}
		}
		orderService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 订单index
	 */
	public void orderIndex() {
		render("order/main.html");
	}

	/**
	 * 订单审核页
	 */
	@NotNullPara({ "id" })
	public void auditOrder() {
		String id = getPara("id");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		// JbootRedis redis = Jboot.me().getRedis();
		// String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, "");
		Orders data = orderService.findById(id);
		Orders firstOrder = orderService.findByAgentIdAndGroupId(data.getAgentId(),data.getOrderGroupId());
		OrderProduct orderProduct = new OrderProduct();
		orderProduct.setOrderId(firstOrder.getId());
		List<OrderProduct> orderProductList = orderProductService.findList(orderProduct);
		setAttr("data", data).setAttr("agentList", agentList).setAttr("orderProductList", orderProductList);
		render("order/audit.html");
	}

	/**
	 * 订单审核提交
	 */
	public void postOrder() {
		Orders order = getBean(Orders.class, "order");
		if (StrKit.isBlank(order.getState())) {
			throw new BusinessException("请先审核！");
		}
		Orders oldOrder = orderService.findById(order.getId());
		if (oldOrder == null) {
			throw new BusinessException("数据不存在");
		}
		if (oldOrder.getOrderType().equals("1")) {// 普通订单
			if ("2".equals(order.getState())) {// 审核通过
				String companyId = AuthUtils.getLoginUser().getCompanyId();
				JbootRedis redis = Jboot.me().getRedis();
				String brandId = redis.get(companyId + "_brandId");
				AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(oldOrder.getOrderCreaterId(), brandId);
				// 上级不是总公司，还有上级代理,上级代理同级提成8%的奖励金
				if (!"0".equals(agentBrand.getParentId())) {
					Agent parentAgent = new Agent();
					parentAgent.setId(agentBrand.getParentId());
					// 查询奖励金
					AgentReward agencyReward = new AgentReward();
					agencyReward.setAgentId(agentBrand.getParentId());
					agencyReward.setOrderId(oldOrder.getId());
					List<AgentReward> agencyRewardList = agencyRewardService.findList(agencyReward);
					if (agencyRewardList.size() > 0) {
						parentAgent.setAgentReward(agencyRewardList.get(0).getRewardMoney());
						if (agentService.updateAgentReward(parentAgent, 0) <= 0) {
							throw new BusinessException("奖励金更新失败");
						}
					}
				}
				// 更新实际库存总数
				Agent agent = agentService.findById(oldOrder.getAgentId());
				Integer actualStock = agent.getActualStock() + oldOrder.getBuyNums();
				agent.setActualStock(actualStock);
				agentService.update(agent);
				// 新增代理商库存信息
				OrderProduct orderProduct = new OrderProduct();
				orderProduct.setOrderId(order.getId());
				List<OrderProduct> orderProductList = orderProductService.findList(orderProduct);
				if (orderProductList.size() > 0) {
					for(OrderProduct op:orderProductList) {
						AgentPrice agentPrice = agentPriceService.findBySkuAndAgentType(orderProduct.getProduct(),
								agent.getAgentType());
						AgentStock agentStock = new AgentStock();
						agentStock.setAgentId(oldOrder.getAgentId());
						agentStock.setCompanyId(companyId);
						agentStock.setOrderId(order.getId());
						agentStock.setPurchasePrice(new BigDecimal(agentPrice.getAgencyPrice()));
						agentStock.setSkuId(op.getProduct());
						agentStock.setSkuName(op.getStr("productName"));
						agentStock.setStockNumber(Integer.valueOf(op.getBuyNum()));
						agentStock.setStockType("1");
						agentStock.setCreateDate(new Date());
						agentStock.setCreateBy(AuthUtils.getLoginUser().getId().toString());
						agentStock.setProductInfo(op.getProductImg());
						agentStock.setDelFlag("0");
						agentStockService.save(agentStock);
					}
				}
		

			} else {// 审核不通过，回滚
				Orders orders = new Orders();
				orders.setOrderGroupId(oldOrder.getOrderGroupId());
				List<Orders> orderList = orderService.findList(orders);// 订单组所有订单
				Agent handleAgent = new Agent();// 订单处理人需要减去奖金、退回收到的货款
				Agent createAgent = new Agent();// 订单创建人需要退回货款
				OrderProduct orderProduct = new OrderProduct();
				orderProduct.setOrderId(oldOrder.getId());
				List<OrderProduct> orderProductList = orderProductService.findList(orderProduct);
				if (orderProductList.size() > 0) {// 恢复库存
					for(OrderProduct op:orderProductList) {				
					skuService.updateStock(Integer.getInteger(op.getBuyNum()), 0, op.getProduct());
					}
				}
				if (orderList.size() > 0) {
					for (Orders ord : orderList) {

						if (!"0".equals(ord.getHandleAgentId())) { // 去除处理人为总公司的订单
							// 处理人退回奖励金
							AgentReward agencyReward = new AgentReward();
							agencyReward.setOrderId(ord.getId());
							List<AgentReward> agencyRewardList = agencyRewardService.findList(agencyReward);
							if (agencyRewardList.size() > 0) {
								agencyReward = agencyRewardList.get(0);
								handleAgent.setId(agencyReward.getAgentId());
								handleAgent.setAgentReward(agencyReward.getRewardMoney());
								if (agentService.updateFrontAgentReward(handleAgent, 1) <= 0) {// 订单处理人需要减去奖金，可以为负数
									throw new BusinessException("奖励金还原失败");
								}
								// 奖励金退款记录
								ShoppingRecord record = new ShoppingRecord();
								record.setOrderId(ord.getId());
								record.setType("5");// 1-进货消费；3-收到货款；6-订单退款；5-奖励退款
								record.setRemarks("奖励退款：" + agencyReward.getRewardMoney());
								record.setShoppingPrice(agencyReward.getRewardMoney() + "");
								record.setAgencyId(agencyReward.getAgentId());
								record.setDelFlag("0");
								record.setCreateDate(new Date());
								record.setCreateBy(AuthUtils.getLoginUser().getId().toString());
								if (!shoppingRecordService.save(record)) {
									throw new BusinessException("审核失败");
								}
								AgentReward newAgencyReward = new AgentReward();
								newAgencyReward.setRewardMoney(agencyReward.getRewardMoney());
								newAgencyReward.setAgentId(agencyReward.getAgentId());
								newAgencyReward.setOrderId(agencyReward.getOrderId());
								newAgencyReward.setRemarks("订单取消，奖励金退款");
								newAgencyReward.setRewardType("4");
								newAgencyReward.setDelFlag("0");
								newAgencyReward.setCreateDate(new Date());
								newAgencyReward.setCreateBy(AuthUtils.getLoginUser().getId().toString());
								if (!agencyRewardService.save(newAgencyReward)) {
									throw new BusinessException("奖励金退款记录新增失败");
								}
							}
						}
						// 处理人退回收到的货款
						ShoppingRecord shopping = new ShoppingRecord();
						shopping.setOrderId(ord.getId());
						shopping.setType("3");// 1-进货消费；3-收到货款；6-订单退款；5-奖励退款
						List<ShoppingRecord> shoppingList = shoppingRecordService.findList(shopping);
						if (shoppingList.size() > 0) {
							shopping = shoppingList.get(0);
							handleAgent.setId(shopping.getAgencyId());
							handleAgent.setAgentDeposit(new BigDecimal(shopping.getShoppingPrice()));
							if (agentService.updateAgentDeposit(handleAgent, 1) <= 0) {// 订单处理人退回收到的货款
								throw new BusinessException("收到的货款退还失败");
							}
							// 记录消费
							ShoppingRecord record = new ShoppingRecord();
							record.setOrderId(shopping.getOrderId());
							record.setType("6");// 1-进货消费；3-收到货款；6-订单退款；5-奖励退款
							record.setRemarks("订单退款：" + shopping.getShoppingPrice());
							record.setShoppingPrice(shopping.getShoppingPrice());
							record.setAgencyId(shopping.getAgencyId());
							record.setDelFlag("0");
							record.setCreateDate(new Date());
							record.setCreateBy(AuthUtils.getLoginUser().getId().toString());
							if (!shoppingRecordService.save(record)) {
								throw new BusinessException("审核失败");
							}
						}

						// 订单创建人退回进货消费
						ShoppingRecord shoppingRecord = new ShoppingRecord();
						shoppingRecord.setOrderId(ord.getId());
						shoppingRecord.setType("1");// 1-进货消费；3-收到货款；6-订单退款；5-奖励退款
						List<ShoppingRecord> shoppingRecordList = shoppingRecordService.findList(shoppingRecord);
						if (shoppingRecordList.size() > 0) {
							shoppingRecord = shoppingRecordList.get(0);
							createAgent.setId(shoppingRecord.getAgencyId());
							createAgent.setAgentDeposit(new BigDecimal(shoppingRecord.getShoppingPrice()));
							if (agentService.updateAgentDeposit(createAgent, 0) <= 0) {// 订单创建人退回进货消费
								throw new BusinessException("进货消费退还失败");
							}
							// 记录消费
							ShoppingRecord record = new ShoppingRecord();
							record.setOrderId(shoppingRecord.getOrderId());
							record.setType("6");// 1-进货消费；3-收到货款；6-订单退款；5-奖励退款
							record.setRemarks("订单退款：" + shoppingRecord.getShoppingPrice());
							record.setShoppingPrice(shoppingRecord.getShoppingPrice());
							record.setAgencyId(shoppingRecord.getAgencyId());
							record.setDelFlag("0");
							record.setCreateDate(new Date());
							record.setCreateBy(AuthUtils.getLoginUser().getId().toString());
							if (!shoppingRecordService.save(record)) {
								throw new BusinessException("审核失败");
							}
						}

					}
				}
			}
		} else if (oldOrder.getOrderType().equals("2")) {// 提货订单
			if ("-1".equals(order.getState())) {// 审核不通过
				AgentTakeStock agentTakeStock = new AgentTakeStock();
				agentTakeStock.setOrderId(order.getId());
				List<AgentTakeStock> agentTakeStockList = agentTakeStockService.findList(agentTakeStock);
				if (agentTakeStockList.size() > 0) {
					for (AgentTakeStock data : agentTakeStockList) {
						data.setUpdateDate(new Date());
						data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
						data.setDelFlag("1");
						agentTakeStockService.update(data);
					}
				}
			}
		}

		order.setUpdateDate(new Date());
		order.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!orderService.update(order)) {
			throw new BusinessException("审核失败");
		}
		orderService.refreshCache();
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
}
