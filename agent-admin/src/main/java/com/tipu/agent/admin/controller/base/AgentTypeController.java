package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.SettingValService;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.SettingVal;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AgentType管理
 * 
 * @author think
 *
 */
@RequestMapping("/base/agent_type")
public class AgentTypeController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService
	private BrandService brandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SettingValService settingService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AgentType data = new AgentType();
		if (StrKit.notBlank(getPara("name"))) {
			data.setName(getPara("name"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<AgentType> dataPage = agentTypeService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentType>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		SettingVal setting = new SettingVal();
		setting.setSettingId("is_recharge");
		setting.setCompanyId(companyId);
		setting.setBrandId(brandId);
		// 系统配置项充值开关
		setting = settingService.getSettingBySysKey(setting);
		String settingRecharge = "";
		if (setting != null) {
			settingRecharge = setting.getVal();
		}
		// 系统配置项保证金开关
		setting = new SettingVal();
		setting.setSettingId("is_bond");
		setting.setCompanyId(companyId);
		setting.setBrandId(brandId);
		setting = settingService.getSettingBySysKey(setting);
		String settingBond = "";
		if (setting != null) {
			settingBond = setting.getVal();
		}
		setAttr("settingRecharge", settingRecharge);
		setAttr("settingBond", settingBond);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		AgentType data = getBean(AgentType.class, "data");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		if (!agentTypeService.save(data)) {
			throw new BusinessException("保存失败");
		}
		agentTypeService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		AgentType data = agentTypeService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		SettingVal setting = new SettingVal();
		setting.setSettingId("is_recharge");
		setting.setCompanyId(companyId);
		setting.setBrandId(brandId);
		// 系统配置项充值开关
		setting = settingService.getSettingBySysKey(setting);
		String settingRecharge = "";
		if (setting != null) {
			settingRecharge = setting.getVal();
		}
		// 系统配置项保证金开关
		setting = new SettingVal();
		setting.setSettingId("is_bond");
		setting.setCompanyId(companyId);
		setting.setBrandId(brandId);
		setting = settingService.getSettingBySysKey(setting);
		String settingBond = "";
		if (setting != null) {
			settingBond = setting.getVal();
		}
		setAttr("settingRecharge", settingRecharge);
		setAttr("settingBond", settingBond);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		AgentType data = getBean(AgentType.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (agentTypeService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!agentTypeService.update(data)) {
			throw new BusinessException("修改失败");
		}
		agentTypeService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		AgentType agentType = new AgentType();
		agentType.setId(id);
		agentType.setDelFlag("1");
		if (!agentTypeService.update(agentType)) {
			throw new BusinessException("删除失败");
		}
		agentTypeService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 更新充值开关的状态
	 */
	public void updateRecharge() {
		String id = getPara("id");
		Integer recharge = getParaToInt("recharge"); // 充值
		if (StrKit.notBlank(id) && recharge != null) {
			AgentType data = agentTypeService.findById(id);
			data.setIsRecharge(recharge);
			if (!agentTypeService.update(data)) {
				throw new BusinessException("修改失败");
			}
			agentTypeService.refreshCache();
			renderJson(RestResult.buildSuccess());
		}
	}

	/**
	 * 更新保证金开关的状态
	 */
	public void updateBond() {
		String id = getPara("id");
		Integer bond = getParaToInt("bond"); // 充值
		if (StrKit.notBlank(id) && bond != null) {
			AgentType data = agentTypeService.findById(id);
			data.setIsBond(bond);
			if (!agentTypeService.update(data)) {
				throw new BusinessException("修改失败");
			}
			agentTypeService.refreshCache();
			renderJson(RestResult.buildSuccess());
		}
	}
	/**
	 * 更新提成比例开关的状态
	 */
	public void updateProfit() {
		String id = getPara("id");
		Integer profit = getParaToInt("profit"); // 充值
		if (StrKit.notBlank(id) && profit != null) {
			AgentType data = agentTypeService.findById(id);
			data.setIsProfit(profit+"");
			if (!agentTypeService.update(data)) {
				throw new BusinessException("修改失败");
			}
			agentTypeService.refreshCache();
			renderJson(RestResult.buildSuccess());
		}
	}
	

}