package com.tipu.agent.admin.controller.anticode;

import java.util.Date;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.anticode.service.api.AnticodeSearchLogService;
import com.tipu.agent.anticode.service.entity.model.AnticodeSearchLog;
import com.tipu.agent.front.service.api.ProductService;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AnticodeSearchLog管理
 * @author think
 *
 */
@RequestMapping("/anticode/anticode_search_log")
public class AnticodeSearchLogController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AnticodeSearchLogService anticodeSearchLogService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private ProductService productService;
	
	/**
     * index
     */
    public void index() {
    	setAttr("products", productService.findAll()); // 产品列表
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        AnticodeSearchLog data = new AnticodeSearchLog();
        
        Date createDate = getParaToDateTime("create_date");
        Date updateDate = getParaToDateTime("update_date");
        if(createDate != null) {
        	data.setCreateDate(createDate);
        }
        if(updateDate != null) {
        	data.setUpdateDate(updateDate);
        }
        
        if(StrKit.notBlank(getPara("productId"))) {
        	data.setProductId(getPara("productId"));
        }
        
        if(StrKit.notBlank(getPara("mobile"))) {
        	data.setMobile(getPara("mobile"));
        }
        
        if(StrKit.notBlank(getPara("logisticsCode"))) {
        	data.setLogisticsCode(getPara("logisticsCode"));
        }
        
        if(StrKit.notBlank(getPara("anticodeContent"))) {
        	data.setAnticodeContent(getPara("anticodeContent"));
        }
        
        if(getParaToInt("sellerChannel") != null) {
        	data.setSellerChannel(getParaToInt("sellerChannel"));
        }
        
        if(getParaToInt("firstFake") != null) {
        	data.setFirstFake(getParaToInt("firstFake"));
        }
        
        if(getParaToInt("fake") != null) {
        	data.setFake(getParaToInt("fake"));
        }

        Page<AnticodeSearchLog> dataPage = anticodeSearchLogService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<AnticodeSearchLog>(dataPage));
    }

    /**
     * add
     */
    public void add() {
    	setAttr("products", productService.findAll()); // 产品列表
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        AnticodeSearchLog data = getBean(AnticodeSearchLog.class, "data");

        if (!anticodeSearchLogService.save(data)) {
            throw new BusinessException("保存失败");
        }
        anticodeSearchLogService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
    	
    	setAttr("products", productService.findAll()); // 产品列表
    	
    	String id = getPara("id");
        AnticodeSearchLog data = anticodeSearchLogService.findById(id);

        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        AnticodeSearchLog data = getBean(AnticodeSearchLog.class, "data");

        if (anticodeSearchLogService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!anticodeSearchLogService.update(data)) {
            throw new BusinessException("修改失败");
        }
        anticodeSearchLogService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
    	String id = getPara("id");
        if (!anticodeSearchLogService.deleteById(id)) {
            throw new BusinessException("删除失败");
        }
        anticodeSearchLogService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}