package com.tipu.agent.admin.controller.base;

import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.util.CommonUtils;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.api.DataService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.admin.service.entity.model.Data;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentAuthService;
import com.tipu.agent.front.service.api.AgentAuthTempletService;
import com.tipu.agent.front.service.api.AgentBondService;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.SettingValService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAuth;
import com.tipu.agent.front.service.entity.model.AgentAuthTemplet;
import com.tipu.agent.front.service.entity.model.AgentBond;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.SettingVal;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AgentAudit管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/agent_auth")
public class AgentAuthController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAuthService agentAuthService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAuthTempletService agentAuthTempletService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SettingValService settingService;

	@JbootrpcService
	private DataService dataApi;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBondService agentBondService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService
	private BrandService brandService;

	/**
	 * index
	 */
	public void index() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AgentAuth data = new AgentAuth();
		if (StrKit.notBlank(getPara("authStatus"))) {
			data.setAuthPic(getPara("authStatus"));
		}
		if (StrKit.notBlank(getPara("agentId"))) {
			data.setAgentId(getPara("agentId"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		// data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<AgentAuth> dataPage = agentAuthService.findPage(data, pageNumber, pageSize);
		agentBondService.refreshCache();
		agentService.refreshCache();
		renderJson(new DataTable<AgentAuth>(dataPage));
	}

	/**
	 * add 添加授权
	 */
	public void addAuth() {
		String agentId = getPara("agentId");
		List<Agent> agentList = new ArrayList<Agent>();
		Agent agent = new Agent();
		if (StrKit.notBlank(agentId)) {
			agent = agentService.findById(agentId);
			agentList = null;
		} else {
			String companyId = AuthUtils.getLoginUser().getCompanyId();
			JbootRedis redis = Jboot.me().getRedis();
			String brandId = redis.get(companyId + "_brandId");
			agentList = agentService.findAll(companyId, brandId);
			agent = null;
		}
		setAttr("agent", agent);
		setAttr("agentList", agentList);
		render("add.html");
	}

	/**
	 * 保存提交
	 * 
	 * @throws IOException
	 * @throws FontFormatException
	 * @throws ParseException
	 */
	public void postAdd() throws FontFormatException, IOException, ParseException {
		AgentAuth agentAuth = getBean(AgentAuth.class, "data");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		// agentAuth.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			agentAuth.setBrandId(brandId);
		}
		String id = StrKit.getRandomUUID();
		agentAuth.setId(id);
		agentAuth.setCreateDate(new Date());
		agentAuth.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		agentAuth.setDelFlag("0");
		if (agentAuth.getAuditStatus().equals("1")) {
			Agent entity = agentService.findById(agentAuth.getAgentId());
			entity.setIsFristAuth(agentAuth.getAuditStatus());
			agentService.update(entity);
			agentService.refreshCache();
		}
		if (!agentAuthService.save(agentAuth)) {
			throw new BusinessException("保存失败");
		}
		agentAuthService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * show
	 */
	public void show() {
		String id = getPara("id");
		AgentAuth data = new AgentAuth();
		data.setId(id);
		List<AgentAuth> list = agentAuthService.findList(data);
		if (list.size() > 0) {
			data = list.get(0);
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		SettingVal setting = new SettingVal();
		setting.setSettingId("is_payimg");
		setting.setCompanyId(companyId);
		setting.setBrandId(brandId);
		setting = settingService.getSettingBySysKey(setting);
		List<Agent> agentList = agentService.findAll();
		setAttr("setting", setting).setAttr("agentList", agentList).setAttr("data", data).render("update.html");
	}

	/**
	 * 授权
	 * 
	 * @throws ParseException
	 * @throws IOException
	 * @throws FontFormatException
	 */
	public void auth() throws FontFormatException, IOException, ParseException {
		AgentAuth data = getBean(AgentAuth.class, "data");// 修改的实体
		AgentAuth agentAuth = agentAuthService.findById(data.getId());
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		Agent agent = agentService.findById(data.getAgentId());
		if (agent == null || agentAuth == null) {
			throw new BusinessException("数据不存在");
		}
		AgentBrand agentBrand = agentBrandService.findByAgentAndBrand(agent.getId(), brandId);
		if (data.getAuditStatus().equals("1")) {
			agent.setIsFristAuth("1");
			if (agentBrand != null) {
				if ("1".equals(agentBrand.getAuditStatus())) {
					String expireTime = "";// 过期时间
					// 查询代理商有效期设置
					SettingVal setting = new SettingVal();
					setting.setSettingId("is_limit");
					setting.setCompanyId(companyId);
					setting.setBrandId(brandId);
					setting = settingService.getSettingBySysKey(setting);
					if (setting != null) {
						if ("1".equals(setting.getEnable())) {
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
							Integer num = 30 * Integer.parseInt(setting.getVal());
							expireTime = getDay(format.format(new Date()), num);
							data.setValidityDate(format.parse(expireTime));// 添加有效期时间
						}
					}
					// 授权书
					Calendar cal = Calendar.getInstance();
					int year = cal.get(Calendar.YEAR);
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Map<String, String> params = new HashMap<String, String>();
					List<Data> list = dataApi.getListByTypeOnUse("agent_level");
					String agentLevelName = ""; // 代理级别名称 数据字典
					AgentType agentType = agentTypeService.findById(agentBrand.getAgentType());
					for (Data selfData : list) {
						if (selfData.getCode().equals(agentType.getGrade().toString())) {
							agentLevelName = selfData.getCodeDesc();
						}
					}
					params.put("level", agentLevelName);
					params.put("name", agent.getName());
					params.put("year", String.valueOf(year));
					String date = format.format(cal.getTime());
					String auditDate = "";
					if (StrKit.notBlank(date)) {
						auditDate =year+"年"+ date.substring(5, 7)+"月"+date.substring(8, 10)+"日";
					}
					params.put("auditDate", auditDate);
					params.put("date", date);
					params.put("startDate", format.format(cal.getTime()));
					params.put("endDate", expireTime);
					params.put("code", agentBrand.getCode());
					params.put("weixin", agent.getWeixin());
					params.put("mobile", agent.getMobile());
					String authDate = format.format(cal.getTime()) + " 至 " + expireTime;
					params.put("authDate", authDate);
					String id = StrKit.getRandomUUID();
					String picPath = File.separator + "auth" + File.separator + "pic" + File.separator + id + ".jpg";
					String savePath = PathKit.getWebRootPath() + picPath;
					AgentAuthTemplet agentAuthTemplet = agentAuthTempletService
							.findByApplyTypeAndBrandId(agentAuth.getApplyType(), agentBrand.getBrandId());
					if(agentAuthTemplet==null) {
						throw new BusinessException("请先进行授权文件配置");
					}
					String ossPic = CommonUtils.genAuthPic(agentAuthTemplet.getAuthTemplet(),
							 savePath, params,agentAuthTemplet.getTempletConfig());
					if (StrKit.notBlank(ossPic)) {
						picPath = ossPic;
					}
					data.setAuthPic(picPath);
					agentBrand.setAuthPic(picPath);

				} else {
					throw new BusinessException("请先进行品牌授权");
				}
			} else {
				throw new BusinessException("请先进行品牌授权");
			}
		}
		agentBrand.setAuthStatus(data.getAuditStatus());
		agentBrand.setUpdateDate(new Date());
		agentBrand.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		agentBrandService.update(agentBrand);
		agentBrandService.refreshCache();
		agent.setUpdateDate(new Date());
		agent.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		agentService.update(agent);
		agentService.refreshCache();
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		agentAuthService.update(data);
		agentAuthService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 查看授权证书
	 */
	public void findAuthPic() {
		String id = getPara("id");
		AgentAuth data = agentAuthService.findById(id);
		setAttr("data", data).render("findAuthPic.html");
	}

	/**
	 * 更新有效期时间
	 * 
	 * @throws ParseException
	 */
	public void updateValidityDate() throws ParseException {
		String id = getPara("id");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		AgentAuth data = agentAuthService.findById(id);
		if (data == null) {
			throw new BusinessException("数据不存在");
		}
		SettingVal setting = new SettingVal();
		setting.setSettingId("is_limit");
		setting.setCompanyId(companyId);
		setting.setBrandId(brandId);
		setting = settingService.getSettingBySysKey(setting);
		if (setting != null) {
			if ("1".equals(setting.getEnable())) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Integer num = 30 * Integer.parseInt(setting.getVal());
				String validityDate = getDay(format.format(new Date()), num);
				data.setValidityDate(format.parse(validityDate));// 添加有效期时间
				data.setUpdateDate(new Date());
				data.setAuditStatus("1");
				data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
				agentAuthService.update(data);
			} else {
				throw new BusinessException("代理商有效期设置未启用!");
			}
		} else {
			throw new BusinessException("代理商有效期设置未创建!");
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 查看保证金
	 */
	public void findBond() {
		String id = getPara("id");
		AgentAuth agentAuth = agentAuthService.findById(id);
		if (agentAuth == null) {
			throw new BusinessException("数据不存在");
		}
		AgentBond agentBond = new AgentBond();
		agentBond.setAgent(agentAuth.getAgentId());
		List<AgentBond> agentBondList = agentBondService.findList(agentBond);
		if (agentBondList.size() > 0) {
			agentBond = agentBondList.get(0);
		}

		setAttr("agentBond", agentBond);
		setAttr("agentBondList", agentBondList);
		render("findAgentBond.html");
	}

	/**
	 * 保证金审核
	 */
	public void updateBond() {
		String id = getPara("id"); // 代理商id
		Integer status = getParaToInt("status"); // 代理商的授权状态
		if (StrKit.notBlank(id) && status != null) {

			AgentBond agentBond = new AgentBond();
			agentBond.setAgent(id);
			List<AgentBond> agentBondList = agentBondService.findList(agentBond);
			if (agentBondList.size() > 0) {
				agentBond = agentBondList.get(0);
				agentBond.setAuditStatus(status + "");
				agentBondService.update(agentBond);
				agentBondService.refreshCache();
			}
			if (status == -1) {// 保证金审核拒绝时修改授权
				Agent agent = agentService.findById(id);
				if (agent != null) {
					agent.setIsFristAuth("-1");
					agentService.update(agent);
					agentService.refreshCache();
				}
			}

		} else {
			throw new BusinessException("审核失败");
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 求后num天
	 * 
	 * @param day
	 * @return
	 */
	public String getDay(String day, int num) {
		Calendar c = Calendar.getInstance();
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(day);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day1 = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day1 + num);

		String dayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
		return dayAfter;
	}
}
