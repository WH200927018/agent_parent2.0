package com.tipu.agent.admin.controller.system;


import java.util.Date;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.front.service.api.SettingService;
import com.tipu.agent.front.service.entity.model.Setting;
import com.tipu.agent.admin.support.auth.AuthUtils;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * SystemConfig管理
 * 
 * @author think
 *
 */
@RequestMapping("/system/setting")
public class SettingController extends BaseController {
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SettingService settingService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		Setting data = new Setting();
		Page<Setting> dataPage = settingService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Setting>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Setting data = getBean(Setting.class, "data");
		String uuid = StrKit.getRandomUUID();
		data.setId(uuid);
		data.setCreateDate(new Date());
		if (!settingService.save(data)) {
			throw new BusinessException("保存失败");
		}
		settingService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		Setting data = settingService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Setting data = getBean(Setting.class, "data");
		Setting oldData = settingService.findById(data.getId());
		data.setUpdateDate(new Date());
		if (oldData == null) {
			throw new BusinessException("数据不存在");
		}
		if (!settingService.update(data)) {
			throw new BusinessException("修改失败");
		}
		settingService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
	

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Setting systemConfig = new Setting();
		systemConfig.setId(id);
		if (!settingService.update(systemConfig)) {
			throw new BusinessException("删除失败");
		}
		settingService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
	
	
}
