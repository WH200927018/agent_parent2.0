package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentInviteService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentInvite;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.admin.service.entity.model.Brand;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AgentInvite管理
 * @author think
 *
 */
@RequestMapping("/base/agent_invite")
public class AgentInviteController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AgentInviteService agentInviteService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AgentService agentService;
	
	@JbootrpcService
	private BrandService brandService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;
	
	/**
     * index
     */
    public void index() {
        String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<Agent> agentList = agentService.findAll(companyId, brandId); //代理商
    	List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);//代理商级别
    	setAttr("agentList", agentList);
    	setAttr("agentTypeList", agentTypeList);
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        AgentInvite data = new AgentInvite();
        if(StrKit.notBlank(getPara("agent"))){
        	data.setAgent(getPara("agent"));
        }
        if(StrKit.notBlank(getPara("agentType"))){
        	data.setAgentType(getPara("agentType"));
        }
        String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrand(brandId);
		}
        Page<AgentInvite> dataPage = agentInviteService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<AgentInvite>(dataPage));
    }

    /**
     * add
     */
    public void add() {
    	String companyId = AuthUtils.getLoginUser().getCompanyId();
 		JbootRedis redis = Jboot.me().getRedis();
 		String brandId = redis.get(companyId + "_brandId");
    	List<Agent> agentList = agentService.findAll(companyId, brandId); //代理商
    	List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);//代理商级别
    	setAttr("agentList", agentList);
    	setAttr("agentTypeList", agentTypeList);
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        AgentInvite data = getBean(AgentInvite.class, "data");
        data.setCreateDate(new Date());
        data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
        data.setDelFlag("0");
        String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrand(brandId);
		}
        if (!agentInviteService.save(data)) {
            throw new BusinessException("保存失败");
        }
        agentInviteService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        String companyId = AuthUtils.getLoginUser().getCompanyId();
 		JbootRedis redis = Jboot.me().getRedis();
 		String brandId = redis.get(companyId + "_brandId");
        AgentInvite data = agentInviteService.findById(id);
        List<Agent> agentList = agentService.findAll(companyId, brandId); //代理商
    	List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);//代理商级别
    	setAttr("agentList", agentList);
    	setAttr("agentTypeList", agentTypeList);
        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        AgentInvite data = getBean(AgentInvite.class, "data");
        data.setUpdateDate(new Date());
        data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
        if (agentInviteService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!agentInviteService.update(data)) {
            throw new BusinessException("修改失败");
        }
        agentInviteService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
    	String id = getPara("id");
    	AgentInvite agentInvite = new AgentInvite();
    	agentInvite.setId(id);
    	agentInvite.setDelFlag("1");
        if (!agentInviteService.update(agentInvite)) {
            throw new BusinessException("删除失败");
        }
        agentInviteService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}