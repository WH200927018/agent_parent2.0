package com.tipu.agent.admin.handler;

import com.jfinal.handler.Handler;
import com.jfinal.kit.StrKit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author:骆宏图
 * @CreateDate: 16:42 2018/4/17.
 * @Description:
 * @Modified By:
 */
public class ResourcePathHandler extends Handler {
    private String resourcePathName;

    public ResourcePathHandler() {
        resourcePathName = "RESOURCE_PATH";
    }

    public ResourcePathHandler(String resourcePathName) {
        if (StrKit.isBlank(resourcePathName)) {
            throw new IllegalArgumentException("contextPathName can not be blank.");
        }
        this.resourcePathName = resourcePathName;
    }

    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
//        request.setAttribute(resourcePathName, InitConst.getResBaseUrl());
        next.handle(target, request, response, isHandled);
    }
}
