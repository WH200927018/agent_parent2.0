package com.tipu.agent.admin.controller.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.util.CommonUtils;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentPriceService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.api.ProductImagesService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.api.SkuService;
import com.tipu.agent.front.service.api.SkuSpecItemService;
import com.tipu.agent.front.service.api.SpecGroupService;
import com.tipu.agent.front.service.api.SpecItemService;
import com.tipu.agent.front.service.api.SpecService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBanks;
import com.tipu.agent.front.service.entity.model.AgentPrice;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.Product;
import com.tipu.agent.front.service.entity.model.ProductImages;
import com.tipu.agent.front.service.entity.model.ProductType;
import com.tipu.agent.front.service.entity.model.Sku;
import com.tipu.agent.front.service.entity.model.SkuSpecItem;
import com.tipu.agent.front.service.entity.model.Spec;
import com.tipu.agent.front.service.entity.model.SpecGroup;
import com.tipu.agent.front.service.entity.model.SpecItem;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Product管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/product")
public class ProductController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductImagesService productImagesService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SpecService specService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SpecGroupService specGroupService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SpecItemService specItemService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SkuService skuService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SkuSpecItemService skuSpecItemService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentPriceService agentPriceService;

	/**
	 * index
	 */
	public void index() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<ProductType> productTypeList = productTypeService.findAll(companyId, brandId);
		setAttr("productTypeList", productTypeList).render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		Product data = new Product();
		if (StrKit.notBlank(getPara("name"))) {
			data.setName(getPara("name"));
		}
		if (StrKit.notBlank(getPara("sn"))) {
			data.setSn(getPara("sn"));
		}
		if (getParaToInt("isMarketable") != null) {
			data.setIsMarketable(getParaToInt("isMarketable"));
		}
		if (StrKit.notBlank(getPara("productType"))) {
			data.setProductType(getPara("productType"));
		}
		if (StrKit.notBlank(getPara("productBrand"))) {
			data.setProductBrand(getPara("productBrand"));
		}
		if (StrKit.notBlank(getPara("productType"))) {
			data.setProductType(getPara("productType"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<Product> dataPage = productService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Product>(dataPage));
	}

	/**
	 * 查看icon
	 */
	public void findImg() {
		String id = getPara("id");
		Product data = productService.findById(id);
		setAttr("data", data).render("findImg.html");
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<ProductType> productTypeList = productTypeService.findAll(companyId, brandId);
		List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);
		List<Spec> specList = specService.findAll(companyId, brandId);// 商品规格
		setAttr("specSice", specList.size()).setAttr("agentTypeList", agentTypeList);
		setAttr("productTypeList", productTypeList).render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Product data = getBean(Product.class, "data");
		ProductImages images = getBean(ProductImages.class, "data");
		Sku sku = getBean(Sku.class, "sku");
		SkuSpecItem skuSpecItem = getBean(SkuSpecItem.class, "skuSpecItem");
		AgentPrice agentPrice = getBean(AgentPrice.class, "agentPrice");
		if (StrKit.isBlank(agentPrice.getAgencyPrice())) {
			throw new BusinessException("会员价格不能为空,请先添加代理商级别!");
		}
		Product lastCodeEntity = productService.findLastCode();
		int lastCode = 0;
		if (lastCodeEntity != null) {
			lastCode = lastCodeEntity.getCode();
			++lastCode;
		}
		String uuid = StrKit.getRandomUUID();
		data.setCode(lastCode);
		data.setSn(CommonUtils.getRandomInt(4));
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		data.setId(uuid);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		if (!productService.save(data)) {
			throw new BusinessException("保存失败");
		}
		// 新增轮播图片
		String url = images.getUrl();
		if (StrKit.notBlank(url)) {
			String[] urls = url.split(",");
			for (int i = 0; i < urls.length; i++) {
				String newUrl = urls[i];
				if (StrKit.notBlank(newUrl)) {
					ProductImages newimages = new ProductImages();
					newimages.setUrl(newUrl);
					newimages.setProductId(uuid);
					newimages.setCreateDate(new Date());
					newimages.setCreateBy(AuthUtils.getLoginUser().getId().toString());
					newimages.setDelFlag("0");
					if (!productImagesService.save(newimages)) {
						throw new BusinessException("保存失败");
					}
					productImagesService.refreshCache();
				}
			}
		}
		// 生成sku
		String skuName = sku.getName();
		String skuImage = sku.getImage();
		String skuKeyword = sku.getKeywords();
		String skuPoint = getPara("skuPoints");
		String skuPrice = getPara("skuPrices");
		String skuStock = getPara("skuStocks");
		String agencyPrice = agentPrice.getAgencyPrice();
		String skuSpecItemId = skuSpecItem.getSpecItemId();
		if (StrKit.notBlank(agencyPrice) && StrKit.notBlank(skuSpecItemId) && StrKit.notBlank(skuName)
				&& StrKit.notBlank(skuImage) && StrKit.notBlank(skuKeyword) && StrKit.notBlank(skuPoint)
				&& StrKit.notBlank(skuPrice) && StrKit.notBlank(skuStock)) {
			String[] skuNames = skuName.split(",");
			String[] skuImages = skuImage.split(",");
			String[] skuKeywords = skuKeyword.split(",");
			String[] skuPoints = skuPoint.split(",");
			String[] skuPrices = skuPrice.split(",");
			String[] skuStocks = skuStock.split(",");
			String[] skuSpecItemIds = skuSpecItemId.split("#");
			for (int i = 0; i < skuNames.length; i++) {
				if (StrKit.notBlank(skuSpecItemIds[i]) && StrKit.notBlank(skuNames[i]) && StrKit.notBlank(skuImages[i])
						&& StrKit.notBlank(skuKeywords[i]) && StrKit.notBlank(skuPoints[i])
						&& StrKit.notBlank(skuPrices[i]) && StrKit.notBlank(skuStocks[i])) {
					String skuId = StrKit.getRandomUUID();
					sku.setId(skuId);
					sku.setName(skuNames[i]);
					sku.setImage(skuImages[i]);
					sku.setVirtualStock(Integer.valueOf(skuKeywords[i]));
					sku.setPoint(Integer.parseInt(skuPoints[i]));
					sku.setPrice(new BigDecimal(skuPrices[i]));
					sku.setStock(Integer.parseInt(skuStocks[i]));
					sku.setSn(CommonUtils.getRandomInt(4));
					sku.setProductId(uuid);
					sku.setProductType(data.getProductType());
					sku.setCreateDate(new Date());
					sku.setCreateBy(AuthUtils.getLoginUser().getId().toString());
					sku.setDelFlag("0");
					sku.setCompanyId(companyId);
					sku.setBrandId(brandId);
					if (!skuService.save(sku)) {
						throw new BusinessException("保存失败");
					}

					// 生成代理商会员价
					String[] agencyPrices = agencyPrice.split(",");
					for (int k = 0; k < agencyPrices.length; k++) {
						if (StrKit.notBlank(agencyPrices[k])) {
							String[] agencyPricess = agencyPrices[k].split(":");
							String agentTypeId = agencyPricess[1];// 代理商等级id
							String price = agencyPricess[0];// 会员价
							if (StrKit.notBlank(agentTypeId) && StrKit.notBlank(price)) {
								agentPrice.setAgencyPrice(price);
								agentPrice.setSkuId(skuId);
								agentPrice.setTypeId(agentTypeId);
								agentPrice.setCreateDate(new Date());
								agentPrice.setCreateBy(AuthUtils.getLoginUser().getId().toString());
								agentPrice.setDelFlag("0");
								if (!agentPriceService.save(agentPrice)) {
									throw new BusinessException("保存失败");
								}
							}
						}

					}

					String[] skuSpecItemIdss = skuSpecItemIds[i].split(",");// 生成sku、spec、spec_item关联
					for (int j = 0; j < skuSpecItemIdss.length; j++) {
						if (StrKit.notBlank(skuSpecItemIdss[j])) {
							String[] skuSpecItemIdsss = skuSpecItemIdss[j].split(";");
							String specItemId = skuSpecItemIdsss[0];// 规格元素id
							String specId = skuSpecItemIdsss[1];// 规格id
							if (StrKit.notBlank(specItemId) && StrKit.notBlank(specId)) {
								skuSpecItem.setSkuId(skuId);
								skuSpecItem.setSpecId(specId);
								skuSpecItem.setSpecItemId(specItemId);
								skuSpecItem.setCreateDate(new Date());
								skuSpecItem.setCreateBy(AuthUtils.getLoginUser().getId().toString());
								skuSpecItem.setDelFlag("0");
								skuSpecItem.setCompanyId(companyId);
								skuSpecItem.setBrandId(brandId);
								if (!skuSpecItemService.save(skuSpecItem)) {
									throw new BusinessException("保存失败");
								}
							}
						}
					}
				}
			}

		} else {
			String skuId = StrKit.getRandomUUID();
			sku.setId(skuId);
			sku.setName(data.getName());
			sku.setImage(data.getImage());
			sku.setKeywords(data.getKeywords());
			if (data.getPoint() != null) {
				sku.setPoint(data.getPoint());
			}
			if (data.getPrice() != null) {
				sku.setPrice(data.getPrice());
			}
			if (data.getStock() != null) {
				sku.setStock(data.getStock());
				sku.setVirtualStock(data.getStock());
			}
			if (data.getSalses() != null) {
				sku.setSalses(data.getSalses());
			}
			sku.setCompanyId(companyId);
			sku.setBrandId(brandId);
			sku.setSn(CommonUtils.getRandomInt(4));
			sku.setProductId(uuid);
			sku.setCreateDate(new Date());
			sku.setCreateBy(AuthUtils.getLoginUser().getId().toString());
			sku.setDelFlag("0");
			if (!skuService.save(sku)) {
				throw new BusinessException("保存失败");
			}
			if (StrKit.notBlank(agencyPrice)) {
				// 生成代理商会员价
				String[] agencyPrices = agencyPrice.split(",");
				for (int k = 0; k < agencyPrices.length; k++) {
					if (StrKit.notBlank(agencyPrices[k])) {
						String[] agencyPricess = agencyPrices[k].split(":");
						String agentTypeId = agencyPricess[1];// 代理商等级id
						String price = agencyPricess[0];// 会员价
						if (StrKit.notBlank(agentTypeId) && StrKit.notBlank(price)) {
							agentPrice.setAgencyPrice(price);
							agentPrice.setSkuId(skuId);
							agentPrice.setTypeId(agentTypeId);
							agentPrice.setCreateDate(new Date());
							agentPrice.setCreateBy(AuthUtils.getLoginUser().getId().toString());
							agentPrice.setDelFlag("0");
							if (!agentPriceService.save(agentPrice)) {
								throw new BusinessException("保存失败");
							}
						}
					}

				}
			}
		}
		productService.refreshCache();
		skuSpecItemService.refreshCache();
		skuService.refreshCache();
		agentPriceService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		Product data = productService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<ProductType> productTypeList = productTypeService.findAll(companyId, brandId);
		ProductImages productImages = new ProductImages();
		productImages.setProductId(id);
		List<ProductImages> productImagesList = productImagesService.findList(productImages);
		String urls = "";
		if (productImagesList.size() > 0) {
			for (int i = 0; i < productImagesList.size(); i++) {
				if (i < productImagesList.size() - 1) {
					urls = urls + productImagesList.get(i).getUrl() + ",";
				} else {
					urls = urls + productImagesList.get(i).getUrl();
				}
			}
		}
		setAttr("urls", urls);
		setAttr("productTypeList", productTypeList).setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Product data = getBean(Product.class, "data");
		ProductImages images = getBean(ProductImages.class, "data");
		/*
		 * if (StrKit.isBlank(data.getIntroduction())) { throw new
		 * BusinessException("介绍不能为空！"); }
		 */
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (productService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		if (!productService.update(data)) {
			throw new BusinessException("修改失败");
		}
		productService.refreshCache();
		// 修改轮播图片
		ProductImages oldProductImages = new ProductImages();
		oldProductImages.setProductId(data.getId());
		List<ProductImages> productImagesList = productImagesService.findList(oldProductImages);
		List<String> oldUrlList = new ArrayList<String>(); // 旧轮播图片url集合
		List<String> newUrlList = new ArrayList<String>(); // 新轮播图片url集合
		List<String> addUrlList = new ArrayList<String>(); // 需要新增的轮播图片url集合
		List<String> deleteUrlList = new ArrayList<String>(); // 需要删除的轮播图片url集合
		if (productImagesList.size() > 0) {
			for (ProductImages productImages : productImagesList) {
				oldUrlList.add(productImages.getUrl());
				deleteUrlList.add(productImages.getUrl());
			}
		}
		String url = images.getUrl();
		if (StrKit.notBlank(url)) {
			String[] urls = url.split(",");
			for (int i = 0; i < urls.length; i++) {
				String newUrl = urls[i];
				if (StrKit.notBlank(newUrl)) {
					newUrlList.add(newUrl);
					addUrlList.add(newUrl);
				}
			}
		}
		addUrlList.removeAll(oldUrlList);// 需要新增的轮播图片url集合
		if (addUrlList.size() > 0) {
			for (String newUrl : addUrlList) {
				ProductImages newimages = new ProductImages();
				newimages.setUrl(newUrl);
				newimages.setProductId(data.getId());
				newimages.setCreateDate(new Date());
				newimages.setCreateBy(AuthUtils.getLoginUser().getId().toString());
				newimages.setDelFlag("0");
				if (!productImagesService.save(newimages)) {
					throw new BusinessException("修改失败");
				}
				productImagesService.refreshCache();
			}
		}
		deleteUrlList.removeAll(newUrlList);// 需要删除的轮播图片url集合
		if (deleteUrlList.size() > 0) {
			for (String newUrl : deleteUrlList) {
				ProductImages productImages = new ProductImages();
				productImages.setUrl(newUrl);
				productImages.setProductId(data.getId());
				List<ProductImages> oldProductImagesList = productImagesService.findList(productImages);
				if (productImagesList.size() > 0) {
					if (!productImagesService.delete(oldProductImagesList.get(0))) {
						throw new BusinessException("修改失败");
					}
					productImagesService.refreshCache();
				}

			}
		}

		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Product product = new Product();
		product.setId(id);
		product.setDelFlag("1");
		if (!productService.update(product)) {
			throw new BusinessException("删除失败");
		}
		productService.refreshCache();
		// 轮播图片删除
		ProductImages productImages = new ProductImages();
		productImages.setProductId(id);
		List<ProductImages> oldProductImagesList = productImagesService.findList(productImages);
		if (oldProductImagesList.size() > 0) {
			for (ProductImages oldProductImages : oldProductImagesList) {
				if (!productImagesService.delete(oldProductImages)) {
					throw new BusinessException("删除失败");
				}
			}
		}
		productImagesService.refreshCache();
		// sku删除
		Sku sku = new Sku();
		sku.setProductId(id);
		List<Sku> skuList = skuService.findList(sku);
		if (skuList.size() > 0) {
			for (Sku oldSku : skuList) {
				oldSku.setDelFlag("1");
				if (!skuService.update(oldSku)) {
					throw new BusinessException("删除失败");
				}
			}
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 查看轮播图
	 */
	public void findCarouselImg() {
		String id = getPara("id");
		Ret ret = new Ret();
		ProductImages productImages = new ProductImages();
		productImages.setProductId(id);
		List<ProductImages> productImagesList = productImagesService.findList(productImages);
		if (productImagesList.size() > 0) {
			ret.set("isOk", true);
			String json = "{\"title\": \"轮播图\",\"id\": 123,\"start\": 0,\"data\": [";
			for (int i = 0; i < productImagesList.size(); i++) {
				productImages = productImagesList.get(i);
				if (i < productImagesList.size() - 1) {
					json += "{\"alt\": \"图片" + (i + 1) + "\",\"pid\": " + i + ",\"src\": \"" + productImages.getUrl()
							+ "\",\"thumb\": \"\"},";
				} else {
					json += "{\"alt\": \"图片" + (i + 1) + "\",\"pid\": " + i + ",\"src\": \"" + productImages.getUrl()
							+ "\",\"thumb\": \"\"}";
				}
			}
			json += "]}";
			ret.set("imageJson", json);
		} else {
			ret.set("isOk", false);
		}
		renderJson(ret);
	}

	/**
	 * 查询商品规格
	 */
	@NotNullPara({ "productType" })
	public void findSpec() {
		String productType = getPara("productType");
		Ret ret = new Ret();
		SpecGroup specGroup = new SpecGroup();
		specGroup.setTypeId(productType);
		List<SpecGroup> specGroupList = specGroupService.findList(specGroup);// 商品规格组
		if (specGroupList.size() > 0) {
			Spec spec = new Spec();
			spec.setTypeId(productType);
			List<Spec> specList = specService.findList(spec);// 商品规格
			SpecItem specItem = new SpecItem();
			List<SpecItem> specItemList = specItemService.findList(specItem);// 商品规格属性元素
			if (specList.size() > 0 && specItemList.size() > 0) {
				for (int i = specList.size() - 1; i >= 0; i--) {// 去除没有规格属性的规格
					int count = 0;
					for (SpecItem item : specItemList) {
						if (item.getSpecId().equals(specList.get(i).getId())) {
							count++;
						}
					}
					if (count == 0) {
						specList.remove(i);
					}
				}
				System.out.println("specList.size()=" + specList.size());
				if (specList.size() > 0) {
					ret.set("specList", specList);
					ret.set("specItemList", specItemList);
					ret.set("isOk", true);
				} else {
					ret.set("isOk", false);
				}
			} else {
				ret.set("isOk", false);
			}
		} else {
			ret.set("isOk", false);
		}
		renderJson(ret);
	}

	/**
	 * 修改上架
	 */
	@NotNullPara({ "id" })
	public void isMarketable() {
		String id = getPara("id");
		Integer isMarketable = getParaToInt("isMarketable");
		Product product = new Product();
		product.setId(id);
		product.setIsMarketable(isMarketable);
		if (!productService.update(product)) {
			throw new BusinessException("修改失败");
		}
		productService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 修改库存页
	 */
	@NotNullPara({ "id" })
	public void showStock() {
		String id = getPara("id");
		setAttr("id", id).render("updateStock.html");
	}

	/**
	 * 修改库存提交
	 */
	@NotNullPara({ "id", "stock", "stockType" })
	public void updateStock() {
		String id = getPara("id");
		Integer stock = getParaToInt("stock");
		Integer stockType = getParaToInt("stockType");
		Product product = productService.findById(id);
		if (0 == product.getIsMarketable()) {
			throw new BusinessException("商品未上架，请先上架，再修改库存！");
		} else {
			if (productService.updateStock(stock, stockType, id) <= 0) {
				throw new BusinessException("商品库存不足！");
			}
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 规格商品页
	 */
	@NotNullPara({ "id" })
	public void findSku() {
		String productId = getPara("id");
		setAttr("productId", productId).render("findSku.html");
	}

	/**
	 * 规格商品表格数据
	 */
	@NotNullPara({ "productId" })
	public void skuTableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		String productId = getPara("productId");
		Sku data = new Sku();
		if (StrKit.notBlank(getPara("name"))) {
			data.setName(getPara("name"));
		}
		if (StrKit.notBlank(getPara("sn"))) {
			data.setSn(getPara("sn"));
		}
		if (StrKit.notBlank(productId)) {
			data.setProductId(productId);
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<Sku> dataPage = skuService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Sku>(dataPage));
	}

	/**
	 * sku修改库存页
	 */
	@NotNullPara({ "id", "productId" })
	public void showSkuStock() {
		String id = getPara("id");
		String productId = getPara("productId");
		setAttr("id", id).setAttr("productId", productId).render("updateSkuStock.html");
	}

	/**
	 * sku修改库存提交
	 */
	@NotNullPara({ "id", "stock", "stockType", "productId" })
	public void updateSkuStock() {
		String id = getPara("id");
		String productId = getPara("productId");
		Integer stock = getParaToInt("stock");
		Integer stockType = getParaToInt("stockType");
		Product product = productService.findById(productId);
		if (0 == product.getIsMarketable()) {
			throw new BusinessException("商品未上架，请先上架，再修改库存！");
		} else {
			if (skuService.updateStock(stock, stockType, id) <= 0) {
				throw new BusinessException("商品库存不足！");
			}
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * sku修改云库存页
	 */
	@NotNullPara({ "id", "productId" })
	public void showVirtualStock() {
		String id = getPara("id");
		String productId = getPara("productId");
		setAttr("id", id).setAttr("productId", productId).render("updateVirtualStock.html");
	}

	/**
	 * sku修改云库存提交
	 */
	@NotNullPara({ "id", "virtual_stock", "stockType", "productId" })
	public void updateVirtualStock() {
		String id = getPara("id");
		String productId = getPara("productId");
		Integer virtual_stock = getParaToInt("virtual_stock");
		Integer stockType = getParaToInt("stockType");
		Product product = productService.findById(productId);
		if (0 == product.getIsMarketable()) {
			throw new BusinessException("商品未上架，请先上架，再修改库存！");
		} else {
			if (skuService.updateVirtuaStock(virtual_stock, stockType, id) <= 0) {
				throw new BusinessException("商品库存不足！");
			}
		}
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * updateSku
	 */
	@NotNullPara({ "id" })
	public void updateSku() {
		String id = getPara("id");
		Sku data = skuService.findById(id);
		AgentPrice agentPrice = new AgentPrice();
		agentPrice.setSkuId(id);
		List<AgentPrice> agentPriceList = agentPriceService.findList(agentPrice);
		setAttr("data", data).setAttr("agentPriceList", agentPriceList).render("updateSku.html");
	}

	/**
	 * sku修改提交
	 */
	public void postUpdateSku() {
		Sku data = getBean(Sku.class, "data");
		AgentPrice agentPrice = getBean(AgentPrice.class, "agentPrice");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (skuService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}
		if (!skuService.update(data)) {
			throw new BusinessException("修改失败");
		}
		skuService.refreshCache();

		// 修改代理商会员价
		String agencyPrice = agentPrice.getAgencyPrice();
		if (StrKit.notBlank(agencyPrice)) {
			String[] agencyPrices = agencyPrice.split(",");
			for (int k = 0; k < agencyPrices.length; k++) {
				if (StrKit.notBlank(agencyPrices[k])) {
					String[] agencyPricess = agencyPrices[k].split(":");
					String agencyPriceId = agencyPricess[1];// 代理商会员价id
					String price = agencyPricess[0];// 会员价
					if (StrKit.notBlank(agencyPriceId) && StrKit.notBlank(price)) {
						agentPrice.setId(agencyPriceId);
						agentPrice.setAgencyPrice(price);
						agentPrice.setUpdateDate(new Date());
						agentPrice.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
						if (!agentPriceService.update(agentPrice)) {
							throw new BusinessException("修改失败");
						}
					}
				}
			}
		}
		agentPriceService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 查看sku图片
	 */
	public void findSkuImg() {
		String id = getPara("id");
		Sku data = skuService.findById(id);
		setAttr("data", data).render("findImg.html");
	}

}