package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentLoginLogService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentLoginLog;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AgentLoginLog管理
 * 
 * @author think
 *
 */
@RequestMapping("/base/agent_login_log")
public class AgentLoginLogController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentLoginLogService agentLoginLogService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	/**
	 * index
	 */
	public void index() {
		List<Agent> agentList = agentService.findAll();
		setAttr("agentList", agentList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AgentLoginLog data = new AgentLoginLog();
		 if(StrKit.notBlank(getPara("agent"))){
	        	data.setAgent(getPara("agent"));
	        }
		Page<AgentLoginLog> dataPage = agentLoginLogService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentLoginLog>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		List<Agent> agentList = agentService.findAll();
		setAttr("agentList", agentList).render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		AgentLoginLog data = getBean(AgentLoginLog.class, "data");
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		if (!agentLoginLogService.save(data)) {
			throw new BusinessException("保存失败");
		}
		agentLoginLogService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		AgentLoginLog data = agentLoginLogService.findById(id);
		List<Agent> agentList = agentService.findAll();
		setAttr("list", agentList);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		AgentLoginLog data = getBean(AgentLoginLog.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (agentLoginLogService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!agentLoginLogService.update(data)) {
			throw new BusinessException("修改失败");
		}
		agentLoginLogService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		AgentLoginLog agentLoginLog = new AgentLoginLog();
		agentLoginLog.setId(id);
		agentLoginLog.setDelFlag("1");
		if (!agentLoginLogService.update(agentLoginLog)) {
			throw new BusinessException("删除失败");
		}
		agentLoginLogService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

}