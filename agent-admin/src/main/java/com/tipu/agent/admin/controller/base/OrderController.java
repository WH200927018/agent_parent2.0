package com.tipu.agent.admin.controller.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.google.common.collect.Maps;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.controller.common.ExcelUtilController;
import com.tipu.agent.admin.controller.common.OrderNumberGenerator;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentAddressService;
import com.tipu.agent.front.service.api.AgentBanksService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.OrderProductService;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.api.SkuService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBanks;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.Product;
import com.tipu.agent.front.service.entity.model.ProductType;
import com.tipu.agent.front.service.entity.model.Sku;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Order管理
 * 
 * @author think
 *
 */
@RequestMapping("/base/order")
public class OrderController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrdersService orderService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrderProductService orderProductService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBanksService agentBanksService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAddressService agentAddressService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductService productService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private ProductTypeService productTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private SkuService skuService;
	
	private static final String ORDERS = PathKit.getWebRootPath() + File.separator + "export\\orders" + File.separator ;
	

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		Orders data = new Orders();
		if (StrKit.notBlank(getPara("agent"))) {
			data.setAgentId(getPara("agent"));
		}
		if (StrKit.notBlank(getPara("id"))) {
			data.setId(getPara("id"));
		}
		if (StrKit.notBlank(getPara("state"))) {
			data.setState(getPara("state"));
		}
		if (StrKit.notBlank(getPara("orderType"))) {
			data.setOrderType(getPara("orderType"));
		}
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<Orders> dataPage = orderService.findPage(data, pageNumber, pageSize);

		renderJson(new DataTable<Orders>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		List<ProductType> productTypeList = productTypeService.findAll(companyId, brandId);
		setAttr("agentList", agentList).setAttr("productTypeList", productTypeList);
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		Orders data = getBean(Orders.class, "data");
		String uuid = OrderNumberGenerator.get();
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		data.setId(uuid);
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		if (!orderService.save(data)) {
			throw new BusinessException("保存失败");
		}
		orderService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		List<AgentBanks> agentBanksList = agentBanksService.findAll(companyId, brandId);
		setAttr("agentList", agentList).setAttr("agentBanksList", agentBanksList);
		Orders data = orderService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		Orders data = getBean(Orders.class, "data");
		Orders oldOrder = orderService.findById(data.getId());
		if (oldOrder == null) {
			throw new BusinessException("数据不存在");
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!orderService.update(data)) {
			throw new BusinessException("修改失败");
		}
		orderService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Orders Order = new Orders();
		Order.setId(id);
		Order.setDelFlag("1");
		if (!orderService.update(Order)) {
			throw new BusinessException("删除失败");
		}
		orderService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 修改提交
	 */
	@NotNullPara({ "id", "state" })
	public void stateUpdate() {
		String id = getPara("id");
		String state = getPara("state");
		Orders order = orderService.findById(id);
		if (order == null) {
			throw new BusinessException("数据不存在");
		}
		Orders data = new Orders();
		data.setOrderGroupId(order.getOrderGroupId());
		List<Orders> orderList = orderService.findList(data);
		if (orderList.size() > 0) {//修改订单组的所有订单
			for (Orders oldData : orderList) {
				oldData.setState(state);
				oldData.setUpdateDate(new Date());
				oldData.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
				if (!orderService.update(oldData)) {
					throw new BusinessException("修改失败");
				}
			}
		}
		orderService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
	
	/**
	 * 导出订单界面
	 */
	public void exportOrdersExcel(){
		render("exportOrdersExcel.html");
	}

	/**
	 * 导出订单操作
	 * 		用此方法导出的顺序不会根据代码的顺序对应
	 * 		解决办法，sql中不要用*查询，写出查询的字段 根绝查询字段的顺序对应
	 */
	public void exportOrdersOperation(){
		String startDate = getPara("startDate");
		String endDate = getPara("endDate");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if(StrKit.notBlank(startDate) && StrKit.notBlank(endDate)&& StrKit.notBlank(companyId)&& StrKit.notBlank(brandId)){
			String sql = "select id,agent_id,"
						+"receive_name,"
						+"receiver_phone,"
						+"receive_address,"
						+"state,"
						+"buy_sum,"
						+"buy_nums,"
						+"brand_name,"
						+"logistica_info,"
						+"product_unit,"
						+"order_creater_id,"
						+"handle_agent_id,"
						+"create_date from `orders`"
						+" where order_type = 1 and del_flag =0 and product_type = 1 AND company_id = '"+companyId+"' AND brand_id = '"+brandId+"'and create_date >='"+ startDate + "  00:00:00' and create_date<='" +endDate+" 23:59:59'";
			Map<String, String> titleData = Maps.newLinkedHashMap();//标题，后面用到
			titleData.put("id", "订单编号");
			titleData.put("agent_id", "创建者名称");
			titleData.put("receive_name", "收货人姓名");
			titleData.put("receiver_phone", "收货人电话");
			titleData.put("receive_address", "收货人地址");
			titleData.put("state", "订单状态");
			titleData.put("buy_sum", "购买金额");
			titleData.put("buy_nums", "购买个数");
			titleData.put("brand_name", "商品");
			titleData.put("logistica_info", "物流信息");
			titleData.put("product_unit", "商品单位");
			titleData.put("order_creater_id", "发起人订单");
			titleData.put("handle_agent_id", "处理人订单");
			titleData.put("create_date", "订单创建时间");
			File file = new File(getTitle(startDate,endDate));
			file = saveFile(titleData, sql, file,startDate+"至"+endDate+"订单报表");
			this.renderFile(file);
		}
	}
	
	public File saveFile(Map<String, String> headData, String sql, File file,String title) {
		// 创建工作薄
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		// sheet:一张表的简称
		// row:表里的行
		// 创建工作薄中的工作表
		HSSFSheet hssfSheet = hssfWorkbook.createSheet(title);
		// 创建行
		HSSFRow row = hssfSheet.createRow(0);
		// 创建单元格，设置表头 创建列
		HSSFCell cell = null;
		// 初始化索引
		int rowIndex = 0;
		int cellIndex = 0;

		// 创建标题行
		row = hssfSheet.createRow(rowIndex);
		rowIndex++;
		// 遍历标题
		for (String h : headData.keySet()) {
			//创建列
			cell = row.createCell(cellIndex);
			//索引递增
			cellIndex++;
			//逐列插入标题
			cell.setCellValue(headData.get(h));
		}
		// 得到所有记录 行：列
		List<Record> list = orderService.findRecord(sql);
		Record record = null;
		
		String orderState = ""; //订单状态
		String productUnit = ""; //商品单位
		if (list != null) {
			// 获取所有的记录 有多少条记录就创建多少行
			for (int i = 0; i < list.size(); i++) {
				row = hssfSheet.createRow(rowIndex);
				// 得到所有的行 一个record就代表 一行
				record = list.get(i);
				//下一行索引
				rowIndex++;
				//刷新新行索引
				cellIndex = 0;
				// 在有所有的记录基础之上，便利传入进来的表头,再创建N行
				for (String h : headData.keySet()) {
					cell = row.createCell(cellIndex);
					cellIndex++;
					//按照每条记录匹配数据
					if(h.equals("state")){
						if(record.get(h).toString().equals("0")){
							orderState = "待我处理";
						}else if(record.get(h).toString().equals("1")){
							orderState = "待我发货";
						}else if(record.get(h).toString().equals("2")){
							orderState = "待总部发货";
						}else if(record.get(h).toString().equals("3")){
							orderState = "待收货";
						}else if(record.get(h).toString().equals("4")){
							orderState = "结束";
						}
						cell.setCellValue( StrKit.isBlank(orderState) ? "" : orderState);
					}else if(h.equals("product_unit")){
						if(record.get(h).toString().equals("0")){
							productUnit = "个";
						}else if(record.get(h).toString().equals("1")){
							productUnit = "箱";
						}
						cell.setCellValue( StrKit.isBlank(productUnit) ? "" : productUnit);
					}else if(h.equals("order_creater_id")){//发起人订单
						String agentName = 	getAgent(record.get(h).toString());
						cell.setCellValue( StrKit.isBlank(agentName) ? "" : agentName);
					}else if(h.equals("handle_agent_id")){//处理人订单
						String agentName = 	getAgent(record.get(h).toString());
						cell.setCellValue( StrKit.isBlank(agentName) ? "" : agentName);
					}else if(h.equals("agent_id")){//订单创建人人订单
						String agentName = 	getAgent(record.get(h).toString());
						cell.setCellValue( StrKit.isBlank(agentName) ? "" : agentName);
					}else{
						cell.setCellValue(record.get(h) == null ? "" : record.get(h).toString());
					}
				}
			}
		}
		try {
			FileOutputStream fileOutputStreane = new FileOutputStream(file);
			hssfWorkbook.write(fileOutputStreane);
			fileOutputStreane.flush();
			fileOutputStreane.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	
	public String getTitle(String startdate,String enddate){
	     String title=ORDERS+startdate+"至"+enddate+"订单报表.xls";
	     File temp = new File(ORDERS);
	     if(temp.exists()==false) {
			 temp.mkdirs();				
        }
	     return title;
	}
	
	
	public String getAgent(String id){
		if(id.equals("0")){
			return "总公司";
		}else{
			Agent agent = agentService.findById(id);
			return agent.getName();
		}
	}
	
	/**
	 * 统计订单界面
	 */
	public void countOrders(){
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<ProductType> productTypeList = productTypeService.findAll(companyId,brandId);
		List<Product> productList = productService.findAll(companyId,brandId);
		setAttr("productList", productList).setAttr("productTypeList", productTypeList);
		render("countOrders.html");
	}
	
	/**
	 * 统计订单操作
	 */
	public void countOrdersOperation(){
		String startDate = getPara("startDate");//开始时间
		String endDate = getPara("endDate"); //结束时间
		String productType = getPara("productType"); //商品分类
		String product = getPara("product"); //商品
		Map<String, String> titleData = Maps.newLinkedHashMap();
		//商品分类
		String productId = "";
		Product productEntity = new Product();
		Sku sku = new Sku();
		String productIds = "";
		String productTypeName = null;
		if(StrKit.notBlank(productType)){
			productTypeName = productTypeService.findById(productType).getName();
			productEntity.setProductType(productType);
			//通过商品分类找到商品
			List<Product> productList = productService.findList(productEntity);
			for (Product p : productList) {
				productId += p.getId() + ",";//一个商品分类里 可能会有2种 3种。。。商品，把商品用逗号分割
			}
			String[] productIdSplit =  productId.split(",");//把商品分割后 去sku里查 sku的id 然后用逗号拼接
			for (String s : productIdSplit) {
				sku.setProductId(s);
				List<Sku> skuList = skuService.findList(sku);
				for (Sku sku2 : skuList) {
					productIds += sku2.getId() +",";
				}
			}
		}
		//通过商品id找到 sku里的所有 sku 然后把 skuId（这里是productIds）拼接起来
		//商品
		String productName = null;
		if(StrKit.notBlank(product)){
			productName = productService.findById(product).getName();
			sku = new Sku();
			sku.setProductId(product);
			List<Sku> skuList = skuService.findList(sku);
			for (Sku sku2 : skuList) {
				productIds += sku2.getId() +",";
			}
		}
		//状态
		String state = "-1,0,1,2,3,4";
		String[] stateSplit = state.split(",");
		for (String string : stateSplit) {
			// -1 拒绝 0 待处理  1审核通过 2 待发货 3待收货 4结束
			BigDecimal sum = orderService.countOrdersByState(productIds, string, startDate, endDate);
			if(string.equals("-1")){
				titleData.put(sum.toString(),"拒绝");//-1
			}else if(string.equals("0")){
				titleData.put(sum.toString(),"待处理");//0
			}else if(string.equals("1")){
				titleData.put(sum.toString(),"审核通过");//1
			}else if(string.equals("2")){
				titleData.put(sum.toString(),"待发货");//2
			}else if(string.equals("3")){
				titleData.put(sum.toString(),"待收货");//3
			}else if(string.equals("4")){
				titleData.put(sum.toString(),"结束");//4
			}
		}
		ExcelUtilController export = new ExcelUtilController();
		File file = export.export(productTypeName,productName,titleData,startDate,endDate);
		this.renderFile(file);
	}

}