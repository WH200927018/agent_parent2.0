package com.tipu.agent.admin.controller.base;

import java.util.Date;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AdFieldService;
import com.tipu.agent.front.service.entity.model.AdField;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AdField管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/ad_field")
public class AdFieldController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AdFieldService adFieldService;

	/**
	 * index
	 */
	public void index() {
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AdField data = new AdField();
		data.setName(getPara("name"));
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
		Page<AdField> dataPage = adFieldService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AdField>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		AdField data = getBean(AdField.class, "data");
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		if (!adFieldService.save(data)) {
			throw new BusinessException("保存失败");
		}
		adFieldService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		AdField data = adFieldService.findById(id);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		AdField data = getBean(AdField.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (adFieldService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!adFieldService.update(data)) {
			throw new BusinessException("修改失败");
		}
		adFieldService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		AdField adField = new AdField();
		adField.setId(id);
		adField.setDelFlag("1");
		if (!adFieldService.update(adField)) {
			throw new BusinessException("删除失败");
		}
		adFieldService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

}