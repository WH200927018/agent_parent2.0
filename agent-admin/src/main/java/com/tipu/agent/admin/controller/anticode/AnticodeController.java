package com.tipu.agent.admin.controller.anticode;

import java.util.ArrayList;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.anticode.service.api.AnticodeConfigService;
import com.tipu.agent.anticode.service.api.AnticodeService;
import com.tipu.agent.anticode.service.entity.model.Anticode;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Anticode管理
 * @author think
 *
 */
@RequestMapping("/anticode/anticode")
public class AnticodeController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AnticodeService anticodeService;
	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AnticodeConfigService anticodeConfigService;
	
	/**
     * index
     */
    public void index() {
    	setAttr("configs", anticodeConfigService.findAll());
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);
        
        String configNo = getPara("configNo");
        
        Anticode data = new Anticode();
        Page<Anticode> dataPage = null;
        if(StrKit.notBlank(configNo)) {
        	dataPage = anticodeService.findPage(data, configNo, pageNumber, pageSize);
        } else {
        	dataPage = new Page<Anticode>(new ArrayList<Anticode>(), pageNumber, pageSize, 0 ,0);
        }
        renderJson(new DataTable<Anticode>(dataPage));
    }

    /**
     * add
     */
    public void add() {
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        Anticode data = getBean(Anticode.class, "data");

        if (!anticodeService.save(data)) {
            throw new BusinessException("保存失败");
        }
        anticodeService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        Anticode data = anticodeService.findById(id);

        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        Anticode data = getBean(Anticode.class, "data");

        if (anticodeService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!anticodeService.update(data)) {
            throw new BusinessException("修改失败");
        }
        anticodeService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
        String id = getPara("id");
        if (!anticodeService.deleteById(id)) {
            throw new BusinessException("删除失败");
        }
        anticodeService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}