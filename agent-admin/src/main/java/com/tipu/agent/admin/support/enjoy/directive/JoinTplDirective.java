package com.tipu.agent.admin.support.enjoy.directive;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.anticode.service.api.AntiRuleService;
import com.tipu.agent.anticode.service.api.AnticodeConfigService;
import com.tipu.agent.anticode.service.entity.model.AntiRule;
import com.tipu.agent.anticode.service.entity.model.AnticodeConfig;
import com.tipu.agent.front.service.api.AdFieldService;
import com.tipu.agent.front.service.api.AdItemService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.api.ProductTypeService;
import com.tipu.agent.front.service.entity.model.AdField;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Product;
import com.tipu.agent.front.service.entity.model.ProductType;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

/**
 * 下拉Option指令
 */
@JFinalDirective("joinTpl")
public class JoinTplDirective extends JbootDirectiveBase {

    @JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
    private ProductService productService;
    @JbootrpcService
    private BrandService brandService;
    @JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
    private AgentService agentService;
    @JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
    private AntiRuleService antiRuleService;
    @JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
    private AnticodeConfigService anticodeConfigService;
    @JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
    private AgentTypeService agentTypeService;
    @JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
    private AdFieldService adFieldService;
    @JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
    private ProductTypeService productTypeService;

    /** 数据字典类型编码 */
    private String table;
    /** 属性名默认status，laytpl 里 d.attrName */
    private String attrName;

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        if (exprList.length() > 2) {
            throw new ParseException("Wrong number parameter of #joinTpl directive, two parameters allowed at most", location);
        }

        table = getParam(0, scope);
        if (StrKit.isBlank(table)) {
            throw new ParseException("table is null", location);
        }

        if (exprList.length() > 1) {
            attrName = getParam(1, "", scope);
        }
        
        if(table.equals("product")) {
        	List<Product> list = productService.findAll();
            write(writer, "<div>");
            for (Product data : list) {
                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
                write(writer, data.getName());
                write(writer, "{{#  } }}");
            }
        } else if(table.equals("brand")) {
        	List<Brand> list = brandService.findAll();
            write(writer, "<div>");
            for (Brand data : list) {
                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
                write(writer, data.getName());
                write(writer, "{{#  } }}");
            }
        } else if(table.equals("anti_rule")) {
        	List<AntiRule> list = antiRuleService.findAll();
            write(writer, "<div>");
            for (AntiRule data : list) {
                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
                write(writer, data.getName());
                write(writer, "{{#  } }}");
            }
        } else if(table.equals("anticode_config")) {
        	List<AnticodeConfig> list = anticodeConfigService.findAll();
            write(writer, "<div>");
            for (AnticodeConfig data : list) {
            	if(StrKit.notBlank(data.getConfigNo())) {
	                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
	                write(writer, data.getConfigNo());
	                write(writer, "{{#  } }}");
            	}
            }
        } else if(table.equals("agent")) {
        	List<Agent> list = agentService.findAll();
            write(writer, "<div>");
            for (Agent data : list) {
                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
                write(writer, data.getName());
                write(writer, "{{#  } }}");
            }
        } else if(table.equals("agent_type")) {
        	List<AgentType> list = agentTypeService.findAll();
            write(writer, "<div>");
            for (AgentType data : list) {
                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
                write(writer, data.getName());
                write(writer, "{{#  } }}");
            }
        }else if(table.equals("ad_field")) {
        	List<AdField> list = adFieldService.findAll();
            write(writer, "<div>");
            for (AdField data : list) {
                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
                write(writer, data.getName());
                write(writer, "{{#  } }}");
            }
        }else if(table.equals("product_type")) {
        	List<ProductType> list = productTypeService.findAll();
            write(writer, "<div>");
            for (ProductType data : list) {
                write(writer, "{{#  if(d." + attrName + " == \\'" + data.getId() + "\\') { }}");
                write(writer, data.getName());
                write(writer, "{{#  } }}");
            }
        }
        
        
        write(writer, "</div>");
    }

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

    }
}
