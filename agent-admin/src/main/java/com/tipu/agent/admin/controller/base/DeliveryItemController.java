package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.DeliveryItemService;
import com.tipu.agent.front.service.api.DeliveryService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.front.service.entity.model.Delivery;
import com.tipu.agent.front.service.entity.model.DeliveryItem;
import com.tipu.agent.front.service.entity.model.Product;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * DeliveryItem管理
 * @author think
 *
 */
@RequestMapping("/base/delivery_item")
public class DeliveryItemController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private DeliveryItemService deliveryItemService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private DeliveryService deliveryService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private ProductService productService;
	
	/**
     * index
     */
    public void index() {
  		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<Delivery> deliveryList = deliveryService.findAll(companyId,brandId);
    	List<Product> productList = productService.findAll(companyId,brandId);
    	setAttr("deliveryList",deliveryList);
    	setAttr("productList",productList);
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        DeliveryItem data = new DeliveryItem();
        if (StrKit.notBlank(getPara("delivery"))) {
        	data.setDelivery(getPara("delivery"));
  		}
  		if (StrKit.notBlank(getPara("product"))) {
  			data.setProduct(getPara("product"));
  		}
  		if (StrKit.notBlank(getPara("antiCode"))) {
  			data.setAntiCode(getPara("antiCode"));
  		}
  		if (StrKit.notBlank(getPara("logisticsCode"))) {
  			data.setLogisticsCode(getPara("logisticsCode"));
  		}
  		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
        Page<DeliveryItem> dataPage = deliveryItemService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<DeliveryItem>(dataPage));
    }

    /**
     * add
     */
    public void add() {
  		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<Delivery> deliveryList = deliveryService.findAll(companyId,brandId);
    	List<Product> productList = productService.findAll(companyId,brandId);
    	setAttr("deliveryList",deliveryList);
    	setAttr("productList",productList);
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        DeliveryItem data = getBean(DeliveryItem.class, "data");
        data.setCreateDate(new Date());
        data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
        data.setDelFlag("0");
  		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
        if (!deliveryItemService.save(data)) {
            throw new BusinessException("保存失败");
        }
        deliveryItemService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
    	String id = getPara("id");
        DeliveryItem data = deliveryItemService.findById(id);
  		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<Delivery> deliveryList = deliveryService.findAll(companyId,brandId);
    	List<Product> productList = productService.findAll(companyId,brandId);
    	setAttr("deliveryList",deliveryList);
    	setAttr("productList",productList);
    	
        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        DeliveryItem data = getBean(DeliveryItem.class, "data");
        data.setUpdateDate(new Date());
        data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
        if (deliveryItemService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!deliveryItemService.update(data)) {
            throw new BusinessException("修改失败");
        }
        deliveryItemService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
    	String id = getPara("id");
    	DeliveryItem deliveryItem = new DeliveryItem();
    	deliveryItem.setId(id);
    	deliveryItem.setDelFlag("1");
        if (!deliveryItemService.update(deliveryItem)) {
            throw new BusinessException("删除失败");
        }
        deliveryItemService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}