package com.tipu.agent.admin;

import io.jboot.Jboot;

/**
 * 服务 启动入口
 * 
 * @author Rlax
 *
 */
public class Application {
    public static void main(String [] args){
        Jboot.run(args);
    }
}
