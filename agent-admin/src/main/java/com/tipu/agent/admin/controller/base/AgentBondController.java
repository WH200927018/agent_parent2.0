package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentBondService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentBond;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 代理商保证金管理
 * @author hulin
 *
 */
@RequestMapping("/base/agent_bond")
public class AgentBondController extends BaseController {

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBondService agentBondService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	/**
	 * index
	 */
	public void index() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AgentBond data = new AgentBond();
		if(StrKit.notBlank(getPara("agent"))){
	    	data.setAgent(getPara("agent"));
	    }
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
		Page<AgentBond> dataPage = agentBondService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentBond>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("agentList", agentList).render("add.html");
	}

	/**
	 * 保存提交
	 */
	public void postAdd() {
		AgentBond data = getBean(AgentBond.class, "data");
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
		if (!agentBondService.save(data)) {
			throw new BusinessException("保存失败");
		}
		agentBondService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		AgentBond data = agentBondService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<Agent> agentList = agentService.findAll(companyId, brandId);
		setAttr("list", agentList);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 */
	public void postUpdate() {
		AgentBond data = getBean(AgentBond.class, "data");
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (agentBondService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}

		if (!agentBondService.update(data)) {
			throw new BusinessException("修改失败");
		}
		agentBondService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		AgentBond agentBond = new AgentBond();
		agentBond.setId(id);
		agentBond.setDelFlag("1");
		if (!agentBondService.update(agentBond)) {
			throw new BusinessException("删除失败");
		}
		agentBondService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
	
	/**
     * 查看保证金凭证
     */
    public void findImg(){
    	String id = getPara("id");
    	AgentBond data = agentBondService.findById(id);
        setAttr("data", data).render("findImg.html");
    }

}