package com.tipu.agent.admin.controller.anticode;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.util.CommonUtils;
import com.tipu.agent.admin.base.util.DateUtils;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.anticode.service.api.AntiRuleRecordService;
import com.tipu.agent.anticode.service.api.AntiRuleService;
import com.tipu.agent.anticode.service.api.AnticodeConfigService;
import com.tipu.agent.anticode.service.api.AnticodeService;
import com.tipu.agent.anticode.service.codegen.api.AnticodeGenService;
import com.tipu.agent.anticode.service.entity.model.AntiRule;
import com.tipu.agent.anticode.service.entity.model.AntiRuleRecord;
import com.tipu.agent.anticode.service.entity.model.AnticodeConfig;
import com.tipu.agent.admin.service.api.BrandService;
import com.tipu.agent.front.service.api.ProductService;
import com.tipu.agent.admin.service.entity.model.Brand;
import com.tipu.agent.front.service.entity.model.Product;

import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.utils.StringUtils;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AnticodeConfig管理
 * @author think
 *
 */
@RequestMapping("/anticode/anticode_config")
public class AnticodeConfigController extends BaseController {
	
	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AnticodeConfigService anticodeConfigService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AnticodeService anticodeService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODEGEN, version=ServiceConst.VERSION_1_0)
	private AnticodeGenService anticodeGenService;

	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AntiRuleService antiRuleService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_ANTICODE, version=ServiceConst.VERSION_1_0)
	private AntiRuleRecordService antiRuleRecordService;
	
	@JbootrpcService
	private BrandService brandService;

	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private ProductService productService;
	
//	public static final String NUMBERCHAR = "0123456789";
	
	/**
     * index
     */
    public void index() {
    	setAttr("brands", brandService.findAll()); // 品牌列表
    	setAttr("products", productService.findAll()); // 产品列表
    	setAttr("rules", antiRuleService.findAll()); // 规则列表
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);

        AnticodeConfig data = new AnticodeConfig();
        
        if(StrKit.notBlank(getPara("configNo"))) {
        	data.setConfigNo(getPara("configNo"));
        }
        
        if(StrKit.notBlank(getPara("brandId"))) {
        	data.setBrandId(getPara("brandId"));
        }
        
        if(StrKit.notBlank(getPara("productId"))) {
        	data.setProductId(getPara("productId"));
        }
        
        if(StrKit.notBlank(getPara("antiRuleId"))) {
        	data.setAntiRuleId(getPara("antiRuleId"));
        }

        Page<AnticodeConfig> dataPage = anticodeConfigService.findPage(data, pageNumber, pageSize);
        
        renderJson(new DataTable<AnticodeConfig>(dataPage));
    }

    /**
     * add
     */
    public void add() {
    	
    	setAttr("brands", brandService.findAll()); // 品牌列表
    	setAttr("products", productService.findAll()); // 产品列表
    	setAttr("rules", antiRuleService.findAll()); // 规则列表
    	
        render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        AnticodeConfig data = getBean(AnticodeConfig.class, "data");
        
        // 生成防伪码
        String configNo = "";
        String newCode = PropKit.get("jboot.anticode.newcode", "0");
        if("1".equals(newCode)) {
        	configNo = CommonUtils.getRandomInt(4);
        } else {
        	configNo = DateUtils.getDate("yyyyMMddHHmmss");
        }
        data.setConfigNo(configNo);
        
        String brandId = data.getBrandId();
        String productId = data.getProductId();
        String ruleId = data.getAntiRuleId();
        
        Product product = productService.findById(productId);
        Brand brand = brandService.findById(brandId);
        AntiRule rule = antiRuleService.findById(ruleId);
        
        AntiRuleRecord ruleRecord = new AntiRuleRecord();
        ruleRecord.setBrandId(brandId);
        ruleRecord.setProductId(productId);
        List<AntiRuleRecord> recordList = antiRuleRecordService.findList(ruleRecord);
        if(recordList.size() > 0) {
        	ruleRecord = recordList.get(0);
        } else {
        	ruleRecord.setId(StringUtils.uuid());
        	ruleRecord.setUpdateDate(new Date());
        	ruleRecord.setAntiRuleId(data.getAntiRuleId());
        	ruleRecord.setStartBig(0);
        	ruleRecord.setStartMiddle(0);
        	ruleRecord.setStartSmall(0);
        	ruleRecord.setDelFlag("0");
        	antiRuleRecordService.save(ruleRecord);
        	recordList.add(ruleRecord);
        }
        
        Integer startBig = ruleRecord.getStartBig();
		Integer maxStartCode = startBig + data.getGenCount();
		Integer smallCode = rule.getSmall();
//		Integer bigCount = ruleRecord.getStartBig()+data.getGenCount();
        data.setStartBig(ruleRecord.getStartBig() + 1);
        data.setEndBig(maxStartCode);
        data.setId(StringUtils.uuid());
        if(data.getCodeLength() < 10 || data.getCodeLength() > 20) {
        	throw new BusinessException("防伪码长度要在10到20位之间");
        }
        if(product.getSn().length() != 4) {
        	throw new BusinessException("产品序列号要是4位字符");
        }
        if(configNo.length() != 4) {
        	throw new BusinessException("防伪配置批号要是4位字符");
        }
        if (!anticodeConfigService.save(data)) {
            throw new BusinessException("保存失败");
        }
        anticodeConfigService.refreshCache();
        
        // 生成anticode_configno表
        anticodeService.saveNewTable(configNo);
        
        // 处理防伪数据-----begin
        Integer brandCode = brand.getCode();
		Integer productCode = product.getCode();
//		StringBuffer writer = new StringBuffer();
//		String savePath = getRequest().getServletContext().getRealPath("/anticode/" + configNo);
		String qrcodeUrl = PropKit.get("jboot.anticode.mainUrl");
		
		// 生成防伪码
		anticodeGenService.saveGenCodes(data.getId(), configNo, product.getSn(), data.getCodeLength(), data.getChannel(), qrcodeUrl, ruleId, brandId, productId, 
				startBig, maxStartCode, smallCode, productCode, brandCode, recordList);
		
//		File folder = new File(savePath);
//		if(!folder.exists()) {
//			folder.mkdirs();
//		}
//		String fileName = "anticode_" + productCode + "_" + configNo + ".txt";
//		try {
//			FileWriter fw = new FileWriter(savePath + File.separator +fileName);
//			
//			List<Record> codes = new ArrayList<Record>();
//			Date nowDate = new Date();
//			// 处理防伪数据-----end
//			for (int i = startBig + 1; i <= maxStartCode; i++) {
//				String antiCode = randomCode();
//				String logisCode = brandCode.toString() + productCode.toString();
//				logisCode += addZero(maxStartCode.toString().length(), i);
//				Anticode newAnticode = new Anticode();
//				String bigId = StringUtils.uuid();
//				newAnticode.setId(bigId);
//				newAnticode.setCreateDate(nowDate);
//				newAnticode.setBrandId(brandId);
//				newAnticode.setProductId(productId);
//				newAnticode.setAntiRuleId(ruleRecord.getId());
//				newAnticode.setAntiCode(Encodes.entryptPassword(antiCode));
//				newAnticode.setLogisticsCode(logisCode);
//				newAnticode.setType(1);
//				newAnticode.setConfigId(data.getId());
//				codes.add(newAnticode.toRecord());
//				StringBuilder bigBuilder = new StringBuilder();
//				bigBuilder.append(logisCode).append(",").append(qrcodeUrl).append("?rand=").append(productCode).append("&id=").append(URLEncoder.encode(AESUtil.aesEncode(bigId + "_" + antiCode.substring(0, 12) + "_" + configNo, productCode), "UTF-8"));
//				if(smallCode > 0) {
//					for (int j = 1; j <= smallCode; j++) {
//						String antiCodeSmall = randomCode();
//						String logisCodeSmall = logisCode + "-" + addZero(smallCode.toString().length(), j);
//						Anticode smallAnticode = new Anticode();
//						String smallId = StringUtils.uuid();
//						smallAnticode.setId(smallId);
//						smallAnticode.setCreateDate(nowDate);
//						smallAnticode.setBrandId(brandId);
//						smallAnticode.setProductId(productId);
//						smallAnticode.setAntiRuleId(ruleRecord.getId());
//						smallAnticode.setAntiCode(Encodes.entryptPassword(antiCodeSmall));
//						smallAnticode.setLogisticsCode(logisCodeSmall);
//						smallAnticode.setType(3);
//						smallAnticode.setConfigId(data.getId());
//						codes.add(smallAnticode.toRecord());
//						writer.append(bigBuilder)
//							.append(",").append(logisCodeSmall)
//							.append(",").append(antiCodeSmall)
//							.append(",").append(qrcodeUrl).append("?rand=").append(productCode).append("&id=").append(URLEncoder.encode(AESUtil.aesEncode(smallId + "_" + antiCodeSmall.substring(0, 12) + "_" + configNo, productCode), "UTF-8")).append("\r\n");
//					}
//				}
//				logger.info("-------写入"+logisCode+"-------");
//			}
//			logger.info("---------刷新到文件---------");
//			fw.write(writer.toString());
//			fw.flush();
//			fw.close();
//			writer = null;
//			logger.info("--------------防伪码数据文件生成完成----------");
//			// 分批次插入，每次1万条，多线程执行
//			if(codes.size() <= 10000) {
//				batchSave(configNo, codes);
//			} else {
//				int num = codes.size() / 10000;
//				int left = codes.size() % 10000;
//				for(int i = 0; i<num; i++) {
//					List<Record> subCodes = new ArrayList<Record>();
//					for(int j=i*10000; j<i*10000+10000; j++) {
//						subCodes.add(codes.get(j));
//					}
//					generateAntiCode(i, configNo, subCodes);
//				}
//				if(left > 0) {
//					List<Record> subCodes = new ArrayList<Record>();
//					for(int j=codes.size() - left; j<codes.size(); j++) {
//						subCodes.add(codes.get(j));
//					}
//					generateAntiCode(num, configNo, subCodes);
//				}
//			}
//			
//			for (AntiRuleRecord record : recordList) {
//				record.setStartBig(bigCount);
//				antiRuleRecordService.update(record);
//			}
//			
//			logger.info("----------防伪码生成结束--------");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
        renderJson(RestResult.buildSuccess());
    }
    
//    /**
//	 * 生成16位随机数
//	 * @return
//	 */
//	private String randomCode() {
//		StringBuffer sb = new StringBuffer();  
//        Random random = new Random();  
//        for (int i = 0; i < 16; i++) {  
//            sb.append(NUMBERCHAR.charAt(random.nextInt(NUMBERCHAR.length())));  
//        }
//        return sb.toString();
//	}
//	
//	private String addZero(int size, Integer number) {
//		String numberStr = number.toString();
//		String result = "";
//		for(int i=numberStr.length(); i<size; i++) {
//			result += "0";
//		}
//		return result += numberStr;
//	}
//	
//	/**
//	 * 循环生成防伪码
//	 */
//	private void generateAntiCode(int count, String configNo, List<Record> anticodeList){
//		try {
//			Map<String, Object> eventParam = new HashMap<String, Object>();
//			eventParam.put("count", count);
//			eventParam.put("codes", anticodeList);
//			eventParam.put("configNo", configNo);
//			Jboot.sendEvent("anticodeGenEvent", eventParam);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//	}

//	/**
//	 * 批量保存
//	 * @param anticodeList 防伪码
//	 */
//	private void batchSave(String configNo, final List<Record> anticodeList) {
//		anticodeService.saveAll(configNo, anticodeList);
//	}

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        AnticodeConfig data = anticodeConfigService.findById(id);
        
        setAttr("brands", brandService.findAll()); // 品牌列表
    	setAttr("products", productService.findAll()); // 产品列表
    	setAttr("rules", antiRuleService.findAll()); // 规则列表

        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        AnticodeConfig data = getBean(AnticodeConfig.class, "data");

        if (anticodeConfigService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!anticodeConfigService.update(data)) {
            throw new BusinessException("修改失败");
        }
        anticodeConfigService.refreshCache();

        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
        String id = getPara("id");
        if (!anticodeConfigService.deleteById(id)) {
            throw new BusinessException("删除失败");
        }
        anticodeConfigService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}