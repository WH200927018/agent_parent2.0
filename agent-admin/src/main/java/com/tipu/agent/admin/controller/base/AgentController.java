package com.tipu.agent.admin.controller.base;

import java.awt.FontFormatException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.google.common.collect.Maps;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.util.CommonUtils;
import com.tipu.agent.admin.base.util.Encodes;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.controller.common.OrderNumberGenerator;
import com.tipu.agent.admin.service.api.DataService;
import com.tipu.agent.admin.service.entity.model.Data;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AgentAddressService;
import com.tipu.agent.front.service.api.AgentBanksService;
import com.tipu.agent.front.service.api.AgentBondService;
import com.tipu.agent.front.service.api.AgentBrandService;
import com.tipu.agent.front.service.api.AgentExtendService;
import com.tipu.agent.front.service.api.AgentLoginLogService;
import com.tipu.agent.front.service.api.AgentService;
import com.tipu.agent.front.service.api.AgentTypeService;
import com.tipu.agent.front.service.api.OrdersService;
import com.tipu.agent.front.service.entity.model.Agent;
import com.tipu.agent.front.service.entity.model.AgentAddress;
import com.tipu.agent.front.service.entity.model.AgentBanks;
import com.tipu.agent.front.service.entity.model.AgentBond;
import com.tipu.agent.front.service.entity.model.AgentBrand;
import com.tipu.agent.front.service.entity.model.AgentExtend;
import com.tipu.agent.front.service.entity.model.AgentLoginLog;
import com.tipu.agent.front.service.entity.model.AgentType;
import com.tipu.agent.front.service.entity.model.Orders;
import com.tipu.agent.front.service.entity.model.ProductType;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * Agent管理
 * 
 * @author wh
 *
 */
@RequestMapping("/base/agent")
public class AgentController extends BaseController {

	@JbootrpcService
	private DataService dataApi;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentService agentService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentTypeService agentTypeService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBrandService agentBrandService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBondService agentBondService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentLoginLogService agentLoginLogService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentAddressService agentAddressService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentBanksService agentBanksService;

	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private OrdersService orderService;
	
	@JbootrpcService(group = ServiceConst.SERVICE_BASE, version = ServiceConst.VERSION_1_0)
	private AgentExtendService agentExtendService;

	private static String productName = PropKit.get("jboot.productName");

	/**
	 * index
	 */
	public void index() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);// 代理商级别
		List<Agent> agentList = agentService.findAll(companyId,brandId);
		setAttr("agentList", agentList);
		setAttr("agentTypeList", agentTypeList);
		render("main.html");
	}

	/**
	 * 表格数据
	 */
	public void tableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		Agent data = new Agent();
		if (StrKit.notBlank(getPara("parentId"))) {
			data.set("parentId",getPara("parentId"));
		}
		if (StrKit.notBlank(getPara("name"))) {
			data.setName(getPara("name"));
		}
		if (StrKit.notBlank(getPara("agentType"))) {
			data.set("agentType",getPara("agentType"));
		}
		if (StrKit.notBlank(getPara("auditStatus"))) {
			data.set("auditStatus",getPara("auditStatus"));
		}
		if (StrKit.notBlank(getPara("mobile"))) {
			data.setMobile(getPara("mobile"));
		}
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.set("brandId",brandId);	
        }
		Page<Agent> dataPage = agentService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<Agent>(dataPage));
	}

	/**
	 * add
	 */
	public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);
		List<Agent> agentList = agentService.findAll(companyId,brandId);
		setAttr("agentList", agentList);
		setAttr("agentTypeList", agentTypeList);
		render("add.html");
	}

	/**
	 * 保存提交
	 * 
	 * @throws IOException
	 * @throws FontFormatException
	 * @throws ParseException
	 */
	public void postAdd() throws FontFormatException, IOException, ParseException {
		Agent data = getBean(Agent.class, "data");
		String agentId=StrKit.getRandomUUID();
		data.setId(agentId);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		List<Agent>agentList=agentService.findAgentList(data);
		if(agentList.size()>0) {
			throw new BusinessException("用户名重复");	
		}		
		// 密码
		String password = data.getPassword();
		//密码加密 （采用多平台通用加密方式） 如果平台的产品名称为 nma 则加密走if
		String encodePassword = HashKit.md5(password);
//		if(productName.toLowerCase().contains("nma")){
//			encodePassword = HashKit.sha256(password);//nma 的加密方式
//		}else{
//			encodePassword = Encodes.entryptPassword(password); // ray anna  at加密方式
//		}
		data.setPassword(encodePassword);
		data.setCreateDate(new Date());
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		if (!agentService.save(data)) {
			throw new BusinessException("保存失败");
		}
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * update
	 */
	@NotNullPara({ "id" })
	public void update() {
		String id = getPara("id");
		Agent data = agentService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		List<AgentType> agentTypeList = agentTypeService.findAll(companyId, brandId);
		List<Agent> agentList = agentService.findAll(companyId,brandId);
		setAttr("agentList", agentList);
		setAttr("agentTypeList", agentTypeList);
		setAttr("data", data).render("update.html");
	}

	/**
	 * 修改提交
	 * 
	 * @throws IOException
	 * @throws FontFormatException
	 * @throws ParseException
	 */
	public void postUpdate() throws FontFormatException, IOException, ParseException {
		Agent data = getBean(Agent.class, "data");// 修改的实体
		if (agentService.findById(data.getId()) == null) {
			throw new BusinessException("数据不存在");
		}
		List<Agent>agentList=agentService.findAgentList(data);
		if(agentList.size()>0) {
			throw new BusinessException("用户名重复");	
		}
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		if (!agentService.update(data)) {
			throw new BusinessException("修改失败");
		}
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 删除
	 */
	@NotNullPara({ "id" })
	public void delete() {
		String id = getPara("id");
		Agent agent = new Agent();
		agent.setId(id);
		agent.setDelFlag("1");
		if (!agentService.update(agent)) {
			throw new BusinessException("删除失败");
		}
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}

	/**
	 * 查看授权证书
	 */
	public void findAuthPic() {
		String id = getPara("id");
		Agent data = agentService.findById(id);
		setAttr("data", data).render("findAuthPic.html");
	}

	

	/**
	 * 查看登录日志
	 * 
	 */
	@NotNullPara({ "id" })
	public void findAgentLoginLog() {
		String agentId = getPara("id");// 代理商id
		setAttr("agentId", agentId);
		render("findAgentLoginLog.html");
	}

	/**
	 * 表格数据
	 */
	@NotNullPara({ "agentId" })
	public void agentLoginLogTableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AgentLoginLog data = new AgentLoginLog();
		if (StrKit.notBlank(getPara("agentId"))) {// 代理商id
			data.setAgent(getPara("agentId"));
		}
		Page<AgentLoginLog> dataPage = agentLoginLogService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentLoginLog>(dataPage));
	}

	/**
	 * 查看常用地址
	 * 
	 */
	@NotNullPara({ "id" })
	public void findAgentAddress() {
		String agentId = getPara("id");// 代理商id
		setAttr("agentId", agentId);
		render("findAgentAddress.html");
	}

	/**
	 * 常用地址表格数据
	 */
	@NotNullPara({ "agentId" })
	public void agentAddressTableData() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);

		AgentAddress data = new AgentAddress();
		if (StrKit.notBlank(getPara("agentId"))) {// 代理商id
			data.setAgentId(getPara("agentId"));
		}
		Page<AgentAddress> dataPage = agentAddressService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentAddress>(dataPage));
	}
	
	/**
	 * 充值页
	 */
	public void showCharge() {
		String agent=getPara("id");
		AgentBanks agentBanks=new AgentBanks();
		agentBanks.setIsCompany("1");//是否公司账户0-否；1-是
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		agentBanks.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	agentBanks.setBrandId(brandId);	
        }
		List<AgentBanks> agentBanksList = agentBanksService.findList(agentBanks);
		setAttr("agent", agent).setAttr("agentBanksList", agentBanksList);
		render("charge.html");
	}

	/**
	 * 保存提交充值
	 */
	public void postCharge() {
		Orders data = getBean(Orders.class, "data");
		String uuid = OrderNumberGenerator.get();
		data.setId(uuid);
		data.setCreateDate(new Date());
		data.setOrderType("4");
		data.setState("0");//0 待处理 1审核通过 2 待发货 3 待收货 4 结束 -1 拒接取消订单
		data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
		data.setDelFlag("0");
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
        if(StrKit.notBlank(brandId)) {
        	data.setBrandId(brandId);	
        }
		if (!orderService.save(data)) {
			throw new BusinessException("保存失败");
		}
		orderService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
	
	/**
	 * 导出代理商信息
	 */
	public void export(){
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		List<AgentType> agentTypeList = agentTypeService.findAll(companyId,brandId);
		List<Agent> agentList = agentService.findAll(companyId,"");
		setAttr("agentList", agentList);
		setAttr("agentTypeList", agentTypeList);
		render("export.html");
	}
	
	/**
	 * 导出代理商操作
	 */
	public void exportAgent(){
		String parentId = getPara("parentId");//上级代理
		String authStatus = getPara("authStatus");//授权状态
		String agentType = getPara("agentType");//代理商类型
		String companyId=AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
        String brandId=redis.get(companyId+"_brandId");
		String sql = "select a.*,b.code,b.audit_status,b.agent_type,b.parent_id from agent a,agent_brand b where a.del_flag = 0 AND b.audit_status=1 AND b.agent_id=a.id ";
		
		if(StrKit.notBlank(companyId)){
			sql += " AND  a.company_id = '"+companyId+"' ";
		}
		if(StrKit.notBlank(brandId)){
			sql += " AND  b.brand_id = '"+brandId+"' ";
		}
		if(StrKit.notBlank(parentId)){
			sql += " AND  b.parent_id = '"+parentId+"' ";
		}
		if(StrKit.notBlank(authStatus)){
			sql += " AND b.audit_status = '"+authStatus+"' ";
		}
		if(StrKit.notBlank(agentType)){
			sql += " AND b.agent_type = '"+agentType+"' ";
		}
		Map<String, String> titleData = Maps.newLinkedHashMap();//标题，后面用到
		titleData.put("parent_id", "上级代理");
		titleData.put("username", "用户名");
		titleData.put("name", "名称");
		titleData.put("code", "代理商代码");
		titleData.put("weixin", "微信号");
		titleData.put("mobile", "手机号");
		titleData.put("avatar", "头像地址");
		titleData.put("address", "地址");
		titleData.put("id_no", "身份证号");
		titleData.put("auth_pic", "授权书地址");
		titleData.put("audit_status", "授权状态");
		titleData.put("agent_type", "代理商类型");
		titleData.put("agent_deposit", "余额");
		titleData.put("agent_reward", "奖励金");
		File file = new File(getTitle());
		file = saveFile(titleData, sql, file,"代理商信息");
		this.renderFile(file);
	}
	
	private static final String FILEPATH = PathKit.getWebRootPath() + File.separator + "export\\excel-agent" + File.separator ;
	public String getTitle(){
	     String title=FILEPATH+"代理商信息.xls";
	     File temp = new File(FILEPATH);
	     if(temp.exists()==false) {
			 temp.mkdirs();				
        }
	     return title;
	}
	
	/**
	 * 导出代理商信息
	 * @param headData
	 * @param sql
	 * @param file
	 * @param title
	 * @return
	 */
	public File saveFile(Map<String, String> headData, String sql, File file,String title) {
		// 创建工作薄
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		// sheet:一张表的简称
		// row:表里的行
		// 创建工作薄中的工作表
		HSSFSheet hssfSheet = hssfWorkbook.createSheet(title);
		// 创建行
		HSSFRow row = hssfSheet.createRow(0);
		// 创建单元格，设置表头 创建列
		HSSFCell cell = null;
		// 初始化索引
		int rowIndex = 0;
		int cellIndex = 0;

		// 创建标题行
		row = hssfSheet.createRow(rowIndex);
		rowIndex++;
		// 遍历标题
		for (String h : headData.keySet()) {
			//创建列
			cell = row.createCell(cellIndex);
			//索引递增
			cellIndex++;
			//逐列插入标题
			cell.setCellValue(headData.get(h));
		}
		// 得到所有记录 行：列
		List<Record> list = agentService.findRecord(sql);
		Record record = null;
		
//		String parentId = ""; //上级代理
		String authStatus = ""; //授权状态
		String agentLevel = "";//代理级别
//		String agentType = ""; //代理商类型
		String linkStatus = "";//是否为分享链接加入
		if (list != null) {
			// 获取所有的记录 有多少条记录就创建多少行
			for (int i = 0; i < list.size(); i++) {
				row = hssfSheet.createRow(rowIndex);
				// 得到所有的行 一个record就代表 一行
				record = list.get(i);
				//下一行索引
				rowIndex++;
				//刷新新行索引
				cellIndex = 0;
				// 在有所有的记录基础之上，便利传入进来的表头,再创建N行
				for (String h : headData.keySet()) {
					cell = row.createCell(cellIndex);
					cellIndex++;
					//按照每条记录匹配数据
					if(h.equals("parent_id")){
						if(StrKit.notBlank(record.get(h).toString())){
							String agentName =	getAgent(record.get(h).toString());
							cell.setCellValue( StrKit.isBlank(agentName) ? "" : agentName);
						}
					}else if(h.equals("agent_type")){
						if(StrKit.notBlank(record.get(h).toString())){
							AgentType type = agentTypeService.findById(record.get(h).toString());
							if(type != null){
								cell.setCellValue( StrKit.isBlank(type.getName()) ? "" : type.getName());
							}
						}
					}else if(h.equals("audit_status")){//授权状态
						if(record.get(h) != null){
							List<Data> listData = dataApi.getListByTypeOnUse("auth_status");
							for (Data selfData : listData) {
								if (selfData.getCode().equals(record.get(h).toString())) {
									authStatus = selfData.getCodeDesc();
								}
							}
							cell.setCellValue( StrKit.isBlank(authStatus) ? "" : authStatus);
						}
					}else{
						cell.setCellValue(record.get(h) == null ? "" : record.get(h).toString());
					}
				}
			}
		}
		try {
			FileOutputStream fileOutputStreane = new FileOutputStream(file);
			hssfWorkbook.write(fileOutputStreane);
			fileOutputStreane.flush();
			fileOutputStreane.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	
	/**
	 * 通过id获取代理商
	 * @param id
	 * @return
	 */
	public String getAgent(String id){
		if(id.equals("0")){
			return "总公司";
		}else{
			Agent agent = agentService.findById(id);
			return agent.getName();
		}
	}
	
	/**
	 * 重置密码
	 */
	public void resetPwd(){
		String id = getPara("id");
		Agent agent = agentService.findById(id);
		//密码加密 （采用多平台通用加密方式） 如果平台的产品名称为 nma 则加密走if
		String encodePassword = HashKit.md5("123456");
//		if(productName.toLowerCase().contains("nma")){
//			encodePassword = HashKit.sha256("123456");//nma 的加密方式
//		}else{
//			encodePassword = Encodes.entryptPassword("123456"); // ray anna  at加密方式
//		}
		agent.setPassword(encodePassword);
		if(!agentService.update(agent)){
			throw new BusinessException("重置失败");
		}
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
	
	/**
	 * 查看代理授权
	 * 
	 */
	@NotNullPara({ "id" })
	public void findAgentAuth() {
		String agentId = getPara("id");// 代理商id
		setAttr("agentId", agentId);
		render("findAgentAuth.html");
	}
	
	/**
	 * 查看图片材料
	 * 
	 */
	@NotNullPara({ "id" })
	public void findExtend() {
		String agentId = getPara("id");// 代理商id
		setAttr("agentId", agentId);
		render("findAgentExtend.html");
	}
	
	/**
	 * 查看图片材料
	 * 
	 */
	public void showExtend() {
		String agentId = getPara("agentId");// 代理商id
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize", 30);
		
		AgentExtend data = new AgentExtend();
		if (StrKit.notBlank(agentId)) {
			data.setAgentId(agentId);
		}
		Page<AgentExtend>agentExtendList=agentExtendService.findPage(data, pageNumber, pageSize);
		renderJson(new DataTable<AgentExtend>(agentExtendList));
	}
	
	/**
	 * 更新审核权限
	 * 
	 * @throws ParseException
	 */
	public void isAuth() {
		String id = getPara("id");
		String isAuth = getPara("isAuth");
		Agent data = agentService.findById(id);
		if (data == null) {
			throw new BusinessException("数据不存在");
		}
		data.setIsFristAuth(isAuth);
		data.setUpdateDate(new Date());
		data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
		agentService.update(data);
		agentService.refreshCache();
		renderJson(RestResult.buildSuccess());
	}
}