package com.tipu.agent.admin.controller.base;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.tipu.agent.admin.base.common.RestResult;
import com.tipu.agent.admin.base.common.ServiceConst;
import com.tipu.agent.admin.base.exception.BusinessException;
import com.tipu.agent.admin.base.interceptor.NotNullPara;
import com.tipu.agent.admin.base.rest.datatable.DataTable;
import com.tipu.agent.admin.base.web.base.BaseController;
import com.tipu.agent.admin.support.auth.AuthUtils;
import com.tipu.agent.front.service.api.AdFieldService;
import com.tipu.agent.front.service.api.AdItemService;
import com.tipu.agent.front.service.entity.model.AdField;
import com.tipu.agent.front.service.entity.model.AdItem;

import io.jboot.Jboot;
import io.jboot.component.redis.JbootRedis;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * AdItem管理
 * @author think
 *
 */
@RequestMapping("/base/ad_item")
public class AdItemController extends BaseController {

	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AdItemService adItemService;
	
	@JbootrpcService(group=ServiceConst.SERVICE_BASE, version=ServiceConst.VERSION_1_0)
	private AdFieldService adFieldService;
	
	
	/**
     * index
     */
    public void index() {
        render("main.html");
    }

    /**
     * 表格数据
     */
    public void tableData() {
        int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 30);
        AdItem data = new AdItem();
        if(StrKit.notBlank(getPara("name"))){
        	data.setName(getPara("name"));
        }
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}
        Page<AdItem> dataPage = adItemService.findPage(data, pageNumber, pageSize);
        renderJson(new DataTable<AdItem>(dataPage));
    }
    
    /**
     * 查看图片
     */
    public void findimg(){
    	String id = getPara("id");
    	AdItem data = adItemService.findById(id);
        setAttr("data", data).render("findimg.html");
    }
    
    /**
     * 查看视频
     */
    public void findvideo(){
    	String id = getPara("id");
    	AdItem data = adItemService.findById(id);
        setAttr("data", data).render("findvideo.html");
    }

    /**
     * add
     */
    public void add() {
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<AdField> adFieldList =  adFieldService.findAll(companyId,brandId);   	
    	setAttr("list", adFieldList).render("add.html");
    }

    /**
     * 保存提交
     */
    public void postAdd() {
        AdItem data = getBean(AdItem.class, "data");
        if (StrKit.isBlank(data.getContent())) {
			throw new BusinessException("文本内容不能为空！");
		}
        data.setCreateDate(new Date());
        data.setCreateBy(AuthUtils.getLoginUser().getId().toString());
        data.setDelFlag("0");
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		data.setCompanyId(companyId);
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
		if (StrKit.notBlank(brandId)) {
			data.setBrandId(brandId);
		}else {
        	throw new BusinessException("请先为公司添加品牌！");
        }
        if (!adItemService.save(data)) {
            throw new BusinessException("保存失败");
        }
        adItemService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * update
     */
    @NotNullPara({"id"})
    public void update() {
        String id = getPara("id");
        AdItem data = adItemService.findById(id);
		String companyId = AuthUtils.getLoginUser().getCompanyId();
		JbootRedis redis = Jboot.me().getRedis();
		String brandId = redis.get(companyId + "_brandId");
    	List<AdField> adFieldList =  adFieldService.findAll(companyId,brandId);
        setAttr("adFieldList", adFieldList);
        setAttr("data", data).render("update.html");
    }

    /**
     * 修改提交
     */
    public void postUpdate() {
        AdItem data = getBean(AdItem.class, "data");
        if (StrKit.isBlank(data.getContent())) {
			throw new BusinessException("文本内容不能为空！");
		}
        data.setUpdateDate(new Date());
        data.setUpdateBy(AuthUtils.getLoginUser().getId().toString());
        if (adItemService.findById(data.getId()) == null) {
            throw new BusinessException("数据不存在");
        }

        if (!adItemService.update(data)) {
            throw new BusinessException("修改失败");
        }
        adItemService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

    /**
     * 删除
     */
    @NotNullPara({"id"})
    public void delete() {
    	String id = getPara("id");
    	AdItem adItem = new AdItem();
    	adItem.setId(id);
    	adItem.setDelFlag("1");
        if (!adItemService.update(adItem)) {
            throw new BusinessException("删除失败");
        }
        adItemService.refreshCache();
        renderJson(RestResult.buildSuccess());
    }

}