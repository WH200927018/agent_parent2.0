Table: anticode	Remarks: 防伪码管理
----------------+--------------+------+-----+---------+---------
 Field          | Type         | Null | Key | Default | Remarks 
----------------+--------------+------+-----+---------+---------
 id             | VARCHAR(64)  | NO   | PRI |         | 主键      
 create_by      | VARCHAR(64)  | YES  |     |         | 创建者     
 create_date    | DATETIME(19) | YES  |     |         | 创建时间    
 update_by      | VARCHAR(64)  | YES  |     |         | 更新者     
 update_date    | DATETIME(19) | YES  |     |         | 更新时间    
 config_id      | VARCHAR(64)  | YES  |     |         | 配置ID    
 brand_id       | VARCHAR(64)  | YES  |     |         | 品牌      
 product_id     | VARCHAR(64)  | YES  |     |         | 商品      
 anti_rule_id   | VARCHAR(64)  | YES  |     |         | 生成规则    
 anti_code      | VARCHAR(255) | YES  |     |         | 防伪码     
 logistics_code | VARCHAR(255) | YES  |     |         | 物流码     
 agent_id       | VARCHAR(64)  | YES  |     |         | 代理商     
 type           | INT(10)      | YES  |     |         |         
 remarks        | VARCHAR(255) | YES  |     |         | 备注信息    
 del_flag       | VARCHAR(64)  | YES  |     |         | 逻辑删除标记（0：显示；1：隐藏）
----------------+--------------+------+-----+---------+---------

Table: anticode_config	Remarks: 防伪码配置
--------------+--------------+------+-----+---------+---------
 Field        | Type         | Null | Key | Default | Remarks 
--------------+--------------+------+-----+---------+---------
 id           | VARCHAR(64)  | NO   | PRI |         |         
 create_date  | DATETIME(19) | YES  |     |         | 创建日期    
 update_date  | DATETIME(19) | YES  |     |         | 更新日期    
 config_no    | VARCHAR(20)  | YES  |     |         | 批号      
 brand_id     | VARCHAR(64)  | YES  |     |         | 品牌      
 product_id   | VARCHAR(64)  | YES  |     |         | 商品      
 anti_rule_id | VARCHAR(64)  | YES  |     |         | 生成规则    
 gen_count    | INT(10)      | YES  |     |         | 生成数量    
 start_big    | INT(10)      | YES  |     |         | 大标起始值   
 end_big      | INT(10)      | YES  |     |         | 大标结束值   
 color_config | VARCHAR(255) | YES  |     |         | 颜色配置    
 remarks      | VARCHAR(255) | YES  |     |         | 备注      
 del_flag     | VARCHAR(2)   | YES  |     |         | 是否删除    
--------------+--------------+------+-----+---------+---------

Table: anticode_search_log	Remarks: 防伪码查询记录
------------------+---------------+------+-----+---------+---------
 Field            | Type          | Null | Key | Default | Remarks 
------------------+---------------+------+-----+---------+---------
 id               | VARCHAR(64)   | NO   | PRI |         | 主键      
 create_by        | VARCHAR(64)   | YES  |     |         | 创建者     
 create_date      | DATETIME(19)  | YES  |     |         | 创建时间    
 update_by        | VARCHAR(64)   | YES  |     |         | 更新者     
 update_date      | DATETIME(19)  | YES  |     |         | 更新时间    
 product_id       | VARCHAR(64)   | YES  |     |         | 产品      
 anticode_content | VARCHAR(20)   | YES  |     |         | 防伪码内容   
 logistics_code   | VARCHAR(20)   | YES  |     |         | 箱码      
 ip               | VARCHAR(64)   | YES  |     |         | ip地址    
 lat              | VARCHAR(255)  | YES  |     |         | 经度      
 lng              | VARCHAR(255)  | YES  |     |         | 纬度      
 address          | VARCHAR(255)  | YES  |     |         | 地址      
 format_address   | VARCHAR(255)  | YES  |     |         | 详细地址    
 nation           | VARCHAR(255)  | YES  |     |         | 国家      
 province         | VARCHAR(255)  | YES  |     |         | 省       
 city             | VARCHAR(255)  | YES  |     |         | 市       
 district         | VARCHAR(255)  | YES  |     |         | 区       
 street           | VARCHAR(255)  | YES  |     |         | 街道      
 street_number    | VARCHAR(255)  | YES  |     |         | 街道号     
 mobile           | VARCHAR(20)   | YES  |     |         | 手机号     
 msg              | VARCHAR(255)  | YES  |     |         | 短信内容    
 verify_code      | VARCHAR(10)   | YES  |     |         | 验证码     
 verify_date      | DATETIME(19)  | YES  |     |         | 验证时间    
 first_fake       | INT(10)       | YES  |     |         | 网站验证真伪  
 fake             | INT(10)       | YES  |     |         | 是否真假    
 has_reply        | INT(10)       | YES  |     | 0       | 是否短信回复  
 reply_date       | DATETIME(19)  | YES  |     |         | 回复日期    
 reply_content    | VARCHAR(255)  | YES  |     |         | 回复内容    
 reply_count      | INT(10)       | YES  |     | 0       | 回复次数    
 seller_channel   | INT(10)       | YES  |     |         | 购买渠道    
 seller_name      | VARCHAR(50)   | YES  |     |         | 卖家姓名    
 seller_wechat    | VARCHAR(30)   | YES  |     |         | 卖家微信    
 seller_mobile    | VARCHAR(20)   | YES  |     |         | 卖家手机号   
 seller_link      | VARCHAR(255)  | YES  |     |         | 卖家链接    
 city_data        | VARCHAR(2000) | YES  |     |         | 从淘宝获取到的数据
 remarks          | VARCHAR(255)  | YES  |     |         | 备注信息    
 del_flag         | VARCHAR(64)   | YES  |     |         | 逻辑删除标记（0：显示；1：隐藏）
------------------+---------------+------+-----+---------+---------

Table: anti_rule	Remarks: 生成规则
-------------+--------------+------+-----+---------+---------
 Field       | Type         | Null | Key | Default | Remarks 
-------------+--------------+------+-----+---------+---------
 id          | VARCHAR(64)  | NO   | PRI |         | 主键      
 create_by   | VARCHAR(64)  | YES  |     |         | 创建者     
 create_date | DATETIME(19) | YES  |     |         | 创建时间    
 update_by   | VARCHAR(64)  | YES  |     |         | 更新者     
 update_date | DATETIME(19) | YES  |     |         | 更新时间    
 name        | VARCHAR(64)  | YES  |     |         | 名称      
 big         | INT(10)      | YES  |     |         | 大       
 middle      | INT(10)      | YES  |     |         | 中       
 small       | INT(10)      | YES  |     |         | 小       
 remarks     | VARCHAR(255) | YES  |     |         | 备注信息    
 del_flag    | VARCHAR(64)  | YES  |     |         | 逻辑删除标记（0：显示；1：隐藏）
-------------+--------------+------+-----+---------+---------

Table: anti_rule_record	Remarks: 规则记录
--------------+--------------+------+-----+---------+---------
 Field        | Type         | Null | Key | Default | Remarks 
--------------+--------------+------+-----+---------+---------
 id           | VARCHAR(64)  | NO   | PRI |         | 主键      
 create_by    | VARCHAR(64)  | YES  |     |         | 创建者     
 create_date  | DATETIME(19) | YES  |     |         | 创建时间    
 update_by    | VARCHAR(64)  | YES  |     |         | 更新者     
 update_date  | DATETIME(19) | YES  |     |         | 更新时间    
 remarks      | VARCHAR(255) | YES  |     |         | 备注信息    
 del_flag     | VARCHAR(64)  | YES  |     |         | 逻辑删除标记（0：显示；1：隐藏）
 anti_rule_id | VARCHAR(64)  | YES  |     |         | 生成规则    
 brand_id     | VARCHAR(64)  | YES  |     |         | 品牌      
 product_id   | VARCHAR(64)  | YES  |     |         | 商品      
 start_big    | INT(10)      | YES  |     |         | 大箱起始值   
 start_middle | INT(10)      | YES  |     |         | 中箱起始值   
 start_small  | INT(10)      | YES  |     |         | 单品起始值   
--------------+--------------+------+-----+---------+---------

